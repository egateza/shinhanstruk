package isoxml;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.persistence.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
//import pelangi_utils.*;
import ppob_sls_iqbal.Settings;
import org.apache.log4j.*;
import model.Inboxes;
import model.Mutations;
import model.Outboxes;
import model.TokenUnsolds;
import model.Users;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAPersistence;
import org.jpos.iso.ISOException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import pelangi_utils.ParseISOMessage;
import pelangi_utils.Saldo;

public class PLN implements Runnable {

    private static final Logger logger = Logger.getLogger("PLN");
    Saldo mutasi = new Saldo();
    Connection conx = null;
    Connection conOtomax = null;
    Settings setting = new Settings();
    double pln_price = 0;
    double initial_nominal = 0;
    long curr_saldo = 0;
    int inbox_id = 0;
    String serializeQueue = "";
    String[] arrayQueue = new String[8];

//    public PLN(int id) {
    public PLN(String serializeQueue) {
        this.serializeQueue = serializeQueue;
        arrayQueue = serializeQueue.split(";");

        this.inbox_id = Integer.parseInt(arrayQueue[0]);
        setting.setConnections();

//        System.out.println(serializeQueue);
//        System.out.println("length : "+arrayQueue.length);
    }

    public void run() {
        process();
    }

    public boolean itung(double nominal, int inbox_id, int user_id, String msg, int product_id, int price_template_id, int jumlah_rekening) {

        boolean boleh = false;
        RainbowCaller core = null;
        
        try {
            core = new RainbowCaller();
            
            String itungan = core.call("itung." + (int) nominal+"."+inbox_id+"."+user_id+"."+"PLN"+"."+product_id+"."+price_template_id+"."+jumlah_rekening);
            logger.log(Level.INFO,"Hasil itungan : "+itungan);
            String rex[] = itungan.split("\\|");
            logger.log(Level.INFO,"hasil : "+rex[0]);
            if (rex[0].equalsIgnoreCase("true")) {
                boleh = true;
                pln_price = Double.parseDouble(rex[1]); logger.log(Level.INFO,"price : "+rex[1]);
                curr_saldo = Long.parseLong(rex[2]);logger.log(Level.INFO,"saldo : "+rex[2]);
            }

            core.close();

        } catch (Exception e) {
            boleh = false;
            logger.log(Level.FATAL, e.getMessage() + " < " + inbox_id + " > " + e.toString());
            try {
                core.close();
            } catch (Exception ex) {
            }

        } finally {
        }

        return boleh;
    }
    
    public long getSaldo(int user_id) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();
        long saldo = 0;
        
        try {

                      
            //checking balance
            String sql = "select SUM(m.amount) from Mutations m where m.userId=:uid";
            Query qq = em.createQuery(sql);
            qq.setParameter("uid", user_id);

            Number saldoq=(Number) qq.getSingleResult();
            
            saldo = saldoq.longValue();

            em.close();
            factory.close();

        }
        catch (Exception e) {

            logger.log(Level.FATAL, e.getMessage()+" "+e.toString());

            if (em.getTransaction().isActive())
                em.getTransaction().rollback();

            if (em.isOpen()) {
                em.close();                
            }
            
            if (factory.isOpen()) {
                factory.close();
            }
        }
        finally {           
            
            if (em.isOpen()) {
                em.close();                
            }
            
            if (factory.isOpen()) {
                factory.close();
            }
            
        }

        return saldo;
    }
    
    public int getPriceTemplateId(int user_id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();
        
        int price_template_id = 0;
        System.out.println("User id : "+user_id);
        try {

            Users user = em.find(Users.class, user_id); 
            price_template_id = user.getPriceTemplateId();            
            
            em.close();
            factory.close();

        }
        catch(Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL,e.toString() );
        }
        finally {
            
            if (em.isOpen()) {
                em.close();                
            }
            
            if (factory.isOpen()) {
                factory.close();
            }
            
        }

        return price_template_id;
    }

    public synchronized void process() {

        int postpaid_pid = 100, prepaid_pid = 80, nontaglist_pid = 105;
        int price_template_id = 0;

        PrePaid pre = new PrePaid();
        PrepaidResponseCode preRC = new PrepaidResponseCode();
        PrepaidProcessing prePro = new PrepaidProcessing();

        PostPaid post = new PostPaid();
        PostpaidResponseCode postRC = new PostpaidResponseCode();
        PostPaidProcessing postPro = new PostPaidProcessing();

        NonTagList nonTagList = new NonTagList();
        NonTagListResponseCode nonRC = new NonTagListResponseCode();

        String hasil = "";
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();


        String detail[] = new String[9];

        RainbowCaller core = null;
        String id_pelanggan2 = "";
        try {            

            core = new RainbowCaller();
            
            //inbox message
            detail[0] = arrayQueue[1];
            //inbox sender
            detail[1] = arrayQueue[2];
            //inbox media type id
            detail[2] = arrayQueue[3];
            //inbox id
            detail[3] = String.valueOf(inbox_id);
            //inbox receiver
            detail[4] = arrayQueue[4];
            //inbox user id
            detail[5] = arrayQueue[5];
            //inbox sender type
            detail[6] = arrayQueue[6];
            detail[7] = "0";
            detail[8] = "0";

            // diganti iwan 20140411
            //price_template_id = Integer.parseInt(core.call("pricetemplate." + detail[5]));
            price_template_id = this.getPriceTemplateId(Integer.parseInt(detail[5]));
            
            /////////////////UPDATE STATUS INBOX/////////////////
            //String setInboxStatus = core.call("status.902." + detail[3]); logger.log(Level.INFO,setInboxStatus);
            setStatus(902, inbox_id);
            /////////////////////////////////////////////////////
            
            ////////////////get saldo/////////////////
            
            logger.log(Level.INFO,"Retrieving Saldo");
            
            /// diganti biar cepetan dikit - iwan - 20140411
            ///String theBalance = core.call("saldo." + detail[5]);
            
            String theBalance = String.valueOf(this.getSaldo(Integer.parseInt(detail[5])));
            
            double balq = Double.parseDouble(theBalance);
            curr_saldo = (long) balq;
            
            logger.log(Level.INFO,"Saldo : "+curr_saldo);
                        
            ///////////////////////////////////////

            logger.log(Level.INFO, "message : " + detail[0]);
            
            //Delivery Channel
            //String merchant_code = "6012"; //teller
            String merchant_code = "6021"; 

            String terminal_id = String.format("%16s", detail[5]).replace(' ', '0');

            //validate idpel
            String rex[] = detail[0].split("\\.");
            String id_pelanggan = rex[2];
            id_pelanggan2 = id_pelanggan;
            //inquiry PLN Prepaid
            if (Pattern.matches("cek.pln.[a-z0-9]+.[0-9]+", detail[0].toLowerCase())) {

                if (id_pelanggan.length() == 12 && id_pelanggan.charAt(0) == '0') {
                    id_pelanggan = id_pelanggan.substring(1);
                    logger.log(Level.INFO, "ID PEL correction : " + id_pelanggan);
                }

                if (id_pelanggan.length() < 11 || id_pelanggan.length() > 12) {
                    hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                            + "<response>"
                            + "<stan>"+String.valueOf(inbox_id)+"</stan>"                            
                            + "<produk>PLN</produk>"
                            + "<code>0014</code>"
                            + "<desc>NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI.</desc>"
                            + "<saldo>" + curr_saldo + "</saldo>"
                            + "</response>";
                    
                    /////////////////UPDATE STATUS INBOX/////////////////
                    //setInboxStatus = core.call("status.906." + detail[3]);
                    setStatus(906, inbox_id);
                    /////////////////////////////////////////////////////
                    
                } else {
                    hasil = pre.request_inquiry(detail[0], detail[5], String.valueOf(inbox_id), merchant_code, terminal_id);
                    detail[7] = String.valueOf(curr_saldo);
                    hasil = prePro.processInquiry(hasil,detail);
                }

            }//note : inquiry VIA SMS
            else if (Pattern.matches("cek.pln.[a-z0-9]+.[0-9]+.[0-9]+.[0-9]+", detail[0].toLowerCase())) {
                if (id_pelanggan.length() == 12 && id_pelanggan.charAt(0) == '0') {
                    id_pelanggan = id_pelanggan.substring(1);
                    logger.log(Level.INFO, "ID PEL correction : " + id_pelanggan);
                }

                if (id_pelanggan.length() < 11 || id_pelanggan.length() > 12) {
                    hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                            + "<response>"
                            + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                            + "<produk>PLN</produk>"
                            + "<code>0014</code>"
                            + "<desc>NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI.</desc>"
                            + "<saldo>" + curr_saldo + "</saldo>"
                            + "</response>";
                    
                    /////////////////UPDATE STATUS INBOX/////////////////
                    //setInboxStatus = core.call("status.906." + detail[3]);
                    setStatus(906, inbox_id);
                    /////////////////////////////////////////////////////
                    
                } else {
                    hasil = pre.request_inquiry(detail[0], detail[5], String.valueOf(inbox_id), merchant_code, terminal_id);      
                    detail[7] = String.valueOf(curr_saldo);
                    hasil = prePro.processInquiry(hasil,detail);
                }
            }//manual advice PLN Prepaid
            else if (Pattern.matches("advice.pln[0-9]+.[a-z0-9]+.[0-9]+.[0-9a-zA-Z\\s]+.[0-9a-zA-Z\\s\\-]+", detail[0].toLowerCase())) {

                String req[] = detail[0].toLowerCase().split("\\.");
                String _48 = req[4];
                String _62 = req[5];

                if (id_pelanggan.length() == 12 && id_pelanggan.charAt(0) == '0') {
                    id_pelanggan = id_pelanggan.substring(1);
                    logger.log(Level.INFO, "ID PEL correction : " + id_pelanggan);
                }

                if (id_pelanggan.length() < 11 || id_pelanggan.length() > 12) {
                    hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                            + "<response>"
                            + "<produk>PLN</produk>"
                            + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                            + "<code>0014</code>"
                            + "<desc>NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI.</desc>"
                            + "<saldo>" + curr_saldo + "</saldo>"
                            + "</response>";
                    
                    /////////////////UPDATE STATUS INBOX/////////////////
                    //setInboxStatus = core.call("status.906." + detail[3]);
                    setStatus(906, inbox_id);
                    /////////////////////////////////////////////////////
                    
                } else {
                    hasil = pre.adviceManual(detail[0], String.valueOf(inbox_id), detail[5], merchant_code, terminal_id, _48, _62);
                    hasil = prePro.processPurchase(hasil, detail, pln_price);
                }
            } //buy token PLN
            else if (Pattern.matches("token[0-9]*.pln[0-9]+.[a-z0-9]+.[0-9]+", detail[0].toLowerCase())) {

                //cek saldo
                String req[] = detail[0].toLowerCase().split("\\.");
                String nominal = req[1].substring(3, req[1].length());

                if (id_pelanggan.length() == 12 && id_pelanggan.charAt(0) == '0') {
                    id_pelanggan = id_pelanggan.substring(1);
                    logger.log(Level.INFO, "ID PEL correction : " + id_pelanggan);
                }

                if (id_pelanggan.length() < 11 || id_pelanggan.length() > 12) {
                    hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                            + "<response>"                            
                            + "<produk>PLN</produk>"
                            + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                            + "<code>0014</code>"
                            + "<desc>NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI.</desc>"
                            + "<saldo>" + curr_saldo + "</saldo>"
                            + "</response>";
                    
                    /////////////////UPDATE STATUS INBOX/////////////////
                    //setInboxStatus = core.call("status.906." + detail[3]);
                    setStatus(906, inbox_id);
                    /////////////////////////////////////////////////////
                    
                } else {

                    //kalau cukup saldonya                
                    if (itung(Double.parseDouble(nominal), inbox_id, Integer.parseInt(detail[5]), detail[0].toLowerCase(), prepaid_pid, price_template_id,1)) {
                        
                        //cari stok token by idpel / nometer + nominal                            
                        if (cekTokenUnsold(req[2], Integer.parseInt(nominal), inbox_id)) {
                            hasil = getTokenUnsold(req[2], Integer.parseInt(nominal), inbox_id);

                        }
                        else {
                            hasil = pre.purchase_token(detail[0], String.valueOf(inbox_id), detail[5], merchant_code, terminal_id);
                        }
                        

                        detail[7] = String.valueOf(curr_saldo);

                        logger.log(Level.INFO, "hasil purchase : " + hasil);
                        hasil = prePro.processPurchase(hasil, detail, pln_price);
                    } else {
                        hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                                + "<response>"
                                + "<produk>PLNPREPAID</produk>"
                                + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                                + "<code>0046</code>"
                                + "<desc>Saldo tidak cukup</desc>"
                                + "<saldo>" + curr_saldo + "</saldo>"
                                + "</response>";
                        
                        /////////////////UPDATE STATUS INBOX/////////////////
                    //setInboxStatus = core.call("status.400." + detail[3]);
                        setStatus(400, inbox_id);
                    /////////////////////////////////////////////////////
                    }
                }

            }//buy token PLN with TRX ID
            else if (Pattern.matches("token[0-9]*.pln[0-9]+.[a-z0-9]+.[0-9]+.[a-z0-9]+", detail[0].toLowerCase())) {

                //cek saldo
                String req[] = detail[0].toLowerCase().split("\\.");
                String nominal = req[1].substring(3, req[1].length());

                if (id_pelanggan.length() < 11 || id_pelanggan.length() > 12) {
                    hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                            + "<response>"
                            + "<trx_id>" + req[4] + "</trx_id>"
                            + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                            + "<produk>PLNPREPAID</produk>"
                            + "<code>0014</code>"
                            + "<desc>NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI.</desc>"
                            + "<saldo>" + curr_saldo + "</saldo>"
                            + "</response>";
                    
                    /////////////////UPDATE STATUS INBOX/////////////////
                    //setInboxStatus = core.call("status.906." + detail[3]);
                    setStatus(906, inbox_id);
                    /////////////////////////////////////////////////////
                    
                } else {

                    //check trx-id
                    int inbox_dup = isDuplicateTrxId(req[4], detail[5], inbox_id);
                    
                    //////////////////make sure not about the balance/////////////
                    int inbox_dup_temp = 0;
                                        
                    //////////////////////////////////////////////////////////////
                    hasil = "";
                    if (inbox_dup > 0) {
                        hasil = getMessageOutbox(inbox_dup);
                        if (hasil.contains("<code>0046</code>")) {
                            inbox_dup_temp = inbox_dup;    
                            inbox_dup = 0;                        
                            
                        }
                        else if (hasil.contains("<code>0017</code>")) {
                            inbox_dup_temp = inbox_dup;    
                            inbox_dup = 0;                 
                            
                        }
//                        else if (hasil.contains("<code>0005</code><desc>ERROR-OTHER</desc>")) {
//                            inbox_dup_temp = inbox_dup;    
//                            inbox_dup = 0;                        
//                            
//                        }
//                        else if (hasil.contains("<code>0068</code>")) {
//                            inbox_dup_temp = inbox_dup;    
//                            inbox_dup = 0;                        
//                            
//                        }
//                        else if (hasil.contains("<code>0063</code>")) {
//                            inbox_dup_temp = inbox_dup;    
//                            inbox_dup = 0;                        
//                            
//                        }
                        else if (hasil.contains("<code>0001</code>")) {
                            inbox_dup_temp = inbox_dup;    
                            //inbox_dup = 0;           
                            
                            logger.log(Level.INFO,"hasil 0001 "+inbox_dup);
                            
                            //cek waktu kl ud liwat 5 menit kirim ulang ke PLN
                            boolean expired = checkExpiryOutbox(inbox_dup);
                            
                            //kl liwat 5 menit 
                            if (expired) {                             
                                inbox_dup = 0;  
                            }
                        }
                        else if (hasil.trim().length()==0) {
                            //inbox_dup_temp = inbox_dup;  
                            
                            logger.log(Level.INFO,"hasilnya kosong : "+inbox_dup);
                            
                            //cek waktu kl ud liwat 5 menit kirim ulang
                            boolean expired = false;//checkExpiryOutbox(inbox_dup);
                            
                            //kl liwat 5 menit 
                            if (expired) {
                                inbox_dup_temp = inbox_dup;    
                                inbox_dup = 0;  
                            }
                        }
                    }
                    
                    if (inbox_dup > 0) {
                        
                        logger.log(Level.INFO, "Existing inbox : " + inbox_dup);
                        //hasil = getMessageOutbox(inbox_dup);
                        
                        hasil = hasil.replaceAll("<stan>[\\w]*</stan>", "<stan>"+String.valueOf(inbox_id)+"</stan>");
                        
                        if (hasil.length()<10) {
                            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                             + "<response>"
                             + "<trx_id>"+req[4]+"</trx_id>"
                             + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                             + "<produk>PLNPREPAID</produk>"
                             + "<code>0001</code>"
                             + "<desc>TRANSAKSI PENDING, MENUNGGU JAWABAN</desc>"
                             + "<saldo>"+curr_saldo+"</saldo>"   
                             + "</response>";
                            
                            /////////////////UPDATE STATUS INBOX/////////////////
                            //setInboxStatus = core.call("status.902." + detail[3]);
                            setStatus(902, inbox_id);
                            /////////////////////////////////////////////////////
                        }
                        
                        logger.log(Level.INFO, "Outbox msg : " + hasil);
                    } else {
                        //kalau cukup saldonya                
                        if (itung(Double.parseDouble(nominal), inbox_id, Integer.parseInt(detail[5]), detail[0].toLowerCase(), prepaid_pid, price_template_id,1)) {
                            
                            //cari stok token by idpel / nometer + nominal                            
                            if (cekTokenUnsold(req[2], Integer.parseInt(nominal), inbox_id)) {
                                hasil = getTokenUnsold(req[2], Integer.parseInt(nominal), inbox_id);
                                
                                
                                
                            }
                            else {
                                hasil = pre.purchase_token(detail[0], String.valueOf(inbox_id), detail[5], merchant_code, terminal_id);
                            }
                            
                            logger.log(Level.INFO,"hasil iso : "+hasil);
                            detail[7] = String.valueOf(curr_saldo);
                            
                            //kalo bukan sukses update status inbox
                            String[] resultingPre = pre.parsePurchaseMsgResponse(hasil,false);
                            if (!resultingPre[9].equalsIgnoreCase("0000")) {
                                /////////////////UPDATE STATUS INBOX/////////////////                                
                                setStatus(901, inbox_id);
                                /////////////////////////////////////////////////////
                            }

                            logger.log(Level.INFO,"hasil purchase token : " + hasil);
                            hasil = prePro.processPurchase(hasil, detail, pln_price);
                            
                            //replace the last result to the database
                            if (inbox_dup_temp>0) {
                                try {
                                    logger.log(Level.INFO,"replace inbox id : "+inbox_dup_temp);     
                                    logger.log(Level.INFO,"hasil : "+hasil);
                                    replaceOutboxMessage(inbox_dup_temp, hasil);
                                }
                                catch(Exception e) {
                                    e.printStackTrace();
                                }
                                
                            }
                            
                            
                        } else {
                            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                                    + "<response>"
                                    + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                                    + "<produk>PLNPREPAID</produk>"
                                    + "<code>0046</code>"
                                    + "<desc>Saldo tidak cukup</desc>"
                                    + "<saldo>" + curr_saldo + "</saldo>"
                                    + "</response>";
                            
                            /////////////////UPDATE STATUS INBOX/////////////////
                            //setInboxStatus = core.call("status.400." + detail[3]);
                            setStatus(400, inbox_id);
                            /////////////////////////////////////////////////////
                        }
                    }
                }
            }//note:  buy token PLN VIA SMS
            else if (Pattern.matches("token[0-9]*.pln[0-9]+.[a-z0-9]+.[0-9]+.[a-z0-9]+.[a-z0-9]+", detail[0].toLowerCase())) {
                //cek saldo
                String req[] = detail[0].toLowerCase().split("\\.");
                String nominal = req[1].substring(3, req[1].length());

                if (id_pelanggan.length() < 11 || id_pelanggan.length() > 12) {
                    hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                            + "<response>"
                            + "<trx_id>" + req[4] + "</trx_id>"
                            + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                            + "<produk>PLNPREPAID</produk>"
                            + "<code>0014</code>"
                            + "<desc>NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI.</desc>"
                            + "<saldo>" + curr_saldo + "</saldo>"
                            + "</response>";
                    
                    /////////////////UPDATE STATUS INBOX/////////////////
                    //setInboxStatus = core.call("status.906." + detail[3]);
                    setStatus(906, inbox_id);
                    /////////////////////////////////////////////////////
                } else {
                    //check trx-id
                    int inbox_dup = isDuplicateTrxId(req[5], detail[5], inbox_id);
                    if (inbox_dup > 0) {
                        logger.log(Level.INFO, "Existing trx_id : " + inbox_dup);
                        hasil = getMessageOutbox(inbox_dup);
                        logger.log(Level.INFO, "Outbox msg : " + hasil);
                        
                        /////////////////UPDATE STATUS INBOX/////////////////
                        //setInboxStatus = core.call("status.900." + detail[3]);
                        setStatus(900, inbox_id);
                        /////////////////////////////////////////////////////
                        
                    } else {
                        //kalau cukup saldonya                
                        if (itung(Double.parseDouble(nominal), inbox_id, Integer.parseInt(detail[5]), detail[0].toLowerCase(), prepaid_pid, price_template_id,1)) {
                            hasil = pre.purchase_token(detail[0], String.valueOf(inbox_id), detail[5], merchant_code, terminal_id);

                            detail[7] = String.valueOf(curr_saldo);

                            System.out.println("hasil purchase : " + hasil);
                            hasil = prePro.processPurchase(hasil, detail, pln_price);
                        } else {
                            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                                    + "<response>"
                                    + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                                    + "<produk>PLNPREPAID</produk>"
                                    + "<code>0046</code>"
                                    + "<desc>Saldo tidak cukup</desc>"
                                    + "<saldo>" + curr_saldo + "</saldo>"
                                    + "</response>";
                            
                            /////////////////UPDATE STATUS INBOX/////////////////
                            //setInboxStatus = core.call("status.400." + detail[3]);
                            setStatus(400, inbox_id);
                            /////////////////////////////////////////////////////
                            
                        }
                    }
                }

            }//buy token PLN unsold
            else if (Pattern.matches("unsoldtoken.pln[0-9]+.[a-z0-9]+.[0-9]+", detail[0].toLowerCase())) {

                //cek saldo
                String req[] = detail[0].toLowerCase().split("\\.");
                String nominal = req[1].substring(3, req[1].length());

                if (id_pelanggan.length() < 11 || id_pelanggan.length() > 12) {
                    hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                            + "<response>"
                            + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                            + "<produk>PLNPREPAID</produk>"
                            + "<code>0014</code>"
                            + "<desc>NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI.</desc>"
                            + "<saldo>" + curr_saldo + "</saldo>"
                            + "</response>";
                    
                    /////////////////UPDATE STATUS INBOX/////////////////
                    //setInboxStatus = core.call("status.906." + detail[3]); logger.log(Level.INFO,setInboxStatus);
                    setStatus(906, inbox_id);
                    /////////////////////////////////////////////////////
                } else {

                    //kalau cukup saldonya                
                    if (itung(Double.parseDouble(nominal), inbox_id, Integer.parseInt(detail[5]), detail[0].toLowerCase(), prepaid_pid, price_template_id,1)) {
                        hasil = pre.purchase_token_unsold(detail[0], String.valueOf(inbox_id), detail[5], merchant_code, terminal_id);

                        detail[7] = String.valueOf(curr_saldo);

                        System.out.println("hasil purchase : " + hasil);
                        hasil = prePro.processPurchase(hasil, detail, pln_price);
                    } else {
                        hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                                + "<response>"
                                + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                                + "<produk>PLNPREPAID</produk>"
                                + "<code>0046</code>"
                                + "<desc>Saldo tidak cukup</desc>"
                                + "<saldo>" + curr_saldo + "</saldo>"
                                + "</response>";
                        
                        /////////////////UPDATE STATUS INBOX/////////////////
                        //setInboxStatus = core.call("status.400." + detail[3]);
                        setStatus(400, inbox_id);
                        /////////////////////////////////////////////////////
                    }

                }

            } //inquiry PLN Postpaid or with trx_id + hp
            else if (Pattern.matches("cekplnpost.[a-z0-9]+.[0-9]+", detail[0].toLowerCase()) || Pattern.matches("cekplnpost.[a-z0-9]+.[0-9]+.[a-z0-9]+.[a-z0-9]+", detail[0].toLowerCase())) {

                id_pelanggan = rex[1];

                if (id_pelanggan.length() < 12 || id_pelanggan.length() > 12) {
                    hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                            + "<response>"
                            + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                            + "<produk>PLNPOSTPAID</produk>"
                            + "<code>0014</code>"
                            + "<desc>NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI.</desc>"
                            + "<saldo>" + curr_saldo + "</saldo>"
                            + "</response>";
                    
                    /////////////////UPDATE STATUS INBOX/////////////////
                    //setInboxStatus = core.call("status.906." + detail[3]);
                    setStatus(906, inbox_id);
                    /////////////////////////////////////////////////////
                    
                } else {
                    hasil = post.request_inquiry2(detail[0], detail[5], String.valueOf(inbox_id), merchant_code, terminal_id);
                    hasil = postPro.processInquiry(hasil);
                }

            }//pay PLN Postpaid
            else if (Pattern.matches("pln.pay.[0-9]+.[0-9]+", detail[0].toLowerCase())) {
                
                if (id_pelanggan.length() < 12 || id_pelanggan.length() > 12) {
                        hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                                + "<response>"
                                + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                                + "<trx_id></trx_id>"
                                + "<produk>PLNPOSTPAID</produk>"
                                + "<code>0014</code>"
                                + "<desc>NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI.</desc>"
                                + "<saldo>" + curr_saldo + "</saldo>"
                                + "</response>";

                        /////////////////UPDATE STATUS INBOX/////////////////
                        //setInboxStatus = core.call("status.906." + detail[3]);
                        setStatus(906, inbox_id);
                        /////////////////////////////////////////////////////

                    } else {

                        String prehasil = post.request_inquiry2(detail[0], detail[5], String.valueOf(inbox_id), merchant_code, terminal_id);

                        String[] result2 = post.parseInquiryMsgResponse(prehasil, false);

                        System.out.append("result2 : "+result2[8]);

                        if (result2[8].equalsIgnoreCase("0000")) {

                            String nominal = result2[2].substring(4, result2[2].length());
                            String[] reqPost = detail[0].split("\\.");
                            
                            int jumlah_rek = 1;
                            
                            try {
                                //cek jml bulan
                                String[] bit48 = post.parseBit48Inquiry(result2[10],result2[8]);
                                String jml_rek = bit48[2];
                                jumlah_rek = Integer.parseInt(jml_rek);
                            }
                            catch(Exception e) {
                                logger.log(Level.FATAL, e.toString());
                            }

                                    //kalau cukup saldonya                
                                    if (itung(Double.parseDouble(nominal), inbox_id, Integer.parseInt(detail[5]), detail[0].toLowerCase(), postpaid_pid, price_template_id,jumlah_rek)) {
                                        hasil = post.bill_payment2(detail[0], String.valueOf(inbox_id), detail[5], merchant_code, terminal_id, prehasil);

                                        detail[7] = String.valueOf(curr_saldo);
                                        detail[8] = String.valueOf(pln_price);
                                        
                                        hasil = postPro.processPayment(hasil, detail, pln_price);


                                    } else {
                                        hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                                                + "<response>"
                                                + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                                                + "<trx_id></trx_id>"
                                                + "<produk>PLNPOSTPAID</produk>"
                                                + "<code>0046</code>"
                                                + "<desc>Saldo tidak cukup</desc>"
                                                + "<saldo>" + curr_saldo + "</saldo>"
                                                + "</response>";

                                        /////////////////UPDATE STATUS INBOX/////////////////
                                        //setInboxStatus = core.call("status.400." + detail[3]);
                                        setStatus(400, inbox_id);
                                        /////////////////////////////////////////////////////
                                    }


                        }//gangguan
                        else {
                            hasil = postPro.processInquiry(prehasil);

                            /////////////////UPDATE STATUS INBOX/////////////////
                            //setInboxStatus = core.call("status.906." + detail[3]);
                            setStatus(901, inbox_id);
                            /////////////////////////////////////////////////////
                        }
                
                }
            } //PLN Postpaid with HP & TRX_id
            else if (Pattern.matches("pln.pay.[0-9]+.[0-9]+.[a-z0-9]+.[a-z0-9]+", detail[0].toLowerCase())) {
                
                String[] reqPost = detail[0].split("\\.");

                if (id_pelanggan.length() < 12 || id_pelanggan.length() > 12) {
                    hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                            + "<response>"
                            + "<trx_id>" + reqPost[5] + "</trx_id>"
                            + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                            + "<produk>PLNPOSTPAID</produk>"
                            + "<code>0014</code>"
                            + "<desc>NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI.</desc>"
                            + "<saldo>" + curr_saldo + "</saldo>"
                            + "</response>";
                    
                    /////////////////UPDATE STATUS INBOX/////////////////
                    //setInboxStatus = core.call("status.906." + detail[3]);
                    setStatus(906, inbox_id);
                    /////////////////////////////////////////////////////
                    
                } else {
                    
                    String prehasil = post.request_inquiry2(detail[0], detail[5], String.valueOf(inbox_id), merchant_code, terminal_id);
                    System.out.println("prehasil : "+prehasil);
                    String[] result2 = post.parseInquiryMsgResponse(prehasil, false);
                    
                    System.out.println("rc " + result2[8]);
                    if (result2[8].equalsIgnoreCase("0000")) {
                        
                        
                        int jumlah_rek = 1;
                            
                        try {
                            //cek jml bulan
                            String[] bit48 = post.parseBit48Inquiry(result2[10],result2[8]);
                            String jml_rek = bit48[2];
                            jumlah_rek = Integer.parseInt(jml_rek);
                        }
                        catch(Exception e) {
                            logger.log(Level.FATAL, e.toString());
                        }
                        
                        
                        String nominal = result2[2].substring(4, result2[2].length());
                        
                        //check trx-id
                        int inbox_dup = isDuplicateTrxIdPostpaid(reqPost[5], detail[5], inbox_id);

                        //////////////////make sure not about the balance/////////////
                        int inbox_dup_temp = 0;

                        //////////////////////////////////////////////////////////////
                        hasil = "";
                        if (inbox_dup > 0) {
                            hasil = getMessageOutbox(inbox_dup);
                            if (hasil.contains("<code>0046</code>")) {
                                inbox_dup_temp = inbox_dup;    
                                inbox_dup = 0;                        
                            }
                            else if (hasil.contains("<code>0017</code>")) {
                                inbox_dup_temp = inbox_dup;    
                                inbox_dup = 0;                        
                            }
                            else if (hasil.contains("<code>0001</code>")) {
                                inbox_dup_temp = inbox_dup;    
                                //inbox_dup = 0;           

                                logger.log(Level.INFO,"hasil 0001 "+inbox_dup);

                                //cek waktu kl ud liwat 5 menit kirim ulang ke PLN
                                boolean expired = checkExpiryOutbox(inbox_dup);

                                //kl liwat 5 menit 
                                if (expired) {                             
                                    inbox_dup = 0;  
                                }
                            }
                            else if (hasil.trim().length()==0) {
                                inbox_dup_temp = inbox_dup;  

                                logger.log(Level.INFO,"hasilnya kosong : "+inbox_dup);

                                //cek waktu kl ud liwat 5 menit kirim ulang ke PLN
                                boolean expired = false;//checkExpiryOutbox(inbox_dup);

                                //kl liwat 5 menit 
                                if (expired) {
                                    inbox_dup_temp = inbox_dup;    
                                    inbox_dup = 0;  
                                }
                            }
                        }

                        if (inbox_dup > 0) {

                            logger.log(Level.INFO, "Existing inbox : " + inbox_dup);
                            //hasil = getMessageOutbox(inbox_dup);

                            hasil = hasil.replaceAll("<stan>[\\w]*</stan>", "<stan>"+String.valueOf(inbox_id)+"</stan>");

                            if (hasil.length()<10) {
                                hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                                 + "<response>"
                                 + "<trx_id>"+reqPost[5]+"</trx_id>"
                                 + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                                 + "<produk>PLNPOSTPAID</produk>"
                                 + "<code>0001</code>"
                                 + "<desc>TRANSAKSI PENDING, MENUNGGU JAWABAN</desc>"
                                 + "<saldo>"+curr_saldo+"</saldo>"   
                                 + "</response>";

                                /////////////////UPDATE STATUS INBOX/////////////////
                                //setInboxStatus = core.call("status.902." + detail[3]);
                                setStatus(902, inbox_id);
                                /////////////////////////////////////////////////////
                            }

                            logger.log(Level.INFO, "Outbox msg : " + hasil);
                        } else {

                            //kalau cukup saldonya                
                            if (itung(Double.parseDouble(nominal), inbox_id, Integer.parseInt(detail[5]), detail[0].toLowerCase(), postpaid_pid, price_template_id,jumlah_rek)) {
                                hasil = post.bill_payment2(detail[0], String.valueOf(inbox_id), detail[5], merchant_code, terminal_id, prehasil);

                                //kalau hasilnya gagal
                                String[] resultingPost = post.parsePaymentMsgResponse(hasil,false);
                                if (!resultingPost[9].equalsIgnoreCase("0000") ) {
                                    /////////////////UPDATE STATUS INBOX/////////////////                                
                                    setStatus(901, inbox_id);
                                    /////////////////////////////////////////////////////
                                }

                                detail[7] = String.valueOf(curr_saldo);
                                detail[8] = String.valueOf(pln_price);
                                hasil = postPro.processPayment(hasil, detail, pln_price);

                            } else {
                                hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                                        + "<response>"
                                        + "<trx_id>" + reqPost[5] + "</trx_id>"
                                        + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                                        + "<produk>PLNPOSTPAID</produk>"
                                        + "<code>0046</code>"
                                        + "<desc>Saldo tidak cukup</desc>"
                                        + "<saldo>" + curr_saldo + "</saldo>"
                                        + "</response>";

                                /////////////////UPDATE STATUS INBOX/////////////////
                                //setInboxStatus = core.call("status.400." + detail[3]);
                                setStatus(400, inbox_id);
                                /////////////////////////////////////////////////////
                            }
                        }
                    }
                    else {
                        
                        hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                            + "<response>"
                            + "<produk>PLNPOSTPAID</produk>"
                            + "<code>"+result2[8]+"</code>"
                            + "<stan>"+inbox_id+"</stan>"   
                            + "<desc>"+postRC.getPurchaseMsgResponseName(result2[8])+"</desc>"
                            + "</response>";
                    }
                }
            }
            else if (Pattern.matches("pln.non.[0-9]+.[0-9]+", detail[0].toLowerCase())) {
                System.out.println("========pln non inquiry start==========");
                hasil = nonTagList.request_inquiry(detail[0], String.valueOf(inbox_id), detail[5], merchant_code, terminal_id);

                hasil = nonTagList.processInquiry(hasil);

                System.out.println("========NTL xml : " + hasil + "==========");
                System.out.println("========pln  non inquiry done==========");

            }//Pay Non tag list
            else if (Pattern.matches("pln.paynon.[0-9]+.[0-9]+", detail[0].toLowerCase())) {

                String hasilInquiry = "";
                String rc_inq = "";
                hasil = nonTagList.request_inquiry(detail[0], String.valueOf(inbox_id), detail[5], merchant_code, terminal_id);
                hasilInquiry = hasil;

                //note: start
                ParseISOMessage parseISOMessage = new ParseISOMessage();
                Map<String, String> inquiryResp = new HashMap<String, String>();
                String[] bit48 = new String[30];
                try {
                    inquiryResp = parseISOMessage.getBitmap(hasil);
                    rc_inq = inquiryResp.get("39"); System.out.println("rc inqn : " + rc_inq);
                    if (rc_inq.equalsIgnoreCase("0000")) {
                        bit48 = nonTagList.parseBit48Inquiry(inquiryResp.get("48"));
                    }
                } catch (ISOException ex) {
                    java.util.logging.Logger.getLogger(NonTagList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
                
              if (rc_inq.equalsIgnoreCase("0000")) {

                //String nominal = inquiryResp.get("4").substring(4);
                String nominal = ""+bit48[14].substring(0, (bit48[14].length() - Integer.parseInt(bit48[13]) ) );
		//note: end

                logger.log(Level.INFO, ">>===============user id : " + detail[5] + "=======================");
                logger.log(Level.INFO, ">>===============nominal : " + nominal + "=======================");
                //System.out.println("===============balance : " + mutasi.getBalance(detail[5]) + "=======================");

//                note:untuk testing
//                boolean itung = true;
//                if (itung) {
                if (itung(Double.parseDouble(nominal), inbox_id, Integer.parseInt(detail[5]), detail[0].toLowerCase(), nontaglist_pid, price_template_id,1)) {
                    logger.log(Level.INFO, "===============saldo mecukupi=======================");
                    //mutasi.kurangBalance(Double.parseDouble(nominal), detail[5], detail[0], String.valueOf(inbox_id), mutasi.getBalance(detail[5]));
                    hasil = nonTagList.bill_payment(detail[0], String.valueOf(inbox_id), detail[5], merchant_code, terminal_id, hasilInquiry, nominal);

                    detail[7] = String.valueOf(curr_saldo);

                    hasil = nonTagList.processPayment(hasil);

                    logger.log(Level.INFO, "===============hasil payment XML : " + hasil + "=======================");

                    logger.log(Level.INFO, "===============payment selesai=======================");

                } else {
                    logger.log(Level.INFO, "===============saldo tidak mencukupi=======================");
                    hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                            + "<response>"
                            + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                            + "<produk>PLNNONTAGLIS</produk>"
                            + "<code>0046</code>"
                            + "<desc>Saldo tidak cukup</desc>"
                            + "<saldo>" + curr_saldo + "</saldo>"
                            + "</response>";
                    
                    /////////////////UPDATE STATUS INBOX/////////////////
                    //setInboxStatus = core.call("status.400." + detail[3]);
                    setStatus(400, inbox_id);
                    /////////////////////////////////////////////////////
                }
              }
              else {
                  hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                        + "<response>"
                        + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                        + "<produk>PLNNONTAGLIS</produk>"
                        + "<code>"+rc_inq+"</code>"
                        + "<desc>"+nonRC.getInquiryMsgResponseName(rc_inq) +"</desc>"
                        + "<saldo>" + curr_saldo + "</saldo>"
                        + "</response>";
                
                
                    /////////////////UPDATE STATUS INBOX/////////////////
                    //setInboxStatus = core.call("status.907." + detail[3]);
                    setStatus(907, inbox_id);
                    /////////////////////////////////////////////////////
              }

            } else {
                hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                        + "<response>"
                        + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                        + "<produk>PLNNONTAGLIS</produk>"
                        + "<code>0030</code>"
                        + "<desc>Format Salah</desc>"
                        + "<saldo>" + curr_saldo + "</saldo>"
                        + "</response>";
                
                
                /////////////////UPDATE STATUS INBOX/////////////////
                //setInboxStatus = core.call("status.907." + detail[3]);
                setStatus(907, inbox_id);
                /////////////////////////////////////////////////////
            }
            
            logger.log(Level.INFO, "TRX PLN RESULT : " + hasil);
            
            //replace & with dan
            hasil = hasil.replaceAll("&", "dan");
            hasil = hasil.replaceAll("\\P{Print}", "");
            
            //////cek hasil akhir ///////////////
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc;
            InputStream xmlAkhir = new ByteArrayInputStream(hasil.toString().getBytes());
            doc = docBuilder.parse(xmlAkhir);
            doc.getDocumentElement().normalize();
            logger.info(doc.getDocumentElement().getNodeName());
            
            try {
                NodeList nodeRc = doc.getElementsByTagName("code");
                Element eRc = (Element) nodeRc.item(0);
                NodeList rcx = eRc.getChildNodes();

                String rc = ((Node) rcx.item(0)).getNodeValue().trim();
            
                if (!rc.equalsIgnoreCase("0000")) {
                    //setInboxStatus = core.call("status.901." + detail[3]);
                    setStatus(901, inbox_id);
                }
               
            }
            catch(Exception e) {
                logger.log(Level.FATAL,"CODE ra ono : " + e.getMessage());
                //setInboxStatus = core.call("status.200." + detail[3]);
                setStatus(200, inbox_id);
            }
            //////cek hasil akhir ///////////////

            if (hasil.length() > 0) {
                insertToOutbox(hasil, detail[1],
                        detail[6], String.valueOf(this.inbox_id), Integer.parseInt(detail[5]),
                        detail[4], Integer.parseInt(detail[2]), "0000", id_pelanggan);
            }
            
            core.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.toString());

            try {
                core.close();
            } catch (Exception ex) {
            }

            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<stan>"+String.valueOf(inbox_id)+"</stan>"
                    + "<produk>PLN</produk>"
                    + "<code>0005</code>"
                    + "<desc>SYSTEM ERROR</desc>"
                    + "<saldo>" + curr_saldo + "</saldo>"
                    + "</response>";

            insertToOutbox(hasil, detail[1],
                    detail[6], String.valueOf(this.inbox_id), Integer.parseInt(detail[5]),
                    detail[4], Integer.parseInt(detail[2]), "0005", id_pelanggan2);

        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
            
        }
    }

    public int isDuplicateTrxId(String trx_id, String user_id, int inbox_id) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();
        int hasil = 0;
        logger.log(Level.INFO,"cek duplikasi..."+trx_id+"#"+user_id+"#"+inbox_id);
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	     //get current date time with Date()
	     java.util.Date date = new java.util.Date();
             SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	     //System.out.println(dateFormat.format(date));
             
//            String sql = " SELECT o FROM Inboxes o "                        
//                       + " where o.message like 'token.pln%.%.%." + trx_id + "' and o.id not in (:inbox_id) "
//                       + " and o.userId =:user_id "   
//                       //+ " and o.createDate =: tanggal"
//                       + " order by o.id asc";
             
            String sql = " SELECT o FROM Inboxes o "                        
                       + " where o.trxId =:trx_id "
                       + " and o.id not in (:inbox_id) "
                       + " and o.userId =:user_id "
                       + " and o.prodId =:prod_id "
                       + " and o.trxType =:trx_type "
                       + " order by o.id asc";

            Query query = em.createQuery(sql);
            query.setParameter("user_id", Integer.parseInt(user_id));
            query.setParameter("inbox_id", inbox_id);
            query.setParameter("trx_id", trx_id);
            query.setParameter("prod_id", 80);
            query.setParameter("trx_type", "2200");
            //query.setParameter("tanggal", formatter.parse(dateFormat.format(date))); System.out.println("tangagl : "+dateFormat.format(date));
            query.setMaxResults(1);

            //hasil = (Integer)query.getSingleResult(); logger.log(Level.INFO,"hasil inbox id : "+hasil);

            for (Inboxes m : (List<Inboxes>) query.getResultList()) {
                hasil = m.getId().intValue(); logger.log(Level.INFO,"hasil cari duplikat inbox id : "+hasil);
            }
            
            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }
        System.out.println("hasil duplikasi : "+ hasil);
        return hasil;
    }
    
    public int isDuplicateTrxIdPostpaid(String trx_id, String user_id, int inbox_id) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();
        int hasil = 0;
        logger.log(Level.INFO,"cek duplikasi..."+trx_id+"#"+user_id+"#"+inbox_id);
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	     //get current date time with Date()
	     java.util.Date date = new java.util.Date();
             SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	     //System.out.println(dateFormat.format(date));
            
            String sql = " SELECT o FROM Inboxes o "                        
                       + " where o.trxId =:trx_id "
                       + " and o.id not in (:inbox_id) "
                       + " and o.userId =:user_id "
                       + " and o.prodId =:prod_id "
                       + " and o.trxType =:trx_type "
                       + " order by o.id asc";

            Query query = em.createQuery(sql);
            query.setParameter("user_id", Integer.parseInt(user_id));
            query.setParameter("inbox_id", inbox_id);
            query.setParameter("trx_id", trx_id);
            query.setParameter("prod_id", 100);
            query.setParameter("trx_type", "2200");
            //query.setParameter("tanggal", formatter.parse(dateFormat.format(date))); System.out.println("tangagl : "+dateFormat.format(date));
            query.setMaxResults(1);

            //hasil = (Integer)query.getSingleResult(); logger.log(Level.INFO,"hasil inbox id : "+hasil);

            for (Inboxes m : (List<Inboxes>) query.getResultList()) {
                hasil = m.getId().intValue(); logger.log(Level.INFO,"hasil cari duplikat inbox id : "+hasil);
            }
            
            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }
        System.out.println("hasil duplikasi : "+ hasil);
        return hasil;
    }

    protected void resendMessageOutbox(long id) {
        String result = "";
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();

        logger.log(Level.INFO, "Resending... " + id);
        try {
            em.getTransaction().begin();
            Outboxes outbox = em.find(Outboxes.class, id);
            outbox.setStatus(0);
            em.getTransaction().commit();

            em.close();
            factory.close();

            logger.log(Level.INFO, "Resend ok " + id);
        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        } finally {

            if (em.isOpen()) {
                em.close();
            }
            if (factory.isOpen()) {
                factory.close();
            }

        }
    }
    
    public boolean checkExpiryOutbox(int id) {
        boolean result = false;
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();
        try {

            String sql = "SELECT o FROM Outboxes o "
                    + " where o.inboxId =:inbox_id "
                    + " order by o.inboxId asc";
            
            Query query = em.createQuery(sql);
            query.setParameter("inbox_id", id);
            query.setMaxResults(1);

            if (query.getResultList().size()>0) {
                for (Outboxes m : (List<Outboxes>) query.getResultList()) {
                    logger.log(Level.INFO,"id : "+m.getId());

                    java.util.Date dtOutCreated = (java.util.Date) m.getCreateDate();
                    java.util.Date dtNow = new java.util.Date();

                    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

                    long diff =  dtNow.getTime() - dtOutCreated.getTime();

                    long diffSeconds = diff / 1000 % 60;
                    long diffMinutes = diff / (60 * 1000) % 60; logger.log(Level.INFO,"<"+id+"> menit : "+diffMinutes);
                    long diffHours = diff / (60 * 60 * 1000) % 24; logger.log(Level.INFO,"<"+id+"> jam : "+diffHours);
                    long diffDays = diff / (24 * 60 * 60 * 1000); logger.log(Level.INFO,"<"+id+"> hari : "+diffDays);

                    if (diffMinutes > 5 && diffDays>=0 && diffHours >=0) {
                        result = true;
                    }

                    logger.log(Level.INFO, "hasil expiry : "+result);
                }
            }
            else {
                logger.log(Level.INFO,"Outbox kosong : "+inbox_id);
                
                // cek date inbox_id
                sql = "SELECT o FROM Inboxes o "
                    + " where o.id =:inbox_id ";                   
                    
                query = em.createQuery(sql);
                query.setParameter("inbox_id", id);
                query.setMaxResults(1);
                
                if (query.getResultList().size()>0) {
                    for (Inboxes m : (List<Inboxes>) query.getResultList()) {
                        
                        // compare dgn timestamp sekarang
                        
                        java.util.Date dtInbox = (java.util.Date) m.getCreateDate();
                        java.util.Date dtNow = new java.util.Date();

                        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

                        long diff = dtNow.getTime() - dtInbox.getTime();

                        long diffSeconds = diff / 1000 % 60; logger.log(Level.INFO,"detik : "+diffSeconds);
                        long diffMinutes = diff / (60 * 1000) % 60; logger.log(Level.INFO,"menit : "+diffMinutes);
                        long diffHours = diff / (60 * 60 * 1000) % 24; logger.log(Level.INFO,"jam : "+diffHours);
                        long diffDays = diff / (24 * 60 * 60 * 1000); logger.log(Level.INFO,"hari : "+diffDays);

                        if (diffMinutes > 5 && diffDays>=0 && diffHours >=0) { 
                            System.out.println("oioiio");
                            result = true;
                        }
                        
                    }
                }
            }

            em.close();
            factory.close();
        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
        } finally {

            if (em.isOpen()) {
                em.close();
            }
            if (factory.isOpen()) {
                factory.close();
            }

        }

        return result;
    }

    public String getMessageOutbox(int id) {
        String result = "";
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();
        try {

            String sql = "SELECT o FROM Outboxes o "
                    + " where o.inboxId =:inbox_id order by o.inboxId asc";
            Query query = em.createQuery(sql);
            query.setParameter("inbox_id", id);
            query.setMaxResults(1);

            //if (query.getResultList().size()>0) {
            for (Outboxes m : (List<Outboxes>) query.getResultList()) {
                logger.log(Level.INFO,"id : "+m.getId());
                //resendMessageOutbox(m.getId());
                result = m.getMessage();
                logger.log(Level.INFO, result);
            }
            //}

            em.close();
            factory.close();
        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
        } finally {

            if (em.isOpen()) {
                em.close();
            }
            if (factory.isOpen()) {
                factory.close();
            }

        }

        return result;
    }
    
    public void replaceOutboxMessage(int inbox_id, String msg) {
        
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();

        try {
            
            em.getTransaction().begin();
            Query query = em.createQuery("UPDATE Outboxes o SET o.message = :message where o.inboxId=:inbox_id");
            query.setParameter("message", msg);
            int x = query.setParameter("inbox_id", inbox_id).executeUpdate();
            
            System.out.println("update : "+x);
            em.getTransaction().commit();
//            Outboxes outbox = em.find(Outboxes.class,inbox_id);     
//            em.getTransaction().begin();
//            outbox.setMessage(msg);
//            em.persist(outbox);            
//            em.getTransaction().commit();
            
            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());


        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }
        
    }

    public void insertToOutbox(String msg, String receiver, String receiver_type, String transaction_id,
            int user_id, String sender, int media_type_id, String response_code, String id_pelanggan) {
        
        boolean isSMS = false;
        int status = 1;
        if (media_type_id != 1) {
            insertToQueue(msg, receiver, receiver_type, transaction_id,
                    user_id, sender, media_type_id, response_code, id_pelanggan);
        }else if (media_type_id == 1){
            isSMS = true;
            status = 0;
        }

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();

        try {
            Outboxes outbox = new Outboxes();
            em.getTransaction().begin();

            outbox.setMessage(msg);
            outbox.setStatus(status);
            outbox.setCreateDate(new java.util.Date());
            outbox.setReceiver(receiver);
            outbox.setReceiverType(receiver_type);
            outbox.setTransactionId(inbox_id);
            outbox.setInboxId(inbox_id);
            outbox.setUserId(user_id);
            outbox.setSender(sender);
            outbox.setResponseCode(response_code);
            outbox.setMediaTypeId(media_type_id);
            outbox.setSms(isSMS);
            em.persist(outbox);
            em.getTransaction().commit();

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
            
            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

            insertToOutbox(msg, receiver, receiver_type, transaction_id, user_id, sender, media_type_id, response_code, id_pelanggan);

        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }
    }

    private void insertToQueue(String msg, String receiver, String receiver_type, String transaction_id, int user_id, String sender, int media_type_id, String response_code, String id_pelanggan) {
        String QUEUE_NAME = "outbox";

        String TASK_QUEUE_NAME = "outbox_queue";

        String EXCHANGE_NAME = "outbox_general_exchange";

        String ROUTING_KEY = "outbox.general";

        Settings setting = new Settings();
        //setting.setConnections();

        try {

            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(setting.getRabbitHost());
            com.rabbitmq.client.Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            PlnConverter plnConverter = new PlnConverter();
            String message = receiver + "#" + msg ;
            String isXml = "";
            if (sender.contains("NONXML")) {
                message = receiver + "#" + plnConverter.convertPrepaid(msg, id_pelanggan);
                isXml = "NONXML";
            } else {
                QUEUE_NAME = "outboxxml";
                TASK_QUEUE_NAME = "outboxxml_queue";
                EXCHANGE_NAME = "outboxxml_general_exchange";
                ROUTING_KEY = "outboxxml.general";
                isXml = "XML";
            }
            
            message += "#" + media_type_id;
            
            channel.exchangeDeclare(EXCHANGE_NAME, "topic", true);
            channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);

            if (!message.equalsIgnoreCase("0")) {

                channel.basicPublish(EXCHANGE_NAME, ROUTING_KEY,
                        MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());

                //System.out.println(" [x] Sent '" + message + "'");
                logger.log(Level.INFO, " [x] Sent '" + message + "'  <==>  " + sender + " <==> " + receiver + " <==> " + isXml);
            }

            channel.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e);
        }
    }
    
    public boolean setStatus(int code, int id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();
        
        boolean result = false;
        System.out.print("Set status>>");
        try {
            
            em.getTransaction().begin();
            Query query = em.createQuery("UPDATE Inboxes o SET o.status = :stat where o.id=:inbox_id");
            query.setParameter("stat", code);
            int x = query.setParameter("inbox_id", id).executeUpdate();
            
            System.out.println("update : "+x);
            em.getTransaction().commit();
            
//            em.getTransaction().begin();
//            Inboxes inbox = em.find(Inboxes.class, id); 
//            inbox.setStatus(code);
//            em.persist(inbox);
//            em.getTransaction().commit();
            
            em.close();
            factory.close();
            
            result = true;
            System.out.println("<<set status ok");
        }
        catch(Exception e) {
            result = false;
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage()+" "+e.toString());
            
            if (em.isOpen()) {
                em.close();                
            }
            
            if (factory.isOpen()) {
                factory.close();
            }
            
            result = setStatus(code, id);
        }
        finally {
            
            if (em.isOpen()) {
                em.close();                
            }
            
            if (factory.isOpen()) {
                factory.close();
            }
            
        }
        
        return result;
    }
    
    public boolean cekTokenUnsold(String idpel, int nominal, int inbox_id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();
        
        boolean result = false;
        logger.log(Level.INFO,"Checking unsold token...");
        try {
            String sql = "SELECT COUNT(o.id) FROM TokenUnsolds o "
                    + " where (o.idpel =:idpel or o.snmeter =:snmeter) and o.sold=false"
                    + " and o.nominal=:nominal";
            Query query = em.createQuery(sql);
            query.setParameter("idpel", idpel);
            query.setParameter("snmeter", idpel);
            query.setParameter("nominal", nominal);
            //query.setMaxResults(1);
            Number qty = (Number) query.getSingleResult();
           logger.log(Level.INFO, "qty : "+qty.intValue());
            if (qty.intValue() > 0) {
                result = true;
                logger.log(Level.INFO, "ambil stok cuy "+idpel+"#"+nominal);
            } else {
                result = false;
            }
            
            em.close();
            factory.close();
            
        }
        catch(Exception e) {
            result = false;
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage()+" "+e.toString());
            
            if (em.isOpen()) {
                em.close();                
            }
            
            if (factory.isOpen()) {
                factory.close();
            }
            
            
        }
        finally {
            
            if (em.isOpen()) {
                em.close();                
            }
            
            if (factory.isOpen()) {
                factory.close();
            }
            
        }
        
        return result;
    }
    
    public String getTokenUnsold(String idpel, int nominal, int inbox_id) {
        String result = "";
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();

        try {

            long idx = 0;
            
            String sql = "";
            
            sql = "select o from TokenUnsolds o where (o.idpel =:idpel or o.snmeter =:snmeter) and o.sold=false "
                    + "and o.nominal=:nominal";
            
            em.getTransaction().begin();
            
            try {
                
                Query query = em.createQuery(sql);
                query.setParameter("idpel", idpel);
                query.setParameter("snmeter", idpel);
                query.setParameter("nominal", nominal);
                
                query.setMaxResults(1);
                
                for (TokenUnsolds voc : (List<TokenUnsolds>) query.getResultList()) {

                    idx = voc.getId();
                    result = voc.getIso(); logger.log(Level.INFO,"iso unsold : "+result);
                    
                    //replace stan in iso
                    
                }
                
            }
            catch(NoResultException e) {
                System.out.println("Error no result voucher : "+e.getMessage());
                idx = 0;
            }

            if (idx != 0) {
             //   em.getTransaction().begin();
                
                TokenUnsolds voucher = em.find(TokenUnsolds.class, idx);
                //em.lock(voucher, LockModeType.PESSIMISTIC_FORCE_INCREMENT);

                voucher.setSoldDate(new java.util.Date());
                voucher.setSold(Boolean.TRUE);
                voucher.setInboxId(inbox_id);
                
             //   em.getTransaction().commit();
            }
            
            em.getTransaction().commit();

        }        
        catch(Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
        }
        finally {

            if (em.isOpen()) {
                em.close();                
            }
            
            if (factory.isOpen()) {
                factory.close();
            }
            
        }

        return result;
    }
    
}
