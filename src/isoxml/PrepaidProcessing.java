
package isoxml;

import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import model.PlnPrepaids;
import model.Transactions;
import org.apache.log4j.*;
/**
 *
 * @author ngonar
 */
public class PrepaidProcessing {

    private static final Logger logger = Logger.getLogger("PrePaidProcessing");

    PrePaid pre = new PrePaid();
    PrepaidResponseCode preRC = new PrepaidResponseCode();

    public String processPurchase2(String hasil) {
        String xml = "";
        //System.out.println("Entering purchase process");

        try {
            
//            String msg_inbox = detail[0].toLowerCase();
//            String req[] = msg_inbox.split("\\.");

            String[] result = pre.parsePurchaseMsgResponse(hasil,false);
            
            if (result[9].equalsIgnoreCase("0000")) {
                
                String[] bit48 = pre.parseBit48Purchase(result[11]);
                String[] bit62 = pre.parseBit62Purchase(result[12]);
                
                xml = "<?xml version='1.0' encoding='UTF-8'?>"+
                        "<response>"+
                         "<produk>PLNPREPAID</produk>";
                
//                if (req.length>4) {
//                    xml+= "<trx_id>"+req[4]+"</trx_id>";                                 
//                }
                                 
                bit48[7] = bit48[7].replaceAll("<", ".");
                bit48[7] = bit48[7].replaceAll(">", ",");
                
                result[11] = result[11].replaceAll("<", ".");
                result[11] = result[11].replaceAll(">", ",");
                
                result[12] = result[12].replaceAll("<", ".");
                result[12] = result[12].replaceAll(">", ",");
                
                xml+=    
//                        "<harga>"+harga+"</harga>"+
//                         "<saldo>"+detail[7]+"</saldo>"+
                         "<amount>"+result[2]+"</amount>"+
                         //"<stan>"+result[3]+"</stan>"+
//                         "<stan>"+detail[3]+"</stan>"+
                         "<datetime>"+result[4]+"</datetime>"+
                         "<settlement>"+result[5]+"</settlement>"+
                         "<merchant_code>"+result[6]+"</merchant_code>"+
                         "<bank_code>"+result[7]+"</bank_code>"+
                         "<rc>"+result[9]+"</rc>"+
                         "<terminal_id>"+result[10]+"</terminal_id>"+
                         "<material_number>"+bit48[1]+"</material_number>"+
                         "<subscriber_id>"+bit48[2]+"</subscriber_id>"+
                         "<pln_refno>"+bit48[4]+"</pln_refno>"+
                         "<switcher_refno>"+bit48[5]+"</switcher_refno>"+
                         "<vending_refno>"+bit48[6]+"</vending_refno>"+
                         "<subscriber_name>"+bit48[7]+"</subscriber_name>"+                                          
                        "<subscriber_segmentation>"+bit48[8]+"</subscriber_segmentation>"+
                         "<power>"+bit48[9]+"</power>"+
                         "<minor_admin_charge>"+bit48[11]+"</minor_admin_charge>"+
                         "<admin_charge>"+bit48[12]+"</admin_charge>";

                if (result[9].equalsIgnoreCase("0000")) {
                    xml+=    "<minor_meterai>"+bit48[13]+"</minor_meterai>"+
                             "<meterai>"+bit48[14]+"</meterai>"+
                             "<buying_option>"+bit48[10]+"</buying_option>"+
                             "<minor_ppn>"+bit48[15]+"</minor_ppn>"+
                             "<ppn>"+bit48[16]+"</ppn>"+
                             "<minor_ppj>"+bit48[17]+"</minor_ppj>"+
                             "<ppj>"+bit48[18]+"</ppj>"+
                             "<minor_angsuran>"+bit48[19]+"</minor_angsuran>"+
                             "<angsuran>"+bit48[20]+"</angsuran>"+
                             "<minor_power_purchase>"+bit48[21]+"</minor_power_purchase>"+
                             "<power_purchase>"+bit48[22]+"</power_purchase>"+
                             "<minor_purchase_kwh>"+bit48[23]+"</minor_purchase_kwh>"+
                             "<jml_kwh>"+bit48[24]+"</jml_kwh>"+
                             "<token>"+bit48[25]+"</token>";
                }

                xml+=    "<distribution_code>"+bit62[0]+"</distribution_code>"+
                         "<service_unit>"+bit62[1]+"</service_unit>"+
                         "<service_unit_phone>"+bit62[2]+"</service_unit_phone>"+
                         "<max_kwh>"+bit62[3]+"</max_kwh>"+
                         "<total_repeat>"+bit62[4]+"</total_repeat>"+
                         "<bit48>"+result[11]+"</bit48>"+
                         "<bit62>"+result[12]+"</bit62>";

                if (Integer.parseInt(bit62[4])>0) {
                    xml+= "<power_purchase_unsold>"+bit62[5]+"</power_purchase_unsold>";
                }

                if (Integer.parseInt(bit62[4])>1) {
                    xml+= "<power_purchase_unsold2>"+bit62[6]+"</power_purchase_unsold2>";
                }

                xml+=    "<info_text>"+result[13]+"</info_text>"+                        
                        "</response>";


                String amt_meterai = bit48[14].substring(0,(bit48[14].length()-Integer.parseInt(bit48[13])));
                String amt_meterai2 = bit48[14].substring((bit48[14].length()-Integer.parseInt(bit48[13])),bit48[14].length());
                String amt_met = amt_meterai + "." + amt_meterai2;

                String amt_ppn1 = bit48[16].substring(0,(bit48[16].length()-Integer.parseInt(bit48[15])));
                String amt_ppn2 = bit48[16].substring((bit48[16].length()-Integer.parseInt(bit48[15])),bit48[16].length());
                String amt_ppn = amt_ppn1 + "." + amt_ppn2;

                String amt_ppj1 = bit48[18].substring(0,(bit48[18].length()-Integer.parseInt(bit48[17])));
                String amt_ppj2 = bit48[18].substring((bit48[18].length()-Integer.parseInt(bit48[17])),bit48[18].length());
                String amt_ppj = amt_ppj1 + "." + amt_ppj2;

                String amt_angsuran1 = bit48[20].substring(0,(bit48[20].length()-Integer.parseInt(bit48[19])));
                String amt_angsuran2 = bit48[20].substring((bit48[20].length()-Integer.parseInt(bit48[19])),bit48[20].length());
                String amt_angsuran = amt_angsuran1 + "." + amt_angsuran2;

                String amt_pwr1 = bit48[22].substring(0,(bit48[22].length()-Integer.parseInt(bit48[21])));
                String amt_pwr2 = bit48[22].substring((bit48[22].length()-Integer.parseInt(bit48[21])),bit48[22].length());
                String amt_pwr = amt_pwr1 + "." + amt_pwr2;

                String amt_kwh1 = bit48[24].substring(0,(bit48[24].length()-Integer.parseInt(bit48[23])));
                String amt_kwh2 = bit48[24].substring((bit48[24].length()-Integer.parseInt(bit48[23])),bit48[24].length());
                String amt_kwh = amt_kwh1 + "." + amt_kwh2;

                String amt_admin1 = bit48[12].substring(0,(bit48[12].length()-Integer.parseInt(bit48[11])));
                String amt_admin2 = bit48[12].substring((bit48[12].length()-Integer.parseInt(bit48[11])),bit48[12].length());
                String amt_admin = amt_admin1 + "." + amt_admin2;

                //simpan data payment ke database
//                long idx = insertToTrx(Integer.parseInt(detail[5]), 80, detail[4], detail[1],
//                                detail[6], Long.parseLong(result[2].substring(4)), (long) harga,
//                                Integer.parseInt(result[3]),
//                                result[9],"PLN-PREPAID",Long.parseLong("0"),Short.parseShort("0"),
//                                Short.parseShort("0"),bit48[5],detail[4],Short.parseShort("0"));
                
//                 long idx = insertToTrx(Integer.parseInt(detail[5]), 80, detail[4], detail[1],
//                                detail[6], Long.parseLong(result[2].substring(4)), (long) harga,
//                                Integer.parseInt(detail[3]),
//                                result[9],"PLN-PREPAID",Long.parseLong("0"),Short.parseShort("0"),
//                                Short.parseShort("0"),bit48[5],detail[4],Short.parseShort("0"));
//
//                insertToDB(Integer.parseInt(result[3]), 
//                        String.valueOf(idx), "", "", result[9], bit48[1], bit48[7],
//                           bit48[25], bit48[4], bit48[5], bit48[8]+" / "+bit48[9] ,
//                           result[2].substring(4,result[2].length()), amt_admin, amt_met,
//                           amt_ppn, amt_ppj, amt_angsuran,
//                           amt_pwr, amt_kwh, result[13], "", bit48[2], bit48[1], result[5],detail[1],xml,hasil,bit62[2],
//                           result[6],result[7],result[10],result[4]);

                logger.log(Level.INFO, "Saved to Prepaid Trx");
            }
//            else {
//                xml = "<?xml version='1.0' encoding='UTF-8'?>"
//                         + "<response>"
//                         + "<stan>"+result[3]+"</stan>"
//                         + "<produk>PLNPREPAID</produk>"
//                         + "<code>"+result[9]+"</code>"
//                         + "<desc>"+preRC.getPurchaseMsgResponseName(result[9])+"</desc>"
//                         + "<saldo>"+(Double.parseDouble(detail[7])+harga)+"</saldo>"
//                         + "</response>";
//            }

        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return xml;
    }    public String processPurchase(String hasil,String detail[], double harga) {
        String xml = "";
        System.out.println("Entering purchase process");

        try {
            
            String msg_inbox = detail[0].toLowerCase();
            String req[] = msg_inbox.split("\\.");

            String[] result = pre.parsePurchaseMsgResponse(hasil,false);
            
            if (result[9].equalsIgnoreCase("0000")) {
                
                String[] bit48 = pre.parseBit48Purchase(result[11]);
                String[] bit62 = pre.parseBit62Purchase(result[12]);
                
                xml = "<?xml version='1.0' encoding='UTF-8'?>"+
                        "<response>"+
                         "<produk>PLNPREPAID</produk>";
                
                if (req.length>4) {
                    xml+= "<trx_id>"+req[4]+"</trx_id>";                                 
                }
                                 
                bit48[7] = bit48[7].replaceAll("<", ".");
                bit48[7] = bit48[7].replaceAll(">", ",");
                
                result[11] = result[11].replaceAll("<", ".");
                result[11] = result[11].replaceAll(">", ",");
                
                result[12] = result[12].replaceAll("<", ".");
                result[12] = result[12].replaceAll(">", ",");
                
                xml+=    "<harga>"+harga+"</harga>"+
                         "<saldo>"+detail[7]+"</saldo>"+
                         "<amount>"+result[2]+"</amount>"+
                         //"<stan>"+result[3]+"</stan>"+
                         "<stan>"+detail[3]+"</stan>"+
                         "<datetime>"+result[4]+"</datetime>"+
                         "<settlement>"+result[5]+"</settlement>"+
                         "<merchant_code>"+result[6]+"</merchant_code>"+
                         "<bank_code>"+result[7]+"</bank_code>"+
                         "<rc>"+result[9]+"</rc>"+
                         "<terminal_id>"+result[10]+"</terminal_id>"+
                         "<material_number>"+bit48[1]+"</material_number>"+
                         "<subscriber_id>"+bit48[2]+"</subscriber_id>"+
                         "<pln_refno>"+bit48[4]+"</pln_refno>"+
                         "<switcher_refno>"+bit48[5]+"</switcher_refno>"+
                         "<vending_refno>"+bit48[6]+"</vending_refno>"+
                         "<subscriber_name>"+bit48[7]+"</subscriber_name>"+                                          
                        "<subscriber_segmentation>"+bit48[8]+"</subscriber_segmentation>"+
                         "<power>"+bit48[9]+"</power>"+
                         "<minor_admin_charge>"+bit48[11]+"</minor_admin_charge>"+
                         "<admin_charge>"+bit48[12]+"</admin_charge>";

                if (result[9].equalsIgnoreCase("0000")) {
                    xml+=    "<minor_meterai>"+bit48[13]+"</minor_meterai>"+
                             "<meterai>"+bit48[14]+"</meterai>"+
                             "<buying_option>"+bit48[10]+"</buying_option>"+
                             "<minor_ppn>"+bit48[15]+"</minor_ppn>"+
                             "<ppn>"+bit48[16]+"</ppn>"+
                             "<minor_ppj>"+bit48[17]+"</minor_ppj>"+
                             "<ppj>"+bit48[18]+"</ppj>"+
                             "<minor_angsuran>"+bit48[19]+"</minor_angsuran>"+
                             "<angsuran>"+bit48[20]+"</angsuran>"+
                             "<minor_power_purchase>"+bit48[21]+"</minor_power_purchase>"+
                             "<power_purchase>"+bit48[22]+"</power_purchase>"+
                             "<minor_purchase_kwh>"+bit48[23]+"</minor_purchase_kwh>"+
                             "<jml_kwh>"+bit48[24]+"</jml_kwh>"+
                             "<token>"+bit48[25]+"</token>";
                }

                xml+=    "<distribution_code>"+bit62[0]+"</distribution_code>"+
                         "<service_unit>"+bit62[1]+"</service_unit>"+
                         "<service_unit_phone>"+bit62[2]+"</service_unit_phone>"+
                         "<max_kwh>"+bit62[3]+"</max_kwh>"+
                         "<total_repeat>"+bit62[4]+"</total_repeat>"+
                         "<bit48>"+result[11]+"</bit48>"+
                         "<bit62>"+result[12]+"</bit62>";

                if (Integer.parseInt(bit62[4])>0) {
                    xml+= "<power_purchase_unsold>"+bit62[5]+"</power_purchase_unsold>";
                }

                if (Integer.parseInt(bit62[4])>1) {
                    xml+= "<power_purchase_unsold2>"+bit62[6]+"</power_purchase_unsold2>";
                }

                xml+=    "<info_text>"+result[13]+"</info_text>"+                        
                        "</response>";


                String amt_meterai = bit48[14].substring(0,(bit48[14].length()-Integer.parseInt(bit48[13])));
                String amt_meterai2 = bit48[14].substring((bit48[14].length()-Integer.parseInt(bit48[13])),bit48[14].length());
                String amt_met = amt_meterai + "." + amt_meterai2;

                String amt_ppn1 = bit48[16].substring(0,(bit48[16].length()-Integer.parseInt(bit48[15])));
                String amt_ppn2 = bit48[16].substring((bit48[16].length()-Integer.parseInt(bit48[15])),bit48[16].length());
                String amt_ppn = amt_ppn1 + "." + amt_ppn2;

                String amt_ppj1 = bit48[18].substring(0,(bit48[18].length()-Integer.parseInt(bit48[17])));
                String amt_ppj2 = bit48[18].substring((bit48[18].length()-Integer.parseInt(bit48[17])),bit48[18].length());
                String amt_ppj = amt_ppj1 + "." + amt_ppj2;

                String amt_angsuran1 = bit48[20].substring(0,(bit48[20].length()-Integer.parseInt(bit48[19])));
                String amt_angsuran2 = bit48[20].substring((bit48[20].length()-Integer.parseInt(bit48[19])),bit48[20].length());
                String amt_angsuran = amt_angsuran1 + "." + amt_angsuran2;

                String amt_pwr1 = bit48[22].substring(0,(bit48[22].length()-Integer.parseInt(bit48[21])));
                String amt_pwr2 = bit48[22].substring((bit48[22].length()-Integer.parseInt(bit48[21])),bit48[22].length());
                String amt_pwr = amt_pwr1 + "." + amt_pwr2;

                String amt_kwh1 = bit48[24].substring(0,(bit48[24].length()-Integer.parseInt(bit48[23])));
                String amt_kwh2 = bit48[24].substring((bit48[24].length()-Integer.parseInt(bit48[23])),bit48[24].length());
                String amt_kwh = amt_kwh1 + "." + amt_kwh2;

                String amt_admin1 = bit48[12].substring(0,(bit48[12].length()-Integer.parseInt(bit48[11])));
                String amt_admin2 = bit48[12].substring((bit48[12].length()-Integer.parseInt(bit48[11])),bit48[12].length());
                String amt_admin = amt_admin1 + "." + amt_admin2;

                //simpan data payment ke database
//                long idx = insertToTrx(Integer.parseInt(detail[5]), 80, detail[4], detail[1],
//                                detail[6], Long.parseLong(result[2].substring(4)), (long) harga,
//                                Integer.parseInt(result[3]),
//                                result[9],"PLN-PREPAID",Long.parseLong("0"),Short.parseShort("0"),
//                                Short.parseShort("0"),bit48[5],detail[4],Short.parseShort("0"));
                
                 long idx = insertToTrx(Integer.parseInt(detail[5]), 80, detail[4], detail[1],
                                detail[6], Long.parseLong(result[2].substring(4)), (long) harga,
                                Integer.parseInt(detail[3]),
                                result[9],"PLN-PREPAID",Long.parseLong("0"),Short.parseShort("0"),
                                Short.parseShort("0"),bit48[5],detail[4],Short.parseShort("0"));

                insertToDB(Integer.parseInt(result[3]), 
                        String.valueOf(idx), "", "", result[9], bit48[1], bit48[7],
                           bit48[25], bit48[4], bit48[5], bit48[8]+" / "+bit48[9] ,
                           result[2].substring(4,result[2].length()), amt_admin, amt_met,
                           amt_ppn, amt_ppj, amt_angsuran,
                           amt_pwr, amt_kwh, result[13], "", bit48[2], bit48[1], result[5],detail[1],xml,hasil,bit62[2],
                           result[6],result[7],result[10],result[4]);

                logger.log(Level.INFO, "Saved to Prepaid Trx");
            }
            else {
                xml = "<?xml version='1.0' encoding='UTF-8'?>"
                         + "<response>"
                         + "<stan>"+result[3]+"</stan>"
                         + "<produk>PLNPREPAID</produk>"
                         + "<code>"+result[9]+"</code>"
                         + "<desc>"+preRC.getPurchaseMsgResponseName(result[9])+"</desc>"
                         + "<saldo>"+(Double.parseDouble(detail[7])+harga)+"</saldo>"
                         + "</response>";
            }

        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return xml;
    }

    public long insertToTrx(int user_id, int product_id, String destination, String sender,
            String sender_type, long price_sell,long price_buy, int inbox_id,
            String status,String remark,long saldo_awal,short counter,
            short counter2,String sn,String receiver,short resend) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();
        long idx = 0;

        try {
            Transactions trx = new Transactions();
            em.getTransaction().begin();

            trx.setUserId(user_id);
            trx.setProductId(product_id);
            trx.setDestination(destination);
            trx.setSender(sender);
            trx.setSenderType(sender_type);
            trx.setPriceSell(price_sell);
            trx.setPriceBuy(price_buy);
            trx.setInboxId(inbox_id);
            trx.setStatus(status);
            trx.setRemark(remark);
            trx.setSaldoAwal(saldo_awal);
            trx.setCounter(counter);
            trx.setCounter2(counter2);
            trx.setSn(sn);
            trx.setReceiver(receiver);
            trx.setResend(resend);
            trx.setCreateDate(new Date());

            em.persist(trx);
            em.flush();
            idx=trx.getId();

            em.getTransaction().commit();

            em.close();
            factory.close();

        }
        catch(Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
            
            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
            
            idx = insertToTrx(user_id, product_id, destination, sender,sender_type, price_sell,price_buy, inbox_id,status,remark,saldo_awal,counter,counter2,sn,receiver,resend);
        }
        finally {
            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        }

        return idx;
    }

    public void insertToDB(int inbox_id, String transid, String partnertid,String restype,
            String rescode, String idpel, String nama,String sntoken,String plnref,
            String refnbr,String kwh, String tagihan, String admin_charge, String meterai, String ppn,
            String ppj,String angsuran,String rptoken,String jmkwh,String info,String saldo, String snidpel,
            String snmeter,String settlement, String destination,String xml,
            String iso, String telpon, String merchant_cat_code, String bank_code, String terminal_id,
            String dt_trx) {

            EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
            EntityManager em = factory.createEntityManager();

            try {
                PlnPrepaids prepaid = new PlnPrepaids();
                em.getTransaction().begin();

                prepaid.setInboxId(inbox_id);
                prepaid.setTransid(transid);
                prepaid.setPartnertid(partnertid);
                prepaid.setRestype(restype);
                prepaid.setRescode(rescode);
                prepaid.setIdpel(idpel);
                prepaid.setNama(nama);
                prepaid.setSntoken(sntoken);
                prepaid.setPlnref(plnref);
                prepaid.setRefnbr(refnbr);
                prepaid.setKwh(kwh);
                prepaid.setTagihan(tagihan);
                prepaid.setAdmin(admin_charge);
                prepaid.setMeterai(meterai);
                prepaid.setPpj(ppj);
                prepaid.setPpn(ppn);
                prepaid.setAngsuran(angsuran);
                prepaid.setRptoken(rptoken);
                prepaid.setJmkwh(jmkwh);
                prepaid.setInfo(info);
                prepaid.setSaldo(saldo);
                prepaid.setSnidpel(snidpel);
                prepaid.setSnmeter(snmeter);
                prepaid.setSettlement(settlement);
                prepaid.setXml(xml);
                prepaid.setCreateDate(new Date());
                prepaid.setDestination(destination);                
                prepaid.setIso(iso);

                prepaid.setTelpon(telpon);
                prepaid.setMerchantCatCode(merchant_cat_code);
                prepaid.setBankCode(bank_code);
                prepaid.setTerminalId(terminal_id);
                prepaid.setDtTrx(dt_trx);
                prepaid.setReprint(0);

                em.persist(prepaid);
                em.getTransaction().commit();
            
                em.close();
                factory.close();

            }
            catch(Exception e) {
                e.printStackTrace();
                logger.log(Level.FATAL, e.getMessage());
                
                if (em.isOpen()) {
                    em.close();
                }

                if (factory.isOpen()) {
                    factory.close();
                }
                
                insertToDB( inbox_id,  transid,  partnertid, restype,
                        rescode,  idpel,  nama, sntoken, plnref,
                        refnbr, kwh,  tagihan,  admin_charge,  meterai,  ppn,
                        ppj, angsuran, rptoken, jmkwh, info, saldo,  snidpel,
                        snmeter, settlement,  destination, xml,
                        iso,  telpon,  merchant_cat_code,  bank_code,  terminal_id,
                        dt_trx);
            }
            finally {
                if (em.isOpen()) {
                    em.close();
                }

                if (factory.isOpen()) {
                    factory.close();
                }
            }
        
    }

    public String processInquiry(String hasil) {
        String[] result = pre.parseInquiryMsgResponse(hasil,false);

        if (result[7].equalsIgnoreCase("0000")) {
            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"         
                    + "<produk>PLNPREPAID</produk>"
                    + "<stan>"+result[2]+"</stan>"
                    + "<datetime>"+result[3]+"</datetime>"
                    + "<merchant_code>"+result[4]+"</merchant_code>"
                    + "<bank_code>"+result[5]+"</bank_code>"
                    + "<rc>"+result[7]+"</rc>"
                    + "<terminal_id>"+result[8]+"</terminal_id>";

            if (result[7].equalsIgnoreCase("0000")) {

                String bit48[] = pre.parseBit48Inquiry(result[9], result[7]);

                String switcher_id = bit48[0]; //result[9].substring(0, 6);
                String material_number = bit48[1]; // result[9].substring(7, 17);
                String subscriber_id = bit48[2]; //result[9].substring(18, 29);
                String flag = bit48[3]; //result[9].substring(30, 30);
                String plnref = bit48[4]; //result[9].substring(31,62);
                String switcher_refno = bit48[5]; //result[9].substring(63, 94);
                String subscriber_name = bit48[6]; //result[9].substring(95, 129);
                String subscriber_segmentation = bit48[7]; //result[9].substring(130, 133);
                String power_category = bit48[8]; //result[9].substring(134, 143);
                String admin_charge_unit = bit48[9]; //result[9].substring(144, 144);
                String admin_charge = bit48[10]; //result[9].substring(145, 153);

                String bit62[] = pre.parseBit62Inquiry(result[10], result[7] );

                String distribution_code = bit62[0];
                String service_unit = bit62[1];
                String service_phone_unit = bit62[2];
                String max_kwh_unit = bit62[3];
                
                String total_repeat = bit62[4];
                String power_purchase_unsold = bit62[5];
                String power_purchase_unsold2 = bit62[6];

                //try {power_purchase_unsold = bit62[5];} catch(Exception e) {System.out.println("pp unsold : "+e.getMessage());}

                subscriber_name = subscriber_name.replaceAll("<", ".");
                subscriber_name = subscriber_name.replaceAll(">", ",");
                
                hasil  += "<material_number>"+material_number+"</material_number>"
                        + "<subscriber_id>"+subscriber_id+"</subscriber_id>"
                        + "<pln_refno>"+plnref+"</pln_refno>"
                        + "<switcher_refno>"+switcher_refno+"</switcher_refno>"
                        + "<subscriber_name>"+subscriber_name+"</subscriber_name>"
                        + "<subscriber_segmentation>"+subscriber_segmentation+"</subscriber_segmentation>"
                        + "<power>"+power_category+"</power>"                        
                        + "<admin_charge>"+admin_charge+"</admin_charge>"
                        + "<minor_admin_charge>"+admin_charge_unit+"</minor_admin_charge>"
                        + "<distribution_code>"+distribution_code+"</distribution_code>"
                        + "<service_unit>"+service_unit+"</service_unit>"
                        + "<service_unit_phone>"+service_phone_unit+"</service_unit_phone>"
                        + "<max_kwh_unit>"+max_kwh_unit+"</max_kwh_unit>"
                        + "<total_repeat>"+total_repeat+"</total_repeat>";

                if (Integer.parseInt(total_repeat)>0)
                    hasil += "<power_purchase_unsold>"+power_purchase_unsold+"</power_purchase_unsold>";

                if (Integer.parseInt(total_repeat)>1)
                        hasil += "<power_purchase_unsold2>"+power_purchase_unsold2+"</power_purchase_unsold2>";

            }
            else {

                String switcher_id = result[9].substring(0, 6);
                String material_number = result[9].substring(7, 17);
                String subscriber_id = result[9].substring(18, 29);
                String flag = result[9].substring(30, 30);
                String plnref = result[9].substring(31,62);
                String switcher_refno = result[9].substring(63, 94);

                String total_repeat = result[10].substring(27, 27);

                hasil  += "<material_number>"+material_number+"</material_number>"
                        + "<subscriber_id>"+subscriber_id+"</subscriber_id>"
                        + "<pln_refno>"+plnref+"</pln_refno>"
                        + "<switcher_refno>"+switcher_refno+"</switcher_refno>"
                        + "<total_repeat>"+total_repeat+"</total_repeat>";
            }

            hasil   += "</response>";
        }
        else {
            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                         + "<response>"
                         + "<stan>"+result[2]+"</stan>"
                         + "<saldo>"+0+"</saldo>"
                         + "<produk>PLNPREPAID</produk>"
                         + "<code>"+result[7]+"</code>"
                         + "<desc>"+preRC.getInquiryMsgResponseName(result[7])+"</desc>"
                         + "</response>";
        }

        return hasil;
    }
    
    public String processInquiry(String hasil ,String detail[]) {
        String[] result = pre.parseInquiryMsgResponse(hasil,false);

        if (result[7].equalsIgnoreCase("0000")) {
            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"         
                    + "<produk>PLNPREPAID</produk>"
                    + "<stan>"+result[2]+"</stan>"
                    + "<datetime>"+result[3]+"</datetime>"
                    + "<merchant_code>"+result[4]+"</merchant_code>"
                    + "<bank_code>"+result[5]+"</bank_code>"
                    + "<rc>"+result[7]+"</rc>"
                    + "<terminal_id>"+result[8]+"</terminal_id>";

            if (result[7].equalsIgnoreCase("0000")) {

                String bit48[] = pre.parseBit48Inquiry(result[9], result[7]);

                String switcher_id = bit48[0]; //result[9].substring(0, 6);
                String material_number = bit48[1]; // result[9].substring(7, 17);
                String subscriber_id = bit48[2]; //result[9].substring(18, 29);
                String flag = bit48[3]; //result[9].substring(30, 30);
                String plnref = bit48[4]; //result[9].substring(31,62);
                String switcher_refno = bit48[5]; //result[9].substring(63, 94);
                String subscriber_name = bit48[6]; //result[9].substring(95, 129);
                String subscriber_segmentation = bit48[7]; //result[9].substring(130, 133);
                String power_category = bit48[8]; //result[9].substring(134, 143);
                String admin_charge_unit = bit48[9]; //result[9].substring(144, 144);
                String admin_charge = bit48[10]; //result[9].substring(145, 153);

                String bit62[] = pre.parseBit62Inquiry(result[10], result[7] );

                String distribution_code = bit62[0];
                String service_unit = bit62[1];
                String service_phone_unit = bit62[2];
                String max_kwh_unit = bit62[3];
                
                String total_repeat = bit62[4];
                String power_purchase_unsold = bit62[5];
                String power_purchase_unsold2 = bit62[6];

                //try {power_purchase_unsold = bit62[5];} catch(Exception e) {System.out.println("pp unsold : "+e.getMessage());}

                subscriber_name = subscriber_name.replaceAll("<", ".");
                subscriber_name = subscriber_name.replaceAll(">", ",");
                
                hasil  += "<material_number>"+material_number+"</material_number>"
                        + "<subscriber_id>"+subscriber_id+"</subscriber_id>"
                        + "<pln_refno>"+plnref+"</pln_refno>"
                        + "<switcher_refno>"+switcher_refno+"</switcher_refno>"
                        + "<subscriber_name>"+subscriber_name+"</subscriber_name>"
                        + "<subscriber_segmentation>"+subscriber_segmentation+"</subscriber_segmentation>"
                        + "<power>"+power_category+"</power>"                        
                        + "<admin_charge>"+admin_charge+"</admin_charge>"
                        + "<minor_admin_charge>"+admin_charge_unit+"</minor_admin_charge>"
                        + "<distribution_code>"+distribution_code+"</distribution_code>"
                        + "<service_unit>"+service_unit+"</service_unit>"
                        + "<service_unit_phone>"+service_phone_unit+"</service_unit_phone>"
                        + "<max_kwh_unit>"+max_kwh_unit+"</max_kwh_unit>"
                        + "<total_repeat>"+total_repeat+"</total_repeat>";

                if (Integer.parseInt(total_repeat)>0)
                    hasil += "<power_purchase_unsold>"+power_purchase_unsold+"</power_purchase_unsold>";

                if (Integer.parseInt(total_repeat)>1)
                        hasil += "<power_purchase_unsold2>"+power_purchase_unsold2+"</power_purchase_unsold2>";

            }
            else {

                String switcher_id = result[9].substring(0, 6);
                String material_number = result[9].substring(7, 17);
                String subscriber_id = result[9].substring(18, 29);
                String flag = result[9].substring(30, 30);
                String plnref = result[9].substring(31,62);
                String switcher_refno = result[9].substring(63, 94);

                String total_repeat = result[10].substring(27, 27);

                hasil  += "<material_number>"+material_number+"</material_number>"
                        + "<subscriber_id>"+subscriber_id+"</subscriber_id>"
                        + "<pln_refno>"+plnref+"</pln_refno>"
                        + "<switcher_refno>"+switcher_refno+"</switcher_refno>"
                        + "<total_repeat>"+total_repeat+"</total_repeat>";
            }

            hasil   += "</response>";
        }
        else {
            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                         + "<response>"
                         + "<stan>"+result[2]+"</stan>"
                         + "<saldo>"+detail[7]+"</saldo>"
                         + "<produk>PLNPREPAID</produk>"
                         + "<code>"+result[7]+"</code>"
                         + "<desc>"+preRC.getInquiryMsgResponseName(result[7])+"</desc>"
                         + "</response>";
        }

        return hasil;
    }

}
