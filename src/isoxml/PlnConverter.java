
package isoxml;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
//import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.Persistence;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
//import model.Inboxes;
//import model.Users;
//import org.apache.log4j.Level;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author root
 */
public class PlnConverter {
    
    PlnConverter() {
        
    }
    
    protected String convertPrepaidInq(String xml, String idp) {
        
        xml = xml.replaceAll("&", "dan");
        
        System.out.println("xml : "+xml);
        
        /*
         [2012-11-13 14:34:34] BERHASIL. IDPEL: 14024623671 
         Nama: INDAH SUSILOWATI NINGSIH SNToken: 17262268486008061820 
         JmKWH: 27.1 Nominal: 20000 GOL: R1/900 Adm: 2000 Mtr: 0 
         PPJ: 1636.36 
         PPN: 0 
         Angs: 0 
         RpTkn: 16363.64 
         Saldo: 14869089
         */
        
        String hasil = "";
        
        ///////get required field from xml///////////
        //idpel
        String idpel = "";
        String nometer = "";
        //nama
        String nama = "";
        //sntoken
        String sntoken = "";
        //jmkwh
        String jmkwh = "";
        //nominal
        String nominal = "";
        //gol
        String gol = "";
        //adm
        String adm = "";
        //meterai
        String meterai = "";
        //ppj
        String ppj = "";
        //ppn
        String ppn = "";
        //angsuran
        String angsuran = "";
        //rptkn
        String rpToken = "";
        //saldo
        String saldo = "";
        
        String code = "";

        Calendar currentDate = Calendar.getInstance();
        
        String tgl = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        
        try {
                        
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc;
            
            InputStream oi = new ByteArrayInputStream(xml.toString().getBytes());
            doc = docBuilder.parse(oi);
            doc.getDocumentElement().normalize();
            
            try {
                NodeList nodeCode = doc.getElementsByTagName("code");
                Element eCode = (Element) nodeCode.item(0);
                NodeList codex = eCode.getChildNodes();
                code = ((Node) codex.item(0)).getNodeValue().trim();
            }
            catch(Exception e) {
                System.out.println("No code : "+e.getMessage());
                
                NodeList nodeCode = doc.getElementsByTagName("rc");
                Element eCode = (Element) nodeCode.item(0);
                NodeList codex = eCode.getChildNodes();
                code = ((Node) codex.item(0)).getNodeValue().trim();
            }
            
            if (code.equalsIgnoreCase("400")) {
                hasil = "GAGAL "+idp+" - Saldo Kurang";
            }
            else if (code.equalsIgnoreCase("907")) {
                hasil = "GAGAL "+idp+" - Format Salah";
            }
            else if (code.equalsIgnoreCase("0005")) {
                hasil = "GAGAL "+idp+" - Other error from PLN";
            }
            else if (code.equalsIgnoreCase("0009")) {
                hasil = "GAGAL "+idp+" - Transaksi ganda, silahkan menunggu 2 menit untuk transaksi kembali";
            }
            else if (code.equalsIgnoreCase("0014")) {
                hasil = "GAGAL "+idp+" - ID PEL salah";
            }
            else if (code.equalsIgnoreCase("0090")) {
                hasil = "GAGAL "+idp+" - Dalam proses Cut Off PLN";
            }
            else if (code.equalsIgnoreCase("0015")) {
                hasil = "GAGAL "+idp+" - ID PEL salah";
            }
            else if (code.equalsIgnoreCase("0047")) {
                hasil = "GAGAL "+idp+" - Total KWH telah melebihi batas";
            }
            else if (code.equalsIgnoreCase("0068")) {
                hasil = "GAGAL "+idp+" - Waktu transaksi habis, silahkan ulangi beberapa saat lagi";
            }
            else if (code.equalsIgnoreCase("0077")) {
                hasil = "GAGAL "+idp+" - Subscriber suspended";
            }            
            else {
                

                NodeList nodemno = doc.getElementsByTagName("material_number");
                Element emno = (Element) nodemno.item(0);
                NodeList mnox = emno.getChildNodes();
                nometer = ((Node) mnox.item(0)).getNodeValue().trim();
                
                NodeList nodeIdpel = doc.getElementsByTagName("subscriber_id");
                Element eIdpel = (Element) nodeIdpel.item(0);
                NodeList idpelx = eIdpel.getChildNodes();
                idpel = ((Node) idpelx.item(0)).getNodeValue().trim();
                
                NodeList nodeNometer = doc.getElementsByTagName("material_number");
                Element eNometer = (Element) nodeNometer.item(0);
                NodeList nometerx = eNometer.getChildNodes();
                nometer = ((Node) nometerx.item(0)).getNodeValue().trim();

                if (idpel.length()<1) {
                    idpel = nometer;
                }

                NodeList nodeNama = doc.getElementsByTagName("subscriber_name");
                Element eNama = (Element) nodeNama.item(0);
                NodeList namax = eNama.getChildNodes();
                nama = ((Node) namax.item(0)).getNodeValue().trim();

                

                NodeList nodeSegmentasi = doc.getElementsByTagName("subscriber_segmentation");
                Element eSegmentasi = (Element) nodeSegmentasi.item(0);
                NodeList Segmentasix = eSegmentasi.getChildNodes();

                NodeList nodeDaya = doc.getElementsByTagName("power");
                Element eDaya = (Element) nodeDaya.item(0);
                NodeList Dayax = eDaya.getChildNodes();

                gol = ((Node) Segmentasix.item(0)).getNodeValue().trim() + "/" + Integer.parseInt(((Node) Dayax.item(0)).getNodeValue().trim());
                
                hasil = "["+tgl+"] BERHASIL. IDPEL: "+idp+" Nama: "+ nama +                             
                            " GOL: "+gol;
            }
            
        }
        catch(Exception e) {
            e.printStackTrace();
            hasil = "GAGAL "+idp+" - System Error";
        }
        
        return hasil;
    }
    
    protected String convertPln(String xml) {
        String hasil = "";
        
        
        
        return hasil;
    }
    
    protected String convertNontaglis(String xml) {
        String hasil = "";
        
        
        
        return hasil;
    }
    
    protected String convertPostpaid(String xml) {
        
        String hasil = "";
        
        
        
        return hasil;
    }
    
    protected String convertPrepaid(String xml) {
        
        xml = xml.replaceAll("&", "dan");
        
        /*
         [2012-11-13 14:34:34] BERHASIL. IDPEL: 14024623671 
         Nama: INDAH SUSILOWATI NINGSIH SNToken: 17262268486008061820 
         JmKWH: 27.1 Nominal: 20000 GOL: R1/900 Adm: 2000 Mtr: 0 
         PPJ: 1636.36 
         PPN: 0 
         Angs: 0 
         RpTkn: 16363.64 
         Saldo: 14869089
         */
        
        String hasil = "";
        
        ///////get required field from xml///////////
        //idpel
        String idpel = "";
        String nometer = "";
        //nama
        String nama = "";
        //sntoken
        String sntoken = "";
        //jmkwh
        String jmkwh = "";
        //nominal
        String nominal = "";
        //gol
        String gol = "";
        //adm
        String adm = "";
        //meterai
        String meterai = "";
        //ppj
        String ppj = "";
        //ppn
        String ppn = "";
        //angsuran
        String angsuran = "";
        //rptkn
        String rpToken = "";
        //saldo
        String saldo = "";
        
        String code = "";

        Calendar currentDate = Calendar.getInstance();
        
        String tgl = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        
        try {
                        
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc;
            
            InputStream oi = new ByteArrayInputStream(xml.toString().getBytes());
            doc = docBuilder.parse(oi);
            doc.getDocumentElement().normalize();
            
            try {
                NodeList nodeCode = doc.getElementsByTagName("code");
                Element eCode = (Element) nodeCode.item(0);
                NodeList codex = eCode.getChildNodes();
                code = ((Node) codex.item(0)).getNodeValue().trim();
            }
            catch(Exception e) {
                System.out.println("No code : "+e.getMessage());
            }
            
            if (code.equalsIgnoreCase("400")) {
                hasil = "Saldo Kurang";
            }
            else if (code.equalsIgnoreCase("907")) {
                hasil = "Format Salah";
            }
            else if (code.equalsIgnoreCase("0014")) {
                hasil = "ID PEL salah";
            }
            else if (code.equalsIgnoreCase("0015")) {
                hasil = "ID PEL salah";
            }
            else if (code.equalsIgnoreCase("0047")) {
                hasil = "Total KWH telah melebihi batas";
            }
            else if (code.equalsIgnoreCase("0068")) {
                hasil = "Waktu transaksi habis, silahkan ulangi beberapa saat lagi";
            }
            else if (code.equalsIgnoreCase("0090")) {
                hasil = "Sedang dalam proses cut-off PLN, silahkan coba beberapa saat lagi";
            }
            else {
                NodeList nodeToken = doc.getElementsByTagName("token");
                Element eToken = (Element) nodeToken.item(0);
                NodeList tokenx = eToken.getChildNodes();
                sntoken = ((Node) tokenx.item(0)).getNodeValue().trim();

                NodeList nodemno = doc.getElementsByTagName("material_number");
                Element emno = (Element) nodemno.item(0);
                NodeList mnox = emno.getChildNodes();
                nometer = ((Node) mnox.item(0)).getNodeValue().trim();
                
                NodeList nodeIdpel = doc.getElementsByTagName("subscriber_id");
                Element eIdpel = (Element) nodeIdpel.item(0);
                NodeList idpelx = eIdpel.getChildNodes();
                idpel = ((Node) idpelx.item(0)).getNodeValue().trim();
                
                NodeList nodeNometer = doc.getElementsByTagName("material_number");
                Element eNometer = (Element) nodeNometer.item(0);
                NodeList nometerx = eNometer.getChildNodes();
                nometer = ((Node) nometerx.item(0)).getNodeValue().trim();

                if (idpel.length()<1) {
                    idpel = nometer;
                }

                NodeList nodeNama = doc.getElementsByTagName("subscriber_name");
                Element eNama = (Element) nodeNama.item(0);
                NodeList namax = eNama.getChildNodes();
                nama = ((Node) namax.item(0)).getNodeValue().trim();

                NodeList nodeMinorKwh = doc.getElementsByTagName("minor_purchase_kwh");
                Element eMinorKwh = (Element) nodeMinorKwh.item(0);
                NodeList MinorKwhx = eMinorKwh.getChildNodes();

                NodeList nodeJmkwh = doc.getElementsByTagName("jml_kwh");
                Element eJmkwh = (Element) nodeJmkwh.item(0);
                NodeList Jmkwhx = eJmkwh.getChildNodes();
                //jmkwh = ((Node) Jmkwhx.item(0)).getNodeValue().trim();

                String kwh_all = ((Node) Jmkwhx.item(0)).getNodeValue().trim();
                int kwh_minor = Integer.parseInt(((Node) MinorKwhx.item(0)).getNodeValue().trim()); 
                jmkwh = kwh_all.substring(0, kwh_all.length()-kwh_minor)+"."+kwh_all.substring(kwh_all.length()-kwh_minor); 
                //System.out.println("kwh "+jmkwh);

                NodeList nodeNominal = doc.getElementsByTagName("amount");
                Element eNominal = (Element) nodeNominal.item(0);
                NodeList Nominalx = eNominal.getChildNodes();
                nominal = ((Node) Nominalx.item(0)).getNodeValue().trim();

                NodeList nodeSegmentasi = doc.getElementsByTagName("subscriber_segmentation");
                Element eSegmentasi = (Element) nodeSegmentasi.item(0);
                NodeList Segmentasix = eSegmentasi.getChildNodes();

                NodeList nodeDaya = doc.getElementsByTagName("power");
                Element eDaya = (Element) nodeDaya.item(0);
                NodeList Dayax = eDaya.getChildNodes();

                gol = ((Node) Segmentasix.item(0)).getNodeValue().trim() + "/" + Integer.parseInt(((Node) Dayax.item(0)).getNodeValue().trim());

                NodeList nodeAdmin = doc.getElementsByTagName("admin_charge");
                Element eAdmin = (Element) nodeAdmin.item(0);
                NodeList Adminx = eAdmin.getChildNodes();

                NodeList nodeMinorAdmin = doc.getElementsByTagName("minor_admin_charge");
                Element eMinorAdmin = (Element) nodeMinorAdmin.item(0);
                NodeList MinorAdminx = eMinorAdmin.getChildNodes();

                String adm_all = ((Node) Adminx.item(0)).getNodeValue().trim();
                int adm_minor = Integer.parseInt(((Node) MinorAdminx.item(0)).getNodeValue().trim());
                adm = adm_all.substring(0, adm_all.length()-adm_minor);//+"."+adm_all.substring(adm_all.length()-adm_minor);

                NodeList nodePpn = doc.getElementsByTagName("ppn");
                Element ePpn = (Element) nodePpn.item(0);
                NodeList Ppnx = ePpn.getChildNodes();

                NodeList nodeMinorPpn = doc.getElementsByTagName("minor_ppn");
                Element eMinorPpn = (Element) nodeMinorPpn.item(0);
                NodeList MinorPpnx = eMinorPpn.getChildNodes();

                String ppn_all = ((Node) Ppnx.item(0)).getNodeValue().trim();
                int ppn_minor = Integer.parseInt(((Node) MinorPpnx.item(0)).getNodeValue().trim());
                ppn = ppn_all.substring(0, ppn_all.length()-ppn_minor);//+"."+ppn_all.substring(ppn_all.length()-ppn_minor);;

                NodeList nodePpj = doc.getElementsByTagName("ppj");
                Element ePpj = (Element) nodePpj.item(0);
                NodeList Ppjx = ePpj.getChildNodes();

                NodeList nodeMinorPpj = doc.getElementsByTagName("minor_ppj");
                Element eMinorPpj = (Element) nodeMinorPpj.item(0);
                NodeList MinorPpjx = eMinorPpj.getChildNodes();

                String ppj_all = ((Node) Ppjx.item(0)).getNodeValue().trim();
                int ppj_minor = Integer.parseInt(((Node) MinorPpjx.item(0)).getNodeValue().trim());
                ppj = ppj_all.substring(0, ppj_all.length()-ppj_minor);//+"."+ppj_all.substring(ppj_all.length()-ppj_minor);

                NodeList nodeMeterai = doc.getElementsByTagName("meterai");
                Element eMeterai = (Element) nodeMeterai.item(0);
                NodeList Meteraix = eMeterai.getChildNodes();

                NodeList nodeMinorMeterai = doc.getElementsByTagName("minor_meterai");
                Element eMinorMeterai = (Element) nodeMinorMeterai.item(0);
                NodeList MinorMeteraix = eMinorMeterai.getChildNodes();

                String meterai_all = ((Node) Meteraix.item(0)).getNodeValue().trim();
                int meterai_minor = Integer.parseInt(((Node) MinorMeteraix.item(0)).getNodeValue().trim());
                meterai = meterai_all.substring(0, meterai_all.length()-meterai_minor);//+"."+meterai_all.substring(meterai_all.length()-meterai_minor);

                NodeList nodeAngsuran = doc.getElementsByTagName("angsuran");
                Element eAngsuran = (Element) nodeAngsuran.item(0);
                NodeList Angsuranx = eAngsuran.getChildNodes();

                NodeList nodeMinorAngsuran = doc.getElementsByTagName("minor_angsuran");
                Element eMinorAngsuran = (Element) nodeMinorAngsuran.item(0);
                NodeList MinorAngsuranx = eMinorAngsuran.getChildNodes();

                String angsuran_all = ((Node) Angsuranx.item(0)).getNodeValue().trim();
                int angsuran_minor = Integer.parseInt(((Node) MinorAngsuranx.item(0)).getNodeValue().trim());
                angsuran = angsuran_all.substring(0, angsuran_all.length()-angsuran_minor);//+"."+angsuran_all.substring(angsuran_all.length()-angsuran_minor);

                NodeList nodePowerPurchase = doc.getElementsByTagName("power_purchase");
                Element ePowerPurchase = (Element) nodePowerPurchase.item(0);
                NodeList PowerPurchasex = ePowerPurchase.getChildNodes();

                NodeList nodeMinorPowerPurchase = doc.getElementsByTagName("minor_power_purchase");
                Element eMinorPowerPurchase = (Element) nodeMinorPowerPurchase.item(0);
                NodeList MinorPowerPurchasex = eMinorPowerPurchase.getChildNodes();

                String rpToken_all = ((Node) PowerPurchasex.item(0)).getNodeValue().trim();
                int rpToken_minor = Integer.parseInt(((Node) MinorPowerPurchasex.item(0)).getNodeValue().trim());
                rpToken = rpToken_all.substring(0, rpToken_all.length()-rpToken_minor);//+"."+rpToken_all.substring(rpToken_all.length()-rpToken_minor);
                
                try {
                NodeList nodeSaldo = doc.getElementsByTagName("saldo");
                Element eSaldo = (Element) nodeSaldo.item(0);
                NodeList Saldox = eSaldo.getChildNodes();
                saldo = ((Node) Saldox.item(0)).getNodeValue().trim();
                }catch(Exception e) {
                    System.out.println(e.getMessage());
                }
                
                //////////format to efast mode/////////////
                hasil = "["+tgl+"] BERHASIL. IDPEL: "+idpel+" NOMETER: "+ nometer +" Nama: "+ nama + 
                        " SNToken: "+sntoken+" JmKWH: "+Double.parseDouble(jmkwh)+
                        " Nominal: "+Integer.parseInt(nominal.substring(4, nominal.length())) +" GOL: "+gol+
                        " Adm: "+Integer.parseInt(adm)+" Mtr: "+Integer.parseInt(meterai)+
                        " PPJ: "+Integer.parseInt(ppj)+" PPN: "+Integer.parseInt(ppn)+
                        " Angs: "+ Integer.parseInt(angsuran) +" RpTkn: "+Integer.parseInt(rpToken)+" Saldo: "+saldo;
            }
            
        }
        catch(Exception e) {
            e.printStackTrace();
            hasil = "System error";
        }
        
        return hasil;
    }
    
    protected String convertPrepaid(String xml, String idp) {
        
        xml = xml.replaceAll("&", "dan");
        
        System.out.println("xml : "+xml);
        
        /*
         [2012-11-13 14:34:34] BERHASIL. IDPEL: 14024623671 
         Nama: INDAH SUSILOWATI NINGSIH SNToken: 17262268486008061820 
         JmKWH: 27.1 Nominal: 20000 GOL: R1/900 Adm: 2000 Mtr: 0 
         PPJ: 1636.36 
         PPN: 0 
         Angs: 0 
         RpTkn: 16363.64 
         Saldo: 14869089
         */
        
        String hasil = "";
        
        ///////get required field from xml///////////
        //idpel
        String idpel = "";
        String nometer = "";
        //nama
        String nama = "";
        //sntoken
        String sntoken = "";
        //jmkwh
        String jmkwh = "";
        //nominal
        String nominal = "";
        //gol
        String gol = "";
        //adm
        String adm = "";
        //meterai
        String meterai = "";
        //ppj
        String ppj = "";
        //ppn
        String ppn = "";
        //angsuran
        String angsuran = "";
        //rptkn
        String rpToken = "";
        //saldo
        String saldo = "";
        
        String code = "";

        Calendar currentDate = Calendar.getInstance();
        
        String tgl = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        
        try {
                        
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc;
            
            InputStream oi = new ByteArrayInputStream(xml.toString().getBytes());
            doc = docBuilder.parse(oi);
            doc.getDocumentElement().normalize();
            
            try {
                NodeList nodeCode = doc.getElementsByTagName("code");
                Element eCode = (Element) nodeCode.item(0);
                NodeList codex = eCode.getChildNodes();
                code = ((Node) codex.item(0)).getNodeValue().trim();
            }
            catch(Exception e) {
                System.out.println("No code : "+e.getMessage());
                
                NodeList nodeCode = doc.getElementsByTagName("rc");
                Element eCode = (Element) nodeCode.item(0);
                NodeList codex = eCode.getChildNodes();
                code = ((Node) codex.item(0)).getNodeValue().trim();
            }
            
            if (code.equalsIgnoreCase("400")) {
                hasil = "GAGAL "+idp+" - Saldo Kurang";
            }
            else if (code.equalsIgnoreCase("907")) {
                hasil = "GAGAL "+idp+" - Format Salah";
            }
            else if (code.equalsIgnoreCase("0005")) {
                hasil = "GAGAL "+idp+" - Other error from PLN";
            }
            else if (code.equalsIgnoreCase("0009")) {
                hasil = "GAGAL "+idp+" - Transaksi ganda, silahkan menunggu 2 menit untuk transaksi kembali";
            }
            else if (code.equalsIgnoreCase("0014")) {
                hasil = "GAGAL "+idp+" - ID PEL salah";
            }
            else if (code.equalsIgnoreCase("0090")) {
                hasil = "GAGAL "+idp+" - Dalam proses Cut Off PLN";
            }
            else if (code.equalsIgnoreCase("0015")) {
                hasil = "GAGAL "+idp+" - ID PEL salah";
            }
            else if (code.equalsIgnoreCase("0047")) {
                hasil = "GAGAL "+idp+" - Total KWH telah melebihi batas";
            }
            else if (code.equalsIgnoreCase("0068")) {
                hasil = "GAGAL "+idp+" - Waktu transaksi habis, silahkan ulangi beberapa saat lagi";
            }            
            else if (code.equalsIgnoreCase("0077")) {
                hasil = "GAGAL "+idp+" - Subscriber suspended";
            }            
            else {
                NodeList nodeToken = doc.getElementsByTagName("token");
                Element eToken = (Element) nodeToken.item(0);
                NodeList tokenx = eToken.getChildNodes();
                sntoken = ((Node) tokenx.item(0)).getNodeValue().trim();

                NodeList nodemno = doc.getElementsByTagName("material_number");
                Element emno = (Element) nodemno.item(0);
                NodeList mnox = emno.getChildNodes();
                nometer = ((Node) mnox.item(0)).getNodeValue().trim();
                
                NodeList nodeIdpel = doc.getElementsByTagName("subscriber_id");
                Element eIdpel = (Element) nodeIdpel.item(0);
                NodeList idpelx = eIdpel.getChildNodes();
                idpel = ((Node) idpelx.item(0)).getNodeValue().trim();
                
                NodeList nodeNometer = doc.getElementsByTagName("material_number");
                Element eNometer = (Element) nodeNometer.item(0);
                NodeList nometerx = eNometer.getChildNodes();
                nometer = ((Node) nometerx.item(0)).getNodeValue().trim();

                if (idpel.length()<1) {
                    idpel = nometer;
                }

                NodeList nodeNama = doc.getElementsByTagName("subscriber_name");
                Element eNama = (Element) nodeNama.item(0);
                NodeList namax = eNama.getChildNodes();
                nama = ((Node) namax.item(0)).getNodeValue().trim();

                NodeList nodeMinorKwh = doc.getElementsByTagName("minor_purchase_kwh");
                Element eMinorKwh = (Element) nodeMinorKwh.item(0);
                NodeList MinorKwhx = eMinorKwh.getChildNodes();

                NodeList nodeJmkwh = doc.getElementsByTagName("jml_kwh");
                Element eJmkwh = (Element) nodeJmkwh.item(0);
                NodeList Jmkwhx = eJmkwh.getChildNodes();
                //jmkwh = ((Node) Jmkwhx.item(0)).getNodeValue().trim();

                String kwh_all = ((Node) Jmkwhx.item(0)).getNodeValue().trim();
                int kwh_minor = Integer.parseInt(((Node) MinorKwhx.item(0)).getNodeValue().trim()); 
                jmkwh = kwh_all.substring(0, kwh_all.length()-kwh_minor)+"."+kwh_all.substring(kwh_all.length()-kwh_minor); 
                //System.out.println("kwh "+jmkwh);

                NodeList nodeNominal = doc.getElementsByTagName("amount");
                Element eNominal = (Element) nodeNominal.item(0);
                NodeList Nominalx = eNominal.getChildNodes();
                nominal = ((Node) Nominalx.item(0)).getNodeValue().trim();

                NodeList nodeSegmentasi = doc.getElementsByTagName("subscriber_segmentation");
                Element eSegmentasi = (Element) nodeSegmentasi.item(0);
                NodeList Segmentasix = eSegmentasi.getChildNodes();

                NodeList nodeDaya = doc.getElementsByTagName("power");
                Element eDaya = (Element) nodeDaya.item(0);
                NodeList Dayax = eDaya.getChildNodes();

                gol = ((Node) Segmentasix.item(0)).getNodeValue().trim() + "/" + Integer.parseInt(((Node) Dayax.item(0)).getNodeValue().trim());

                NodeList nodeAdmin = doc.getElementsByTagName("admin_charge");
                Element eAdmin = (Element) nodeAdmin.item(0);
                NodeList Adminx = eAdmin.getChildNodes();

                NodeList nodeMinorAdmin = doc.getElementsByTagName("minor_admin_charge");
                Element eMinorAdmin = (Element) nodeMinorAdmin.item(0);
                NodeList MinorAdminx = eMinorAdmin.getChildNodes();

                String adm_all = ((Node) Adminx.item(0)).getNodeValue().trim();
                int adm_minor = Integer.parseInt(((Node) MinorAdminx.item(0)).getNodeValue().trim());
                adm = adm_all.substring(0, adm_all.length()-adm_minor);//+"."+adm_all.substring(adm_all.length()-adm_minor);

                NodeList nodePpn = doc.getElementsByTagName("ppn");
                Element ePpn = (Element) nodePpn.item(0);
                NodeList Ppnx = ePpn.getChildNodes();

                NodeList nodeMinorPpn = doc.getElementsByTagName("minor_ppn");
                Element eMinorPpn = (Element) nodeMinorPpn.item(0);
                NodeList MinorPpnx = eMinorPpn.getChildNodes();

                String ppn_all = ((Node) Ppnx.item(0)).getNodeValue().trim();
                int ppn_minor = Integer.parseInt(((Node) MinorPpnx.item(0)).getNodeValue().trim());
                ppn = ppn_all.substring(0, ppn_all.length()-ppn_minor);//+"."+ppn_all.substring(ppn_all.length()-ppn_minor);;

                NodeList nodePpj = doc.getElementsByTagName("ppj");
                Element ePpj = (Element) nodePpj.item(0);
                NodeList Ppjx = ePpj.getChildNodes();

                NodeList nodeMinorPpj = doc.getElementsByTagName("minor_ppj");
                Element eMinorPpj = (Element) nodeMinorPpj.item(0);
                NodeList MinorPpjx = eMinorPpj.getChildNodes();

                String ppj_all = ((Node) Ppjx.item(0)).getNodeValue().trim();
                int ppj_minor = Integer.parseInt(((Node) MinorPpjx.item(0)).getNodeValue().trim());
                ppj = ppj_all.substring(0, ppj_all.length()-ppj_minor);//+"."+ppj_all.substring(ppj_all.length()-ppj_minor);

                NodeList nodeMeterai = doc.getElementsByTagName("meterai");
                Element eMeterai = (Element) nodeMeterai.item(0);
                NodeList Meteraix = eMeterai.getChildNodes();

                NodeList nodeMinorMeterai = doc.getElementsByTagName("minor_meterai");
                Element eMinorMeterai = (Element) nodeMinorMeterai.item(0);
                NodeList MinorMeteraix = eMinorMeterai.getChildNodes();

                String meterai_all = ((Node) Meteraix.item(0)).getNodeValue().trim();
                int meterai_minor = Integer.parseInt(((Node) MinorMeteraix.item(0)).getNodeValue().trim());
                meterai = meterai_all.substring(0, meterai_all.length()-meterai_minor);//+"."+meterai_all.substring(meterai_all.length()-meterai_minor);

                NodeList nodeAngsuran = doc.getElementsByTagName("angsuran");
                Element eAngsuran = (Element) nodeAngsuran.item(0);
                NodeList Angsuranx = eAngsuran.getChildNodes();

                NodeList nodeMinorAngsuran = doc.getElementsByTagName("minor_angsuran");
                Element eMinorAngsuran = (Element) nodeMinorAngsuran.item(0);
                NodeList MinorAngsuranx = eMinorAngsuran.getChildNodes();

                String angsuran_all = ((Node) Angsuranx.item(0)).getNodeValue().trim();
                int angsuran_minor = Integer.parseInt(((Node) MinorAngsuranx.item(0)).getNodeValue().trim());
                angsuran = angsuran_all.substring(0, angsuran_all.length()-angsuran_minor);//+"."+angsuran_all.substring(angsuran_all.length()-angsuran_minor);

                NodeList nodePowerPurchase = doc.getElementsByTagName("power_purchase");
                Element ePowerPurchase = (Element) nodePowerPurchase.item(0);
                NodeList PowerPurchasex = ePowerPurchase.getChildNodes();

                NodeList nodeMinorPowerPurchase = doc.getElementsByTagName("minor_power_purchase");
                Element eMinorPowerPurchase = (Element) nodeMinorPowerPurchase.item(0);
                NodeList MinorPowerPurchasex = eMinorPowerPurchase.getChildNodes();

                String rpToken_all = ((Node) PowerPurchasex.item(0)).getNodeValue().trim();
                int rpToken_minor = Integer.parseInt(((Node) MinorPowerPurchasex.item(0)).getNodeValue().trim());
                rpToken = rpToken_all.substring(0, rpToken_all.length()-rpToken_minor);//+"."+rpToken_all.substring(rpToken_all.length()-rpToken_minor);
               
                try {
                NodeList nodeSaldo = doc.getElementsByTagName("saldo");
                Element eSaldo = (Element) nodeSaldo.item(0);
                NodeList Saldox = eSaldo.getChildNodes();
                saldo = ((Node) Saldox.item(0)).getNodeValue().trim();
                }catch(Exception e) {
                    System.out.println(e.getMessage());
                }
                
                hasil = "["+tgl+"] BERHASIL. IDPEL: "+idp+" Nama: "+ nama + 
                            " SNToken: "+sntoken+" JmKWH: "+Double.parseDouble(jmkwh)+
                            " Nominal: "+Integer.parseInt(nominal.substring(4, nominal.length())) +" GOL: "+gol+
                            " Adm: "+Integer.parseInt(adm)+" Mtr: "+Integer.parseInt(meterai)+
                            " PPJ: "+Integer.parseInt(ppj)+" PPN: "+Integer.parseInt(ppn)+
                            " Angs: "+ Integer.parseInt(angsuran) +" RpTkn: "+Integer.parseInt(rpToken)+" Saldo: "+saldo;
            }
            
        }
        catch(Exception e) {
            e.printStackTrace();
            hasil = "GAGAL "+idp+" - System Error";
        }
        
        return hasil;
    }
    
}
