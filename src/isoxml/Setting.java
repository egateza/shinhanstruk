package isoxml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ngonar
 */
public class Setting {

    private String dbUrl;
    private String dbDriver;
    private String dbUser;
    private String dbPass;
    private String cid;
    private String userid;
    private String startdate;
    private String enddate;
    private String prepaid;
    private String postpaid;
    private String nontaglis;
    private String telkom;
    private String pdam;
    private String bpjs;
    private String pulsa;
    private String game;
    private String multifinance;
    private String transactionFrom;
    private String tvk;
    private String ftpHost = "";
    private String ftpPort = "";
    private String ftpUsername = "";
    private String ftpPassword = "";

    public Setting() {
        try {
            getFileSetting();
            //System.out.println("==>" + dbUrl + ", " + dbUser + ", " + dbPass);

        } catch (FileNotFoundException ex) {
            System.out.println("==>1");
            Logger.getLogger(Setting.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            System.out.println("==>2");
            Logger.getLogger(Setting.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    public void setConnection(String dbDriver) {
//        this.dbDriver = dbDriver;
//    }

    public Connection getConnection() {
        Connection con = null;
        System.out.println(dbUrl + ", " + dbUser + ", " + dbPass);
        try {
            Class.forName(dbDriver);
            con = DriverManager.getConnection(dbUrl, dbUser, dbPass);

            System.out.println("Connection OTO ok.");

            return con;

        } catch (Exception e) {
            System.err.println("Exception OTO : " + e.getMessage());
        }

        return con;
    }

    public String getCid() {
        return cid;
    }
    
    public String getuserid() {
        return userid;
    }
    
    public String getstartdate() {
        return startdate;
    }
    
    public String getenddate() {
        return enddate;
    }
    
    public String getprepaid(){
        return prepaid;
    }
    
    public String getpostpaid(){
        return postpaid;
    }
    
    public String getnontaglis(){
        return nontaglis;
    }
    
    public String gettelkom(){
        return telkom;
    }
    
    public String getpdam(){
        return pdam;
    }
    
    public String getbpjs(){
        return bpjs;
    }
    
    public String getpulsa(){
        return pulsa;
    }
    
    public String getgame(){
        return game;
    }
    
    public String getmultifinance(){
        return multifinance;
    }

    public String getTvk() {
        return tvk;
    }
    

    private void getFileSetting() throws FileNotFoundException, IOException {
        File f = new File("setting.properties");
        if (f.exists()) {
            Properties pro = new Properties();
            FileInputStream in = new FileInputStream(f);
            pro.load(in);

            dbDriver = pro.getProperty("Application.database.driver");
            //setConnection(dbDriver);
            dbUrl = pro.getProperty("Application.database.url");

            dbUser = pro.getProperty("Application.database.user");
            dbPass = pro.getProperty("Application.database.pass");

           

            cid = pro.getProperty("Switching.hulu.cid");
            
            userid = pro.getProperty("app.user");
            startdate = pro.getProperty("app.datestart");
            enddate = pro.getProperty("app.dateend");
            
            postpaid = pro.getProperty("app.postpaid");
            prepaid = pro.getProperty("app.prepaid");
            nontaglis = pro.getProperty("app.nontaglis");
            
            telkom = pro.getProperty("app.telkom");
            
            pdam = pro.getProperty("app.pdam");
            
            bpjs = pro.getProperty("app.bpjs");
            
            pulsa = pro.getProperty("app.pulsa");
            
            game = pro.getProperty("app.game");
            
            multifinance = pro.getProperty("app.multifinance");
            
            tvk = pro.getProperty("app.tv");

        } else {
            System.out.println("File setting not found");
        }
    }

    public String getTrxFrom() {
        return transactionFrom;
    }

    /**
     * @return the ftpHost
     */
//    public String getFtpHost() {
//        return ftpHost;
//    }
//
//    /**
//     * @return the ftpPort
//     */
//    public String getFtpPort() {
//        return ftpPort;
//    }
//
//    /**
//     * @return the ftpUsername
//     */
//    public String getFtpUsername() {
//        return ftpUsername;
//    }
//
//    /**
//     * @return the ftpPassword
//     */
//    public String getFtpPassword() {
//        return ftpPassword;
//    }
}
