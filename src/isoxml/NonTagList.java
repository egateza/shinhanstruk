package isoxml;

import java.util.Map;

import org.apache.log4j.*;
import java.sql.*;

import org.jpos.iso.*;
import java.io.*;
import java.math.BigInteger;
import java.net.*;
import java.text.SimpleDateFormat;

import java.util.HashMap;
import org.jpos.iso.packager.GenericPackager;
import pelangi_utils.ParseISOMessage;
import pelangi_utils.Saldo;
import ppob_sls_iqbal.Settings;

import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import model.Mutations;
import model.PlnNontaglists;
import model.Transactions;
import model.Users;


public class NonTagList {

    Settings setting = new Settings();
    //Saldo mutasi = new Saldo();
    Connection conx = null;
    Connection conOtomax = null;
    int PORT_SERVER = 12345;
    String SERVER_IP = "localhost";
    int SLEEP = 30000;
    String cid = "";
    String switcher_id = "";
    String bank_code = "";
    private static Logger logger = Logger.getLogger(NonTagList.class);
    SLSLogger slsLog = new SLSLogger();
    ParseISOMessage parseISOMessage = new ParseISOMessage();

    public NonTagList() {

        setting.setConnections();

        //this.PORT_SERVER = Integer.parseInt(setting.getSwitchingPort());
        this.SERVER_IP = setting.getNonIP();
        this.PORT_SERVER = Integer.parseInt(setting.getNonPort());        
        this.cid = setting.getSwitchingCID();
        this.switcher_id = setting.getSwitcherID();
        this.bank_code = setting.getBankCode();
        this.SLEEP = Integer.parseInt(setting.getNonSleep());
    }

    public String request_inquiry(String msg, String inbox_id, String user_id, String merchant_cat_code, String terminal_id) {
        String hasil = "";
        Socket client = null;
        try {
            logConsoleTitle("Connecting to " + SERVER_IP + " on port " + PORT_SERVER);
            client = new Socket(SERVER_IP, PORT_SERVER);
            client.setSoTimeout(this.SLEEP);


            String log = "";

            logger.log(Level.INFO, log);

            String req[] = msg.split("\\.");
            String nominal = req[1].substring(3, req[1].length());
            String id_pel = req[2];

            String flag = "0", no_meter = "0", subscriber_id = "0";

            if (id_pel.length() == 11) {
                flag = "0";
                no_meter = id_pel;

            } else if (id_pel.length() == 12) {
                flag = "1";
                subscriber_id = id_pel;
            }

            String transaction_code = "000";

            log = inquiry(id_pel, user_id, inbox_id, merchant_cat_code, terminal_id, transaction_code, client);
            //System.out.println("=======================RESULT : " + new String(log) + "======================");
            logConsoleTitle("INQUIRY RESPONSE");
            logConsoleValue(log);
            logger.log(Level.INFO, log);

            hasil = log;

            //}
            //log = networkMsg("2",user_id,client); //sign off
            client.close();
        } catch (Exception e) {
            
            try {
                GenericPackager packager = new GenericPackager("packager/isoSLSascii.xml");

                ISOMsg isoMsg = new ISOMsg();
                isoMsg.setPackager(packager);
                
                isoMsg.setMTI("2110");
                isoMsg.set(2,"99504");            
                isoMsg.set(11,String.valueOf(inbox_id));            
                isoMsg.set(26,"6012");
                isoMsg.set(39,"0068");
                
                byte[] datax = isoMsg.pack();
                hasil = new String(datax);
                logger.log(Level.INFO,"ISO RESULT : " + new String(datax));
                
            }
            catch(Exception isox) {
                logger.log(Level.FATAL, "ISO : "+ isox.getMessage());
            }
            
            //hasil = "21104000004002000000059950260120068";
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
        }
        finally {
            try {
                client.close();
            }
            catch(Exception e) {
                logger.log(Level.FATAL,e.getMessage());
            }
        }

        return hasil;
    }

    public String[] parseReversalMsgResponse(String iso) {
        String[] hasil = new String[17];

        try {
            GenericPackager packager = new GenericPackager("isoSLSascii.xml");

            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.unpack(iso.getBytes());

            hasil[0] = isoMsg.getMTI();
            hasil[1] = isoMsg.getString(2);
            hasil[2] = isoMsg.getString(4);
            hasil[3] = isoMsg.getString(11);
            hasil[4] = isoMsg.getString(12);
            hasil[5] = isoMsg.getString(15);
            hasil[6] = isoMsg.getString(26);
            hasil[7] = isoMsg.getString(32);
            hasil[8] = isoMsg.getString(33);
            hasil[9] = isoMsg.getString(39);
            hasil[10] = isoMsg.getString(41);
            hasil[11] = isoMsg.getString(48);
            hasil[12] = isoMsg.getString(62);
            hasil[13] = isoMsg.getString(63);

            return hasil;
        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public String[] parseNetworkMsgResponse(String iso) {
        String[] hasil = new String[10];

        try {
            GenericPackager packager = new GenericPackager("isoSLSascii.xml");

            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.unpack(iso.getBytes());

            hasil[0] = isoMsg.getMTI();
            hasil[1] = isoMsg.getString(12);
            hasil[2] = isoMsg.getString(33);
            hasil[3] = isoMsg.getString(39);
            hasil[4] = isoMsg.getString(40);
            hasil[5] = isoMsg.getString(41);
            hasil[6] = isoMsg.getString(48);

            return hasil;
        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public String inquiry(String id_pel, String user_id, String inbox_id,
            String merchant_cat_code, String terminal_id, String transaction_code, Socket client) {
        String hasil = "";

        try {
            //System.out.println("==> timeout : " + client.getSoTimeout());
            //logConsoleTitle("INQUIRY RESPONSE");
            logConsoleValue("" + client.getSoTimeout());

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            // Create ISO Message


            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2100");
            isoMsg.set(2, "99504");
            isoMsg.set(11, String.format("%12s", inbox_id).replace(' ', '0'));
            isoMsg.set(12, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
            isoMsg.set(26, merchant_cat_code);
            isoMsg.set(32, this.bank_code);
            isoMsg.set(33, this.cid);
            isoMsg.set(41, terminal_id);

            String bit48 = this.switcher_id + id_pel + transaction_code;

            isoMsg.set(48, bit48);

            //Socket clientSocket = new Socket(SERVER_IP, PORT_SERVER);
            String pln_refno = " ";
            String amount = " ";
            int stream = 0;

            //setSLSLog(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()), pln_refno, id_pel, amount, stream);
            //createInquiryLog(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()), pln_refno, id_pel, amount, stream);
            byte[] datax = isoMsg.pack();
            //System.out.println("=======================REQUEST : " + new String(datax) + "======================");
            //String request = new String(datax);

            String networkRequest = new String(datax);
            logConsoleTitle("INQUIRY REQUEST");
            logConsoleValue(networkRequest);
            //parseISOMessage.getBitmap(networkRequest);

            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            String trailer = new String(new char[] {10});
            networkRequest = networkRequest+trailer;
            
            outgoing.print(networkRequest);
            outgoing.flush();

            int data;
            StringBuffer sb = new StringBuffer();
            //incoming.
            while ((data = incoming.read()) != -1) {
                //logConsoleValue(data);
                if (data == -1 || data == 255 || data==10 || data == 65533) {
                    break;
                }

                sb.append((char) data);
            }

            //outgoing.close();
            //incoming.close();
            //client.close();

            return sb.toString();

        } catch (Exception e) {
            logger.log(Level.FATAL, "Error Prepaid inquiry : " + e.getMessage());
        }

        return "Error";
    }

    public String networkMsg(String type, String terminal_id, Socket client) {
        String hasil = "";

        try {

            if (terminal_id.equalsIgnoreCase("")) {
                terminal_id = "PLGDEF0000000001";
            }

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");
            //ISOPackager packager = new CustomPackager2003();

            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2800");
            isoMsg.set(12, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
            isoMsg.set(33, this.cid);

            if (type.equalsIgnoreCase("1")) //sign on
            {
                isoMsg.set(40, "001");
            } else if (type.equalsIgnoreCase("2")) //sign off
            {
                isoMsg.set(40, "002");
            } else if (type.equalsIgnoreCase("3")) //echo test
            {
                isoMsg.set(40, "301");
            } else {
                isoMsg.set(40, "001");
            }

            isoMsg.set(41, terminal_id);
            isoMsg.set(48, switcher_id);

            byte[] datax = isoMsg.pack();
            //System.out.println("==>SIGN ON RESULT : " + new String(datax));
            String networkRequest = new String(datax);
            logConsoleTitle("NETWORK MSG SIGN ON");
            logConsoleValue(networkRequest);

            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            String trailer = new String(new char[] {10});
            networkRequest = networkRequest+trailer;
            
            outgoing.print(networkRequest);
            outgoing.flush();

            int data;
            StringBuffer sb = new StringBuffer();

            while ((data = incoming.read()) != -1) {
                if (data == -1 || data == 255 || data == 10) {
                    break;
                }

                sb.append((char) data);
            }

            //outgoing.close();
            //incoming.close();
            //client.close();

            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return hasil;
    }

    public String bill_payment(String msg, String trx_id, String user_id,
            String merchant_cat_code, String terminal_id, String hasilInquiry, String nominal_inq) {

        String log = "", nominal = "", id_pel = "";
        Socket client = null;
        try {

            logConsoleTitle("Connecting to " + SERVER_IP + " on port " + PORT_SERVER);
            client = new Socket(SERVER_IP, PORT_SERVER);
            client.setSoTimeout(this.SLEEP);
             

            String req[] = msg.split("\\.");
            //nominal = req[1].substring(3, req[1].length());
            id_pel = req[2];

            String no_meter = "0", subscriber_id = "0", flag = "0";

            if (id_pel.length() == 11) {
                flag = "0";
                no_meter = id_pel;
            } else if (id_pel.length() == 12) {
                flag = "1";
                subscriber_id = id_pel;
            }

            String transaction_code = "000";

            //lakukan inquiry
            log = hasilInquiry;
            if (!log.equalsIgnoreCase("")) {

                logger.log(Level.INFO, log);

                //parsing inquiry response
                Map<String, String> inquiryResp = new HashMap<String, String>();
                try {
                    inquiryResp = parseISOMessage.getBitmap(log);
                } catch (ISOException ex) {
                    java.util.logging.Logger.getLogger(NonTagList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }

                //nominal = inquiryResp.get("11");
                nominal = inquiryResp.get("4");
                //action code
                //if (inquiryResp[8].equalsIgnoreCase("0000")) {
                if (inquiryResp.get("39").equalsIgnoreCase("0000")) {
                    
                    //lakukan pembayaran
                    log = payment(id_pel, nominal, trx_id, user_id, merchant_cat_code, terminal_id, inquiryResp.get("48"), inquiryResp.get("62"), client);
                    
                    //System.out.println("====================PAYMENT RESPONSE : " + log + "===============");
                    String hasil = log;
                    logger.log(Level.INFO, log);
                    
                    //parsing pembayaran
                    Map<String, String> purchaseResp = new HashMap<String, String>();
                    try {
                        logConsoleTitle("PAYMENT RESPONSE BIT");
                        purchaseResp = parseISOMessage.getBitmap(log);
                        //logConsoleValue(log);
                        //createLog(purchaseResp, true, false);
                    } catch (ISOException ex) {
                        java.util.logging.Logger.getLogger(NonTagList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                    }

                    //bila tidak berhasil harus dilakukan reversal
                    //if (!purchaseResp[9].equalsIgnoreCase("0000")) {
                    if (!purchaseResp.get("39").equalsIgnoreCase("0000") && !purchaseResp.get("39").equalsIgnoreCase("0017")) {

                        //String mti = "2400";
                        for (int i = 0; i < 3; i++) {

                            //System.out.println("====================MASUK REVERSAL KE-" + i + "===============");
                            logConsoleTitle("MASUK REVERSAL KE-" + i);
                            //logConsoleValue(log);
//delay 30 detik
                            try {
                                Thread.sleep(3000);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            //log = reversal(id_pel, nominal, trx_id, user_id, merchant_cat_code, terminal_id, inquiryResp[8], inquiryResp[9], purchaseResp[2], purchaseResp[3], purchaseResp[4], client);
                            //lakukan reversal 1
                            log = reversal(id_pel, nominal, trx_id, user_id, merchant_cat_code, terminal_id, inquiryResp.get("48"), inquiryResp.get("62"), inquiryResp.get("12"), inquiryResp.get("16"), inquiryResp.get("33"), client, i);

                            //String[] adv1Resp = parseReversalMsgResponse(log);
                            //parsing reversal 1
                            Map<String, String> advResp = new HashMap<String, String>();
                            try {
                                advResp = parseISOMessage.getBitmap(log);
                            } catch (ISOException ex) {
                                java.util.logging.Logger.getLogger(NonTagList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                            }
                            if (advResp.get("39").equalsIgnoreCase("0000")) {
                                
                                logConsoleTitle("reversal : " + i + " Tambahkan balance + Return");
                                //matiin sementara buat testing
                                //mutasi.tambahBalance(Double.parseDouble(nominal), user_id, log, trx_id, mutasi.getBalance(user_id));
                                //////////////////refund the money////////////////////////////////////////////////////////////////////////
                                refund(Double.parseDouble(nominal_inq),"Refund trx nontaglis gagal",Integer.parseInt(trx_id),Integer.parseInt(user_id));
                                //////////////////////////////////////////////////////////////////////////////////////////////////////////
                                
                                return log;
                            } else {
                                if (i == 2) {
                                    logConsoleTitle("force Tambahkan balance + Return");
                                    //matiin sementara buat testing
                                    //mutasi.tambahBalance(Double.parseDouble(nominal), user_id, log, trx_id, mutasi.getBalance(user_id));
                                    //////////////////refund the money////////////////////////////////////////////////////////////////////////
                                    //refund(Double.parseDouble(nominal_inq),"Refund trx nontaglis gagal",Integer.parseInt(trx_id),Integer.parseInt(user_id));
                                    //////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    
                                    return log;
                                }
                            }
                        }

                    } else if (purchaseResp.get("39").equalsIgnoreCase("0000")) {
                        createRecap(purchaseResp, true, true);
                    } else if (purchaseResp.get("39").equalsIgnoreCase("0017")) {
                        refund(Double.parseDouble(nominal_inq),"Refund trx nontaglis gagal",Integer.parseInt(trx_id),Integer.parseInt(user_id));
                    }
                    
                } else {
                    logger.log(Level.INFO, log);
                    return log;
                }

            }

            client.close();
        } catch (Exception e) {
            
            try {
                GenericPackager packager = new GenericPackager("packager/isoSLSascii.xml");

                ISOMsg isoMsg = new ISOMsg();
                isoMsg.setPackager(packager);
                
                isoMsg.setMTI("2110");
                isoMsg.set(2,"99504");            
                isoMsg.set(11,trx_id);            
                isoMsg.set(26,"6012");
                isoMsg.set(39,"0068");
                
                byte[] datax = isoMsg.pack();
                log = new String(datax);
                System.out.println("ISO RESULT : " + new String(datax));
                
            }
            catch(Exception isox) {
                logger.log(Level.FATAL, "ISO : "+ isox.getMessage());
            }
            
            //////////////////refund the money////////////////////////////////////////////////////////////////////////
            refund(Double.parseDouble(nominal_inq),"Refund trx nontaglis gagal",Integer.parseInt(trx_id),Integer.parseInt(user_id));
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            //log = "21104000004002000000059950260120068";
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
        }
        finally {
            try {
                client.close();
            }
            catch(Exception e) {
                logger.log(Level.FATAL,e.getMessage());
            }
        }

        return log;
    }
    
    public void refund(double nominal, String msg, int inbox_id, int user_id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();
        try {
            
            ////// ngilangin 3600000 iso /////////
            //String nominal_refund = String.valueOf(nominal);
            //nominal_refund = nominal_refund.substring(4);
            //nominal = Double.parseDouble(nominal_refund);
            //////////////////////////////////////
            
            Users user = em.find(Users.class, user_id);
            int price_template_id = user.getPriceTemplateId();
            
            //get Product price
            String sq = "select m.price from ProductPrices m where m.priceTemplateId=:ptid and m.productId=:pid";
            Query qqq = em.createQuery(sq);
            qqq.setParameter("pid", 105);
            qqq.setParameter("ptid", price_template_id);
            Number priceq=(Number) qqq.getSingleResult();
            logger.log(Level.INFO,"Harga < "+inbox_id+" > : "+nominal+", admin : "+priceq.longValue());
            System.err.println("Harga < "+inbox_id+" > : "+nominal+", admin : "+priceq.longValue());

            long prcq = priceq.longValue();
            
            em.getTransaction().begin();

            Mutations mut = new Mutations();
            //mut.setAmount((long) (nominal));
            mut.setAmount((long) (nominal)-prcq);
            mut.setNote(msg);
            mut.setJenis((char)'K');
            //mut.setBalance(balq);
            mut.setInboxId(inbox_id);
            mut.setUserId(user_id);
            mut.setCreateDate(new java.util.Date());

            em.persist(mut);

            em.getTransaction().commit();
        }
        catch(Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }
    }

    public String payment(String id_pel, String nominal, String trx_id, String user_id,
            String merchant_cat_code, String terminal_id,
            String inquiry_response48, String inquiry_response62, Socket client) {
        String hasil = "";
        try {

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2200");
            isoMsg.set(2, "99504");

            String curr_code = "360";
            String minor_unit = "0";
            String amount = String.format("%12s", nominal).replace(' ', '0');

            String bit4 = curr_code + minor_unit + amount;
            String datetime = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            //isoMsg.set(4, bit4);
            isoMsg.set(4, nominal);

            isoMsg.set(11, String.format("%12s", trx_id).replace(' ', '0'));
            isoMsg.set(12, datetime);
            isoMsg.set(26, merchant_cat_code);
            isoMsg.set(32, this.bank_code);
            isoMsg.set(33, this.cid);
            isoMsg.set(41, terminal_id);
            isoMsg.set(48, inquiry_response48);
            isoMsg.set(62, inquiry_response62);

            byte[] datax = isoMsg.pack();

            String networkRequest = new String(datax);
            logConsoleTitle("PAYMENT REQUEST");
            logConsoleValue(networkRequest);

            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            String trailer = new String(new char[] {10});
            networkRequest = networkRequest+trailer;
            
            outgoing.print(networkRequest);
            outgoing.flush();

            String[] bit48 = parseBit48Inquiry(inquiry_response48);
      
            int data;
            StringBuffer sb = new StringBuffer();

            while ((data = incoming.read()) != -1) {
                if (data == -1 || data == 255 || data==10 || data == 65533) {
                    break;
                }

                sb.append((char) data);
            }

            logConsoleTitle("PAYMENT RESPONSE");
            logConsoleValue(sb.toString());

            return sb.toString();

        } catch (Exception e) {
            logger.log(Level.ERROR, e.getMessage());
        }

        return hasil;
    }

    public String reversal(String id_pel, String nominal, String trx_id, String user_id,
            String merchant_cat_code, String terminal_id,
            String inquiry_response48, String inquiry_response62, String STANOri, String DateOri, String BankCodeOri, Socket client, int isRepeat) {
        String hasil = "";

        String mti = "2400";
        if (isRepeat > 0) {
            mti = "2401";
        }

        try {

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            //isoMsg.setMTI("2400");
            isoMsg.setMTI(mti);
            isoMsg.set(2, "99504");

            String curr_code = "360";
            String minor_unit = "0";
            String amount = String.format("%12s", nominal).replace(' ', '0');

            String bit4 = curr_code + minor_unit + amount;
            String datetime = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            //isoMsg.set(4, bit4);
            isoMsg.set(4, nominal);
            isoMsg.set(11, String.format("%12s", trx_id).replace(' ', '0'));
            isoMsg.set(12, datetime);
            isoMsg.set(26, merchant_cat_code);
            isoMsg.set(32, this.bank_code);
            isoMsg.set(33, this.cid);
            isoMsg.set(41, terminal_id);
            isoMsg.set(48, inquiry_response48);

            String MTIOri = "2200";
            STANOri = "";
            DateOri = "";
            BankCodeOri = "";

            String bit56 = MTIOri + STANOri + DateOri + BankCodeOri;

            isoMsg.set(56, bit56);
            isoMsg.set(62, inquiry_response62);

            byte[] datax = isoMsg.pack();
            //System.out.println("REVERSAL REQUEST : " + new String(datax));
            String networkRequest = new String(datax);

            //////////////////SENDING ISO MSG////////////////////

            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            String trailer = new String(new char[] {10});
            networkRequest = networkRequest+trailer;
            
            outgoing.print(networkRequest);
            outgoing.flush();

            String[] bit48 = parseBit48Reversal(inquiry_response48);

            String regNumber = bit48[1];
            String pln_refno = bit48[8];
            String totalAmount = bit48[14];
            int stream = 0;
            //int repeat = 0;
            createReversalLog(datetime, pln_refno, regNumber, totalAmount, stream, isRepeat);

            int data;
            StringBuffer sb = new StringBuffer();

            while ((data = incoming.read()) != -1) {
                if (data == -1 || data == 255 || data == 10 || data == 65533) {
                    break;
                }

                sb.append((char) data);
            }

            Map<String, String> reversalResponse = new HashMap<String, String>();
            try {
                reversalResponse = parseISOMessage.getBitmap(hasil);
            } catch (ISOException ex) {
                java.util.logging.Logger.getLogger(NonTagList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            bit48 = new String[20];
            bit48 = parseBit48Reversal(reversalResponse.get("48"));

            regNumber = bit48[1];
            pln_refno = bit48[8];
            totalAmount = bit48[14];
            stream = 1;
            //repeat = 0;
            createReversalLog(datetime, pln_refno, regNumber, totalAmount, stream, isRepeat);

            //outgoing.close();
            //incoming.close();
            //client.close();

            return sb.toString();

        } catch (Exception e) {
            logger.log(Level.ERROR, e);
        }

        return hasil;
    }

    public String[] parsePaymentMsgResponse(String iso, boolean isLogged, boolean isRepeat) {

        String[] hasil = new String[17];

        try {
            GenericPackager packager = new GenericPackager("isoSLSascii.xml");

            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.unpack(iso.getBytes());

            hasil[0] = isoMsg.getMTI();
            hasil[1] = isoMsg.getString(2);
            hasil[2] = isoMsg.getString(4);
            hasil[3] = isoMsg.getString(11);
            hasil[4] = isoMsg.getString(12);
            hasil[5] = isoMsg.getString(15);
            hasil[6] = isoMsg.getString(26);
            hasil[7] = isoMsg.getString(32);
            hasil[8] = isoMsg.getString(33);
            hasil[9] = isoMsg.getString(39);
            hasil[10] = isoMsg.getString(41);
            hasil[11] = isoMsg.getString(48);
            hasil[12] = isoMsg.getString(62);
            hasil[13] = isoMsg.getString(63);

            if (isLogged) {
                //log to file
                try {
                    String strm = "REVERSAL_RESPONSE_STREAM";

                    if (isRepeat) {
                        strm = "REVERSAL_REPEAT_RESPONSE_STREAM";
                    }

                    String toLog = hasil[4] + "|"
                            + hasil[11].substring(22, 54) + "|"
                            + hasil[11].substring(7, 19) + "|"
                            + hasil[2].substring(4) + "|"
                            + strm;
                    slsLog.logAction(toLog, "POSTPAID");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return hasil;
        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public String processPayment(String hasil) {
        //System.out.println("==>>>>>>>"+hasil);
        String iso = hasil;
        String[] bit48 = new String[19];
        NonTagListResponseCode ntlrc = new NonTagListResponseCode();
       
        Map<String, String> result = new HashMap<String, String>();
        try {
            result = parseISOMessage.getBitmap(hasil);
        } catch (ISOException ex) {
            java.util.logging.Logger.getLogger(NonTagList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        if (result.get("39").equalsIgnoreCase("0000") ) {

            bit48 = parseBit48Payment(result.get("48"));

            String tm_switcher_id = bit48[0];
            String tm_reg_no = bit48[1];
            String tm_trx_code = bit48[2];
            String tm_trx_name = bit48[3];
            String tm_reg_date = bit48[4];
            String tm_exp_date = bit48[5];
            String tm_subscriber_id = bit48[6];
            String tm_subscriber_name = bit48[7];

            String tm_pln_ref_no = bit48[8];
            String tm_sls_rcp_ref = bit48[9];
            String tm_su = bit48[10];

            String tm_su_address = bit48[11];
            String tm_su_phone = bit48[12];
            String tm_total_trx_minor = bit48[13];
            String tm_total_trx_amount = bit48[14];
            String tm_pln_bill_minor = bit48[15];
            String tm_pln_bill_val = bit48[16];
            String tm_adm_charge_minor = bit48[17];
            String tm_adm_charge = bit48[18];

            //transaksi         = nama transaksi
            //no reg            = no registrasi
            //tgl registrasi    = tanggal dikeluarkannya no registrasi
            //

            String reversalTag = "";
            if((result.get("MTI").equalsIgnoreCase("2410") ||  result.get("MTI").equalsIgnoreCase("2411"))
                    ){
                        reversalTag = "<msg_type>REVERSAL</msg_type>";
            }
//            if((result.get("39").equalsIgnoreCase("0063") ||
//                    result.get("39").equalsIgnoreCase("0012")) &&
//                    (result.get("MTI").equalsIgnoreCase("2410") ||
//                    result.get("MTI").equalsIgnoreCase("2411"))
//                    ){
//                        reversalTag = "<msg_type>REVERSAL</msg_type>";
//            }
            
            tm_subscriber_name = tm_subscriber_name.replaceAll("<", ".");
            tm_subscriber_name = tm_subscriber_name.replaceAll(">", ",");

            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"                    
                    + "<produk>PLNNONTAGLIS</produk>"
                    + reversalTag
                    + "<transaction_name>" + tm_trx_name + "</transaction_name>"
                    + "<registration_no>" + tm_reg_no + "</registration_no>"
                    + "<registration_date>" + tm_reg_date + "</registration_date>"
                    + "<subscriber_name>" + tm_subscriber_name + "</subscriber_name>"
                    + "<subscriber_id>" + tm_subscriber_id + "</subscriber_id>"
                    + "<pln_bill_minor>" + tm_pln_bill_minor + "</pln_bill_minor>"
                    + "<pln_bill>" + tm_pln_bill_val + "</pln_bill>"
                    + "<switching_refno>" + tm_sls_rcp_ref + "</switching_refno>"
                    + "<service_unit>" + tm_su + "</service_unit>"
                    + "<service_unit_phone>" + tm_su_phone + "</service_unit_phone>"
                    + "<admin_charge_minor>" + tm_adm_charge_minor + "</admin_charge_minor>"
                    + "<admin_charge>" + tm_adm_charge + "</admin_charge>"
                    + "<amount_minor>" + tm_total_trx_minor + "</amount_minor>"
                    + "<amount>" + tm_total_trx_amount + "</amount>"
                    + "<rc>" + result.get("39") + "</rc>"
                    + "<stan>" + result.get("11") + "</stan>"
                    + "<info_text>" + result.get("63") + "</info_text>"
                    + "</response>";

                   

                   String amt_bill1 = tm_pln_bill_val.substring(0,(tm_pln_bill_val.length()-Integer.parseInt(tm_pln_bill_minor)));
                   String amt_bill2 = tm_pln_bill_val.substring((tm_pln_bill_val.length()-Integer.parseInt(tm_pln_bill_minor)),tm_pln_bill_val.length());
                   String amt_bill = amt_bill1 + "." + amt_bill2;

                   String amt_admin1 = tm_adm_charge.substring(0,(tm_adm_charge.length()-Integer.parseInt(tm_adm_charge_minor)));
                   String amt_admin2 = tm_adm_charge.substring((tm_adm_charge.length()-Integer.parseInt(tm_adm_charge_minor)),tm_adm_charge.length());
                   String amt_admin = amt_admin1 + "." + amt_admin2;

                   String amt_tagihan1 = tm_total_trx_amount.substring(0,(tm_total_trx_amount.length()-Integer.parseInt(tm_total_trx_minor)));
                   String amt_tagihan2 = tm_total_trx_amount.substring((tm_total_trx_amount.length()-Integer.parseInt(tm_total_trx_minor)),tm_total_trx_amount.length());
                   String amt_tagihan = amt_tagihan1 + "." + amt_tagihan2;

//                   long idx = insertToTrx(Integer.parseInt(detail[5]), 105, detail[4], detail[1],
//                                detail[6], Long.parseLong(amt_tagihan1),Long.parseLong(amt_tagihan1),Integer.parseInt(detail[3]),
//                                tm_trx_name,"PLN-NONTAGLIS",Long.parseLong("0"),Short.parseShort("0"),
//                                Short.parseShort("0"),tm_sls_rcp_ref,detail[4],Short.parseShort("0"));
//
//                   insertToDB(String.valueOf(idx),tm_reg_no,tm_reg_date,
//                           tm_subscriber_name,tm_subscriber_id,Double.parseDouble(amt_bill),
//                           tm_sls_rcp_ref,Double.parseDouble(amt_admin),
//                           iso,hasil,Integer.parseInt(detail[3]),amt_tagihan,tm_su_phone,tm_trx_name,result.get("63"));
                   
                   //simpan data payment ke database
                   logger.log(Level.INFO, "Save to Nontaglist Trx");


        }
        else if (result.get("39").equalsIgnoreCase("0012") ||
                 result.get("39").equalsIgnoreCase("0063") ||
                 result.get("39").equalsIgnoreCase("0005") ||
                 result.get("39").equalsIgnoreCase("0094")) {

            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<stan>"+result.get("11")+"</stan>"
                    + "<produk>PLNNONTAGLIS</produk>"
                    + "<code>" + result.get("39") + "</code>"
                    + "<desc>TRANSAKSI SEDANG DIPROSES OLEH PLN</desc>"
                    + "</response>";

        }
        else {
            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<stan>"+result.get("11")+"</stan>"
                    + "<produk>PLNNONTAGLIS</produk>"
                    + "<code>" + result.get("39") + "</code>"
                    + "<desc>" + ntlrc.getInquiryMsgResponseName(result.get("39")) + "</desc>"
                    + "</response>";
        }
       // createPaymentLog(result.get("12"), bit48[8], bit48[1], bit48[14], 1);

        return hasil;
    }

    public String[] parseBit48Payment(String msg) {
        logConsoleTitle("PARSE BIT48 PAYMENT");
        String[] hasil = new String[65];

        int[] seq = {7, 13, 3, 25, 8, 8, 12, 25, 32, 32, 5, 35, 15, 1, 17, 1, 17, 1, 10};
        String[] title = {
            "switcher id",
            "reg no.",
            "trx code",
            "trx name",
            "reg date",
            "exp date",
            "subsc id",
            "subsc name",
            "PLN Reference Number",
            "SLS Receipt reference",
            "Service Unit",
            "Service Unit Address",
            "Service Unit Phone",
            "Total transaction amount minor unit",
            "Total transaction amount",
            "PLN bill minor unit",
            "PLN bill",
            "Adm Charge minor unit",
            "Adm Charge"
        };

        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";

                if (seq[i] == 1) {
                    hasil[i] = String.valueOf(msg.charAt(l));
                    logConsoleValue(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    l += seq[i];
                } else if (seq[i] > 1) {
                    f = n;
                    l = n + seq[i];
                    hasil[i] = msg.substring(f, l);
                    logConsoleValue(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                }
                n = l;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return hasil;
    }

    public String[] parseBit48Reversal(String msg) {
        logConsoleTitle("PARSE BIT48");

        String[] hasil = new String[65];
        int[] seq = {7, 13, 3, 25, 8, 8, 12, 25, 32, 32, 5, 35, 15, 1, 17, 1, 17, 1, 10};
        String[] title = {
            "switcher id",
            "reg no.",
            "trx code",
            "trx name",
            "reg date",
            "exp date",
            "subsc id",
            "subsc name",
            "PLN Reference Number",
            "SLS Receipt reference",
            "Service Unit",
            "Service Unit Address",
            "Service Unit Phone",
            "Total transaction amount minor unit",
            "Total transaction amount",
            "PLN bill minor unit",
            "PLN bill",
            "Adm Charge minor unit",
            "Adm Charge"
        };

        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";

                if (seq[i] == 1) {
                    hasil[i] = String.valueOf(msg.charAt(l));
                    logConsoleValue(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    l += seq[i];
                } else if (seq[i] > 1) {
                    f = n;
                    l = n + seq[i];
                    hasil[i] = msg.substring(f, l);
                    logConsoleValue(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                }
                n = l;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return hasil;
    }
    
    public String processInquiry(String hasil) {
        NonTagListResponseCode ntlrc = new NonTagListResponseCode();
        
        Map<String, String> result = new HashMap<String, String>();
        try {
            logConsoleTitle("PARSE ISO INQUIRY RESPONSE");
            result = parseISOMessage.getBitmap(hasil);
        } catch (ISOException ex) {
            java.util.logging.Logger.getLogger(NonTagList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        if (hasil.equalsIgnoreCase("")) {
            logger.log(Level.INFO,"=============KONEKSI DIANGGAP TIMEOUT============");
            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<stan>"+result.get("11")+"</stan>" 
                    + "<produk>PLNNONTAGLIS</produk>"
                    + "<code>0068</code>"
                    + "<desc>" + ntlrc.getInquiryMsgResponseName("0068") + "</desc>"
                    + "</response>";
            return hasil;
        }
        
        

        String[] bit48 = new String[19];
        //String bit48 = new String();
        //if (result[8].equalsIgnoreCase("0000")) {
        if (result.get("39").equalsIgnoreCase("0000")) {
            logConsoleTitle("inquiry response xml");
            //String[] bit48 = parseBit48Inquiry(result[10]);
            bit48 = parseBit48Inquiry(result.get("48"));

            String tm_switcher_id = bit48[0];
            String tm_reg_no = bit48[1];
            String tm_trx_code = bit48[2];// = msg.substring(19, 21); System.out.println("trx code   : "+hasil[2]); //bill status - 1
            String tm_trx_name = bit48[3];// = msg.substring(21, 45); System.out.println("trx name   : "+hasil[3]); //outstanding bill - 2
            String tm_reg_date = bit48[4];// = msg.substring(45, 52); System.out.println("reg date   : "+hasil[4]); //switcher ref no - 32
            String tm_exp_date = bit48[5];// = msg.substring(52, 59); System.out.println("exp date   : "+hasil[5]); //Subscriber name - 25
            String tm_subscriber_id = bit48[6];// = msg.substring(59, 70); System.out.println("subsc id   : "+hasil[6]); //Service unit - 5
            String tm_subscriber_name = bit48[7];// = msg.substring(70, 94); System.out.println("subsc name : "+hasil[7]); //Service unit phone - 15

            String tm_pln_ref_no = bit48[8];// = msg.substring(94, 125); System.out.println("PLN Reference Number : "+hasil[8]); //subscriber segmentation - 4
            String tm_sls_rcp_ref = bit48[9];// = msg.substring(125, 156); System.out.println("SLS Receipt reference : "+hasil[9]);  //Power consuming cat - 9
            String tm_su = bit48[10];// = msg.substring(156, 160); System.out.println("service unit : "+hasil[10]); //Total admin charges - 9

            String tm_su_address = bit48[11];// = msg.substring(160, 194); System.out.println("service unit address : "+hasil[11]); //bill period - 6
            String tm_su_phone = bit48[12];// = msg.substring(194, 208); System.out.println("service unit phone : "+hasil[12]); //due date - 8
            String tm_total_trx_minor = bit48[13];// = String.valueOf(msg.charAt(208)); System.out.println("Total transaction amount minor unit : "+hasil[13]); //meter read date - 8
            String tm_total_trx_amount = bit48[14];// = msg.substring(209, 225); System.out.println("total transaction amount : "+hasil[14]); //total electricity bill - 11
            String tm_pln_bill_minor = bit48[15];// = String.valueOf(msg.charAt(225)); System.out.println("PLN bill minor unit : "+hasil[15]); //incentive - 11
            String tm_pln_bill_val = bit48[16];// = msg.substring(226, 242); System.out.println("PLN bill value : "+hasil[16]); //vat - 10
            String tm_adm_charge_minor = bit48[17];// = String.valueOf(msg.charAt(242)); System.out.println("Adm Charge mino unit: "+hasil[17]); //penalty fee - 9
            String tm_adm_charge = bit48[18];// = msg.substring(243,252); System.out.println("adm charge : "+hasil[18]); //prev meter reading 1 - 8

            
            tm_subscriber_name = tm_subscriber_name.replaceAll("<", ".");
            tm_subscriber_name = tm_subscriber_name.replaceAll(">", ",");

             hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<stan>"+result.get("11")+"</stan>"
                    + "<produk>PLNNONTAGLIS</produk>"
                    + "<registration_no>" + tm_reg_no + "</registration_no>"
                    + "<registration_date>" + tm_reg_date + "</registration_date>"
                    + "<transaction_name>" + tm_trx_name + "</transaction_name>"
                    + "<subscriber_name>" + tm_subscriber_name + "</subscriber_name>"
                    + "<amount_minor>" + tm_total_trx_minor + "</amount_minor>"
                    + "<amount>" + tm_total_trx_amount + "</amount>"
                    + "<datetime>" + tm_reg_date + "</datetime>"
                    + "<rc>" + result.get("39") + "</rc>"
                    + "<subscriber_id>" + tm_subscriber_id + "</subscriber_id>"
                    + "<switcher_refno>" + tm_switcher_id + "</switcher_refno>"
                    + "<service_unit>" + tm_su + "</service_unit>"
                    + "<service_unit_phone>" + tm_su_phone + "</service_unit_phone>"
                    + "<admin_charge_minor>" + tm_adm_charge_minor + "</admin_charge_minor>"
                    + "<admin_charge>" + tm_adm_charge + "</admin_charge>"
                    + "<pln_bill_minor>" + tm_pln_bill_minor + "</pln_bill_minor>"
                    + "<pln_bill>" + tm_pln_bill_val + "</pln_bill>"
                    + "</response>";
            
        } else {
            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<stan>"+result.get("11")+"</stan>"
                    + "<produk>PLNNONTAGLIS</produk>"
                    + "<code>" + result.get("39") + "</code>"
                    + "<desc>" + ntlrc.getInquiryMsgResponseName(result.get("39")) + "</desc>"
                    + "</response>";
        }

        //AMOUNT HARUSNYA RPTAG + RPBK
        //String amount = bit48[16].substring(5);
        //createInquiryLog(result.get("12"), bit48[1], bit48[6], amount, 1);

        return hasil;
    }

    public String[] parseBit48Inquiry(String msg) {
        logConsoleTitle("PASE BIT48 INQUIRY");
        String[] hasil = new String[65];
        int[] seq = {7, 13, 3, 25, 8, 8, 12, 25, 32, 32, 5, 35, 15, 1, 17, 1, 17, 1, 10};
        String[] title = {
            "switcher id",
            "reg no.",
            "trx code",
            "trx name",
            "reg date",
            "exp date",
            "subsc id",
            "subsc id",
            "PLN Reference Number",
            "SLS Receipt reference",
            "Service Unit",
            "Service Unit Address",
            "Service Unit Phone",
            "Total transaction amount minor unit",
            "Total transaction amount",
            "PLN bill minor unit",
            "PLN bill",
            "Adm Charge minor unit",
            "Adm Charge"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";
                //System.out.println(" l : "+l+", f : "+f);
                if (seq[i] == 1) {
                    hasil[i] = String.valueOf(msg.charAt(l));
                    logConsoleValue(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    l += seq[i];
                } else if (seq[i] > 1) {
                    f = n;
                    l = n + seq[i]; 
                    hasil[i] = msg.substring(f, l);
                    logConsoleValue(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                }
                n = l;
            }

        } catch (Exception e) {
            logger.log(Level.FATAL,e.toString());
            e.printStackTrace();
        }

        return hasil;
    }

    private void createRecap(Map<String, String> response, boolean isLogged, boolean isRepeat) {
        
        if (isLogged) {
            //log to file
            try {
               
                String[] bit48 = parseBit48Payment(response.get("48"));

                //bila menggunakan payment
                String toLog = response.get("12") + "|" //DT
                        + response.get("33") + "|" //CENTRAL_ID
                        + response.get("26") + "|" //MERCHANT
                        + bit48[8] + "|" //REFFNUM
                        + bit48[9] + "|" //SREFFNUM
                        + bit48[6] + "|" //SUB_ID
                        + bit48[1] + "|" //REG_NUM
                        + bit48[4] + "|" //REG_DATE
                        + bit48[2] + "|" //TRX_CODE
                        + bit48[14] + "|" //TOTAL_AMOUNT
                        + bit48[14] + "|" //TRANS_AMOUNT
                        + bit48[18] + "|" //ADMIN_CHARGES
                        + response.get("32") + "|" //BANK_CODE
                        + response.get("41") + "|" //PPID
                        ;

                slsLog.recapPLNNon(toLog, "99504");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void createInquiryLog(String dt, String pln_refno, String subsId, String amount, int stream) {
        String strStream = "";
        if (stream == 0) {
            strStream = "INQUIRY_REQUEST_STREAM";
        } else {
            strStream = "INQUIRY_RESPONSE_STREAM";
        }
        setSLSLog(dt, pln_refno, subsId, amount, strStream);
    }

    private void createPaymentLoswitchingCidg(String dt, String pln_refno, String subsId, String amount, int stream) {
        String strStream = "";
        if (stream == 0) {
            strStream = "PAYMENT_REQUEST_STREAM";
        } else if (stream == 1) {
            strStream = "PAYMENT_RESPONSE_STREAM";
        }

        setSLSLog(dt, pln_refno, subsId, amount, strStream);
    }

    private void createReversalLog(String dt, String pln_refno, String subsId, String amount, int stream, int repeat) {
        String strStream = "";
        String strAdvice = "";
        if (repeat == 0) {
            strAdvice = "REVERSAL";
        } else {
            strAdvice = "REVERSAL_REPEAT" + repeat;
        }

        if (stream == 0) {
            strStream = strAdvice + "_REQUEST_STREAM";
        } else if (stream == 1) {
            strStream = strAdvice + "_RESPONSE_STREAM";
        }

        setSLSLog(dt, pln_refno, subsId, amount, strStream);
    }

    private void setSLSLog(String dt, String pln_refno, String subsId, String amount, String strStream) {
        String toLog = String.format("%14s", dt) + "|" //DATETIME
                + String.format("%32s", pln_refno).replace(' ', '0') + "|" //PLN REF NO
                + String.format("%12s", subsId) + "|" //SUBSCRIBER ID
                + String.format("%12s", " ").replace(" ", "0") + "|" //UNKNOWN PART
                + String.format("%-12s", amount).replace(" ", "0") + "|" //AMOUNT
                + strStream //STREAM
                ;

        slsLog.logAction(toLog, "99504");
    }

    private void logConsoleTitle(String value) {
        String a = String.format("%15s", " ").replace(" ", "=");
        String val = String.format("%50s", value);

        logger.log(Level.INFO,a + val + a);
    }

    private void logConsoleValue(String value) {
        logger.log(Level.INFO,value);
    }

    public Long insertToTrx(int user_id, int product_id, String destination, String sender,
            String sender_type, long price_sell,long price_buy, int inbox_id,
            String status,String remark,long saldo_awal,short counter,
            short counter2,String sn,String receiver,short resend) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();
        long idx = 0;

        try {
            Transactions trx = new Transactions();
            em.getTransaction().begin();

            trx.setUserId(user_id);
            trx.setProductId(product_id);
            trx.setDestination(destination);
            trx.setSender(sender);
            trx.setSenderType(sender_type);
            trx.setPriceSell(price_sell);
            trx.setPriceBuy(price_buy);
            trx.setInboxId(inbox_id);
            trx.setStatus(status);
            trx.setRemark(remark);
            trx.setSaldoAwal(saldo_awal);
            trx.setCounter(counter);
            trx.setCounter2(counter2);
            trx.setSn(sn);
            trx.setReceiver(receiver);
            trx.setResend(resend);
            trx.setCreateDate(new Date());

            em.persist(trx);
            em.flush();
            idx=trx.getId();

            em.getTransaction().commit();

            em.close();
            factory.close();

        }
        catch(Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
            
            idx = insertToTrx(user_id, product_id, destination, sender,
                  sender_type, price_sell, price_buy, inbox_id,
                  status, remark, saldo_awal,counter,
                  counter2, sn,receiver,resend);
        }
        finally {
            if (em.isOpen()) {
                em.close();
                factory.close();
            }
        }

        return idx;
    }

    public void insertToDB(String trx_no,String  no_registrasi,String  tgl_registrasi,
            String  nama, String idpel, Double rptag, String switching_ref, Double admin_charge,
            String iso, String xml, int inbox_id, String tagihan,String telpon, String jenis, String info){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();

        try {

            PlnNontaglists nontag = new PlnNontaglists();
            em.getTransaction().begin();

            nontag.setTrxNo(trx_no);
            nontag.setNoRegistrasi(no_registrasi);
            nontag.setTglRegistrasi(tgl_registrasi);
            nontag.setNama(nama);
            nontag.setIdpel(idpel);
            nontag.setRptag(rptag);
            nontag.setSwitchingRef(switching_ref);
            nontag.setAdminCharge(admin_charge);
            nontag.setIso(iso);
            nontag.setXml(xml);
            nontag.setInboxId(inbox_id);
            nontag.setTelpon(telpon);
            nontag.setTagihan(tagihan);
            nontag.setJenisTransaksi(jenis);
            nontag.setInfo(info);
            nontag.setCreateDate(new Date());
            
            em.persist(nontag);
            em.getTransaction().commit();

            em.close();
            factory.close();

        }
        catch(Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
            
//            insertToDB(trx_no, no_registrasi, tgl_registrasi,
//             nama, idpel,  rptag,  switching_ref,  admin_charge,
//             iso,  xml, inbox_id,  tagihan, telpon,  jenis,  info);
        }
        finally {
            if (em.isOpen()) {
                em.close();
                factory.close();
            }
        }
    }

}
