
package isoxml;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMUX;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISORequest;
import org.jpos.iso.channel.ASCIIChannel;
import org.jpos.iso.packager.GenericPackager;

public class ISOUtil {

    private final static String URL_SLS = "202.150.163.45";
    private final static Integer PORT_SERVER = 12345;

    public String getNetworkManagement(int type) {

        try {
            GenericPackager packager = new GenericPackager("isoSLSascii.xml");

            DateFormat dateFormat = new SimpleDateFormat("CCYYMMDDhhmmss");
            Date date = new Date();
            
            String tgl = dateFormat.format(date);
            String central_identification_code = "07"+"";
            /*
             001= sign-on
             002= sign-off
             301=echo test
             */

            String action_code = "";
            if (type==1) action_code="001";
            else if (type==2) action_code = "002";
            else if (type==3) action_code = "301";
            else action_code = "001";

            String terminal_identification_code = "";

            String switcher_identification_code = "07"+"";


            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2800");
            isoMsg.set(12, tgl);
            isoMsg.set(33, central_identification_code);
            isoMsg.set(40, action_code);
            isoMsg.set(41, terminal_identification_code);
            isoMsg.set(48, switcher_identification_code);

            // print the DE list
            logISOMsg(isoMsg);

            // Get and print the output result
            byte[] data = isoMsg.pack();
            System.out.println("RESULT : " + new String(data));

            return new String(data);
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public String parseResponseNetworkManagement(ISOMsg msg) {
        String tgl = "", cid="",response_code = "",action_code="",terminal_id="",switcher_id="";

        try {
            tgl = msg.getString(12);
            cid = msg.getString(33);
            response_code = msg.getString(39);
            action_code = msg.getString(40);
            terminal_id = msg.getString(41);
            switcher_id = msg.getString(48);
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public String getInquiryRequestMsg(String msn, int msn_type) {
        String pan="", stan="", merchant_cat="",bank_code="4510017", terminal_id = "",
               cid="", swither_id="", material_number="", subscriber_id="", flag="";

        if (msn_type==0)
            material_number = msn;
        else
            subscriber_id = msn;

        flag = String.valueOf(msn_type);

            /*            
                6010=Teller
                6011=ATM
                6012=POS
                6013=AutoDebit/giralisasi
                6014=Internet
                6015=Kiosk
                6016=Phone Banking/Call Center
                6017=Mobile Banking
                6018=EDC
            */
        
        try {
            GenericPackager packager = new GenericPackager("iso2003binary.xml");

            DateFormat dateFormat = new SimpleDateFormat("CCYYMMDDhhmmss");
            Date date = new Date();

            String tgl = dateFormat.format(date);
            String.format("%05d", swither_id);

            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2100");
            isoMsg.set(2, pan);
            isoMsg.set(11, stan);
            isoMsg.set(12, tgl);
            isoMsg.set(26, merchant_cat);
            isoMsg.set(32, bank_code);
            isoMsg.set(33, cid);
            isoMsg.set(41, terminal_id);
            isoMsg.set(41, swither_id);
            isoMsg.set(48, material_number);
            isoMsg.set(12, subscriber_id);
            isoMsg.set(12, flag);
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public String parseResponseInquiryRequestMsg(ISOMsg msg) {
        String pan="", stan="", merchant_cat="",bank_code="", terminal_id = "", tgl="", mti="",
               cid="", rc="", swither_id="", material_number="", subscriber_id="", flag="",
               pln_ref_no = "", switcher_ref_no = "", subscriber_name="", customer_segmentation="",
               pwr_consuming_cat = "", minor_unit_admin_charger = "", admin_charge = "",
               distribution_code = "", service_unit = "", service_phone_unit = "", max_kwh_unit="",
               total_repeat="", pwr_purchase_unsold = "";

        /*
            0000=Successful
            0005=ERROR-Other
            0011=ERROR-Need to sign-on
            0014=ERROR-Unknown subscriber
            0030=ERROR-Invalid message
            0031=ERROR-Unregristered Bank Code
            0032=ERROR-Unregristered Switching
            0034=ERROR- Unregristered terminal
            0033=ERROR- Unregristered Product
            0068=ERROR-Timeout
            0077=ERROR-Subscriber Suspended
            0090=ERROR-Cut-off is in progress
         */

        try {
            mti = msg.getMTI();
            pan = msg.getString(2);
            stan = msg.getString(11);
            tgl = msg.getString(12);
            merchant_cat = msg.getString(26);
            bank_code = msg.getString(32);
            cid = msg.getString(33);
            rc = msg.getString(39);
            terminal_id = msg.getString(41);
            swither_id = msg.getString(48);
            material_number = msg.getString(48);
            subscriber_id = msg.getString(48);
            flag = msg.getString(48);
            pln_ref_no = msg.getString(48);
            switcher_ref_no = msg.getString(48);
            subscriber_name=msg.getString(48);
            customer_segmentation=msg.getString(48);
            pwr_consuming_cat = msg.getString(48);
            minor_unit_admin_charger = msg.getString(48);
            admin_charge = msg.getString(48);
            distribution_code = msg.getString(48);
            service_unit = msg.getString(48);
            service_phone_unit = msg.getString(48);
            max_kwh_unit=msg.getString(48);
            total_repeat=msg.getString(48);
            pwr_purchase_unsold = msg.getString(48);

        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public String getPurchaseRequestMsg() {
        String pan="", stan="", merchant_cat="",bank_code="", terminal_id = "",
               cid="", swither_id="", material_number="", subscriber_id="", flag="",
               private_msg1="", private_msg2="";

            /*
                6010=Teller
                6011=ATM
                6012=POS
                6013=AutoDebit/giralisasi
                6014=Internet
                6015=Kiosk
                6016=Phone Banking/Call Center
                6017=Mobile Banking
                6018=EDC
            */

        try {
            GenericPackager packager = new GenericPackager("isoSLSascii.xml");

            DateFormat dateFormat = new SimpleDateFormat("CCYYMMDDhhmmss");
            Date date = new Date();

            String tgl = dateFormat.format(date);
            System.out.println(String.format("%10s", "12345").replace(' ', '0'));

            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);            
            isoMsg.setMTI("2200");
            isoMsg.set(2,pan);
            isoMsg.set(4,"3600000000020000");
            isoMsg.set(11,stan);
            isoMsg.set(12,tgl);
            isoMsg.set(26,merchant_cat);
            isoMsg.set(32,bank_code);
            isoMsg.set(33,cid);
            isoMsg.set(41,terminal_id);
            isoMsg.set(48,private_msg1);
            isoMsg.set(62,private_msg2);
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    private static void logISOMsg(ISOMsg msg) {
            System.out.println("----ISO MESSAGE-----");
            try {
                    System.out.println("  MTI : " + msg.getMTI());
                    for (int i=1;i<=msg.getMaxField();i++) {
                            if (msg.hasField(i)) {
                                    System.out.println("    Field-"+i+" : "+msg.getString(i));
                            }
                    }
            } catch (ISOException e) {
                    e.printStackTrace();
            } finally {
                    System.out.println("--------------------");
            }

    }

    

}
