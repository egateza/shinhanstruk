
package isoxml;

import org.apache.log4j.*;
import java.sql.*;
import org.jpos.iso.*;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jpos.iso.packager.GenericPackager;
import pelangi_utils.Saldo;
import ppob_sls_iqbal.Settings;

import model.Mutations;

import javax.persistence.*;
import model.Users;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAPersistence;

public class PrePaid {

    Settings setting = new Settings();
    SLSLogger slsLog = new SLSLogger();
    
    Connection conx = null;
    Connection conOtomax = null;

    Saldo mutasi = new Saldo();

    int PORT_SERVER = 12345;
    String SERVER_IP = "localhost";
    String cid = "";
    String switcher_id = "";
    String bank_code = "";
    int sleepTime = 0;
    
    private static Logger logger = Logger.getLogger(PrePaid.class);
    
    public PrePaid() {

        setting.setConnections();

        this.PORT_SERVER = Integer.parseInt(setting.getPrePort());
        this.SERVER_IP = setting.getPreIP();
        this.sleepTime = Integer.parseInt(setting.getPreSleep());
        this.cid = setting.getSwitchingCID();
        this.switcher_id = setting.getSwitcherID();
        this.bank_code = setting.getBankCode();

    }
    
     

    public void refund(double nominal, String msg, int inbox_id, int user_id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();
        try {
            
            Users user = em.find(Users.class, user_id);
            int price_template_id = user.getPriceTemplateId();
            
            //get Product price
            String sq = "select m.price from ProductPrices m where m.priceTemplateId=:ptid and m.productId=:pid";
            Query qqq = em.createQuery(sq);
            qqq.setParameter("pid", 80);
            qqq.setParameter("ptid", price_template_id);
            Number priceq=(Number) qqq.getSingleResult();
            logger.log(Level.INFO,"Harga < "+inbox_id+" > : "+nominal+", admin : "+priceq.longValue());

            long prcq = priceq.longValue();
            
            em.getTransaction().begin();

            Mutations mut = new Mutations();
            mut.setAmount((long) (nominal)-prcq);
            mut.setNote(msg);
            mut.setJenis((char)'K');
            //mut.setBalance(balq);
            mut.setInboxId(inbox_id);
            mut.setUserId(user_id);
            mut.setCreateDate(new java.util.Date());

            em.persist(mut);

            em.getTransaction().commit();
        }
        catch(Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            //e.printStackTrace();
        }
        finally {
            
            if (em.isOpen()) {
                em.close();                
            }
            
            if (factory.isOpen()) {
                factory.close();
            }
            
        }
    }
    

    public String request_inquiry(String msg, String user_id, String inbox_id,
            String merchant_cat_code, String terminal_id) {
        String hasil ="";
        Socket client = null;

        try {

            logger.log(Level.INFO,"Connecting to " + SERVER_IP + " on port " + PORT_SERVER);
            client = new Socket(SERVER_IP, PORT_SERVER);
            client.setSoTimeout(this.sleepTime);

            String log = "";

            String req[] = msg.split("\\.");
            String nominal = req[1].substring(3,req[1].length());
            String id_pel = req[2];

            String flag = "0", no_meter="0", subscriber_id="0";
            
            ////////
            if (id_pel.length()==12 && id_pel.charAt(0)=='0') {
                    id_pel = id_pel.substring(1); logger.log(Level.INFO, "ID PEL correction : "+id_pel);
            }
            /////////

            if (id_pel.length()==11) {
                flag = "0";
                no_meter = id_pel;
            }
            else if (id_pel.length()==12) {
                flag = "1";
                subscriber_id = id_pel;
            }
            

            log = inquiry(id_pel, user_id, inbox_id, merchant_cat_code, terminal_id, client);

            String[] inquiryResponse = parseInquiryMsgResponse(log,true);

            //cek diminta sign on lg ga
            if (inquiryResponse[7].equalsIgnoreCase("0011")) { 
                logger.log(Level.INFO,"Signing on...");
                log = networkMsg("1",user_id,client); //sign on
                if (!log.equalsIgnoreCase("")) {
                    logger.log(Level.INFO, log);

                    if (id_pel.length()==11) {
                        flag = "0";
                        no_meter = id_pel;

                    }
                    else if (id_pel.length()==12) {
                        flag = "1";
                        subscriber_id = id_pel;
                    }

                    log = inquiry(id_pel, user_id, inbox_id, merchant_cat_code, terminal_id,client);

                    logger.log(Level.INFO, log);

                    hasil = log;
                }
            }

            logger.log(Level.INFO, log);

            hasil = log;            

            client.close();
        }
        catch(Exception e) {
            try {
                GenericPackager packager = new GenericPackager("packager/isoSLSascii.xml");

                ISOMsg isoMsg = new ISOMsg();
                isoMsg.setPackager(packager);
                
                isoMsg.setMTI("2110");
                isoMsg.set(2,"99502");            
                isoMsg.set(11,String.valueOf(inbox_id));            
                isoMsg.set(26,"6012");
                isoMsg.set(39,"0068");
                
                byte[] datax = isoMsg.pack();
                hasil = new String(datax);
                System.out.println("ISO RESULT : " + new String(datax));
                
            }
            catch(Exception isox) {
                logger.log(Level.FATAL, "ISO : "+ isox.getMessage());
            }
            
            //hasil = "21104000004002000000059950260120068";
            logger.log(Level.FATAL,e.getMessage());
            //e.printStackTrace();
        }
        finally {
            try {
                client.close();
            }
            catch(Exception e) {
                logger.log(Level.FATAL,e.getMessage());
            }
        }

        return hasil;
    }

    public String purchase_token(String msg,String trx_id, String user_id,
            String merchant_cat_code, String terminal_id) {
            
        String hasil="",nominal="0",id_pel="",log="";
        Socket client = null;
        try {
            
            String req[] = msg.split("\\.");
            nominal = req[1].substring(3,req[1].length());
            id_pel = req[2];

            String no_meter="0",subscriber_id="0",flag="0";
            
            ////////
            if (id_pel.length()==12 && id_pel.charAt(0)=='0') {
                    id_pel = id_pel.substring(1); logger.log(Level.INFO, "ID PEL correction : "+id_pel);
            }
            /////////

            if (id_pel.length()==11) {
                flag="0";
                no_meter = id_pel;
            }
            else if (id_pel.length()==12) {
                flag = "1";
                subscriber_id = id_pel;
            }
            
            
            logger.log(Level.INFO,"Connecting to " + SERVER_IP + " on port " + PORT_SERVER);
            client = new Socket(SERVER_IP, PORT_SERVER);
            client.setSoTimeout(this.sleepTime);

            log = inquiry(id_pel, user_id, trx_id, merchant_cat_code, terminal_id, client);

            if (!log.equalsIgnoreCase("")) {

                logger.log(Level.INFO, log);

                String[] inquiryResp = parseInquiryMsgResponse(log,true);

                if (inquiryResp[7].equalsIgnoreCase("0000")) {

                    logger.log(Level.INFO,"===========entering payment==============");
                    
                    log = buy(id_pel, nominal,trx_id, user_id,merchant_cat_code,terminal_id,inquiryResp[9], inquiryResp[10],client);

                    hasil = log;

                    logger.log(Level.INFO, log);

                    String[] purchaseResp = parsePurchaseMsgResponse(log,true);

                    if (purchaseResp[9].equalsIgnoreCase("0068") || purchaseResp[9].equalsIgnoreCase("")) {

                        logger.log(Level.INFO,"Entering advice...");

                        //delay 30 detik
                        try { Thread.sleep(this.sleepTime);}
                        catch(Exception e){//e.printStackTrace();
                         logger.log(Level.FATAL, e.getMessage());   
                        }

                        log = advice(id_pel, nominal,trx_id, user_id,merchant_cat_code,terminal_id,inquiryResp[9], inquiryResp[10], client);
                        hasil = log;
                        
                        String[] adv1Resp = parseAdviceResponse(log);
                        if (adv1Resp[9].equalsIgnoreCase("0068") || adv1Resp[9].equalsIgnoreCase("")) {
                            logger.log(Level.INFO,"Entering repeat 1...");

                            //delay 30 detik
                            try { Thread.sleep(this.sleepTime);}
                            catch(Exception e){//e.printStackTrace(); 
                                logger.log(Level.FATAL, e.getMessage());   
                            }

                            log = repeat(id_pel, nominal,trx_id, user_id,merchant_cat_code,terminal_id,inquiryResp[9], inquiryResp[10], client);

                            String[] adv2Resp = parseAdviceResponse(log);

                            if (adv2Resp[9].equalsIgnoreCase("0068") || adv2Resp[9].equalsIgnoreCase("")) {

                                logger.log(Level.INFO,"Entering repeat 2...");

                                //delay 30 detik
                                try { Thread.sleep(this.sleepTime);}
                                catch(Exception e){e.printStackTrace();
                                    logger.log(Level.FATAL, e.getMessage());   
                                }

                                log = repeat(id_pel, nominal,trx_id, user_id,merchant_cat_code,terminal_id,inquiryResp[9], inquiryResp[10], client);

                                String[] repeat2 = parseAdviceResponse(log);

                                if (!repeat2[9].equalsIgnoreCase("0000") && !repeat2[9].equalsIgnoreCase("0068") && !repeat2[9].equalsIgnoreCase("")) {
                                    //////////////////refund the money////////////////////////////////////////////////////////////////////////
                                    refund(Double.parseDouble(nominal),"Refund trx prepaid gagal",Integer.parseInt(trx_id),Integer.parseInt(user_id));
                                    //////////////////////////////////////////////////////////////////////////////////////////////////////////
                                }

                            }
                            else if (!adv2Resp[9].equalsIgnoreCase("0000") && !adv2Resp[9].equalsIgnoreCase("0068") && !adv2Resp[9].equalsIgnoreCase("")) {
                                //////////////////refund the money////////////////////////////////////////////////////////////////////////
                                refund(Double.parseDouble(nominal),"Refund trx prepaid gagal",Integer.parseInt(trx_id),Integer.parseInt(user_id));
                                //////////////////////////////////////////////////////////////////////////////////////////////////////////
                            }

                            hasil = log;

                        }
                        else if (!adv1Resp[9].equalsIgnoreCase("0000") && !adv1Resp[9].equalsIgnoreCase("0068") && !adv1Resp[9].equalsIgnoreCase("")) {
                            //////////////////refund the money////////////////////////////////////////////////////////////////////////
                            refund(Double.parseDouble(nominal),"Refund trx prepaid gagal",Integer.parseInt(trx_id),Integer.parseInt(user_id));
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////
                        }
                        else { //advice 1 berhasil
                            hasil = log;
                        }
                    }
                    else if (purchaseResp[9].equalsIgnoreCase("0017") || !purchaseResp[9].equalsIgnoreCase("0000")  ) {
                        logger.log(Level.INFO,"Refund due to trx prepaid gagal : "+purchaseResp[9]);
                        //////////////////refund the money due to saldo sds kurang////////////////////////////////////////////////////////////////////////
                        refund(Double.parseDouble(nominal),"Refund trx prepaid gagal "+purchaseResp[9],Integer.parseInt(trx_id),Integer.parseInt(user_id));
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////
                    }

                }
                else {
                    hasil = log;
                    
                    //////////////////refund the money////////////////////////////////////////////////////////////////////////
                    refund(Double.parseDouble(nominal),"Refund trx prepaid gagal",Integer.parseInt(trx_id),Integer.parseInt(user_id));
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////
                    logger.log(Level.INFO, log);
                    
                }
            }
                           
            client.close();
        }
        catch(Exception e) {
            
            try {
                GenericPackager packager = new GenericPackager("packager/isoSLSascii.xml");

                ISOMsg isoMsg = new ISOMsg();
                isoMsg.setPackager(packager);
                
                isoMsg.setMTI("2110");
                isoMsg.set(2,"99502");            
                isoMsg.set(11,String.valueOf(trx_id));            
                isoMsg.set(26,"6012");
                isoMsg.set(39,"0068");
                
                byte[] datax = isoMsg.pack();
                hasil = new String(datax);
                System.out.println("ISO RESULT : " + new String(datax));
                
            }
            catch(Exception isox) {
                logger.log(Level.FATAL, "ISO : "+ isox.getMessage());
            }
            
            //hasil = "21104000004002000000059950260120068";
            
            //mutasi.tambahBalance(Double.parseDouble(nominal), user_id, log, trx_id, mutasi.getBalance(user_id));
            //////////////////refund the money////////////////////////////////////////////////////////////////////////
            refund(Double.parseDouble(nominal),"Refund trx prepaid gagal",Integer.parseInt(trx_id),Integer.parseInt(user_id));
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            //e.printStackTrace();
            logger.log(Level.FATAL, "di catch "+e.getMessage()+ " # "+hasil);
            
            
        }
        finally {
            try {
                client.close();
            }
            catch(Exception e) {
                logger.log(Level.FATAL,"di final "+e.getMessage());
            }
        }

        return hasil;
    }

    public String purchase_token_unsold(String msg,String trx_id, String user_id,
            String merchant_cat_code, String terminal_id) {

        String hasil="",nominal="0",id_pel="",log="";
        Socket client = null;
        try {
            
            String req[] = msg.split("\\.");
            nominal = req[1].substring(3,req[1].length());
            id_pel = req[2];

            String no_meter="0",subscriber_id="0",flag="0";

            if (id_pel.length()==11) {
                flag="0";
                no_meter = id_pel;
            }
            else if (id_pel.length()==12) {
                flag = "1";
                subscriber_id = id_pel;
            }
            
            
            logger.log(Level.INFO,"Connecting to " + SERVER_IP + " on port " + PORT_SERVER);
            client = new Socket(SERVER_IP, PORT_SERVER);
            client.setSoTimeout(this.sleepTime);

            log = inquiry(id_pel, user_id, trx_id, merchant_cat_code, terminal_id, client);

            if (!log.equalsIgnoreCase("")) {

                logger.log(Level.INFO, log);

                String[] inquiryResp = parseInquiryMsgResponse(log,true);

                if (inquiryResp[7].equalsIgnoreCase("0000")) {

                    logger.log(Level.INFO,"===========entering payment==============");

                    log = buy_unsold(id_pel, nominal,trx_id, user_id,merchant_cat_code,terminal_id,inquiryResp[9], inquiryResp[10],client);

                    hasil = log;

                    logger.log(Level.INFO, log);

                    String[] purchaseResp = parsePurchaseMsgResponse(log,true);

                    if (purchaseResp[9].equalsIgnoreCase("0068")) {

                        logger.log(Level.INFO,"Entering advice...");

                        //delay 30 detik
                        try { Thread.sleep(this.sleepTime);}
                        catch(Exception e){//e.printStackTrace();
                            logger.log(Level.FATAL, e.getMessage());   
                        }

                        log = advice(id_pel, nominal,trx_id, user_id,merchant_cat_code,terminal_id,inquiryResp[9], inquiryResp[10], client);

                        String[] adv1Resp = parseAdviceResponse(log);
                        if (adv1Resp[9].equalsIgnoreCase("0068")) {
                            logger.log(Level.INFO,"Entering repeat 1...");

                            //delay 30 detik
                            try { Thread.sleep(this.sleepTime);}
                            catch(Exception e){//e.printStackTrace();
                                logger.log(Level.FATAL, e.getMessage());   
                            }

                            log = repeat(id_pel, nominal,trx_id, user_id,merchant_cat_code,terminal_id,inquiryResp[9], inquiryResp[10], client);

                            String[] adv2Resp = parseAdviceResponse(log);

                            if (adv2Resp[9].equalsIgnoreCase("0068")) {

                                logger.log(Level.INFO,"Entering repeat 2...");

                                //delay 30 detik
                                try { Thread.sleep(this.sleepTime);}
                                catch(Exception e){//e.printStackTrace();
                                    logger.log(Level.FATAL, e.getMessage());   
                                }

                                log = repeat(id_pel, nominal,trx_id, user_id,merchant_cat_code,terminal_id,inquiryResp[9], inquiryResp[10], client);

                                String[] repeat2 = parseAdviceResponse(log);
                                
                                if (!repeat2[9].equalsIgnoreCase("0000") && !repeat2[9].equalsIgnoreCase("0068") && !repeat2[9].equalsIgnoreCase("")) {
                                    //////////////////refund the money////////////////////////////////////////////////////////////////////////
                                    refund(Double.parseDouble(nominal),"Refund trx prepaid gagal",Integer.parseInt(trx_id),Integer.parseInt(user_id));
                                    //////////////////////////////////////////////////////////////////////////////////////////////////////////
                                }

                            }
                            else if (!adv2Resp[9].equalsIgnoreCase("0000") && !adv2Resp[9].equalsIgnoreCase("0068") && !adv2Resp[9].equalsIgnoreCase("")) {
                                //////////////////refund the money////////////////////////////////////////////////////////////////////////
                                refund(Double.parseDouble(nominal),"Refund trx prepaid gagal",Integer.parseInt(trx_id),Integer.parseInt(user_id));
                                //////////////////////////////////////////////////////////////////////////////////////////////////////////
                            }

                            hasil = log;
                            

                        }
                        else if (!adv1Resp[9].equalsIgnoreCase("0000") && !adv1Resp[9].equalsIgnoreCase("0068") && !adv1Resp[9].equalsIgnoreCase("")) {
                            //////////////////refund the money////////////////////////////////////////////////////////////////////////
                            refund(Double.parseDouble(nominal),"Refund trx prepaid gagal",Integer.parseInt(trx_id),Integer.parseInt(user_id));
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////
                        }
                        else { //advice 1 berhasil
                            hasil = log;
                        }
                    }
                    else if (purchaseResp[9].equalsIgnoreCase("0017") || !purchaseResp[9].equalsIgnoreCase("0000") ) {
                        logger.log(Level.INFO,"Refund due to saldo sds kurang");
                        //////////////////refund the money due to saldo sds kurang////////////////////////////////////////////////////////////////////////
                        refund(Double.parseDouble(nominal),"Refund trx saldo kurang",Integer.parseInt(trx_id),Integer.parseInt(user_id));
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////
                    }

                }
                else {
                    hasil = log;
                    //////////////////refund the money////////////////////////////////////////////////////////////////////////
                    refund(Double.parseDouble(nominal),"Refund trx prepaid gagal",Integer.parseInt(trx_id),Integer.parseInt(user_id));
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //mutasi.tambahBalance(Double.parseDouble(nominal), user_id, log, trx_id, String.valueOf(mutasi.getBalance(user_id)));
                    logger.log(Level.INFO, log);

                }
            }

            client.close();
        }
        catch(Exception e) {
            try {
                GenericPackager packager = new GenericPackager("packager/isoSLSascii.xml");

                ISOMsg isoMsg = new ISOMsg();
                isoMsg.setPackager(packager);
                
                isoMsg.setMTI("2110");
                isoMsg.set(2,"99502");            
                isoMsg.set(11,String.valueOf(trx_id));            
                isoMsg.set(26,"6012");
                isoMsg.set(39,"0068");
                
                byte[] datax = isoMsg.pack();
                hasil = new String(datax);
                System.out.println("ISO RESULT : " + new String(datax));
                
            }
            catch(Exception isox) {
                logger.log(Level.FATAL, "ISO : "+ isox.getMessage());
            }
            
            //hasil = "21104000004002000000059950260120068";
            //////////////////refund the money////////////////////////////////////////////////////////////////////////
            refund(Double.parseDouble(nominal),"Refund trx prepaid gagal",Integer.parseInt(trx_id),Integer.parseInt(user_id));
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            //e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage()+" # "+hasil);
        }
        finally {
            try {
                client.close();
            }
            catch(Exception e) {
                logger.log(Level.FATAL,e.getMessage());
            }
        }

        return hasil;
    }


    public String adviceManual(String msg,String trx_id, String user_id,
            String merchant_cat_code, String terminal_id, String _48, String _62) {

        String hasil="",nominal="0",id_pel="",log="";
        Socket client = null;

        try {
            
            String req[] = msg.split("\\.");
            nominal = req[1].substring(3,req[1].length());
            id_pel = req[2];

            String no_meter="0",subscriber_id="0",flag="0";
            
            ////////
            if (id_pel.length()==12 && id_pel.charAt(0)=='0') {
                    id_pel = id_pel.substring(1); logger.log(Level.INFO, "ID PEL correction : "+id_pel);
            }
            /////////

            if (id_pel.length()==11) {
                flag="0";
                no_meter = id_pel;
            }
            else if (id_pel.length()==12) {
                flag = "1";
                subscriber_id = id_pel;
            }
            

            logger.log(Level.INFO,"Connecting to " + SERVER_IP + " on port " + PORT_SERVER);
            client = new Socket(SERVER_IP, PORT_SERVER);
            client.setSoTimeout(this.sleepTime);

            logger.log(Level.INFO,"Entering advice manual...");

            log = advice_manual(id_pel, nominal,trx_id, user_id,merchant_cat_code,terminal_id,_48, _62, client);                
                
            hasil = log;

            client.close();
        }
        catch(Exception e) {
            
            try {
                GenericPackager packager = new GenericPackager("packager/isoSLSascii.xml");

                ISOMsg isoMsg = new ISOMsg();
                isoMsg.setPackager(packager);
                
                isoMsg.setMTI("2110");
                isoMsg.set(2,"99502");            
                isoMsg.set(11,String.valueOf(trx_id));            
                isoMsg.set(26,"6012");
                isoMsg.set(39,"0068");
                
                byte[] datax = isoMsg.pack();
                hasil = new String(datax);
                System.out.println("ISO RESULT : " + new String(datax));
                
            }
            catch(Exception isox) {
                logger.log(Level.FATAL, "ISO : "+ isox.getMessage());
            }
            
            //hasil = "21104000004002000000059950260120068";
            
            //e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage()+" # "+hasil);
        }
        finally{
            try {
                client.close();
            } catch (IOException ex) {
                logger.log(Level.FATAL,ex.getMessage());
            }
        }
        
        return hasil;
    }

    public static String[] parseBit48Inquiry(String msg, String rc) {
        String[] hasil = new String[65];
        int[] seq = {7,11,12,1,32,32,25,4,9,1,10};
        String[] title = {
            "switcher id",
            "meter serial number",
            "subscriber id",
            "flag",
            "pln ref no",
            "switcher ref no",            
            "Subscriber name",
            "subscriber segmentation",
            "Power consuming cat",            
            "minor unit admin charge",
            "admin charge"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";

                if (rc.equalsIgnoreCase("0000")) {
                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
                        System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
                        System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                n = l;
            }

        } catch (Exception e) {
            logger.log(Level.FATAL,e.getMessage());
            //e.printStackTrace();
        }

        return hasil;
    }

    public static String[] parseBit48Purchase(String msg) {
        String[] hasil = new String[65];
        System.out.println("String to parse : "+msg);

        if (msg.length()==241) {
            int[] seq = {7,11,12,1,32,32,8,25,4,9,1,1,10,1,10,1,10,1,10,1,10,1,12,1,10,20};
            String[] title = {
                "switcher id", //0
                "meter serial number", //1
                "subscriber id", //2
                "flag", //3
                "pln ref no", //4
                "switcher ref no", //5
                "vending receive no", //6
                "Subscriber name", //7
                "subscriber segmentation", //8
                "Power consuming cat", //9
                "buying option", //10
                "minor unit admin charge", //11
                "admin charge", //12
                "minor stamp of duty", //13
                "stamp of duty", //14
                "minor unit vat", //15
                "vat", //16
                "minor unit lampu jalan ", //17
                "lampu jalan", //18
                "minor unit angsuran", //19
                "angsuran", //20
                "minor unit power purchase", //21
                "power purchase", //22
                "minor unit power purchase kwh unit", //23
                "purchase kwh unit", //24
                "token number"  //25

            };
            try {
                int n = 0;
                int f = 0;
                int l = 0;
                for (int i = 0; i < seq.length; i++) {
                    hasil[i] = "";

                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
                        //System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l); //System.out.println("f, l : "+f+","+l);
                        //System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                    n = l;
                }

            } catch (Exception e) {
                logger.log(Level.FATAL,e.getMessage());
                //e.printStackTrace();

            }

        }
        else {

            int[] seq = {7,11,12,1,32,32,8,25,4};
            String[] title = {
                "switcher id", //0
                "meter serial number", //1
                "subscriber id", //2
                "flag", //3
                "pln ref no", //4
                "switcher ref no", //5
                "vending receive no", //6
                "Subscriber name", //7
                "subscriber segmentation" //8
                

            };

            try {
                int n = 0;
                int f = 0;
                int l = 0;
                for (int i = 0; i < seq.length; i++) {
                    hasil[i] = "";

                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
                        //System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l); System.out.println("f, l : "+f+","+l);
                        //System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                    n = l;
                }



            } catch (Exception e) {
                logger.log(Level.FATAL,e.getMessage());
                //e.printStackTrace();

            }

        }

        return hasil;
    }

    public String[] parseBit62Inquiry(String msg, String rc) {
        String[] hasil = new String[64];
        int[] seq = {2,5,15,5,1};
        String[] title = {
            "distribution code", //0
            "service unit", //1
            "service unit phone", //2
            "max kwh unit", //3
            "total repeat", //4
            "power purchase unsold" //5
        };

        try {

             int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";

                if (seq[i] == 1) {
                    hasil[i] = String.valueOf(msg.charAt(l));
                    //System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    l += seq[i];
                } else if (seq[i] > 1) {
                    f = n;
                    l = n + seq[i];
                    hasil[i] = msg.substring(f, l); //System.out.println("f, l : "+f+","+l);
                    //System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                }
                n = l;
            }

            if (Integer.parseInt(hasil[4])>0) {
                hasil[5] = msg.substring(l, l+11); //System.out.println("f, l : "+f+","+l);
                //System.out.println(String.format("%-50s", "pp unsold 1") + " : " + String.format("%-30s", hasil[5] ) + " ==> " + hasil[5].length());
            }

            if (Integer.parseInt(hasil[4])>1) {
                hasil[6] = msg.substring(l+11, l+22); //System.out.println("f, l : "+l+","+l+11);
                //System.out.println(String.format("%-50s", "pp unsold 2") + " : " + String.format("%-30s", hasil[6] ) + " ==> " + hasil[6].length());
            }

        }
        catch(Exception e) {
            //e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
        }

        return hasil;
    }

    public String[] parseBit62Purchase(String msg) {
        String[] hasil = new String[64];

        int[] seq = {2,5,15,5,1};
        String[] title = {
            "distribution code","service unit","service unit phone","max kwh unit","total repeat","power purchase unsold"
        };

        try {

            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";

                if (seq[i] == 1) {
                    hasil[i] = String.valueOf(msg.charAt(l));
                    //System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    l += seq[i];
                } else if (seq[i] > 1) {
                    f = n;
                    l = n + seq[i];
                    hasil[i] = msg.substring(f, l);
                    //System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                }
                n = l;
            }

            if (Integer.parseInt(hasil[4])==1) {
                hasil[5] = msg.substring(l, l+11); //System.out.println("f, l : "+f+","+l);
                //System.out.println(String.format("%-50s", "pp unsold 1") + " : " + String.format("%-30s", hasil[5] ) + " ==> " + hasil[5].length());
            }

            if (Integer.parseInt(hasil[4])==2) {
                hasil[6] = msg.substring(l+11, l+22); //System.out.println("f, l : "+l+","+l+11);
                //System.out.println(String.format("%-50s", "pp unsold 2") + " : " + String.format("%-30s", hasil[6] ) + " ==> " + hasil[6].length());
            }

        }
        catch(Exception e) {
            //e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
        }

        return hasil;
    }

    public String[] parsePurchaseMsgResponse(String iso, boolean isLogged) {
        String[] hasil = new String[17];

        //System.out.println("ISO to unpack : "+iso);
        System.out.println(">>>Entering parsing iso purchase<<<<");

        try {

            if (iso.length()>10) {
                GenericPackager packager = new GenericPackager("packager/isoSLSascii.xml");

                ISOMsg isoMsg = new ISOMsg();
                isoMsg.setPackager(packager);
                isoMsg.unpack(iso.getBytes());

                hasil[0] = isoMsg.getMTI();
                hasil[1] = isoMsg.getString(2); //pan
                hasil[2] = isoMsg.getString(4); //trx amount (curr code, curr unit, value )
                hasil[3] = isoMsg.getString(11); // switcher trace audit number
                hasil[4] = isoMsg.getString(12); //datetime
                hasil[5] = isoMsg.getString(15); //date settlement
                hasil[6] = isoMsg.getString(26); //merchant cat code
                hasil[7] = isoMsg.getString(32); //bank code
                hasil[8] = isoMsg.getString(33); //cid
                hasil[9] = isoMsg.getString(39); //response code
                hasil[10] = isoMsg.getString(41); //terminal id
                hasil[11] = isoMsg.getString(48); //private data 1
                hasil[12] = isoMsg.getString(62); //private data 2
                hasil[13] = isoMsg.getString(63); //info text
            }
            else {
                hasil[9] = "";
            }

            return hasil;
        }
        catch(Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public String[] parseAdviceResponse(String iso) {
        String[] hasil = new String[17];

        try {
            GenericPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            ISOMsg isoMsg = new ISOMsg();
	    isoMsg.setPackager(packager);
	    isoMsg.unpack(iso.getBytes());

            hasil[0] = isoMsg.getMTI();
            hasil[1] = isoMsg.getString(2);
            hasil[2] = isoMsg.getString(4);
            hasil[3] = isoMsg.getString(11);
            hasil[4] = isoMsg.getString(12);
            hasil[5] = isoMsg.getString(15);
            hasil[6] = isoMsg.getString(26);
            hasil[7] = isoMsg.getString(32);
            hasil[8] = isoMsg.getString(33);
            hasil[9] = isoMsg.getString(39);
            hasil[10] = isoMsg.getString(41);
            hasil[11] = isoMsg.getString(48);
            hasil[12] = isoMsg.getString(62);
            hasil[13] = isoMsg.getString(63);

            return hasil;
        }
        catch(Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            //e.printStackTrace();
        }

        return hasil;
    }

    public String[] parseInquiryMsgResponse(String iso, boolean isLogged) {
        String[] hasil = new String[17];

        try {
            GenericPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            ISOMsg isoMsg = new ISOMsg();
	    isoMsg.setPackager(packager);
	    isoMsg.unpack(iso.getBytes());

            hasil[0] = isoMsg.getMTI();
            hasil[1] = isoMsg.getString(2); //PAN
            hasil[2] = isoMsg.getString(11); //Switcher trace number
            hasil[3] = isoMsg.getString(12); // datetime, local trx
            hasil[4] = isoMsg.getString(26); // merchant cat code
            hasil[5] = isoMsg.getString(32); // bank code
            hasil[6] = isoMsg.getString(33); //cid
            hasil[7] = isoMsg.getString(39); //RC
            hasil[8] = isoMsg.getString(41); // terminal id
            hasil[9] = isoMsg.getString(48); //private data 1
            
            if (hasil[7].equalsIgnoreCase("0000"))
                hasil[10] = isoMsg.getString(62); //private data 2

            return hasil;
        }
        catch(Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public String[] parseNetworkMsgResponse(String iso) {
        String[] hasil = new String[10];

        try {
            GenericPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            ISOMsg isoMsg = new ISOMsg();
	    isoMsg.setPackager(packager);
	    isoMsg.unpack(iso.getBytes());

            hasil[0] = isoMsg.getMTI();
            hasil[1] = isoMsg.getString(12);
            hasil[2] = isoMsg.getString(33);
            hasil[3] = isoMsg.getString(39);
            hasil[4] = isoMsg.getString(40);
            hasil[5] = isoMsg.getString(41);
            hasil[6] = isoMsg.getString(48);

            return hasil;
        }
        catch(Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            //e.printStackTrace();
        }

        return hasil;
    }
  
    public String inquiry(String id_pel,String user_id, String inbox_id,
            String merchant_cat_code, String terminal_id, Socket client) {
        String hasil = "";

        try {

            logger.log(Level.INFO,"Entering inquiry");

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2100");
            isoMsg.set(2,"99502");
            isoMsg.set(11,String.format("%12s", inbox_id).replace(' ', '0'));
            isoMsg.set(12,tgl);
            isoMsg.set(26,merchant_cat_code);
            isoMsg.set(32,this.bank_code);
            isoMsg.set(33,this.cid);
            isoMsg.set(41,terminal_id);

            String no_meter="0", subscriber_id="0",flag="0";

            if (id_pel.length()==11) {
                subscriber_id = "000000000000";
                no_meter = id_pel;
                flag = "0";
            }
            else if (id_pel.length()==12) {
                flag = "1";
                subscriber_id = id_pel;
                no_meter = "00000000000";
            }
            
            String bit48 = this.switcher_id+no_meter+subscriber_id+flag;

            isoMsg.set(48,bit48);

            byte[] datax = isoMsg.pack();
            logger.log(Level.INFO,"SENDING : " + new String(datax));
            String networkRequest = new String(datax);
            
            //String trailer = new String(new char[] {255});
            String trailer = new String(new char[] {10});
            
            networkRequest = networkRequest+trailer;
            
//            DataOutputStream outgoing = new DataOutputStream(client.getOutputStream());
//            DataInputStream incoming = new DataInputStream(client.getInputStream());
//
//            outgoing.writeBytes(networkRequest);

            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());
            
            outgoing.print(networkRequest);
            outgoing.flush();
            
            int data;
            StringBuffer sb = new StringBuffer();

            while((data = incoming.read()) != -1 ) {
                if (data==-1 || data==255 || data==10 || data==65533 )
                    break;

                sb.append((char) data);
            }

            logger.log(Level.INFO,"RECEIVE : "+sb.toString());
           
//            incoming.close();
//            outgoing.close();

            return sb.toString();
            
        }
        catch(Exception e) {
            logger.log(Level.FATAL,"Error Prepaid inquiry : "+e.getMessage());
            //e.printStackTrace();
        }

        return "Error";
    }

    public String networkMsg(String type, String terminal_id, Socket client) {
        String hasil = "";

        logger.log(Level.INFO,"Entering Network Message type : "+type);

        try {

            if (terminal_id.length()<12)
                terminal_id = String.format("%16s", terminal_id).replace(' ', '0');

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");
            //ISOPackager packager = new CustomPackager2003();
            
            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2800");
            isoMsg.set(12, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
            isoMsg.set(33, this.cid); logger.log(Level.INFO,"CID : "+this.cid);

            if (type.equalsIgnoreCase("1")) //sign on
                isoMsg.set(40, "001");
            else if (type.equalsIgnoreCase("2")) //sign off
                isoMsg.set(40, "002");
            else if (type.equalsIgnoreCase("3")) //echo test
                isoMsg.set(40, "301");
            else
                isoMsg.set(40, "001");

            isoMsg.set(41, terminal_id); //System.out.println("Terminal ID : "+terminal_id);
            isoMsg.set(48, this.switcher_id); logger.log(Level.INFO,"Switcher ID : "+this.switcher_id);

            byte[] datax = isoMsg.pack();
            logger.log(Level.INFO,"SENDING NETMSG : " + new String(datax));
            String networkRequest = new String(datax);
            
            String trailer = new String(new char[] {10});
            
            networkRequest = networkRequest+trailer;
            
//            DataOutputStream outgoing = new DataOutputStream(client.getOutputStream());
//            DataInputStream incoming = new DataInputStream(client.getInputStream());
//
//            outgoing.writeBytes(networkRequest);

            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            outgoing.print(networkRequest);
            outgoing.flush();

            int data;
            StringBuffer sb = new StringBuffer();

            while((data = incoming.read()) != -1 ) {
                if (data==-1 || data==255 || data==10 || data==65533)
                    break;

                sb.append((char) data);
            }

            logger.log(Level.INFO,"RECEIVE NETMSG : "+sb.toString());
            
//            incoming.close();
//            outgoing.close();

            return sb.toString();
        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage());
            //e.printStackTrace();
        }

        return hasil; 
    }

    public String buildXMLPurchase(String isoMsg) {
        String hasil = "";
        logger.log(Level.INFO,"build xml");
        try {

            String[] result = parsePurchaseMsgResponse(isoMsg,false);

            String amount = result[2].substring(4);

            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<amount>"+amount+"</amount>"
                    + "<switch_refno>"+result[3]+"</switch_refno>"
                    + "<datetime>"+result[4]+"</datetime>"
                    + "<date_settlement>"+result[5]+"</date_settlement>"
                    + "<merchant_category>"+result[6]+"</merchant_category>"
                    + "<bank_code>"+result[7]+"</bank_code>"
                    + "<response_code>"+result[9]+"</response_code>"
                    + "<response_code_desc>"+result[9]+"</response_code_desc>"
                    + "<terminal_id>"+result[10]+"</terminal_id>";

            if (result[9].equalsIgnoreCase("0000")) {

                String switcher_id = result[11].substring(0, 6);
                String material_number = result[9].substring(7, 17);
                String subscriber_id = result[9].substring(18, 29);
                String flag = result[9].substring(30, 30);
                String plnref = result[9].substring(31,62);
                String switcher_refno = result[9].substring(63, 94);
                String subscriber_name = result[9].substring(95, 129);
                String subscriber_segmentation = result[9].substring(130, 133);
                String power_category = result[9].substring(134, 143);
                String admin_charge_unit = result[9].substring(144, 144);
                String admin_charge = result[9].substring(145, 153);

                String distribution_code = result[10].substring(0, 1);
                String service_unit = result[10].substring(2, 6);
                String service_phone_unit = result[10].substring(7, 21);
                String max_kwh_unit = result[10].substring(22, 26);
                String total_repeat = result[10].substring(27, 27);
                String power_purchase_unsold = result[10].substring(28, 38);

                hasil  += "<material_number>"+material_number+"</material_number>"
                        + "<subscriber_id>"+subscriber_id+"</subscriber_id>"
                        + "<plnref>"+plnref+"</plnref>"
                        + "<switcher_refno>"+switcher_refno+"</switcher_refno>"
                        + "<subscriber_name>"+subscriber_name+"</subscriber_name>"
                        + "<subscriber_segmentation>"+subscriber_segmentation+"</subscriber_segmentation>"
                        + "<power_category>"+power_category+"</power_category>"
                        + "<admin_charge_unit>"+admin_charge_unit+"</admin_charge_unit>"
                        + "<admin_charge>"+admin_charge+"</admin_charge>"
                        + "<distribution_code>"+distribution_code+"</distribution_code>"
                        + "<service_unit>"+service_unit+"</service_unit>"
                        + "<service_phone_unit>"+service_phone_unit+"</service_phone_unit>"
                        + "<max_kwh_unit>"+max_kwh_unit+"</max_kwh_unit>"
                        + "<total_repeat>"+total_repeat+"</total_repeat>"
                        + "<power_purchase_unsold>"+power_purchase_unsold+"</power_purchase_unsold>";
            }
            else {

                String switcher_id = result[9].substring(0, 6);
                String material_number = result[9].substring(7, 17);
                String subscriber_id = result[9].substring(18, 29);
                String flag = result[9].substring(30, 30);
                String plnref = result[9].substring(31,62);
                String switcher_refno = result[9].substring(63, 94);

                String total_repeat = result[10].substring(27, 27);

                hasil  += "<material_number>"+material_number+"</material_number>"
                        + "<subscriber_id>"+subscriber_id+"</subscriber_id>"
                        + "<plnref>"+plnref+"</plnref>"
                        + "<switcher_refno>"+switcher_refno+"</switcher_refno>"
                        + "<total_repeat>"+total_repeat+"</total_repeat>";
            }

            hasil   += "</response>";

        }
        catch(Exception e) {
            //e.printStackTrace();
            logger.log(Level.FATAL,e.getMessage());
        }

        return hasil;
    }

    public String buildXMLAdvice(String isoMsg) {
        return "";
    }

    public String buy(String id_pel,String nominal, String trx_id, String user_id, 
            String merchant_cat_code, String terminal_id, 
            String inquiry_response48, String inquiry_response62, Socket client) {
        String hasil = "";
        try {

            logger.log(Level.INFO,"Entering buy");

            if (terminal_id.length()<12)
                terminal_id = String.format("%16s", terminal_id).replace(' ', '0');

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");
                        
            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2200");
            isoMsg.set(2,"99502");

            String curr_code = "360";
            String minor_unit = "0";
            String amount = String.format("%12s", nominal).replace(' ', '0');

            String bit4 = curr_code + minor_unit + amount;

            isoMsg.set(4,bit4);

            String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

            String pp_unsold = "0";

            isoMsg.set(11,String.format("%12s", trx_id).replace(' ', '0'));
            isoMsg.set(12,tgl);
            isoMsg.set(26,merchant_cat_code);
            isoMsg.set(32,this.bank_code);
            isoMsg.set(33,this.cid);
            isoMsg.set(41,terminal_id);
            isoMsg.set(48,inquiry_response48+pp_unsold);
            isoMsg.set(62,inquiry_response62);

            byte[] datax = isoMsg.pack();
            logger.log(Level.INFO,"PURCHASE SENDING : " + new String(datax));
            String networkRequest = new String(datax);

            //log to file
            try {

                String bit48Inq[] = parseBit48Inquiry(inquiry_response48,"0000");
                
                String toLog = tgl+"|"
                        + bit48Inq[4] +"|"  //pln ref
                        + bit48Inq[5] + "|"   //switch ref
                        + bit48Inq[1] + "|" //no meter
                        + amount + "|"
                        + "PURCHASE_REQUEST_STREAM";
                slsLog.logAction(toLog, "99502");
            }
            catch(Exception e) {
                //e.printStackTrace();
                logger.log(Level.FATAL,e.getMessage());
            }
            
            String trailer = new String(new char[] {10});
            
            networkRequest = networkRequest+trailer;
            
//            DataOutputStream outgoing = new DataOutputStream(client.getOutputStream());
//            DataInputStream incoming = new DataInputStream(client.getInputStream());
//
//            outgoing.writeBytes(networkRequest);

            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            outgoing.print(networkRequest);
            outgoing.flush();

            int data;
            StringBuffer sb = new StringBuffer();

            while((data = incoming.read()) != -1 ) {
                if (data==-1 || data==255 || data==10 || data==65533)
                    break;

                sb.append((char) data);
            }

            logger.log(Level.INFO,"PURCHASE RECEIVE : "+sb.toString());
            
//            incoming.close();
//            outgoing.close();

            return sb.toString();
            
        }
        catch(Exception e) {
            logger.log(Level.ERROR,e.toString());
            e.printStackTrace();
        }

        return hasil;
    }

    public String buy_unsold(String id_pel,String nominal, String trx_id, String user_id,
            String merchant_cat_code, String terminal_id,
            String inquiry_response48, String inquiry_response62, Socket client) {
        String hasil = "";
        try {

            logger.log(Level.INFO,"Entering buy");

            if (terminal_id.length()<12)
                terminal_id = String.format("%16s", terminal_id).replace(' ', '0');

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2200");
            isoMsg.set(2,"99502");

            String curr_code = "360";
            String minor_unit = "0";
            String amount = String.format("%12s", nominal).replace(' ', '0');

            String bit4 = curr_code + minor_unit + amount;

            isoMsg.set(4,bit4);

            String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

            String pp_unsold = "1";

            isoMsg.set(11,String.format("%12s", trx_id).replace(' ', '0'));
            isoMsg.set(12,tgl);
            isoMsg.set(26,merchant_cat_code);
            isoMsg.set(32,this.bank_code);
            isoMsg.set(33,this.cid);
            isoMsg.set(41,terminal_id);
            isoMsg.set(48,inquiry_response48+pp_unsold);
            isoMsg.set(62,inquiry_response62);

            byte[] datax = isoMsg.pack();
            logger.log(Level.INFO,"SENDING : " + new String(datax));
            String networkRequest = new String(datax);
            
            String trailer = new String(new char[] {10});
            
            networkRequest = networkRequest+trailer;
            
//            DataOutputStream outgoing = new DataOutputStream(client.getOutputStream());
//            DataInputStream incoming = new DataInputStream(client.getInputStream());
//
//            outgoing.writeBytes(networkRequest);

            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            outgoing.print(networkRequest);
            outgoing.flush();

            int data;
            StringBuffer sb = new StringBuffer();

            while((data = incoming.read()) != -1 ) {
                if (data==-1 || data==255 || data==10 || data==65533)
                    break;

                sb.append((char) data);
            }
            
//            incoming.close();
//            outgoing.close();

            logger.log(Level.INFO,"RECEIVE : "+sb.toString());

            
            return sb.toString();

        }
        catch(Exception e) {
            logger.log(Level.ERROR,e.getMessage());
            //e.printStackTrace();
        }

        return hasil;
    }

    public String advice(String id_pel,String nominal, String trx_id, String user_id,
            String merchant_cat_code, String terminal_id,
            String inquiry_response48, String inquiry_response62, Socket client) {
        String hasil = "";

        logger.log(Level.INFO,"====Advice iso====");

        try {

            logger.log(Level.INFO,"Entering advice");

            if (terminal_id.length()<16)
                terminal_id = String.format("%16s", terminal_id).replace(' ', '0');

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2220");
            isoMsg.set(2,"99502");

            String curr_code = "360";
            String minor_unit = "0";
            String amount = String.format("%12s", nominal).replace(' ', '0');

            String bit4 = curr_code + minor_unit + amount;

            isoMsg.set(4,bit4);
            isoMsg.set(11,String.format("%12s", trx_id).replace(' ', '0'));
            isoMsg.set(12,new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
            isoMsg.set(26,merchant_cat_code);
            isoMsg.set(32,this.bank_code);
            isoMsg.set(33,this.cid);
            isoMsg.set(41,terminal_id);
            isoMsg.set(48,inquiry_response48);
            isoMsg.set(62,inquiry_response62);

            byte[] datax = isoMsg.pack();
            logger.log(Level.INFO,"SENDING : " + new String(datax));
            String networkRequest = new String(datax);

            //////////////////SENDING ISO MSG////////////////////
            
            String trailer = new String(new char[] {10});
            
            networkRequest = networkRequest+trailer;
            
//            DataOutputStream outgoing = new DataOutputStream(client.getOutputStream());
//            DataInputStream incoming = new DataInputStream(client.getInputStream());
//
//            outgoing.writeBytes(networkRequest);

            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            outgoing.print(networkRequest);
            outgoing.flush();

            int data;
            StringBuffer sb = new StringBuffer();

            while((data = incoming.read()) != -1 ) {
                if (data==-1 || data==255 || data==10 || data==65533)
                    break;

                sb.append((char) data);
            }

            logger.log(Level.INFO,"RECEIVE : "+sb.toString());

//            incoming.close();
//            outgoing.close();

            return sb.toString();

        }
        catch(Exception e) {
            logger.log(Level.ERROR,e.getMessage());
            //e.printStackTrace();
        }

        return hasil;
    }

    public String advice_manual(String id_pel,String nominal, String trx_id, String user_id,
            String merchant_cat_code, String terminal_id,
            String inquiry_response48, String inquiry_response62, Socket client) {
        String hasil = "";

        logger.log(Level.INFO,"====Manual Advice iso====");

        try {

            logger.log(Level.INFO,"Entering manual advice");

            if (terminal_id.length()<16)
                terminal_id = String.format("%16s", terminal_id).replace(' ', '0');

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2220");
            isoMsg.set(2,"99502");

            String curr_code = "360";
            String minor_unit = "0";
            String amount = String.format("%12s", nominal).replace(' ', '0');

            String bit4 = curr_code + minor_unit + amount;

            isoMsg.set(4,bit4);
            isoMsg.set(11,String.format("%12s", trx_id).replace(' ', '0'));
            isoMsg.set(12,new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
            isoMsg.set(26,merchant_cat_code);
            isoMsg.set(32,this.bank_code);
            isoMsg.set(33,this.cid);
            isoMsg.set(41,terminal_id);
            isoMsg.set(48,inquiry_response48.toUpperCase());
            isoMsg.set(62,inquiry_response62.toUpperCase());

            byte[] datax = isoMsg.pack();
            logger.log(Level.INFO,"SENDING : " + new String(datax));
            String networkRequest = new String(datax);

            //////////////////SENDING ISO MSG////////////////////
            
            String trailer = new String(new char[] {10});
            
            networkRequest = networkRequest+trailer;
            
//            DataOutputStream outgoing = new DataOutputStream(client.getOutputStream());
//            DataInputStream incoming = new DataInputStream(client.getInputStream());
//
//            outgoing.writeBytes(networkRequest);

            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            outgoing.print(networkRequest);
            outgoing.flush();

            int data;
            StringBuffer sb = new StringBuffer();

            while((data = incoming.read()) != -1 ) {
                if (data==-1 || data==255 || data==10 || data==65533)
                    break;

                sb.append((char) data);
            }

            logger.log(Level.INFO,"RECEIVE : "+sb.toString());
           
//            incoming.close();
//            outgoing.close();
            
            return sb.toString();

        }
        catch(Exception e) {
            logger.log(Level.ERROR,e.getMessage());
            //e.printStackTrace();
        }

        return hasil;
    }

    public String repeat(String id_pel,String nominal, String trx_id, String user_id,
            String merchant_cat_code, String terminal_id,
            String inquiry_response48, String inquiry_response62, Socket client) {
        String hasil = "";

        logger.log(Level.INFO,"====Advice Repeat iso====");

        try {

            logger.log(Level.INFO,"Entering advice");

            if (terminal_id.length()<16)
                terminal_id = String.format("%16s", terminal_id).replace(' ', '0');

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");
            
            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2221");
            isoMsg.set(2,"99502");

            String curr_code = "360";
            String minor_unit = "0";
            String amount = String.format("%12s", nominal).replace(' ', '0');

            String bit4 = curr_code + minor_unit + amount;

            isoMsg.set(4,bit4);
            isoMsg.set(11,String.format("%12s", trx_id).replace(' ', '0'));
            isoMsg.set(12,new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
            isoMsg.set(26,merchant_cat_code);
            isoMsg.set(32,this.bank_code);
            isoMsg.set(33,this.cid);
            isoMsg.set(41,terminal_id);
            isoMsg.set(48,inquiry_response48);
            isoMsg.set(62,inquiry_response62);

            byte[] datax = isoMsg.pack();
            logger.log(Level.INFO,"SENDING : " + new String(datax));
            String networkRequest = new String(datax);

            //////////////////SENDING ISO MSG////////////////////
            
            String trailer = new String(new char[] {10});
            
            networkRequest = networkRequest+trailer;
            
//            DataOutputStream outgoing = new DataOutputStream(client.getOutputStream());
//            DataInputStream incoming = new DataInputStream(client.getInputStream());
//
//            outgoing.writeBytes(networkRequest);

            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            outgoing.print(networkRequest);
            outgoing.flush();

            int data;
            StringBuffer sb = new StringBuffer();

            while((data = incoming.read()) != -1 ) {
                if (data==-1 || data==255 || data==10 || data==65533)
                    break;

                sb.append((char) data);
            }

            logger.log(Level.INFO,"RECEIVE : "+sb.toString());
         
//            incoming.close();
//            outgoing.close();
            
            return sb.toString();
            
        }
        catch(Exception e) {
            logger.log(Level.ERROR,e.getMessage());
            //e.printStackTrace();
        }

        return hasil;
    }

}
