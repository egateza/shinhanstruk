
package isoxml;

/**
 *
 * @author ngonar
 */
public class NonTagListResponseCode {

    public NonTagListResponseCode() {

    }

    public String getReversalMsgResponseName(String code) {
        if (code.equalsIgnoreCase("0000")) {
            return "SUCCESSFUL";
        }
        else if(code.equalsIgnoreCase("0004")) {
            return "ERROR-UNREGISTERED BILLER";
        }
        else if(code.equalsIgnoreCase("0005")) {
            return "ERROR-OTHER";
        }
        else if(code.equalsIgnoreCase("0006")) {
            return "ERROR-blocked partner central";
        }
        else if(code.equalsIgnoreCase("0007")) {
            return "ERROR-blocked terminal";
        }
        else if(code.equalsIgnoreCase("0008")) {
            return "ERROR-invalid access time";
        }
        else if(code.equalsIgnoreCase("0011")) {
            return "ERROR-Need to sign on";
        }
        else if(code.equalsIgnoreCase("0012")) {
            return "ERROR-Reversal melebihi batas waktu";
        }
        else if(code.equalsIgnoreCase("0013")) {
            return "ERROR-Invalid Transaction Amount";
        }
        else if(code.equalsIgnoreCase("0014")) {
            return "ERROR-Unknown subscriber";
        }
        else if(code.equalsIgnoreCase("0015")) {
            return "ERROR-Unknown registration number";
        }
        else if(code.equalsIgnoreCase("0016")) {
            return "ERROR-PRR subscriber";
        }
        else if(code.equalsIgnoreCase("0017")) {
            return "ERROR-Subscriber still have bills to pay";
        }
        else if(code.equalsIgnoreCase("0030")) {
            return "ERROR-Invalid message";
        }
        else if(code.equalsIgnoreCase("0031")) {
            return "ERROR-Unregistered Bank Code";
        }
        else if(code.equalsIgnoreCase("0032")) {
            return "ERROR-Unregistered Switching";
        }
        else if(code.equalsIgnoreCase("0033")) {
            return "ERROR-Unregistered Product";
        }
        else if(code.equalsIgnoreCase("0034")) {
            return "ERROR-Unregistered Terminal";
        }
        else if(code.equalsIgnoreCase("0041")) {
            return "ERROR-Transaction Amount below minimum purchase amount";
        }
        else if(code.equalsIgnoreCase("0042")) {
            return "ERROR-Transaction Amount exceed maximum purchase amount";
        }
        else if(code.equalsIgnoreCase("0045")) {
            return "ERROR-Invalid admin charge";
        }
        else if(code.equalsIgnoreCase("0046")) {
            return "ERROR-Insufficient Deposit";
        }
        else if(code.equalsIgnoreCase("0047")) {
            return "ERROR-Total KWH is over the limit";
        }
        else if(code.equalsIgnoreCase("0063")) {
            return "ERROR-No payment";
        }
        else if(code.equalsIgnoreCase("0068")) {
            return "ERROR-Timeout";
        }
        else if(code.equalsIgnoreCase("0077")) {
            return "ERROR-Subscriber suspended";
        }
        else if(code.equalsIgnoreCase("0088")) {
            return "ERROR-Bills already paid";
        }
        else if(code.equalsIgnoreCase("0089")) {
            return "ERROR-Current Bill is not available";
        }
        else if(code.equalsIgnoreCase("0090")) {
            return "ERROR-Cut-off is in progress";
        }
        else if(code.equalsIgnoreCase("0092")) {
            return "ERROR-Invalid Starlink Solusi Reference Nuumber";
        }
        else if(code.equalsIgnoreCase("0093")) {
            return "ERROR-Invalid Partner Central Trace Audit Number";
        }
        else if(code.equalsIgnoreCase("0094")) {
            return "ERROR-Reversal had been done";
        }
        else if(code.equalsIgnoreCase("0097")) {
            return "ERROR-Switching ID and / or Bank Code is not identical with payment";
        }
        else if(code.equalsIgnoreCase("0098")) {
            return "ERROR-PLN Ref Number is not valid";
        }


        return "";
    }

    public String getPurchaseMsgResponseName(String code) {
        if (code.equalsIgnoreCase("0000")) {
            return "SUCCESSFUL";
        }
        else if(code.equalsIgnoreCase("0004")) {
            return "ERROR-UNREGISTERED BILLER";
        }
        else if(code.equalsIgnoreCase("0005")) {
            return "ERROR-OTHER";
        }
        else if(code.equalsIgnoreCase("0006")) {
            return "ERROR-blocked partner central";
        }
        else if(code.equalsIgnoreCase("0007")) {
            return "ERROR-blocked terminal";
        }
        else if(code.equalsIgnoreCase("0008")) {
            return "ERROR-invalid access time";
        }
        else if(code.equalsIgnoreCase("0011")) {
            return "ERROR-Need to sign on";
        }
        else if(code.equalsIgnoreCase("0013")) {
            return "ERROR-Invalid Transaction Amount";
        }
        else if(code.equalsIgnoreCase("0014")) {
            return "ERROR-Unknown subscriber";
        }
        else if(code.equalsIgnoreCase("0015")) {
            return "ERROR-Unknown registration number";
        }
        else if(code.equalsIgnoreCase("0016")) {
            return "ERROR-PRR Subscriber";
        }
        else if(code.equalsIgnoreCase("0017")) {
            return "ERROR-Subscriber still have bills to pay";
        }
        else if(code.equalsIgnoreCase("0018")) {
            return "ERROR-Subscription is 3-phase subscriber";
        }
        else if(code.equalsIgnoreCase("0030")) {
            return "ERROR-Invalid message";
        }
        else if(code.equalsIgnoreCase("0031")) {
            return "ERROR-Unregistered Bank Code";
        }
        else if(code.equalsIgnoreCase("0032")) {
            return "ERROR-Unregistered Switching";
        }
        else if(code.equalsIgnoreCase("0033")) {
            return "ERROR-Unregistered Product";
        }
        else if(code.equalsIgnoreCase("0034")) {
            return "ERROR-Unregistered Terminal";
        }
        else if(code.equalsIgnoreCase("0041")) {
            return "ERROR-Transaction Amount below minimum purchase amount";
        }
        else if(code.equalsIgnoreCase("0042")) {
            return "ERROR-Transaction Amount exceed maximum purchase amount";
        }
        else if(code.equalsIgnoreCase("0043")) {
            return "ERROR-New power category is smaller";
        }
        else if(code.equalsIgnoreCase("0044")) {
            return "ERROR-New power category is not a valid value";
        }
        else if(code.equalsIgnoreCase("0045")) {
            return "ERROR-Invalid admin charge";
        }
        else if(code.equalsIgnoreCase("0046")) {
            return "ERROR-Inadequate deposit for terminal / client of legacy system. Please contact PLN";
        }
        else if(code.equalsIgnoreCase("0047")) {
            return "ERROR-Total KWH is over the limit";
        }
        else if(code.equalsIgnoreCase("0063")) {
            return "ERROR-No payment";
        }
        else if(code.equalsIgnoreCase("0068")) {
            return "ERROR-Timeout";
        }
        else if(code.equalsIgnoreCase("0077")) {
            return "ERROR-Subscriber suspended";
        }
        else if(code.equalsIgnoreCase("0088")) {
            return "ERROR-Bills already paid";
        }
        else if(code.equalsIgnoreCase("0089")) {
            return "ERROR-Current Bill is not available";
        }
        else if(code.equalsIgnoreCase("0090")) {
            return "ERROR-Cut-off is in progress";
        }
        else if(code.equalsIgnoreCase("0092")) {
            return "ERROR-Invalid Starlink Solusi Reference Nuumber";
        }
        else if(code.equalsIgnoreCase("0093")) {
            return "ERROR-Invalid Switcher Trace Audit Number";
        }
        else if(code.equalsIgnoreCase("0094")) {
            return "ERROR-Reversal had been done";
        }
        else if(code.equalsIgnoreCase("0097")) {
            return "ERROR-Switching ID and / or Bank Code is not identical with payment";
        }
        else if(code.equalsIgnoreCase("0098")) {
            return "ERROR-PLN Ref Number is not valid";
        }
        
        return "";
    }

    public String getInquiryMsgResponseName(String code) {
        if (code.equalsIgnoreCase("0000")) {
            return "SUCCESSFUL";
        }
        else if(code.equalsIgnoreCase("0004")) {
            return "ERROR-Unregistered biller";
        }
        else if(code.equalsIgnoreCase("0005")) {
            return "ERROR-OTHER";
        }
        else if(code.equalsIgnoreCase("0006")) {
            return "ERROR-Blocked partner central";
        }
        else if(code.equalsIgnoreCase("0007")) {
            return "ERROR-Blocked terminal";
        }
        else if(code.equalsIgnoreCase("0008")) {
            return "ERROR-Invalid access time";
        }
        else if(code.equalsIgnoreCase("0011")) {
            return "ERROR-Need to sign on";
        }
        else if(code.equalsIgnoreCase("0014")) {
            return "ERROR-Unknown subscriber";
        }
        else if(code.equalsIgnoreCase("0015")) {
            return "ERROR-Unknown registration number";
        }
        else if(code.equalsIgnoreCase("0030")) {
            return "ERROR-Invalid message";
        }
        else if(code.equalsIgnoreCase("0031")) {
            return "ERROR-Unregistered Bank Code";
        }
        else if(code.equalsIgnoreCase("0032")) {
            return "ERROR-Unregistered Partner Central";
        }
        else if(code.equalsIgnoreCase("0034")) {
            return "ERROR-Unregistered Terminal";
        }
        else if(code.equalsIgnoreCase("0033")) {
            return "ERROR-Unregistered Product";
        }
        else if(code.equalsIgnoreCase("0068")) {
            return "ERROR-Timeout";
        }
        else if(code.equalsIgnoreCase("0077")) {
            return "ERROR-Subscriber suspended";
        }
        else if(code.equalsIgnoreCase("0088")) {
            return "ERROR-Bills already paid";
        }
        else if(code.equalsIgnoreCase("0089")) {
            return "ERROR-Current Bill is not available";
        }
        else if(code.equalsIgnoreCase("0090")) {
            return "ERROR-Cut-off is in progress";
        }

        return "";
    }

    public String getNetworkMsgResponseName(String code) {
        if (code.equalsIgnoreCase("0000")) {
            return "SUCCESSFUL";
        }
        else if(code.equalsIgnoreCase("0005")) {
            return "ERROR-OTHER";
        }
        else if(code.equalsIgnoreCase("0006")) {
            return "ERROR-BLOCKED PARTNER CENTRAL";
        }
        else if(code.equalsIgnoreCase("0007")) {
            return "ERROR-BLOCKED TERMINAL";
        }
        else if(code.equalsIgnoreCase("0008")) {
            return "ERROR-INVALID ACCESS TIME";
        }
        else if(code.equalsIgnoreCase("0011")) {
            return "ERROR-NEED TO SIGN ON";
        }
        else if(code.equalsIgnoreCase("0015")) {
            return "ERROR-Unknown registration number";
        }
        else if(code.equalsIgnoreCase("0030")) {
            return "ERROR-INVALID MESSAGE";
        }
        else if(code.equalsIgnoreCase("0032")) {
            return "ERROR-UNREGISTERED PARTNER CENTRAL";
        }
        else if(code.equalsIgnoreCase("0034")) {
            return "ERROR-UNREGISTERED TERMINAL";
        }
        else if(code.equalsIgnoreCase("0068")) {
            return "ERROR-TIMEOUT";
        }
        else if(code.equalsIgnoreCase("0090")) {
            return "ERROR-CUT-OFF IS IN PROGRESS";
        }

        return "";
    }

}
