
package isoxml;

import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import model.PlnPostpaids;
import model.Transactions;
import org.apache.log4j.*;
import ppob_sls_iqbal.Settings;

/**
 *
 * @author ngonar
 */
public class PostPaidProcessing {

    private static final Logger logger = Logger.getLogger("PostPaidProcessing");
    
    PostPaid post = new PostPaid();
    PostpaidResponseCode postRC = new PostpaidResponseCode();
    Settings setting = new Settings();

    public PostPaidProcessing() {

    }

    public String processPayment2(String hasil) {

        String iso = hasil;
        String[] result = post.parsePaymentMsgResponse(hasil,false);

        String blth_summary = "";
        String stand_meter_summary = "", sm_last = "";

        
        if (result[9].equalsIgnoreCase("0000") ) {

            String[] bit48 = post.parseBit48Payment(result[11], result[9]);

            String switcher_id = bit48[0];
            String subscriber_id = bit48[1];
            String bill_status = bit48[2];
            String payment_status = bit48[3];
            String outstanding_bill = bit48[4];
            String switcher_ref = bit48[5];
            String subscriber_name = bit48[6];
            String service_unit = bit48[7];
            String service_unit_phone = bit48[8];
            String subscriber_segmentation = bit48[9];
            String power_consuming_category = bit48[10];
            String total_admin_charge = bit48[11];

            double total_bk=0, total_tag=0;

            //1 bill
            String bill_period      = bit48[11+1];
            String due_date         = bit48[12+1];
            String meter_read_date  = bit48[13+1];
            String total_electricity_bill = bit48[14+1];
            String incentive        = bit48[15+1];
            String vat              = bit48[16+1];
            String penalty_fee      = bit48[17+1];
            String prev_meter_reading1 = bit48[18+1];
            String curr_meter_reading1 = bit48[19+1];
            String prev_meter_reading2 = bit48[20+1];
            String curr_meter_reading2 = bit48[21+1];
            String prev_meter_reading3 = bit48[22+1];
            String curr_meter_reading3 = bit48[23+1];

            String bill_period_ = "",bill_period__ = "",bill_period___ = "";
            String due_date_ = "",due_date__ = "",due_date___ = "";
            String meter_read_date_ = "",meter_read_date__ = "",meter_read_date___ = "";
            String total_electricity_bill_ = "0",total_electricity_bill__ = "0",total_electricity_bill___ = "0";
            String incentive_ = "0",incentive__ = "0",incentive___ = "0";
            String vat_ = "0",vat__ = "0",vat___ = "0";
            String penalty_fee_ = "0",penalty_fee__ = "0",penalty_fee___ = "0";
            String prev_meter_reading1_ = "",prev_meter_reading1__ = "",prev_meter_reading1___ = "";
            String curr_meter_reading1_ = "",curr_meter_reading1__= "",curr_meter_reading1___ = "";
            String prev_meter_reading2_ = "",prev_meter_reading2__ = "",prev_meter_reading2___ = "";
            String curr_meter_reading2_ = "",curr_meter_reading2__ = "",curr_meter_reading2___ = "";
            String prev_meter_reading3_ = "",prev_meter_reading3__ = "",prev_meter_reading3___ = "";
            String curr_meter_reading3_ = "",curr_meter_reading3__ = "",curr_meter_reading3___ = "";

            if (Integer.parseInt(bill_status)>1) {
                //2 bill
                bill_period_        = bit48[25];
                due_date_           = bit48[26];
                meter_read_date_    = bit48[27];
                total_electricity_bill_ = bit48[28];
                incentive_          = bit48[29];
                vat_                = bit48[30];
                penalty_fee_        = bit48[31];
                prev_meter_reading1_ = bit48[32];
                curr_meter_reading1_ = bit48[33];
                prev_meter_reading2_ = bit48[34];
                curr_meter_reading2_ = bit48[35];
                prev_meter_reading3_ = bit48[36];
                curr_meter_reading3_ = bit48[37];
            }

            if (Integer.parseInt(bill_status)>2) {
                //3 bill
                bill_period__       = bit48[38];
                due_date__          = bit48[39];
                meter_read_date__   = bit48[40];
                total_electricity_bill__ = bit48[41];
                incentive__         = bit48[42];
                vat__               = bit48[43];
                penalty_fee__       = bit48[44];
                prev_meter_reading1__ = bit48[45];
                curr_meter_reading1__ = bit48[46];
                prev_meter_reading2__ = bit48[47];
                curr_meter_reading2__ = bit48[48];
                prev_meter_reading3__ = bit48[49];
                curr_meter_reading3__ = bit48[50];
            }

            if (Integer.parseInt(bill_status)>3) {
                //4 bill
                bill_period___          = bit48[51];
                due_date___             = bit48[52];
                meter_read_date___      = bit48[53];
                total_electricity_bill___ = bit48[54];
                incentive___            = bit48[55];
                vat___                  = bit48[56];
                penalty_fee___          = bit48[57];
                prev_meter_reading1___ = bit48[58];
                curr_meter_reading1___ = bit48[59];
                prev_meter_reading2___ = bit48[60];
                curr_meter_reading2___ = bit48[61];
                prev_meter_reading3___ = bit48[62];
                curr_meter_reading3___ = bit48[63];
            }

            hasil  = "<?xml version='1.0' encoding='UTF-8'?>"+
                       "<response>"+ 
                       "<produk>PLNPOSTPAID</produk>";

            if (result[0].equalsIgnoreCase("2410") || result[0].equalsIgnoreCase("2411") ){
                hasil  += "<msg_type>REVERSAL</msg_type>";
            }
            
            subscriber_name = subscriber_name.replaceAll("<", ".");
            subscriber_name = subscriber_name.replaceAll(">", ",");

            hasil +=   "<amount>"+result[2].substring(4)+"</amount>"+
                       "<stan>"+result[3]+"</stan>"+
                       "<datetime>"+result[4]+"</datetime>"+
                       "<date_settlement>"+result[5]+"</date_settlement>"+
                       "<merchant_code>"+result[6]+"</merchant_code>"+
                       "<bank_code>"+result[7]+"</bank_code>"+
                       "<rc>"+result[9]+"</rc>"+
                       "<terminal_id>"+result[10]+"</terminal_id>"+
                       "<bill_status>"+bill_status+"</bill_status>"+
                       "<outstanding_bill>"+outstanding_bill+"</outstanding_bill>"+
                       "<payment_status>"+payment_status+"</payment_status>"+
                       "<subscriber_id>"+subscriber_id+"</subscriber_id>"+
                       "<switcher_refno>"+switcher_ref+"</switcher_refno>"+
                       "<subscriber_name>"+subscriber_name+"</subscriber_name>"+
                       "<service_unit>"+service_unit+"</service_unit>"+
                       "<service_unit_phone>"+service_unit_phone+"</service_unit_phone>"+
                       "<subscriber_segmentation>"+subscriber_segmentation+"</subscriber_segmentation>"+
                       "<power>"+power_consuming_category+"</power>"+
                       "<admin_charge>"+total_admin_charge+"</admin_charge>"+
                       "<info_text>"+setting.getPostInfo()+"</info_text>"+
                       "<bills>" +
                       "<bill>"+
                       "<bill_period>"+bill_period+"</bill_period>"+
                       "<due_date>"+due_date+"</due_date>"+
                       "<meter_read_date>"+meter_read_date+"</meter_read_date>"+
                       "<total_electricity_bill>"+total_electricity_bill+"</total_electricity_bill>"+
                       "<incentive>"+incentive+"</incentive>"+
                       "<value_added_tax>"+vat+"</value_added_tax>"+
                       "<penalty_fee>"+penalty_fee+"</penalty_fee>"+
                       "<previous_meter_reading1>"+prev_meter_reading1+"</previous_meter_reading1>"+
                       "<current_meter_reading1>"+curr_meter_reading1+"</current_meter_reading1>"+
                       "<previous_meter_reading2>"+prev_meter_reading2+"</previous_meter_reading2>"+
                       "<current_meter_reading2>"+curr_meter_reading2+"</current_meter_reading2>"+
                       "<previous_meter_reading3>"+prev_meter_reading3+"</previous_meter_reading3>"+
                       "<current_meter_reading3>"+curr_meter_reading3+"</current_meter_reading3>"+
                       "</bill>";

                       blth_summary += getMonthName(bill_period.substring(4))+bill_period.substring(2,4);
                       stand_meter_summary += prev_meter_reading1;
                       sm_last = curr_meter_reading1;

                       total_bk += Double.parseDouble(penalty_fee);
                       total_tag += Double.parseDouble(total_electricity_bill);

                 if (Integer.parseInt(bill_status)>1) {

                       hasil+=   "<bill>"+
                                   "<bill_period>"+bill_period_+"</bill_period>"+
                                   "<due_date>"+due_date_+"</due_date>"+
                                   "<meter_read_date>"+meter_read_date_+"</meter_read_date>"+
                                   "<total_electricity_bill>"+total_electricity_bill_+"</total_electricity_bill>"+
                                   "<incentive>"+incentive_+"</incentive>"+
                                   "<value_added_tax>"+vat_+"</value_added_tax>"+
                                   "<penalty_fee>"+penalty_fee_+"</penalty_fee>"+
                                   "<previous_meter_reading1>"+prev_meter_reading1_+"</previous_meter_reading1>"+
                                   "<current_meter_reading1>"+curr_meter_reading1_+"</current_meter_reading1>"+
                                   "<previous_meter_reading2>"+prev_meter_reading2_+"</previous_meter_reading2>"+
                                   "<current_meter_reading2>"+curr_meter_reading2_+"</current_meter_reading2>"+
                                   "<previous_meter_reading3>"+prev_meter_reading3_+"</previous_meter_reading3>"+
                                   "<current_meter_reading3>"+curr_meter_reading3_+"</current_meter_reading3>"+
                                   "</bill>";

                       blth_summary += ", "+getMonthName(bill_period_.substring(4))+bill_period_.substring(2,4);
                       sm_last = curr_meter_reading1_;

                       total_bk += Double.parseDouble(penalty_fee_);
                       total_tag += Double.parseDouble(total_electricity_bill_);

                   }

                   if (Integer.parseInt(bill_status)>2) {
                       hasil+="<bill>"+
                               "<bill_period>"+bill_period__+"</bill_period>"+
                               "<due_date>"+due_date__+"</due_date>"+
                               "<meter_read_date>"+meter_read_date__+"</meter_read_date>"+
                               "<total_electricity_bill>"+total_electricity_bill__+"</total_electricity_bill>"+
                               "<incentive>"+incentive__+"</incentive>"+
                               "<value_added_tax>"+vat__+"</value_added_tax>"+
                               "<penalty_fee>"+penalty_fee__+"</penalty_fee>"+
                               "<previous_meter_reading1>"+prev_meter_reading1__+"</previous_meter_reading1>"+
                               "<current_meter_reading1>"+curr_meter_reading1__+"</current_meter_reading1>"+
                               "<previous_meter_reading2>"+prev_meter_reading2__+"</previous_meter_reading2>"+
                               "<current_meter_reading2>"+curr_meter_reading2__+"</current_meter_reading2>"+
                               "<previous_meter_reading3>"+prev_meter_reading3__+"</previous_meter_reading3>"+
                               "<current_meter_reading3>"+curr_meter_reading3__+"</current_meter_reading3>"+
                               "</bill>";

                       blth_summary += ", "+getMonthName(bill_period__.substring(4))+bill_period__.substring(2,4);
                       sm_last = curr_meter_reading1__;

                       total_bk += Double.parseDouble(penalty_fee__);
                       total_tag += Double.parseDouble(total_electricity_bill__);
                    }

                    if (Integer.parseInt(bill_status)>3) {
                       hasil+="<bill>"+
                               "<bill_period>"+bill_period___+"</bill_period>"+
                               "<due_date>"+due_date___+"</due_date>"+
                               "<meter_read_date>"+meter_read_date___+"</meter_read_date>"+
                               "<total_electricity_bill>"+total_electricity_bill___+"</total_electricity_bill>"+
                               "<incentive>"+incentive___+"</incentive>"+
                               "<value_added_tax>"+vat___+"</value_added_tax>"+
                               "<penalty_fee>"+penalty_fee___+"</penalty_fee>"+
                               "<previous_meter_reading1>"+prev_meter_reading1___+"</previous_meter_reading1>"+
                               "<current_meter_reading1>"+curr_meter_reading1___+"</current_meter_reading1>"+
                               "<previous_meter_reading2>"+prev_meter_reading2___+"</previous_meter_reading2>"+
                               "<current_meter_reading2>"+curr_meter_reading2___+"</current_meter_reading2>"+
                               "<previous_meter_reading3>"+prev_meter_reading3___+"</previous_meter_reading3>"+
                               "<current_meter_reading3>"+curr_meter_reading3___+"</current_meter_reading3>"+
                               "</bill>";

                       blth_summary += ", "+getMonthName(bill_period___.substring(4))+bill_period___.substring(2,4);
                       sm_last = curr_meter_reading1___;

                       total_bk += Double.parseDouble(penalty_fee___);
                       total_tag += Double.parseDouble(total_electricity_bill___);
                   }

                   hasil+="</bills>"
                               + "<blth_summary>"+blth_summary+"</blth_summary>"
                               + "<stand_meter_summary>"+stand_meter_summary+" - "+sm_last+"</stand_meter_summary>"
                               + "<bk>"+total_bk+"</bk>"
                               + "<rptag>"+total_tag+"</rptag>"
//                               + "<saldo>" + detail[7] + "</saldo>" 
//                               + "<harga>" + harga + "</harga>" 
                               + "</response>";

                   //simpan data payment ke database
                   logger.log(Level.INFO, "Save to Postpaid Trx");

                   double total_vat = Double.parseDouble(vat) + Double.parseDouble(vat_) +
                                      Double.parseDouble(vat__) + Double.parseDouble(vat___);

                   double total_penalty = Double.parseDouble(penalty_fee) + Double.parseDouble(penalty_fee_) +
                                          Double.parseDouble(penalty_fee__) + Double.parseDouble(penalty_fee___);

//                   if (!result[0].equalsIgnoreCase("2400") && !result[0].equalsIgnoreCase("2410") && !result[0].equalsIgnoreCase("2411")){
//                       long idx = insertToTrx(Integer.parseInt(detail[5]), 100, detail[4], detail[1],
//                                    detail[6], Long.parseLong(result[2].substring(4)),(long) harga,Integer.parseInt(result[3]),
//                                    result[9],"PLN-POSTPAID",Long.parseLong("0"),Short.parseShort("0"),
//                                    Short.parseShort("0"),switcher_ref,detail[4],Short.parseShort("0"));
//
//                       insertToDB(Integer.parseInt(result[3]), String.valueOf(idx), "", "", result[9], subscriber_id,
//                               subscriber_name,subscriber_segmentation+" / "+power_consuming_category,
//                               bill_status, outstanding_bill, result[2].substring(4),
//                               penalty_fee, total_admin_charge,
//                               incentive, blth_summary, stand_meter_summary+" - "+sm_last,
//                               switcher_ref, result[12], "", "", "", service_unit_phone,  hasil, iso,
//                               result[7],result[6],String.valueOf(total_tag+total_bk),
//                               String.valueOf(total_vat),
//                               String.valueOf(total_penalty),
//                               result[10], result[4]
//                               );
//                       //simpan data transaksi                   
//                    }
            }
//            else if (result[9].equalsIgnoreCase("0005") || result[9].equalsIgnoreCase("0012") || result[9].equalsIgnoreCase("0063") || result[9].equalsIgnoreCase("0094")) {
//                hasil = "<?xml version='1.0' encoding='UTF-8'?>"
//                     + "<response>"
//                     + "<produk>PLNPOSTPAID</produk>"
//                     + "<code>"+result[9]+"</code>"
//                     + "<stan>"+result[3]+"</stan>"   
//                     + "<desc>TRANSAKSI SEDANG DIPROSES OLEH PLN</desc>"
//                     + "<saldo>" + detail[7] + "</saldo>" 
//                     + "</response>";
//            }
//            else {
//
//                hasil = "<?xml version='1.0' encoding='UTF-8'?>"
//                     + "<response>"
//                     + "<produk>PLNPOSTPAID</produk>"
//                     + "<code>"+result[9]+"</code>"
//                     + "<stan>"+result[3]+"</stan>"   
//                     + "<desc>"+postRC.getPurchaseMsgResponseName(result[9])+"</desc>"
//                     + "<saldo>" + detail[7] + "</saldo>" 
//                     + "</response>";
//            }        

        return hasil;
    } public String processPayment(String hasil, String detail[],double harga) {

        String iso = hasil;
        String[] result = post.parsePaymentMsgResponse(hasil,false);

        String blth_summary = "";
        String stand_meter_summary = "", sm_last = "";

        
        if (result[9].equalsIgnoreCase("0000") ) {

            String[] bit48 = post.parseBit48Payment(result[11], result[9]);

            String switcher_id = bit48[0];
            String subscriber_id = bit48[1];
            String bill_status = bit48[2];
            String payment_status = bit48[3];
            String outstanding_bill = bit48[4];
            String switcher_ref = bit48[5];
            String subscriber_name = bit48[6];
            String service_unit = bit48[7];
            String service_unit_phone = bit48[8];
            String subscriber_segmentation = bit48[9];
            String power_consuming_category = bit48[10];
            String total_admin_charge = bit48[11];

            double total_bk=0, total_tag=0;

            //1 bill
            String bill_period      = bit48[11+1];
            String due_date         = bit48[12+1];
            String meter_read_date  = bit48[13+1];
            String total_electricity_bill = bit48[14+1];
            String incentive        = bit48[15+1];
            String vat              = bit48[16+1];
            String penalty_fee      = bit48[17+1];
            String prev_meter_reading1 = bit48[18+1];
            String curr_meter_reading1 = bit48[19+1];
            String prev_meter_reading2 = bit48[20+1];
            String curr_meter_reading2 = bit48[21+1];
            String prev_meter_reading3 = bit48[22+1];
            String curr_meter_reading3 = bit48[23+1];

            String bill_period_ = "",bill_period__ = "",bill_period___ = "";
            String due_date_ = "",due_date__ = "",due_date___ = "";
            String meter_read_date_ = "",meter_read_date__ = "",meter_read_date___ = "";
            String total_electricity_bill_ = "0",total_electricity_bill__ = "0",total_electricity_bill___ = "0";
            String incentive_ = "0",incentive__ = "0",incentive___ = "0";
            String vat_ = "0",vat__ = "0",vat___ = "0";
            String penalty_fee_ = "0",penalty_fee__ = "0",penalty_fee___ = "0";
            String prev_meter_reading1_ = "",prev_meter_reading1__ = "",prev_meter_reading1___ = "";
            String curr_meter_reading1_ = "",curr_meter_reading1__= "",curr_meter_reading1___ = "";
            String prev_meter_reading2_ = "",prev_meter_reading2__ = "",prev_meter_reading2___ = "";
            String curr_meter_reading2_ = "",curr_meter_reading2__ = "",curr_meter_reading2___ = "";
            String prev_meter_reading3_ = "",prev_meter_reading3__ = "",prev_meter_reading3___ = "";
            String curr_meter_reading3_ = "",curr_meter_reading3__ = "",curr_meter_reading3___ = "";

            if (Integer.parseInt(bill_status)>1) {
                //2 bill
                bill_period_        = bit48[25];
                due_date_           = bit48[26];
                meter_read_date_    = bit48[27];
                total_electricity_bill_ = bit48[28];
                incentive_          = bit48[29];
                vat_                = bit48[30];
                penalty_fee_        = bit48[31];
                prev_meter_reading1_ = bit48[32];
                curr_meter_reading1_ = bit48[33];
                prev_meter_reading2_ = bit48[34];
                curr_meter_reading2_ = bit48[35];
                prev_meter_reading3_ = bit48[36];
                curr_meter_reading3_ = bit48[37];
            }

            if (Integer.parseInt(bill_status)>2) {
                //3 bill
                bill_period__       = bit48[38];
                due_date__          = bit48[39];
                meter_read_date__   = bit48[40];
                total_electricity_bill__ = bit48[41];
                incentive__         = bit48[42];
                vat__               = bit48[43];
                penalty_fee__       = bit48[44];
                prev_meter_reading1__ = bit48[45];
                curr_meter_reading1__ = bit48[46];
                prev_meter_reading2__ = bit48[47];
                curr_meter_reading2__ = bit48[48];
                prev_meter_reading3__ = bit48[49];
                curr_meter_reading3__ = bit48[50];
            }

            if (Integer.parseInt(bill_status)>3) {
                //4 bill
                bill_period___          = bit48[51];
                due_date___             = bit48[52];
                meter_read_date___      = bit48[53];
                total_electricity_bill___ = bit48[54];
                incentive___            = bit48[55];
                vat___                  = bit48[56];
                penalty_fee___          = bit48[57];
                prev_meter_reading1___ = bit48[58];
                curr_meter_reading1___ = bit48[59];
                prev_meter_reading2___ = bit48[60];
                curr_meter_reading2___ = bit48[61];
                prev_meter_reading3___ = bit48[62];
                curr_meter_reading3___ = bit48[63];
            }

            hasil  = "<?xml version='1.0' encoding='UTF-8'?>"+
                       "<response>"+ 
                       "<produk>PLNPOSTPAID</produk>";

            if (result[0].equalsIgnoreCase("2410") || result[0].equalsIgnoreCase("2411") ){
                hasil  += "<msg_type>REVERSAL</msg_type>";
            }
            
            subscriber_name = subscriber_name.replaceAll("<", ".");
            subscriber_name = subscriber_name.replaceAll(">", ",");

            hasil +=   "<amount>"+result[2].substring(4)+"</amount>"+
                       "<stan>"+result[3]+"</stan>"+
                       "<datetime>"+result[4]+"</datetime>"+
                       "<date_settlement>"+result[5]+"</date_settlement>"+
                       "<merchant_code>"+result[6]+"</merchant_code>"+
                       "<bank_code>"+result[7]+"</bank_code>"+
                       "<rc>"+result[9]+"</rc>"+
                       "<terminal_id>"+result[10]+"</terminal_id>"+
                       "<bill_status>"+bill_status+"</bill_status>"+
                       "<outstanding_bill>"+outstanding_bill+"</outstanding_bill>"+
                       "<payment_status>"+payment_status+"</payment_status>"+
                       "<subscriber_id>"+subscriber_id+"</subscriber_id>"+
                       "<switcher_refno>"+switcher_ref+"</switcher_refno>"+
                       "<subscriber_name>"+subscriber_name+"</subscriber_name>"+
                       "<service_unit>"+service_unit+"</service_unit>"+
                       "<service_unit_phone>"+service_unit_phone+"</service_unit_phone>"+
                       "<subscriber_segmentation>"+subscriber_segmentation+"</subscriber_segmentation>"+
                       "<power>"+power_consuming_category+"</power>"+
                       "<admin_charge>"+total_admin_charge+"</admin_charge>"+
                       "<info_text>"+setting.getPostInfo()+"</info_text>"+
                       "<bills>" +
                       "<bill>"+
                       "<bill_period>"+bill_period+"</bill_period>"+
                       "<due_date>"+due_date+"</due_date>"+
                       "<meter_read_date>"+meter_read_date+"</meter_read_date>"+
                       "<total_electricity_bill>"+total_electricity_bill+"</total_electricity_bill>"+
                       "<incentive>"+incentive+"</incentive>"+
                       "<value_added_tax>"+vat+"</value_added_tax>"+
                       "<penalty_fee>"+penalty_fee+"</penalty_fee>"+
                       "<previous_meter_reading1>"+prev_meter_reading1+"</previous_meter_reading1>"+
                       "<current_meter_reading1>"+curr_meter_reading1+"</current_meter_reading1>"+
                       "<previous_meter_reading2>"+prev_meter_reading2+"</previous_meter_reading2>"+
                       "<current_meter_reading2>"+curr_meter_reading2+"</current_meter_reading2>"+
                       "<previous_meter_reading3>"+prev_meter_reading3+"</previous_meter_reading3>"+
                       "<current_meter_reading3>"+curr_meter_reading3+"</current_meter_reading3>"+
                       "</bill>";

                       blth_summary += getMonthName(bill_period.substring(4))+bill_period.substring(2,4);
                       stand_meter_summary += prev_meter_reading1;
                       sm_last = curr_meter_reading1;

                       total_bk += Double.parseDouble(penalty_fee);
                       total_tag += Double.parseDouble(total_electricity_bill);

                 if (Integer.parseInt(bill_status)>1) {

                       hasil+=   "<bill>"+
                                   "<bill_period>"+bill_period_+"</bill_period>"+
                                   "<due_date>"+due_date_+"</due_date>"+
                                   "<meter_read_date>"+meter_read_date_+"</meter_read_date>"+
                                   "<total_electricity_bill>"+total_electricity_bill_+"</total_electricity_bill>"+
                                   "<incentive>"+incentive_+"</incentive>"+
                                   "<value_added_tax>"+vat_+"</value_added_tax>"+
                                   "<penalty_fee>"+penalty_fee_+"</penalty_fee>"+
                                   "<previous_meter_reading1>"+prev_meter_reading1_+"</previous_meter_reading1>"+
                                   "<current_meter_reading1>"+curr_meter_reading1_+"</current_meter_reading1>"+
                                   "<previous_meter_reading2>"+prev_meter_reading2_+"</previous_meter_reading2>"+
                                   "<current_meter_reading2>"+curr_meter_reading2_+"</current_meter_reading2>"+
                                   "<previous_meter_reading3>"+prev_meter_reading3_+"</previous_meter_reading3>"+
                                   "<current_meter_reading3>"+curr_meter_reading3_+"</current_meter_reading3>"+
                                   "</bill>";

                       blth_summary += ", "+getMonthName(bill_period_.substring(4))+bill_period_.substring(2,4);
                       sm_last = curr_meter_reading1_;

                       total_bk += Double.parseDouble(penalty_fee_);
                       total_tag += Double.parseDouble(total_electricity_bill_);

                   }

                   if (Integer.parseInt(bill_status)>2) {
                       hasil+="<bill>"+
                               "<bill_period>"+bill_period__+"</bill_period>"+
                               "<due_date>"+due_date__+"</due_date>"+
                               "<meter_read_date>"+meter_read_date__+"</meter_read_date>"+
                               "<total_electricity_bill>"+total_electricity_bill__+"</total_electricity_bill>"+
                               "<incentive>"+incentive__+"</incentive>"+
                               "<value_added_tax>"+vat__+"</value_added_tax>"+
                               "<penalty_fee>"+penalty_fee__+"</penalty_fee>"+
                               "<previous_meter_reading1>"+prev_meter_reading1__+"</previous_meter_reading1>"+
                               "<current_meter_reading1>"+curr_meter_reading1__+"</current_meter_reading1>"+
                               "<previous_meter_reading2>"+prev_meter_reading2__+"</previous_meter_reading2>"+
                               "<current_meter_reading2>"+curr_meter_reading2__+"</current_meter_reading2>"+
                               "<previous_meter_reading3>"+prev_meter_reading3__+"</previous_meter_reading3>"+
                               "<current_meter_reading3>"+curr_meter_reading3__+"</current_meter_reading3>"+
                               "</bill>";

                       blth_summary += ", "+getMonthName(bill_period__.substring(4))+bill_period__.substring(2,4);
                       sm_last = curr_meter_reading1__;

                       total_bk += Double.parseDouble(penalty_fee__);
                       total_tag += Double.parseDouble(total_electricity_bill__);
                    }

                    if (Integer.parseInt(bill_status)>3) {
                       hasil+="<bill>"+
                               "<bill_period>"+bill_period___+"</bill_period>"+
                               "<due_date>"+due_date___+"</due_date>"+
                               "<meter_read_date>"+meter_read_date___+"</meter_read_date>"+
                               "<total_electricity_bill>"+total_electricity_bill___+"</total_electricity_bill>"+
                               "<incentive>"+incentive___+"</incentive>"+
                               "<value_added_tax>"+vat___+"</value_added_tax>"+
                               "<penalty_fee>"+penalty_fee___+"</penalty_fee>"+
                               "<previous_meter_reading1>"+prev_meter_reading1___+"</previous_meter_reading1>"+
                               "<current_meter_reading1>"+curr_meter_reading1___+"</current_meter_reading1>"+
                               "<previous_meter_reading2>"+prev_meter_reading2___+"</previous_meter_reading2>"+
                               "<current_meter_reading2>"+curr_meter_reading2___+"</current_meter_reading2>"+
                               "<previous_meter_reading3>"+prev_meter_reading3___+"</previous_meter_reading3>"+
                               "<current_meter_reading3>"+curr_meter_reading3___+"</current_meter_reading3>"+
                               "</bill>";

                       blth_summary += ", "+getMonthName(bill_period___.substring(4))+bill_period___.substring(2,4);
                       sm_last = curr_meter_reading1___;

                       total_bk += Double.parseDouble(penalty_fee___);
                       total_tag += Double.parseDouble(total_electricity_bill___);
                   }

                   hasil+="</bills>"
                               + "<blth_summary>"+blth_summary+"</blth_summary>"
                               + "<stand_meter_summary>"+stand_meter_summary+" - "+sm_last+"</stand_meter_summary>"
                               + "<bk>"+total_bk+"</bk>"
                               + "<rptag>"+total_tag+"</rptag>"
                               + "<saldo>" + detail[7] + "</saldo>" 
                               + "<harga>" + harga + "</harga>" 
                               + "</response>";

                   //simpan data payment ke database
                   logger.log(Level.INFO, "Save to Postpaid Trx");

                   double total_vat = Double.parseDouble(vat) + Double.parseDouble(vat_) +
                                      Double.parseDouble(vat__) + Double.parseDouble(vat___);

                   double total_penalty = Double.parseDouble(penalty_fee) + Double.parseDouble(penalty_fee_) +
                                          Double.parseDouble(penalty_fee__) + Double.parseDouble(penalty_fee___);

                   if (!result[0].equalsIgnoreCase("2400") && !result[0].equalsIgnoreCase("2410") && !result[0].equalsIgnoreCase("2411")){
                       long idx = insertToTrx(Integer.parseInt(detail[5]), 100, detail[4], detail[1],
                                    detail[6], Long.parseLong(result[2].substring(4)),(long) harga,Integer.parseInt(result[3]),
                                    result[9],"PLN-POSTPAID",Long.parseLong("0"),Short.parseShort("0"),
                                    Short.parseShort("0"),switcher_ref,detail[4],Short.parseShort("0"));

                       insertToDB(Integer.parseInt(result[3]), String.valueOf(idx), "", "", result[9], subscriber_id,
                               subscriber_name,subscriber_segmentation+" / "+power_consuming_category,
                               bill_status, outstanding_bill, result[2].substring(4),
                               penalty_fee, total_admin_charge,
                               incentive, blth_summary, stand_meter_summary+" - "+sm_last,
                               switcher_ref, result[12], "", "", "", service_unit_phone,  hasil, iso,
                               result[7],result[6],String.valueOf(total_tag+total_bk),
                               String.valueOf(total_vat),
                               String.valueOf(total_penalty),
                               result[10], result[4]
                               );
                       //simpan data transaksi                   
                    }
            }
            else if (result[9].equalsIgnoreCase("0005") || result[9].equalsIgnoreCase("0012") || result[9].equalsIgnoreCase("0063") || result[9].equalsIgnoreCase("0094")) {
                hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                     + "<response>"
                     + "<produk>PLNPOSTPAID</produk>"
                     + "<code>"+result[9]+"</code>"
                     + "<stan>"+result[3]+"</stan>"   
                     + "<desc>TRANSAKSI SEDANG DIPROSES OLEH PLN</desc>"
                     + "<saldo>" + detail[7] + "</saldo>" 
                     + "</response>";
            }
            else {

                hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                     + "<response>"
                     + "<produk>PLNPOSTPAID</produk>"
                     + "<code>"+result[9]+"</code>"
                     + "<stan>"+result[3]+"</stan>"   
                     + "<desc>"+postRC.getPurchaseMsgResponseName(result[9])+"</desc>"
                     + "<saldo>" + detail[7] + "</saldo>" 
                     + "</response>";
            }        

        return hasil;
    }

    public String getMonthName(String x) {
        String bulan = "";

        if (x.equalsIgnoreCase("01")) {bulan="JAN";}
        else if (x.equalsIgnoreCase("02")) {bulan="FEB";}
        else if (x.equalsIgnoreCase("03")) {bulan="MAR";}
        else if (x.equalsIgnoreCase("04")) {bulan="APR";}
        else if (x.equalsIgnoreCase("05")) {bulan="MEI";}
        else if (x.equalsIgnoreCase("06")) {bulan="JUN";}
        else if (x.equalsIgnoreCase("07")) {bulan="JUL";}
        else if (x.equalsIgnoreCase("08")) {bulan="AUG";}
        else if (x.equalsIgnoreCase("09")) {bulan="SEP";}
        else if (x.equalsIgnoreCase("10")) {bulan="OKT";}
        else if (x.equalsIgnoreCase("11")) {bulan="NOV";}
        else if (x.equalsIgnoreCase("12")) {bulan="DES";}

        return bulan;
    }

    public Long insertToTrx(int user_id, int product_id, String destination, String sender,
            String sender_type, long price_sell,long price_buy, int inbox_id,
            String status,String remark,long saldo_awal,short counter,
            short counter2,String sn,String receiver,short resend) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();
        long idx = 0;

        try {
            Transactions trx = new Transactions();
            em.getTransaction().begin();

            trx.setUserId(user_id);
            trx.setProductId(product_id);
            trx.setDestination(destination);
            trx.setSender(sender);
            trx.setSenderType(sender_type);
            trx.setPriceSell(price_sell);
            trx.setPriceBuy(price_buy);
            trx.setInboxId(inbox_id);
            trx.setStatus(status);
            trx.setRemark(remark);
            trx.setSaldoAwal(saldo_awal);
            trx.setCounter(counter);
            trx.setCounter2(counter2);
            trx.setSn(sn);
            trx.setReceiver(receiver);
            trx.setResend(resend);
            trx.setCreateDate(new Date());

            

            em.persist(trx);
            em.flush();
            idx=trx.getId();
            
            em.getTransaction().commit();            

            em.close();
            factory.close();

        }
        catch(Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
            
            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
            
            idx = insertToTrx(user_id, product_id, destination, sender,
                  sender_type, price_sell,price_buy, inbox_id,
                  status, remark, saldo_awal, counter,
                  counter2, sn, receiver, resend);
        }
        finally {
            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        }

        return idx;
    }

    public void insertToDB(
            int inbox_id, String transid, String partnertid, String restype, String rescode, String idpel, String nama,
            String kwh, String jumlahrek, String outstanding, String tagihan, String denda, String admin, String insentive,
            String bulan, String mlalu, String refnbr, String info, String saldo, String create_date, String destination,
            String telpon,  String xml, String iso,
            String ca_id, String merchant, String total_electricity_bill, String ppn,
            String penalty, String terminal_id, String dt_trx
            ) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
        EntityManager em = factory.createEntityManager();
        
        try {
            PlnPostpaids post = new PlnPostpaids();
            em.getTransaction().begin();

            //long amt_denda = Long.parseLong(denda);
            //long amt_insentive = Long.parseLong(insentive);
            //long amt_tagihan = Long.parseLong(tagihan);
            //long amt_admin = Long.parseLong(admin);

            post.setAdmin(admin);
            post.setBulan(bulan);
            
            post.setDenda(denda);
            post.setDestination(destination);
            post.setIdpel(idpel);
            post.setInboxId(inbox_id);
            post.setInfo(info);

            post.setInsentive(String.valueOf(insentive));
            post.setJumlahrek(jumlahrek);
            post.setKwh(kwh);
            post.setMlalu(mlalu);
            post.setNama(nama);
            post.setOutstanding(outstanding);
            post.setPartnertid(partnertid);
            post.setRefnbr(refnbr);
            post.setRescode(rescode);
            post.setTagihan(tagihan);
            post.setTransid(transid);
            post.setXml(xml);
            post.setIso(iso);
            post.setCreateDate(new Date());
            post.setTelpon(telpon);
            post.setBillPeriod(bulan);
            post.setCaid(ca_id);
            post.setMerchant(merchant);
            post.setTotalElectricityBill(total_electricity_bill);
            post.setPpn(ppn);
            post.setPenalty(penalty);
            post.setTerminalId(terminal_id);
            post.setDtTrx(dt_trx);

            em.persist(post);
            em.getTransaction().commit();

            em.close();
            factory.close();

        }
        catch(Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
            
            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
            
//            insertToDB(
//             inbox_id,  transid,  partnertid,  restype,  rescode,  idpel,  nama,
//             kwh,  jumlahrek,  outstanding,  tagihan,  denda,  admin,  insentive,
//             bulan,  mlalu,  refnbr,  info,  saldo,  create_date,  destination,
//             telpon,   xml,  iso,
//             ca_id,  merchant,  total_electricity_bill,  ppn,
//             penalty,  terminal_id,  dt_trx
//            );
        }
        finally {
            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        }
    }

    public String processInquiry(String hasil) {

        String[] result = post.parseInquiryMsgResponse(hasil,false);

        if (result[8].equalsIgnoreCase("0000")) {

            String[] bit48 = post.parseBit48Inquiry(result[10],result[8]);

            String blth_summary = "";
            String stand_meter_summary = "", sm_last = "";

            String switcher_id = bit48[0];
            String subscriber_id = bit48[1];
            String bill_status = bit48[2];
            String outstanding_bill = bit48[3];
            String switcher_ref = bit48[4];
            String subscriber_name = bit48[5];
            String service_unit = bit48[6];
            String service_unit_phone = bit48[7];
            String subscriber_segmentation = bit48[8];
            String power_consuming_category = bit48[9];
            String total_admin_charge = bit48[10];

            //1 bill
            String bill_period = bit48[11];
            String due_date = bit48[12];
            String meter_read_date = bit48[13];
            String total_electricity_bill = bit48[14];
            String incentive = bit48[15];
            String vat = bit48[16];
            String penalty_fee = bit48[17];
            String prev_meter_reading1 = bit48[18];
            String curr_meter_reading1 = bit48[19];
            String prev_meter_reading2 = bit48[20];
            String curr_meter_reading2 = bit48[21];
            String prev_meter_reading3 = bit48[22];
            String curr_meter_reading3 = bit48[23];

            String bill_period_ = "",bill_period__ = "",bill_period___ = "";
            String due_date_ = "",due_date__ = "",due_date___ = "";
            String meter_read_date_ = "",meter_read_date__ = "",meter_read_date___ = "";
            String total_electricity_bill_ = "",total_electricity_bill__ = "",total_electricity_bill___ = "";
            String incentive_ = "",incentive__ = "",incentive___ = "";
            String vat_ = "",vat__ = "",vat___ = "";
            String penalty_fee_ = "",penalty_fee__ = "",penalty_fee___ = "";
            String prev_meter_reading1_ = "",prev_meter_reading1__ = "",prev_meter_reading1___ = "";
            String curr_meter_reading1_ = "",curr_meter_reading1__= "",curr_meter_reading1___ = "";
            String prev_meter_reading2_ = "",prev_meter_reading2__ = "",prev_meter_reading2___ = "";
            String curr_meter_reading2_ = "",curr_meter_reading2__ = "",curr_meter_reading2___ = "";
            String prev_meter_reading3_ = "",prev_meter_reading3__ = "",prev_meter_reading3___ = "";
            String curr_meter_reading3_ = "",curr_meter_reading3__ = "",curr_meter_reading3___ = "";

            if (Integer.parseInt(bill_status)>1) {
                //2 bill
                bill_period_ = bit48[11+13];
                due_date_ = bit48[12+13];
                meter_read_date_ = bit48[13+13];
                total_electricity_bill_ = bit48[14+13];
                incentive_ = bit48[15+13];
                vat_ = bit48[16+13];
                penalty_fee_ = bit48[17+13];
                prev_meter_reading1_ = bit48[18+13];
                curr_meter_reading1_ = bit48[19+13];
                prev_meter_reading2_ = bit48[20+13];
                curr_meter_reading2_ = bit48[21+13];
                prev_meter_reading3_ = bit48[22+13];
                curr_meter_reading3_ = bit48[23+13];
            }

            if (Integer.parseInt(bill_status)>2) {
                //3 bill
                bill_period__ = bit48[11+(13*2)];
                due_date__ = bit48[12+(13*2)];
                meter_read_date__ = bit48[13+(13*2)];
                total_electricity_bill__ = bit48[14+(13*2)];
                incentive__ = bit48[15+(13*2)];
                vat__ = bit48[16+(13*2)];
                penalty_fee__ = bit48[17+(13*2)];
                prev_meter_reading1__ = bit48[18+(13*2)];
                curr_meter_reading1__ = bit48[19+(13*2)];
                prev_meter_reading2__ = bit48[20+(13*2)];
                curr_meter_reading2__ = bit48[21+(13*2)];
                prev_meter_reading3__ = bit48[22+(13*2)];
                curr_meter_reading3__ = bit48[23+(13*2)];
            }

            if (Integer.parseInt(bill_status)>3) {
                //4 bill
                bill_period___ = bit48[11+(13*3)];
                due_date___ = bit48[12+(13*3)];
                meter_read_date___ = bit48[13+(13*3)];
                total_electricity_bill___ = bit48[14+(13*3)];
                incentive___ = bit48[15+(13*3)];
                vat___ = bit48[16+(13*3)];
                penalty_fee___ = bit48[17+(13*3)];
                prev_meter_reading1___ = bit48[18+(13*3)];
                curr_meter_reading1___ = bit48[19+(13*3)];
                prev_meter_reading2___ = bit48[20+(13*3)];
                curr_meter_reading2___ = bit48[21+(13*3)];
                prev_meter_reading3___ = bit48[22+(13*3)];
                curr_meter_reading3___ = bit48[23+(13*3)];
            }

            //amount currency + minor desimal + amount

            hasil  = "<?xml version='1.0' encoding='UTF-8'?>"+
                       "<response>"+ 
                       "<produk>PLNPOSTPAID</produk>";

            if (result[0].equalsIgnoreCase("2400") || result[0].equalsIgnoreCase("2410")){
                hasil  += "<msg_type>REVERSAL</msg_type>";
            }
            
            subscriber_name = subscriber_name.replaceAll("<", ".");
            subscriber_name = subscriber_name.replaceAll(">", ",");

            hasil  += "<amount>"+result[2].substring(4)+"</amount>"+
                       "<stan>"+result[3]+"</stan>"+
                       "<datetime>"+result[4]+"</datetime>"+
                       "<merchant_code>"+result[5]+"</merchant_code>"+
                       "<bank_code>"+result[6]+"</bank_code>"+
                       "<rc>"+result[8]+"</rc>"+
                       "<terminal_id>"+result[9]+"</terminal_id>"+
                       "<subscriber_id>"+subscriber_id+"</subscriber_id>"+
                       "<bill_status>"+bill_status+"</bill_status>"+
                       "<outstanding_bill>"+outstanding_bill+"</outstanding_bill>"+
                       "<switcher_refno>"+switcher_ref+"</switcher_refno>"+
                       "<subscriber_name>"+subscriber_name+"</subscriber_name>"+
                       "<service_unit>"+service_unit+"</service_unit>"+
                       "<service_unit_phone>"+service_unit_phone+"</service_unit_phone>"+
                       "<subscriber_segmentation>"+subscriber_segmentation+"</subscriber_segmentation>"+
                       "<power>"+power_consuming_category+"</power>"+
                       "<admin_charge>"+total_admin_charge+"</admin_charge>"+
                       "<bills>" +

                       "<bill>"+
                       "<bill_period>"+bill_period+"</bill_period>"+
                       "<due_date>"+due_date+"</due_date>"+
                       "<meter_read_date>"+meter_read_date+"</meter_read_date>"+
                       "<total_electricity_bill>"+total_electricity_bill+"</total_electricity_bill>"+
                       "<incentive>"+incentive+"</incentive>"+
                       "<value_added_tax>"+vat+"</value_added_tax>"+
                       "<penalty_fee>"+penalty_fee+"</penalty_fee>"+
                       "<previous_meter_reading1>"+prev_meter_reading1+"</previous_meter_reading1>"+
                       "<current_meter_reading1>"+curr_meter_reading1+"</current_meter_reading1>"+
                       "<previous_meter_reading2>"+prev_meter_reading2+"</previous_meter_reading2>"+
                       "<current_meter_reading2>"+curr_meter_reading2+"</current_meter_reading2>"+
                       "<previous_meter_reading3>"+prev_meter_reading3+"</previous_meter_reading3>"+
                       "<current_meter_reading3>"+curr_meter_reading3+"</current_meter_reading3>"+
                       "</bill>";

                       blth_summary += getMonthName(bill_period.substring(4))+bill_period.substring(2,4);
                       stand_meter_summary += prev_meter_reading1;
                       sm_last = curr_meter_reading1;

                       if (Integer.parseInt(bill_status)>1) {

                           hasil+=   "<bill>"+
                                       "<bill_period>"+bill_period_+"</bill_period>"+
                                       "<due_date>"+due_date_+"</due_date>"+
                                       "<meter_read_date>"+meter_read_date_+"</meter_read_date>"+
                                       "<total_electricity_bill>"+total_electricity_bill_+"</total_electricity_bill>"+
                                       "<incentive>"+incentive_+"</incentive>"+
                                       "<value_added_tax>"+vat_+"</value_added_tax>"+
                                       "<penalty_fee>"+penalty_fee_+"</penalty_fee>"+
                                       "<previous_meter_reading1>"+prev_meter_reading1_+"</previous_meter_reading1>"+
                                       "<current_meter_reading1>"+curr_meter_reading1_+"</current_meter_reading1>"+
                                       "<previous_meter_reading2>"+prev_meter_reading2_+"</previous_meter_reading2>"+
                                       "<current_meter_reading2>"+curr_meter_reading2_+"</current_meter_reading2>"+
                                       "<previous_meter_reading3>"+prev_meter_reading3_+"</previous_meter_reading3>"+
                                       "<current_meter_reading3>"+curr_meter_reading3_+"</current_meter_reading3>"+
                                       "</bill>";

                           blth_summary += ", "+getMonthName(bill_period_.substring(4))+bill_period_.substring(2,4);
                           sm_last = curr_meter_reading1_;

                       }

                       if (Integer.parseInt(bill_status)>2) {
                           hasil+="<bill>"+
                                   "<bill_period>"+bill_period__+"</bill_period>"+
                                   "<due_date>"+due_date__+"</due_date>"+
                                   "<meter_read_date>"+meter_read_date__+"</meter_read_date>"+
                                   "<total_electricity_bill>"+total_electricity_bill__+"</total_electricity_bill>"+
                                   "<incentive>"+incentive__+"</incentive>"+
                                   "<value_added_tax>"+vat__+"</value_added_tax>"+
                                   "<penalty_fee>"+penalty_fee__+"</penalty_fee>"+
                                   "<previous_meter_reading1>"+prev_meter_reading1__+"</previous_meter_reading1>"+
                                   "<current_meter_reading1>"+curr_meter_reading1__+"</current_meter_reading1>"+
                                   "<previous_meter_reading2>"+prev_meter_reading2__+"</previous_meter_reading2>"+
                                   "<current_meter_reading2>"+curr_meter_reading2__+"</current_meter_reading2>"+
                                   "<previous_meter_reading3>"+prev_meter_reading3__+"</previous_meter_reading3>"+
                                   "<current_meter_reading3>"+curr_meter_reading3__+"</current_meter_reading3>"+
                                   "</bill>";

                           blth_summary += ", "+getMonthName(bill_period__.substring(4))+bill_period__.substring(2,4);
                           sm_last = curr_meter_reading1__;
                        }

                        if (Integer.parseInt(bill_status)>3) {
                           hasil+="<bill>"+
                                   "<bill_period>"+bill_period___+"</bill_period>"+
                                   "<due_date>"+due_date___+"</due_date>"+
                                   "<meter_read_date>"+meter_read_date___+"</meter_read_date>"+
                                   "<total_electricity_bill>"+total_electricity_bill___+"</total_electricity_bill>"+
                                   "<incentive>"+incentive___+"</incentive>"+
                                   "<value_added_tax>"+vat___+"</value_added_tax>"+
                                   "<penalty_fee>"+penalty_fee___+"</penalty_fee>"+
                                   "<previous_meter_reading1>"+prev_meter_reading1___+"</previous_meter_reading1>"+
                                   "<current_meter_reading1>"+curr_meter_reading1___+"</current_meter_reading1>"+
                                   "<previous_meter_reading2>"+prev_meter_reading2___+"</previous_meter_reading2>"+
                                   "<current_meter_reading2>"+curr_meter_reading2___+"</current_meter_reading2>"+
                                   "<previous_meter_reading3>"+prev_meter_reading3___+"</previous_meter_reading3>"+
                                   "<current_meter_reading3>"+curr_meter_reading3___+"</current_meter_reading3>"+
                                   "</bill>";

                           blth_summary += ", "+getMonthName(bill_period___.substring(4))+bill_period___.substring(2,4);
                           sm_last = curr_meter_reading1___;
                       }

                       stand_meter_summary +=" - "+sm_last;

                       hasil+="</bills>"
                               + "<blth_summary>"+blth_summary+"</blth_summary>"
                               + "<stand_meter_summary>"+stand_meter_summary+"</stand_meter_summary>"+
                             "</response>";

            }
            else {
                    hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                         + "<response>"
                         + "<stan>"+result[3]+"</stan>"   
                         + "<code>"+result[8]+"</code>"
                         + "<produk>PLNPOSTPAID</produk>" 
                         + "<desc>"+postRC.getInquiryMsgResponseName(result[8])+"</desc>"
                         + "</response>";
            }
       
        return hasil;
    }

    public String processReversal(String hasil) {

        String[] result = post.parsePaymentMsgResponse(hasil,false);

        if (result[9].equalsIgnoreCase("0000")) {

            String[] bit48 = post.parseBit48Payment(result[11],result[9]);

            String switcher_id = bit48[0];
            String subscriber_id = bit48[1];
            String bill_status = bit48[2];
            String payment_status = bit48[2];
            String outstanding_bill = bit48[3];
            String switcher_ref = bit48[4];
            String subscriber_name = bit48[5];
            String service_unit = bit48[6];
            String service_unit_phone = bit48[7];
            String subscriber_segmentation = bit48[8];
            String power_consuming_category = bit48[9];
            String total_admin_charge = bit48[10];

            //1 bill
            String bill_period = bit48[11];
            String due_date = bit48[12];
            String meter_read_date = bit48[13];
            String total_electricity_bill = bit48[14];
            String incentive = bit48[15];
            String vat = bit48[16];
            String penalty_fee = bit48[17];
            String prev_meter_reading1 = bit48[18];
            String curr_meter_reading1 = bit48[19];
            String prev_meter_reading2 = bit48[20];
            String curr_meter_reading2 = bit48[21];
            String prev_meter_reading3 = bit48[22];
            String curr_meter_reading3 = bit48[23];

            String bill_period_ = "",bill_period__ = "",bill_period___ = "";
            String due_date_ = "",due_date__ = "",due_date___ = "";
            String meter_read_date_ = "",meter_read_date__ = "",meter_read_date___ = "";
            String total_electricity_bill_ = "",total_electricity_bill__ = "",total_electricity_bill___ = "";
            String incentive_ = "",incentive__ = "",incentive___ = "";
            String vat_ = "",vat__ = "",vat___ = "";
            String penalty_fee_ = "",penalty_fee__ = "",penalty_fee___ = "";
            String prev_meter_reading1_ = "",prev_meter_reading1__ = "",prev_meter_reading1___ = "";
            String curr_meter_reading1_ = "",curr_meter_reading1__= "",curr_meter_reading1___ = "";
            String prev_meter_reading2_ = "",prev_meter_reading2__ = "",prev_meter_reading2___ = "";
            String curr_meter_reading2_ = "",curr_meter_reading2__ = "",curr_meter_reading2___ = "";
            String prev_meter_reading3_ = "",prev_meter_reading3__ = "",prev_meter_reading3___ = "";
            String curr_meter_reading3_ = "",curr_meter_reading3__ = "",curr_meter_reading3___ = "";

            if (Integer.parseInt(bill_status)>1) {
                //2 bill
                bill_period_ = bit48[11+13];
                due_date_ = bit48[12+13];
                meter_read_date_ = bit48[13+13];
                total_electricity_bill_ = bit48[14+13];
                incentive_ = bit48[15+13];
                vat_ = bit48[16+13];
                penalty_fee_ = bit48[17+13];
                prev_meter_reading1_ = bit48[18+13];
                curr_meter_reading1_ = bit48[19+13];
                prev_meter_reading2_ = bit48[20+13];
                curr_meter_reading2_ = bit48[21+13];
                prev_meter_reading3_ = bit48[22+13];
                curr_meter_reading3_ = bit48[23+13];
            }

            if (Integer.parseInt(bill_status)>2) {
                //3 bill
                bill_period__ = bit48[11+(13*2)];
                due_date__ = bit48[12+(13*2)];
                meter_read_date__ = bit48[13+(13*2)];
                total_electricity_bill__ = bit48[14+(13*2)];
                incentive__ = bit48[15+(13*2)];
                vat__ = bit48[16+(13*2)];
                penalty_fee__ = bit48[17+(13*2)];
                prev_meter_reading1__ = bit48[18+(13*2)];
                curr_meter_reading1__ = bit48[19+(13*2)];
                prev_meter_reading2__ = bit48[20+(13*2)];
                curr_meter_reading2__ = bit48[21+(13*2)];
                prev_meter_reading3__ = bit48[22+(13*2)];
                curr_meter_reading3__ = bit48[23+(13*2)];
            }

            if (Integer.parseInt(bill_status)>3) {
                //4 bill
                bill_period___ = bit48[11+(13*3)];
                due_date___ = bit48[12+(13*3)];
                meter_read_date___ = bit48[13+(13*3)];
                total_electricity_bill___ = bit48[14+(13*3)];
                incentive___ = bit48[15+(13*3)];
                vat___ = bit48[16+(13*3)];
                penalty_fee___ = bit48[17+(13*3)];
                prev_meter_reading1___ = bit48[18+(13*3)];
                curr_meter_reading1___ = bit48[19+(13*3)];
                prev_meter_reading2___ = bit48[20+(13*3)];
                curr_meter_reading2___ = bit48[21+(13*3)];
                prev_meter_reading3___ = bit48[22+(13*3)];
                curr_meter_reading3___ = bit48[23+(13*3)];
            }
            
            subscriber_name = subscriber_name.replaceAll("<", ".");
            subscriber_name = subscriber_name.replaceAll(">", ",");

            hasil  = "<?xml version='1.0' encoding='UTF-8'?>"+
                       "<response>"+
                       "<produk>PLNPOSTPAID</produk>" +
                       "<msg_type>REVERSAL</msgtype>"+
                       "<amount>"+result[2].substring(4)+"</amount>"+
                       "<stan>"+result[3]+"</stan>"+
                       "<datetime>"+result[4]+"</datetime>"+
                       "<date_settlement>"+result[5]+"</date_settlement>"+
                       "<merchant_code>"+result[6]+"</merchant_code>"+
                       "<bank_code>"+result[7]+"</bank_code>"+
                       "<rc>"+result[9]+"</rc>"+
                       "<terminal_id>"+result[10]+"</terminal_id>"+
                       "<bill_status>"+bill_status+"</bill_status>"+
                       "<payment_status>"+result[7]+"</payment_status>"+
                       "<subscriber_id>"+subscriber_id+"</subscriber_id>"+
                       "<switcher_refno>"+switcher_ref+"</switcher_refno>"+
                       "<subscriber_name>"+subscriber_name+"</subscriber_name>"+
                       "<service_unit>"+service_unit+"</service_unit>"+
                       "<service_unit_phone>"+service_unit_phone+"</service_unit_phone>"+
                       "<subscriber_segmentation>"+subscriber_segmentation+"</subscriber_segmentation>"+
                       "<power>"+power_consuming_category+"</power>"+
                       "<admin_charge>"+total_admin_charge+"</admin_charge>"+
                       "<info_text>"+result[12]+"</admin_charge>"+
                       "<bills>" +
                       "<bill>"+
                       "<bill_period>"+bill_period+"</bill_period>"+
                       "<due_date>"+due_date+"</due_date>"+
                       "<meter_read_date>"+meter_read_date+"</meter_read_date>"+
                       "<total_electricity_bill>"+total_electricity_bill+"</total_electricity_bill>"+
                       "<incentive>"+incentive+"</incentive>"+
                       "<value_added_tax>"+vat+"</value_added_tax>"+
                       "<penalty_fee>"+penalty_fee+"</penalty_fee>"+
                       "<previous_meter_reading1>"+prev_meter_reading1+"</previous_meter_reading1>"+
                       "<current_meter_reading1>"+curr_meter_reading1+"</current_meter_reading1>"+
                       "<previous_meter_reading2>"+prev_meter_reading2+"</previous_meter_reading2>"+
                       "<current_meter_reading2>"+curr_meter_reading2+"</current_meter_reading2>"+
                       "<previous_meter_reading3>"+prev_meter_reading3+"</previous_meter_reading3>"+
                       "<current_meter_reading3>"+curr_meter_reading3+"</current_meter_reading3>"+
                       "</bill>";

                 if (Integer.parseInt(bill_status)>1) {

                       hasil+=   "<bill>"+
                                   "<bill_period>"+bill_period_+"</bill_period>"+
                                   "<due_date>"+due_date_+"</due_date>"+
                                   "<meter_read_date>"+meter_read_date_+"</meter_read_date>"+
                                   "<total_electricity_bill>"+total_electricity_bill_+"</total_electricity_bill>"+
                                   "<incentive>"+incentive_+"</incentive>"+
                                   "<value_added_tax>"+vat_+"</value_added_tax>"+
                                   "<penalty_fee>"+penalty_fee_+"</penalty_fee>"+
                                   "<previous_meter_reading1>"+prev_meter_reading1_+"</previous_meter_reading1>"+
                                   "<current_meter_reading1>"+curr_meter_reading1_+"</current_meter_reading1>"+
                                   "<previous_meter_reading2>"+prev_meter_reading2_+"</previous_meter_reading2>"+
                                   "<current_meter_reading2>"+curr_meter_reading2_+"</current_meter_reading2>"+
                                   "<previous_meter_reading3>"+prev_meter_reading3_+"</previous_meter_reading3>"+
                                   "<current_meter_reading3>"+curr_meter_reading3_+"</current_meter_reading3>"+
                                   "</bill>";

                   }

                   if (Integer.parseInt(bill_status)>2) {
                       hasil+="<bill>"+
                               "<bill_period>"+bill_period__+"</bill_period>"+
                               "<due_date>"+due_date__+"</due_date>"+
                               "<meter_read_date>"+meter_read_date__+"</meter_read_date>"+
                               "<total_electricity_bill>"+total_electricity_bill__+"</total_electricity_bill>"+
                               "<incentive>"+incentive__+"</incentive>"+
                               "<value_added_tax>"+vat__+"</value_added_tax>"+
                               "<penalty_fee>"+penalty_fee__+"</penalty_fee>"+
                               "<previous_meter_reading1>"+prev_meter_reading1__+"</previous_meter_reading1>"+
                               "<current_meter_reading1>"+curr_meter_reading1__+"</current_meter_reading1>"+
                               "<previous_meter_reading2>"+prev_meter_reading2__+"</previous_meter_reading2>"+
                               "<current_meter_reading2>"+curr_meter_reading2__+"</current_meter_reading2>"+
                               "<previous_meter_reading3>"+prev_meter_reading3__+"</previous_meter_reading3>"+
                               "<current_meter_reading3>"+curr_meter_reading3__+"</current_meter_reading3>"+
                               "</bill>";
                    }

                    if (Integer.parseInt(bill_status)>3) {
                       hasil+="<bill>"+
                               "<bill_period>"+bill_period___+"</bill_period>"+
                               "<due_date>"+due_date___+"</due_date>"+
                               "<meter_read_date>"+meter_read_date___+"</meter_read_date>"+
                               "<total_electricity_bill>"+total_electricity_bill___+"</total_electricity_bill>"+
                               "<incentive>"+incentive___+"</incentive>"+
                               "<value_added_tax>"+vat___+"</value_added_tax>"+
                               "<penalty_fee>"+penalty_fee___+"</penalty_fee>"+
                               "<previous_meter_reading1>"+prev_meter_reading1___+"</previous_meter_reading1>"+
                               "<current_meter_reading1>"+curr_meter_reading1___+"</current_meter_reading1>"+
                               "<previous_meter_reading2>"+prev_meter_reading2___+"</previous_meter_reading2>"+
                               "<current_meter_reading2>"+curr_meter_reading2___+"</current_meter_reading2>"+
                               "<previous_meter_reading3>"+prev_meter_reading3___+"</previous_meter_reading3>"+
                               "<current_meter_reading3>"+curr_meter_reading3___+"</current_meter_reading3>"+
                               "</bill>";
                   }

                   hasil+="</bills>"+
                         "</response>";

            }
            else {

                hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                     + "<response>"
                     + "<stan>"+result[3]+"</stan>"   
                     + "<produk>PLNPOSTPAID</produk>"   
                     + "<code>"+result[9]+"</code>"
                     + "<desc>"+postRC.getPurchaseMsgResponseName(result[9])+"</desc>"
                     + "</response>";
            }

        return hasil;
    }
    
}
