package huaweistruk;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
//import org.apache.log4j.Priority;

/**
 *
 * @author ngonar
 */
public class PrepaidResponseCode {

    public PrepaidResponseCode() {

    }

    public String getPurchaseMsgResponseName(String code) {
        if (code.equalsIgnoreCase("0000")) {
            return "SUCCESSFUL";
        } else if (code.equalsIgnoreCase("0005")) {
            return "ERROR-LAINNYA";
        } else if (code.equalsIgnoreCase("0011")) {
            return "ERROR-SIGN ON TERLEBIH DAHULU";
        } else if (code.equalsIgnoreCase("0012")) {
            return "ERROR-REVERSAL MELEBIHI BATAS WAKTU";
        } else if (code.equalsIgnoreCase("0013")) {
//            return "ERROR-Invalid Transaction Amount";
            return "ERROR-AMOUNT TRANSAKSI TIDAK VALID";
        } else if (code.equalsIgnoreCase("0014")) {
//            return "ERROR-Unknown subscriber";
            return "IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI";
        } else if (code.equalsIgnoreCase("0030")) {
            return "ERROR-PESAN TIDAK VALID";
        } else if (code.equalsIgnoreCase("0031")) {
            return "ERROR-KODE BANK TIDAK TERDAFTAR";
        } else if (code.equalsIgnoreCase("0032")) {
            return "ERROR-SWITCHING TIDAK TERDAFTAR";
        } else if (code.equalsIgnoreCase("0033")) {
            return "ERROR-PRODUCT TIDAK TERDAFTAR";
        } else if (code.equalsIgnoreCase("0034")) {
            return "ERROR-TERMINAL TIDAK TERDAFTAR";
        } else if (code.equalsIgnoreCase("0041")) {
            //return "ERROR-Transaction Amount below minimum purchase amount";
            return "PEMBELIAN MINIMAL RP. 20RIBU";
        } else if (code.equalsIgnoreCase("0042")) {
            //return "ERROR-Transaction Amount exceed maximum purchase amount";
            return "PEMBELIAN MINIMAL RP. 20RIBU";
        } else if (code.equalsIgnoreCase("0045")) {
            return "ERROR-BIAYA ADMIN TIDAK VALID";
        } else if (code.equalsIgnoreCase("0046")) {
            return "ERROR-SALDO ANDA TIDAK MENCUKUPI";
        } else if (code.equalsIgnoreCase("0047")) {
            return "ERROR-TOTAL KWH MELEBIHI BATAS MAKSIMUM";
        } else if (code.equalsIgnoreCase("0068")) {
            return "ERROR-WAKTU TRANSAKSI TELAH HABIS";
        } else if (code.equalsIgnoreCase("0077")) {
            return "ERROR-IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI";
        } else if (code.equalsIgnoreCase("0088")) {
            return "ERROR-TAGIHAN SUDAH TERBAYAR";
        } else if (code.equalsIgnoreCase("0089")) {
            return "ERROR-TAGIHAN BULAN [BERJALAN] BELUM TERSEDIA";
        } else if (code.equalsIgnoreCase("0090")) {
            return "ERROR-CUT-OFF SEDANG DILAKUKAN";
        } else if (code.equalsIgnoreCase("0092")) {
            return "ERROR-SWITCHER RECEIPT";
        } else if (code.equalsIgnoreCase("0093")) {
            return "ERROR-NOMOR REFERENSI SWITCHING TIDAK VALID";
        } else if (code.equalsIgnoreCase("0097")) {
            return "ERROR-SWITCHING ID DAN / ATAU KODE BANK TIDAK SAMA DENGAN SAAT INQUIRY";
        } else if (code.equalsIgnoreCase("0098")) {
            return "ERROR-NOMOR REFERENSI PLN TIDAK VALID";
        }

        return "";
    }

    public String getInquiryMsgResponseName(String code) {
        if (code.equalsIgnoreCase("0000")) {
            return "SUCCESSFUL";
        } else if (code.equalsIgnoreCase("0005")) {
            return "ERROR-LAINNYA";
        } else if (code.equalsIgnoreCase("0009")) {
            return "NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI";
        } else if (code.equalsIgnoreCase("0011")) {
            return "ERROR-SIGN ON TERLEBIH DAHULU";
        } else if (code.equalsIgnoreCase("0014")) {
            //return "ERROR-Unknown subscriber";
            return "NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI";
        } else if (code.equalsIgnoreCase("0030")) {
            return "ERROR-PESAN TIDAK VALID";
        } else if (code.equalsIgnoreCase("0031")) {
            return "ERROR-KODE BANK TIDAK TERDAFTAR";
        } else if (code.equalsIgnoreCase("0032")) {
            return "ERROR-SWITCHING TIDAK TERDAFTAR";
        } else if (code.equalsIgnoreCase("0034")) {
            return "ERROR-TERMINAL TIDAK TERDAFTAR";
        } else if (code.equalsIgnoreCase("0033")) {
            return "ERROR-PRODUCT TIDAK TERDAFTAR";
        } else if (code.equalsIgnoreCase("0047")) {
            return "TOTAL KWH MELEBIHI BATAS MAKSIMUM";
        } else if (code.equalsIgnoreCase("0068")) {
            return "ERROR-WAKTU TRANSAKSI HABIS";
        } else if (code.equalsIgnoreCase("0077")) {
            //return "ERROR-Subscriber suspended";
            return "NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI";
        } else if (code.equalsIgnoreCase("0090")) {
            return "ERROR-CUT-OFF SEDANG DILAKUKAN";
        }

        return "";
    }

    public String getNetworkMsgResponseName(String code) {
        if (code.equalsIgnoreCase("0000")) {
            return "SUCCESSFUL";
        } else if (code.equalsIgnoreCase("0005")) {
            return "ERROR-LAINNYA";
        } else if (code.equalsIgnoreCase("0011")) {
            return "ERROR-SIGN ON TERLEBIH DAHULU";
        } else if (code.equalsIgnoreCase("0030")) {
            return "ERROR-PESAN TIDAK VALID";
        } else if (code.equalsIgnoreCase("0032")) {
            return "ERROR-SWITCHING TIDAK TERDAFTAR";
        } else if (code.equalsIgnoreCase("0068")) {
            return "ERROR-WAKTU TRANSAKSI TELAH HABIS";
        } else if (code.equalsIgnoreCase("0090")) {
            return "ERROR-CUT-OFF SEDANG DILAKUKAN";
        }

        return "";
    }


    public String responseParser(String code) {
        String keluaran = "";

        try {
            Map<String, String> slsRC = new HashMap<String, String>();
            slsRC.put("0000", " Approved ");
            slsRC.put("0005", " Error lainnya ");
            slsRC.put("0014", " NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH  MOHON TELITI KEMBALI");
//slsRC.put("0014"," NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH  MOHON TELITI KEMBALI");
            slsRC.put("0068", " Request Timeout");
            slsRC.put("0031", " Id Bank masih belum didaftarkan");
            slsRC.put("0032", " Switcher Id belum terdaftar");
//            slsRC.put("0033", " Produk belum terdaftar");
            slsRC.put("0047", " Total KWH Melebih Batas");
            slsRC.put("0017", " Deposit tidak mencukupi untuk melakukan transaksi");
            slsRC.put("0041", " “PEMBELIAN MINIMAL RP. 20 RIBU  MAKSIMAL RP. 1 JUTA");
//slsRC.put("0042","");
//slsRC.put("0014"," NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH  MOHON TELITI kembali");
            slsRC.put("0090", "Transaksi tidak bisa dilakukan karena cut off sedang dalam progress ");
            slsRC.put("0063", "Advice tidak berhasil,tidak ada purchase");
            slsRC.put("0098", "PLN reference number tidak sesuai dengan PLN Refnum saat Inquery Gagal");
            slsRC.put("0031", " Kode bank belum terdaftar, silahkan hubungin tim Jatelindo Gagal");
//slsRC.put("0033","Kode produk belum terdaftar di TID, silahkan hubungi tim Jatelindo ");
            slsRC.put("0033", "Kode produk belum terdaftar di TID");

            keluaran = slsRC.get(code).toUpperCase();

        } catch (Exception e) {

            if (code.equalsIgnoreCase("0088")) {
                keluaran = "TAGIHAN SUDAH TERBAYAR";
            } else if (code.equalsIgnoreCase("0015")) {
                keluaran = "NOMOR REGISTRASI YANG ANDA MASUKKAN SALAH. MOHON TELITI KEMBALI.";
            } else if (code.equalsIgnoreCase("0016")) {
                keluaran = "TRANSAKSI DITOLAK, HUBUNGI KANTOR PLN TERKAIT.";
            } else if (code.equalsIgnoreCase("0030")) {
                keluaran = "PESAN TIDAK DIKENAL";
            } else if (code.equalsIgnoreCase("0041")) {
                keluaran = "PEMBELIAN MINIMAL RP 20RIBU, MAKSIMAL RP. 1JUTA.";
            } else if (code.equalsIgnoreCase("0042")) {
                keluaran = "PEMBELIAN MINIMAL RP 20RIBU, MAKSIMAL RP. 1JUTA.";
            } else if (code.equalsIgnoreCase("0047")) {
                keluaran = "TOTAL KWH MELEBIHI BATAS";
            } else if (code.equalsIgnoreCase("0048")) {
                keluaran = "NOMOR REGISTRASI KADALUARSA, MOHON HUBUNGI PLN.";
            } else if (code.equalsIgnoreCase("0068")) {
                keluaran = "WAKTU TRANSAKSI HABIS, COBA SESAAT LAGI";
            } else if (code.equalsIgnoreCase("0088")) {
                keluaran = "TAGIHAN SUDAH TERBAYAR.";
            } else if (code.equalsIgnoreCase("0089")) {
                keluaran = "TAGIHAN BULAN BERJALAN BELUM TERSEDIA";
            } else if (code.equalsIgnoreCase("0077")) {
                keluaran = "NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH MOHON TELITI KEMBALI.";
            } else if (code.equalsIgnoreCase("0014")) {
                keluaran = "IDPEL YANG ANDA MASUKKAN SALAH MOHON TELITI KEMBALI.";
            } else if (code.equalsIgnoreCase("0097")) {
                keluaran = "FORMAT SALAH";
            } else if (code.equalsIgnoreCase("0009")) {
                keluaran = "NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH MOHON TELITI KEMBALI.";
            } else if (code.equalsIgnoreCase("400")) {
                keluaran = "SALDO ANDA TIDAK MENCUKUPI";
            } else if (code.equalsIgnoreCase("0095")) {
                keluaran = "GANGGUAN PADA SISTEM, SILAHKAN COBA SESAAT LAGI";
            } else {
                keluaran = "";
            }
        }
        return keluaran;
    }
}
