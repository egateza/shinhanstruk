package huaweistruk;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ngonar
 */
public class NonTagListResponseCode {

    public NonTagListResponseCode() {

    }

    public String getReversalMsgResponseName(String code) {
        if (code.equalsIgnoreCase("0000")) {
            return "SUCCESSFUL";
        } else if (code.equalsIgnoreCase("0004")) {
            return "ERROR-UNREGISTERED BILLER";
        } else if (code.equalsIgnoreCase("0005")) {
            return "ERROR-OTHER";
        } else if (code.equalsIgnoreCase("0006")) {
            return "ERROR-blocked partner central";
        } else if (code.equalsIgnoreCase("0007")) {
            return "ERROR-blocked terminal";
        } else if (code.equalsIgnoreCase("0008")) {
            return "ERROR-invalid access time";
        } else if (code.equalsIgnoreCase("0011")) {
            return "ERROR-Need to sign on";
        } else if (code.equalsIgnoreCase("0012")) {
            return "ERROR-Reversal melebihi batas waktu";
        } else if (code.equalsIgnoreCase("0013")) {
            return "ERROR-Invalid Transaction Amount";
        } else if (code.equalsIgnoreCase("0014")) {
            return "ERROR-Unknown subscriber";
        } else if (code.equalsIgnoreCase("0030")) {
            return "ERROR-Invalid message";
        } else if (code.equalsIgnoreCase("0031")) {
            return "ERROR-Unregistered Bank Code";
        } else if (code.equalsIgnoreCase("0032")) {
            return "ERROR-Unregistered Switching";
        } else if (code.equalsIgnoreCase("0033")) {
            return "ERROR-Unregistered Product";
        } else if (code.equalsIgnoreCase("0034")) {
            return "ERROR-Unregistered Terminal";
        } else if (code.equalsIgnoreCase("0041")) {
            return "ERROR-Transaction Amount below minimum purchase amount";
        } else if (code.equalsIgnoreCase("0042")) {
            return "ERROR-Transaction Amount exceed maximum purchase amount";
        } else if (code.equalsIgnoreCase("0045")) {
            return "ERROR-Invalid admin charge";
        } else if (code.equalsIgnoreCase("0046")) {
            return "ERROR-Insufficient Deposit";
        } else if (code.equalsIgnoreCase("0047")) {
            return "ERROR-Total KWH is over the limit";
        } else if (code.equalsIgnoreCase("0063")) {
            return "ERROR-No payment";
        } else if (code.equalsIgnoreCase("0068")) {
            return "WAKTU TRANSAKSI HABIS, COBA SESAAT LAGI";
            //return "ERROR-Timeout";
        } else if (code.equalsIgnoreCase("0077")) {
            return "ERROR-Subscriber suspended";
        } else if (code.equalsIgnoreCase("0088")) {
            return "ERROR-Bills already paid";
        } else if (code.equalsIgnoreCase("0089")) {
            return "ERROR-Current Bill is not available";
        } else if (code.equalsIgnoreCase("0090")) {
            return "ERROR-Cut-off is in progress";
        } else if (code.equalsIgnoreCase("0092")) {
            return "ERROR-Invalid Starlink Solusi Reference Nuumber";
        } else if (code.equalsIgnoreCase("0093")) {
            return "ERROR-Invalid Partner Central Trace Audit Number";
        } else if (code.equalsIgnoreCase("0094")) {
            return "ERROR-Reversal had been done";
        } else if (code.equalsIgnoreCase("0097")) {
            return "ERROR-Switching ID and / or Bank Code is not identical with payment";
        } else if (code.equalsIgnoreCase("0098")) {
            return "ERROR-PLN Ref Number is not valid";
        }

        return "";
    }

    public String getPurchaseMsgResponseName(String code) {
        if (code.equalsIgnoreCase("0000")) {
            return "SUCCESSFUL";
        } else if (code.equalsIgnoreCase("0004")) {
            return "ERROR-UNREGISTERED BILLER";
        } else if (code.equalsIgnoreCase("0005")) {
            return "ERROR-OTHER";
        } else if (code.equalsIgnoreCase("0006")) {
            return "ERROR-blocked partner central";
        } else if (code.equalsIgnoreCase("0007")) {
            return "ERROR-blocked terminal";
        } else if (code.equalsIgnoreCase("0008")) {
            return "ERROR-invalid access time";
        } else if (code.equalsIgnoreCase("0011")) {
            return "ERROR-Need to sign on";
        } else if (code.equalsIgnoreCase("0013")) {
            return "ERROR-Invalid Transaction Amount";
        } else if (code.equalsIgnoreCase("0014")) {
            return "ERROR-Unknown subscriber";
        } else if (code.equalsIgnoreCase("0016")) {
            return "ERROR-PRR Subscriber";
        } else if (code.equalsIgnoreCase("0017")) {
            return "ERROR-Subscriber still have bills to pay";
        } else if (code.equalsIgnoreCase("0018")) {
            return "ERROR-Subscription is 3-phase subscriber";
        } else if (code.equalsIgnoreCase("0030")) {
            return "ERROR-Invalid message";
        } else if (code.equalsIgnoreCase("0031")) {
            return "ERROR-Unregistered Bank Code";
        } else if (code.equalsIgnoreCase("0032")) {
            return "ERROR-Unregistered Switching";
        } else if (code.equalsIgnoreCase("0033")) {
            return "ERROR-Unregistered Product";
        } else if (code.equalsIgnoreCase("0034")) {
            return "ERROR-Unregistered Terminal";
        } else if (code.equalsIgnoreCase("0041")) {
            return "ERROR-Transaction Amount below minimum purchase amount";
        } else if (code.equalsIgnoreCase("0042")) {
            return "ERROR-Transaction Amount exceed maximum purchase amount";
        } else if (code.equalsIgnoreCase("0043")) {
            return "ERROR-New power category is smaller";
        } else if (code.equalsIgnoreCase("0044")) {
            return "ERROR-New power category is not a valid value";
        } else if (code.equalsIgnoreCase("0045")) {
            return "ERROR-Invalid admin charge";
        } else if (code.equalsIgnoreCase("0046")) {
            return "ERROR-Inadequate deposit for terminal / client of legacy system. Please contact PLN";
        } else if (code.equalsIgnoreCase("0047")) {
            return "ERROR-Total KWH is over the limit";
        } else if (code.equalsIgnoreCase("0063")) {
            return "ERROR-No payment";
        } else if (code.equalsIgnoreCase("0068")) {
            return "WAKTU TRANSAKSI HABIS, COBA SESAAT LAGI";
            //return "ERROR-Timeout";
        } else if (code.equalsIgnoreCase("0077")) {
            return "ERROR-Subscriber suspended";
        } else if (code.equalsIgnoreCase("0088")) {
            //return "ERROR-Bills already paid";
            return "TAGIHAN SUDAH TERBAYAR";
        } else if (code.equalsIgnoreCase("0089")) {
            return "ERROR-Current Bill is not available";
        } else if (code.equalsIgnoreCase("0090")) {
            return "ERROR-Cut-off is in progress";
        } else if (code.equalsIgnoreCase("0092")) {
            return "ERROR-Invalid Starlink Solusi Reference Nuumber";
        } else if (code.equalsIgnoreCase("0093")) {
            return "ERROR-Invalid Switcher Trace Audit Number";
        } else if (code.equalsIgnoreCase("0094")) {
            return "ERROR-Reversal had been done";
        } else if (code.equalsIgnoreCase("0097")) {
            return "ERROR-Switching ID and / or Bank Code is not identical with payment";
        } else if (code.equalsIgnoreCase("0098")) {
            return "ERROR-PLN Ref Number is not valid";
        } else if (code.equalsIgnoreCase("400")) {
            return "SALDO ANDA TIDAK MENCUKUPI";
        }

        return "";
    }

    public String getInquiryMsgResponseName(String code) {
        if (code.equalsIgnoreCase("0000")) {
            return "SUCCESSFUL";
        } else if (code.equalsIgnoreCase("0004")) {
            return "ERROR-Unregistered biller";
        } else if (code.equalsIgnoreCase("0005")) {
            return "ERROR-OTHER";
        } else if (code.equalsIgnoreCase("0006")) {
            return "ERROR-Blocked partner central";
        } else if (code.equalsIgnoreCase("0007")) {
            return "ERROR-Blocked terminal";
        } else if (code.equalsIgnoreCase("0008")) {
            return "ERROR-Invalid access time";
        } else if (code.equalsIgnoreCase("0011")) {
            return "ERROR-Need to sign on";
        } else if (code.equalsIgnoreCase("0014")) {
            return "ERROR-Unknown subscriber";
        } else if (code.equalsIgnoreCase("0015")) {
            return "NOMOR REGISTRASI YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI";
        } else if (code.equalsIgnoreCase("0016")) {
            return "TRANSAKSI DITOLAK, HUBUNGI KANTOR PLN TERKAIT";
        } else if (code.equalsIgnoreCase("0030")) {
            return "ERROR-Invalid message";
        } else if (code.equalsIgnoreCase("0031")) {
            return "ERROR-Unregistered Bank Code";
        } else if (code.equalsIgnoreCase("0032")) {
            return "ERROR-Unregistered Partner Central";
        } else if (code.equalsIgnoreCase("0034")) {
            return "ERROR-Unregistered Terminal";
        } else if (code.equalsIgnoreCase("0033")) {
            return "ERROR-Unregistered Product";
        } else if (code.equalsIgnoreCase("0048")) {
            return "NOMOR REGISTRASI KADALUARSA, MOHON HUBUNGI PLN";
        } else if (code.equalsIgnoreCase("0068")) {
            //return "ERROR-Timeout";
            return "WAKTU TRANSAKSI HABIS, COBA SESAAT LAGI";
        } else if (code.equalsIgnoreCase("0077")) {
            return "ERROR-Subscriber suspended";
        } else if (code.equalsIgnoreCase("0088")) {
            //return "ERROR-Bills already paid";
            return "TAGIHAN SUDAH TERBAYAR";
        } else if (code.equalsIgnoreCase("0089")) {
            return "ERROR-Current Bill is not available";
        } else if (code.equalsIgnoreCase("0090")) {
            return "ERROR-Cut-off is in progress";
        }

        return "";
    }

    public String getNetworkMsgResponseName(String code) {
        if (code.equalsIgnoreCase("0000")) {
            return "SUCCESSFUL";
        } else if (code.equalsIgnoreCase("0005")) {
            return "ERROR-OTHER";
        } else if (code.equalsIgnoreCase("0006")) {
            return "ERROR-BLOCKED PARTNER CENTRAL";
        } else if (code.equalsIgnoreCase("0007")) {
            return "ERROR-BLOCKED TERMINAL";
        } else if (code.equalsIgnoreCase("0008")) {
            return "ERROR-INVALID ACCESS TIME";
        } else if (code.equalsIgnoreCase("0011")) {
            return "ERROR-NEED TO SIGN ON";
        } else if (code.equalsIgnoreCase("0030")) {
            return "ERROR-INVALID MESSAGE";
        } else if (code.equalsIgnoreCase("0032")) {
            return "ERROR-UNREGISTERED PARTNER CENTRAL";
        } else if (code.equalsIgnoreCase("0034")) {
            return "ERROR-UNREGISTERED TERMINAL";
        } else if (code.equalsIgnoreCase("0068")) {
            return "WAKTU TRANSAKSI HABIS, COBA SESAAT LAGI";
            //return "ERROR-TIMEOUT";
        } else if (code.equalsIgnoreCase("0090")) {
            return "ERROR-CUT-OFF IS IN PROGRESS";
        }

        return "";
    }

    public String responseParser(String code) {
        String keluaran = "";

        try {
            Map<String, String> slsRC = new HashMap<String, String>();
            slsRC.put("0000", "Approved ");
            slsRC.put("0005", "Error lainnya ");
            slsRC.put("0014", "Id pel yg anda masukkan salah , mohon teliti kembali");
            slsRC.put("0016", "transaksi ditolak, hub pln terkait");
            slsRC.put("0063", "unpaid transaction");
            slsRC.put("0088", "transaksi sudah terbayar");
            slsRC.put("0017", "deposit tidak mencukupi");
            slsRC.put("0015", "no registrasi salah, mohon teliti kembali");
            slsRC.put("0094", "reversal telah dilakukan");
            slsRC.put("0063", "reversal gagal, tidak ada pembayaran");
            slsRC.put("0012", "reversal telah melebihi batas waktu");
            slsRC.put("0098", "invalid ref number");
            slsRC.put("0013", "invalid bill amount");
            slsRC.put("0068", "payment melebihi batas waktu yang ditentukan");
            slsRC.put("0093", "transaksi payment tidak diperbolehkan");
            slsRC.put("0048", "no registrasi kadaluarsa");
            slsRC.put("0033", "kode produk belum terdaftar di gateway");
            slsRC.put("0031", "kode bank blm terdaftar");
            slsRC.put("0033", "kode produk blm terdafatar di tid");
            slsRC.put("0068", "app server timeout");

            keluaran = slsRC.get(code).toUpperCase();

        } catch (Exception e) {
            if (code.equalsIgnoreCase("0088")) {
                keluaran = "TAGIHAN SUDAH TERBAYAR";
            } else if (code.equalsIgnoreCase("0015")) {
                keluaran = "NOMOR REGISTRASI YANG ANDA MASUKKAN SALAH. MOHON TELITI KEMBALI.";
            } else if (code.equalsIgnoreCase("0016")) {
                keluaran = "TRANSAKSI DITOLAK, HUBUNGI KANTOR PLN TERKAIT.";
            } else if (code.equalsIgnoreCase("0030")) {
                keluaran = "PESAN TIDAK DIKENAL";
            } else if (code.equalsIgnoreCase("0041")) {
                keluaran = "PEMBELIAN MINIMAL RP 20RIBU, MAKSIMAL RP. 1JUTA.";
            } else if (code.equalsIgnoreCase("0042")) {
                keluaran = "PEMBELIAN MINIMAL RP 20RIBU, MAKSIMAL RP. 1JUTA.";
            } else if (code.equalsIgnoreCase("0047")) {
                keluaran = "TOTAL KWH MELEBIHI BATAS";
            } else if (code.equalsIgnoreCase("0048")) {
                keluaran = "NOMOR REGISTRASI KADALUARSA, MOHON HUBUNGI PLN.";
            } else if (code.equalsIgnoreCase("0068")) {
                System.out.println("padahal masuk lho");
                keluaran = "WAKTU TRANSAKSI HABIS, COBA SESAAT LAGI";
            } else if (code.equalsIgnoreCase("0088")) {
                keluaran = "TAGIHAN SUDAH TERBAYAR.";
            } else if (code.equalsIgnoreCase("0089")) {
                keluaran = "TAGIHAN BULAN BERJALAN BELUM TERSEDIA";
            } else if (code.equalsIgnoreCase("0077")) {
                keluaran = "NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH MOHON TELITI KEMBALI.";
            } else if (code.equalsIgnoreCase("0014")) {
                keluaran = "IDPEL YANG ANDA MASUKKAN SALAH MOHON TELITI KEMBALI.";
            } else if (code.equalsIgnoreCase("0009")) {
                keluaran = "NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH MOHON TELITI KEMBALI.";
            } else if (code.equalsIgnoreCase("400")) {
                keluaran = "SALDO ANDA TIDAK MENCUKUPI";
            } else {
                keluaran = "";
            }
        }
        return keluaran;
    }

}
