/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package huaweistruk;

//import com.jcraft.jsch.*;
import isoxml.NonTagList;
import isoxml.PostPaid;
import isoxml.PostPaidProcessing;
import isoxml.PrepaidProcessing;
import isoxml.Setting;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import javax.swing.*;
import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import pelangi_utils.ParseISOMessage;

public class HuaweiStruk {

//    static long sleepTime = 1000;
    NumberFormat formatter = new DecimalFormat("##,##0.00");
    NumberFormat formatter2 = new DecimalFormat("##,##0");

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("FTRCreatorDirect");
    public static int option = 1;
    public static String message = "";
    public static Setting setting = new Setting();
    public static Calendar cal = Calendar.getInstance();
    public static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    //System.out.println("Today's date is "+dateFormat.format(cal.getTime()));
    public static Calendar calFl = Calendar.getInstance();
    public static DateFormat dateFormatFl = new SimpleDateFormat("yyyyMMdd");
    public static String startParam = "";
    public static String endParam = "";
    public static String fileDate = "";
    private static Map<String, String> map = new HashMap<String, String>();
    private static String limit = "";
    private static String partnerId = "4511090";
    private static String getdatestart = setting.getstartdate();
    private static String getdateend = setting.getenddate();
    private static String getUserid = setting.getuserid();
    private static String getpostpaid = setting.getpostpaid();
    private static String getprepaid = setting.getprepaid();
    private static String getnontaglis = setting.getnontaglis();
    private static String gettelkom = setting.gettelkom();
    private static String getpdam = setting.getpdam();
    private static String getbpjs = setting.getbpjs();
    private static String getpulsa = setting.getpulsa();
    private static String getgame = setting.getgame();
    private static String getmultifinance = setting.getmultifinance();
    private static String gettvk =  setting.getTvk();
    private static boolean status;
    
//    private static String partnerId = "4511012";

    public static void main(String[] args) throws IOException {

        System.err.print(System.getProperty("user.dir"));
        String banyakHari = "";
        try {
            banyakHari = args[0];
        } catch (Exception e) {
            banyakHari = "6";
        }
        Calendar calNow = Calendar.getInstance();
        DateFormat dateFormat1 = new SimpleDateFormat("yyyyMMdd");

        Calendar calNow2 = Calendar.getInstance();
        DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");

        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //cal.add(Calendar.DATE, (Integer.parseInt(banyakHari) * (-1)));
        cal.add(Calendar.DATE, -1);
        

//        Calendar calEnd = Calendar.getInstance();
//        DateFormat dateFormatEnd = new SimpleDateFormat("yyyy-MM-dd");
//        calEnd.add(Calendar.DATE, -1);
        partnerId = setting.getCid();
        startParam = dateFormat.format(cal.getTime());
        endParam = dateFormat2.format(cal.getTime());
//        endParam = args[1];.replace("-", "")
        fileDate = dateFormat1.format(calNow.getTime());

//        String[] splitEndTanggal = endParam.split("-");
////        fileDate = splitEndTanggal[0] + splitEndTanggal[1] + (Integer.parseInt(splitEndTanggal[2]) + 1);
//
//        cal.add(Calendar.DATE, -3);
        //calFl.add(Calendar.DATE, -1);
        //cal.add(Calendar.DATE, -1);
        if(getUserid != null && !getUserid.isEmpty()){
            
            if((getUserid != null && !getUserid.isEmpty()) && 
                (getdatestart != null && !getdatestart.isEmpty()) && 
                (getdateend!=null && !getdateend.isEmpty())){
                
                status = true;
                
                System.out.println("Start date was " + getdatestart);
                System.out.println("end date was " + getdateend);
                System.out.println("User is "+ getUserid);
                System.out.println("folder name is export set");
                //System.out.println("folder name is " + fileDate.substring(0, 6));
        //        System.out.println(message);
                if(getprepaid != null && !getprepaid.isEmpty() && getprepaid.equalsIgnoreCase("true")){
                    
                    System.out.println("===========================");
                    System.out.println("Inisialisasi Pln Prepaid   ");
                    System.out.println("===========================");
                    plnPrepaidFtr();
                    System.out.println("=====Done PLN Prepaid======");
                }else{
                    
                }
                
                //System.exit(1);
                if(getpostpaid != null && !getpostpaid.isEmpty() && getpostpaid.equalsIgnoreCase("true")){
                    System.out.println("===========================");
                    System.out.println("Inisialisasi Pln Postpaid  ");
                    System.out.println("===========================");
                    plnPostpaidFtr();
                    System.out.println("=====Done PLN Postpaid=====");
                    System.out.println("===========================");
                }else{
                    
                }
                
                if(getnontaglis != null && !getnontaglis.isEmpty() && getnontaglis.equalsIgnoreCase("true")){
                    System.out.println("Inisialisasi Pln Nontaglish");
                    System.out.println("===========================");
                    plnNonFtr();
                    System.out.println("====Done PLN Nontaglish====");
                    System.out.println("===========================");
                }else{
                    
                }
                
                if(gettelkom != null && !gettelkom.isEmpty() && gettelkom.equalsIgnoreCase("true")){
                    System.out.println("Inisialisasi Telkom        ");
                    System.out.println("===========================");
                    TelkomFtr();
                    System.out.println("=========DoneTelkom========");
                    System.out.println("===========================");
                }else{
                    
                }
                
                if(getpdam != null && !getpdam.isEmpty() && getpdam.equalsIgnoreCase("true")){
                    System.out.println("Inisialisasi PDAM          ");
                    System.out.println("===========================");
                    PdamFtr();
                    System.out.println("=========Done pdam=========");
                    System.out.println("===========================");
                }else{
                    
                }
                
                if(getbpjs != null && !getbpjs.isEmpty() && getbpjs.equalsIgnoreCase("true")){
                    System.out.println("Inisialisasi BPJS          ");
                    System.out.println("===========================");
                    BpjsFtr();
                    System.out.println("=========Done BPJS=========");
                    System.out.println("===========================");
                }else{
                    
                }
                
                if(getpulsa != null && !getpulsa.isEmpty() && getpulsa.equalsIgnoreCase("true")){
                    System.out.println("Inisialisasi PULSA         ");
                    System.out.println("===========================");
                    PulsaFtr();
                    System.out.println("=========Done PULSA========");
                    System.out.println("===========================");
                }else{
                    
                }
                
                if(getgame != null && !getgame.isEmpty() && getgame.equalsIgnoreCase("true")){
                    System.out.println("Inisialisasi GAME          ");
                    System.out.println("===========================");
                    GameFtr();
                    System.out.println("=========Done GAME=========");
                    System.out.println("===========================");
                }else{
                    
                }
                
                if(getmultifinance != null && !getmultifinance.isEmpty() && getmultifinance.equalsIgnoreCase("true")){
                    System.out.println("Inisialisasi MULTIFINANCE  ");
                    System.out.println("===========================");
                    MultiFtr();
                    System.out.println("=====Done MULTIFINANCE=====");
                }else{
                    
                }
                
                if(gettvk != null && !gettvk.isEmpty() && gettvk.equalsIgnoreCase("true")){
                    System.out.println("Inisialisasi TVK  ");
                    System.out.println("===========================");
                    TvkFtr();
                    System.out.println("=====Done TVK=====");
                }else{
                    
                }

            }else if((getUserid != null && !getUserid.isEmpty()) && 
                (startParam != null && !startParam.isEmpty()) && 
                (endParam!=null && !endParam.isEmpty())){
                
                status = false;
                
                System.out.println("Start date was " + startParam);
                System.out.println("end date was " + endParam);
                System.out.println("User is "+ getUserid);
                System.out.println("filename is " + fileDate);
                System.out.println("folder name is " + fileDate.substring(0, 6));
        //        System.out.println(message);
                System.out.println("===========================");
                System.out.println("Inisialisasi Pln Prepaid   ");
                System.out.println("===========================");
                plnPrepaidFtr();
                System.out.println("=====Done PLN Prepaid======");
                System.out.println("===========================");
                System.out.println("Inisialisasi Pln Postpaid  ");
                System.out.println("===========================");
                plnPostpaidFtr();
                System.out.println("=====Done PLN Postpaid=====");
                System.out.println("===========================");
                System.out.println("Inisialisasi Pln Nontaglish");
                System.out.println("===========================");
                plnNonFtr();
                System.out.println("====Done PLN Nontaglish====");
                System.out.println("===========================");
                System.out.println("Inisialisasi Telkom        ");
                System.out.println("===========================");
                TelkomFtr();
                System.out.println("=========DoneTelkom========");
                System.out.println("===========================");
                System.out.println("Inisialisasi PDAM          ");
                System.out.println("===========================");
                PdamFtr();
                System.out.println("=========Done pdam=========");
                System.out.println("===========================");
                System.out.println("Inisialisasi BPJS          ");
                System.out.println("===========================");
                BpjsFtr();
                System.out.println("=========Done BPJS=========");
                System.out.println("===========================");
                System.out.println("Inisialisasi PULSA         ");
                System.out.println("===========================");
                PulsaFtr();
                System.out.println("=========Done PULSA========");
                System.out.println("===========================");
                System.out.println("Inisialisasi GAME          ");
                System.out.println("===========================");
                GameFtr();
                System.out.println("=========Done GAME=========");
                System.out.println("===========================");
                System.out.println("Inisialisasi MULTIFINANCE  ");
                System.out.println("===========================");
                MultiFtr();
                System.out.println("=====Done MULTIFINANCE=====");
                

            }
        
        }else{
            System.out.println("System will be shutdown because parameter userid is null"
                        + " please insert value of parameter and then restart aplication!");
            System.exit(0);
        }
       
       
    }

    private static void createImage(String sampleText, String noreg, String nama, String layanan, String inboxId, String datetime) {
        try {
            String fileName="";
//            if(layanan.equals("prepaid")){
//                fileName = noreg + "-" + nama.replace(" ", "") + "-" + inboxId + "-" + datetime;
//            }else if(layanan.equals("postpaid")){
//                fileName = noreg + "-" + nama.replace(" ", "") + "-" + inboxId + "-" + datetime;
//            }else if(layanan.equals("nontaglis")){
                //fileName = noreg + "-" + nama.replace(" ", "") + "-" + inboxId + "-" + datetime;
                //fileName = datetime + "~" + noreg + "~" + nama.replace(" ", "") + "~" + inboxId;
                //fileName = datetime + "~" + inboxId + "~" + nama.replace(" ", "") + "~" + noreg;
                fileName = noreg + "~" + nama.replace(" ", "") + "~" + inboxId;
            //}
            
            String location = "";
            /* default location folder is date format
               if the parameter date start & date end is not null, name of folder is export set
            */
            if(status==true){
                location = "export set/" + layanan;
            }else{
                location = startParam.replace("-", "") + "/" + layanan;
            }
            //System.out.println("locations "+location);
            File folder = new File(location);
            if (folder.exists() && folder.isDirectory()) {
            } else {
                System.out.println("mkdir");
                folder.mkdirs();
                System.out.println("done");
            }
            folder = new File(System.getProperty("user.dir") + "/" + location);

            //create a File Object
            File newFile = new File(folder, fileName + ".jpeg");

            //create the font you wish to use
            Font font = new Font("monospaced", Font.PLAIN, 11);
            Font font2 = new Font("Monospaced", Font.ROMAN_BASELINE, 16);

            //create the FontRenderContext object which helps us to measure the text
            FontRenderContext frc = new FontRenderContext(null, true, true);

            //get the height and width of the text
            Rectangle2D bounds = font.getStringBounds(sampleText, frc);
            int w = (int) bounds.getWidth();
            int h = (int) bounds.getHeight();

//            w = 1000;
//            h = 300;
            w = 850;
            h = 300;
            //create a BufferedImage object
            BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);

            //calling createGraphics() to get the Graphics2D
            Graphics2D g = image.createGraphics();

            g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
            g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

            //set color and other parameters
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, w, h);
            g.setColor(Color.BLACK);
            g.setFont(font);

//        float x = (float) bounds.getX();
//        float y = (float) -bounds.getY();
            int x = 20;
            int y = 0;
            for (String line : sampleText.split("\n")) {
//            System.out.println(line);
//                g.drawString(line, x, y += g.getFontMetrics().getHeight());

                try {
//                            System.out.println(nextLine + "      <===========>       " + map.get("token_format"));
//                            if (nextLine.toLowerCase().contains(map.get("token_format"))) {
//                                String[] spliter = nextLine.split("STROOM/TOKEN:");
                    if (line.toLowerCase().contains("stroom/token:")) {
                        String[] spliter = line.split("STROOM/TOKEN:");
                        g.drawString(spliter[0], x, y += g.getFontMetrics().getHeight());

                        g.setFont(font2);
                        g.drawString("STROOM/TOKEN:" + spliter[1], x + 277, y);
                    } else {
                        g.setFont(font);
                        g.drawString(line, x, y += g.getFontMetrics().getHeight());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    g.setFont(font);
                    g.drawString(line, x, y += g.getFontMetrics().getHeight());
                }

            }
            //releasing resources
            g.dispose();

            //creating the file
            ImageIO.write(image, "jpeg", newFile);
//        }

//        ImageIO.wr        
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(HuaweiStruk.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void createImage2(String sampleText, String noreg, String nama, String layanan, String inboxId, String datetime) {
        try {
            String fileName = "";
            //noreg + "-" + nama.replace(" ", "") + "-" + inboxId + "-" + datetime;
            //fileName = datetime + "~" + noreg + "~" + nama.replace(" ", "") + "~" + inboxId;
            fileName = noreg + "~"+ nama.replace(" ", "") + "~" + inboxId;
            String location = "";
            /* default location folder is date format
               if the parameter date start & date end is not null, name of folder is export set
            */
            if(status==true){
                location = "export set/" + layanan;
            }else{
                location = startParam.replace("-", "") + "/" + layanan;
            }
            //String location = startParam.replace("-", "") + "/" + layanan;
            File folder = new File(location);
            if (folder.exists() && folder.isDirectory()) {
            } else {
                System.out.println("mkdir");
                folder.mkdirs();
                System.out.println("done");
            }
            folder = new File(System.getProperty("user.dir") + "/" + location);

            //create a File Object
            File newFile = new File(folder, fileName + ".jpeg");

            //create the font you wish to use
            Font font = new Font("monospaced", Font.PLAIN, 11);
            Font font2 = new Font("Monospaced", Font.ROMAN_BASELINE, 16);

            //create the FontRenderContext object which helps us to measure the text
            FontRenderContext frc = new FontRenderContext(null, true, true);

            //get the height and width of the text
            Rectangle2D bounds = font.getStringBounds(sampleText, frc);
            int w = (int) bounds.getWidth();
            int h = (int) bounds.getHeight();

//            w = 1000;
//            h = 300;
            w = 900;
            h = 300;
            //create a BufferedImage object
            BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);

            //calling createGraphics() to get the Graphics2D
            Graphics2D g = image.createGraphics();

            g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
            g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

            //set color and other parameters
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, w, h);
            g.setColor(Color.BLACK);
            g.setFont(font);

//        float x = (float) bounds.getX();
//        float y = (float) -bounds.getY();
            int x = 20;
            int y = 0;
            for (String line : sampleText.split("\n")) {
//            System.out.println(line);
//                g.drawString(line, x, y += g.getFontMetrics().getHeight());

                try {
//                            System.out.println(nextLine + "      <===========>       " + map.get("token_format"));
//                            if (nextLine.toLowerCase().contains(map.get("token_format"))) {
//                                String[] spliter = nextLine.split("STROOM/TOKEN:");
                    if (line.toLowerCase().contains("stroom/token:")) {
                        String[] spliter = line.split("STROOM/TOKEN:");
                        g.drawString(spliter[0], x, y += g.getFontMetrics().getHeight());

                        g.setFont(font2);
                        g.drawString("STROOM/TOKEN:" + spliter[1], x + 277, y);
                    } else {
                        g.setFont(font);
                        g.drawString(line, x, y += g.getFontMetrics().getHeight());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    g.setFont(font);
                    g.drawString(line, x, y += g.getFontMetrics().getHeight());
                }

            }
            //releasing resources
            g.dispose();

            //creating the file
            ImageIO.write(image, "jpeg", newFile);
//        }

//        ImageIO.wr        
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(HuaweiStruk.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void plnNonFtr() {

        HuaweiStruk hs = new HuaweiStruk();
        NonTagList nonTagList = new NonTagList();
        map = new HashMap<String, String>();
        ParseISOMessage parseISOMessage = new ParseISOMessage();

        String switchingCid = setting.getCid();
        String lastId = "0";
        String param = "";
        String partnerTid = partnerId;
        String kodeLayanan = "50504";

        String xml = "";
        String sql = "";
        String strukFormat = "";
        //String nama = "HCPT_110624              ";
//        String iso = "2210403000418281000405995040001234567892016032211545360210745100170745110900000123456          267ST145S35235240000209040BERHENTI SBG PELANGGAN   2016031817042016523520599799HCPT_110624              B8E2EC0D3DCF4B80B64798638F3CB98345188171AU158758084E1731U977858E52352JL. PEMUDA 25 AMBARAWA             123            200000000072090500200000000072090500200000000000220101200000000000000000ÿ";
        //String iso = "22105032004182810006059950436000000009386000000865986082016122710501320161227602107451001707451020800000000000000006852267ST145S35255213000068013PERUBAHAN DAYA           2016122726012017523080327277KUSMIYATI                14241DAAA94A4B699DEF9174470444A60SYM218170446207242192657519167052552JL. GATOT SUBROTO 6                123            200000000093860000200000000093860000200000000000220101200000000000000000057Informasi Hubungi Call Center 123 Atau Hub PLN Terdekat :ÿ";
        //String createDate = "2016-03-22";
        //xml = nonTagList.processPayment(iso);
//     System.exit(1);     
        //strukFormat = hs.getPaymentNonTagListResponse(xml, "0", createDate);
//        createImage(strukFormat, "1234567891234",
//                nama.replaceAll("[^\\w\\s]", ""),
//                "nontaglis", "0");

        try {
            Connection con = setting.getConnection();
            Statement st = con.createStatement();

            String cidSql = " and partner_cid = '" + partnerTid + "'";
            /*String sql = "select * from pln_nontaglists "
                    + "where (date(dt_trx) >= '" + startParam + "' and date(dt_trx) < '" + endParam + "') "
                    + " or (date(create_date) >= '" + startParam + "' and date(create_date) < '" + endParam + "')"
                    + cidSql;*/
            if(status==true){
                sql = "select pln_nontaglists.* from pln_nontaglists "
                    + "left join transactions on (pln_nontaglists.inbox_id = transactions.inbox_id)"
                    + "where (date(pln_nontaglists.dt_trx) >= '"+getdatestart+"' and date(pln_nontaglists.dt_trx) <= '"+getdateend+"') "
                    + " or (date(pln_nontaglists.create_date) >= '"+getdatestart+"' and date(pln_nontaglists.create_date) <= '"+getdateend+"') "
                    + "and transactions.user_id = "+getUserid;
                    //+ cidSql
            }else{
                sql = "select pln_nontaglists.* from pln_nontaglists "
                    + "left join transactions on (pln_nontaglists.inbox_id = transactions.inbox_id)"
                    + "where (date(pln_nontaglists.dt_trx) >= '"+startParam+"' and date(pln_nontaglists.dt_trx) <= '"+endParam+"') "
                    + " or (date(pln_nontaglists.create_date) >= '"+startParam+"' and date(pln_nontaglists.create_date) <= '"+endParam+"') "
                    + "and transactions.user_id = "+getUserid;
            }

            System.out.println(sql);
            //System.out.println(sql);
            //            String sql = "SELECT VERSION()";
            ResultSet rs = st.executeQuery(sql + limit);
            int counter = 0;
            if(!rs.isBeforeFirst()){
                    System.out.println("No Data Found"); //data not exist
            }else{
                while (rs.next()) {
                    //hasil = rs.getString("iso");
                    System.out.println(rs.getString("iso"));
                    String[] hasil = new String[17];
                    GenericPackager packager;

                    try {
                        packager = new GenericPackager("isoSLSascii.xml");

                        Map<String, String> purchaseResp = new HashMap<String, String>();
    //                    purchaseResp = parseISOMessage.getBitmap(rs.getString("iso"));
    //                    createRecap(purchaseResp, partnerTid, true, true);
                        xml = nonTagList.processPayment(rs.getString("iso"));
                        System.out.println(xml);
                        System.out.println("counter : " + counter);
                        strukFormat = hs.getPaymentNonTagListResponse(xml, "0", rs.getString("create_date"));
                        createImage(strukFormat, rs.getString("no_registrasi"),
                                rs.getString("nama").replaceAll("[^\\w\\s]", ""),
                                "nontaglis", rs.getString("inbox_id"),rs.getString("create_date"));
                        counter++;
                        
                    } catch (ISOException ex) {
                        System.out.println("gagal dalam pembuatan ftr nontaglis");
    //                    java.util.logging.Logger.getLogger(FTRCreatorMitra.class.getName()).log(Level.SEVERE, null, ex + "gagal dalam pembuatan ftr nontaglis");
                    }
                }
                st.close();
                con.close();
            }

        } catch (Exception e) {
            System.out.println(e.toString());
            System.out.println("misi pengambilan data nontaglis  gagal");
        }

    }
    
    private static void plnPostpaidFtr() {
        map = new HashMap<String, String>();
//        SLSLogger slsLog = new SLSLogger();
//        APIConnection apic = new APIConnection();
        //jDateChooser1.getDate();
        //String start = sdf.format(jDateChooser1.getDate());
        //String end = sdf.format(jDateChooser2.getDate());

        //String param = "startDate=" + start + "&endDate=" + end + "&page=" + page + "&limit=" + limit;
        //Document doc = apic.getConnectionAndData(param, "https://202.150.163.60/apis/transactionList", "xml", "Authorization", "PELANGIREST username=admin&password=ngonar.1&signature=");
        //Document doc = apic.getConnectionAndData(param, "/apis/transactionList", "xml", "Authorization", "");
        long row = 0;
        double grandAmount = 0;
        double grandTotalElectricityBill = 0;
        double grandBillAmount = 0;
        String grandIncentive = "";
        double grandVat = 0;
        double grandPenalty = 0;
        double grandAdminCharge = 0;
        NumberFormat formatter = new DecimalFormat("###.#####");

        //SLSLogger slsLog = new SLSLogger();
        String switchingCid = setting.getCid();
        String lastId = "0";
        String param = "";
        String partnerTid = partnerId;
        String kodeLayanan = "50501";
        int counterPartnerPostpaid = 0;
        String sql = "";
        
        try {
            Connection con = setting.getConnection();
            Statement st = con.createStatement();
            //String yesterdayStart = dateFormat.format(cal.getTime()) + " 00:00:00";
            //String yesterdayEnd = dateFormat.format(cal.getTime()) + " 23:59:59";
//            String sql = "select * from pln_postpaids where create_date >= '" + dateStart + "' and create_date <= '" + dateEnd + "' ";
//            String sql = "select * from pln_postpaids where date(create_date) >= '" + startParam + "' ";
            String cidSql = " and partnertid = '" + partnerTid + "'";
            /*String sql = "select * from pln_postpaids where "
                    + "date(create_date) >= '" + startParam + "' "
                    + "and date(create_date) < '" + endParam + "' " //                    + "and cast(jumlahrek as integer) > 1 limit 1"
                    + cidSql;*/
            
//            if(status==true){
//                sql= "select * from pln_postpaids where "
//                    + "date(create_date) >= '" + getdatestart + "' "
//                    + "and date(create_date) <='" + getdateend + "' and user_ = '"+getUserid+"' group by id, user_ order by create_date desc";//                    + "and cast(jumlahrek as integer) > 1 limit 1"
//                    //+ cidSql;
//            }else{
//                sql= "select * from pln_postpaids where "
//                    + "date(create_date) >= '" + startParam + "' "
//                    + "and date(create_date) <='" + endParam + "' and user_ = '"+getUserid+"' group by id, user_ order by create_date desc";
//            }
            if(status==true){
                sql = "select a.id as id, a.inbox_id as inbox_id, a.idpel as idpel, a.nama as nama, a.create_date as create_date, b.message as message, b.user_id as user_id "
                    + "from pln_postpaids a inner join outboxes b on a.inbox_id = b.inbox_id"
                    + " where date(a.create_date) >= '" + getdatestart + "' "
                    + "and date(a.create_date) <='" + getdateend + "' and b.user_id = '"+getUserid+"' order by a.create_date desc";
            }else{
                sql = "select a.id as id, a.inbox_id as inbox_id, a.idpel as idpel, a.nama as nama, a.create_date as create_date, b.message as message, b.user_id as user_id "
                    + "from pln_postpaids a inner join outboxes b on a.inbox_id = b.inbox_id"
                    + " where date(a.create_date) >= '" + startParam + "' "
                    + "and date(a.create_date) <='" + endParam + "' and b.user_id = '"+getUserid+"' order by a.create_date desc";
            }




//            String sql = "SELECT VERSION()";
            System.out.println(sql);
            ResultSet rs = st.executeQuery(sql + limit);

            String xml = "";
            String strukFormat = "";
            PostPaidProcessing postPaidProcessing = new PostPaidProcessing();
            HuaweiStruk hs = new HuaweiStruk();
            int counter = 0;
            
            if(!rs.isBeforeFirst()){
                    System.out.println("No Data Found"); //data not exist
            }else{
                while (rs.next()) {
                    //hasil = rs.getString("iso");
                    //System.out.println(rs.getString("iso"));
                    //lastId = rs.getString("id");

                    String[] hasil = new String[17];
                    GenericPackager packager;
                    try {
                        packager = new GenericPackager("isoSLSascii.xml");
    //                    Map<String, String> purchaseResp = new HashMap<String, String>();
    //                    purchaseResp = parseISOMessage.getBitmap(rs.getString("iso"));
    //                    createRecap(purchaseResp, partnerTid, true, true);
                        //xml = postPaidProcessing.processPayment2(rs.getString("iso"));
                        
                        xml = rs.getString("message");
                        System.out.println(xml);
                        strukFormat = hs.getPaymentPostPaidResponse(xml, "0");
    //production
                        createImage(strukFormat, rs.getString("idpel"), rs.getString("nama").replaceAll("[^\\w\\s]", ""),
                                "postpaid", rs.getString("inbox_id"),rs.getString("create_date"));
    //production

    //khusus untuk suspect                    
    //                    String idpel = rs.getString("iso").substring(129, 141);
    //                    String nama = rs.getString("iso").substring(177, 202);
    //                    System.out.println(idpel+" :::: "+nama);
    //                    createImage(strukFormat, idpel, nama.replaceAll("[^\\w\\s]", ""),
    //                            "postpaid", "x"+counter+"x");
    //khusus untuk suspect                    
                        counter++;
                        System.out.println("counter : " + counter);

                    } catch (ISOException ex) {
                        System.out.println("gagal dalam pembuatan ftr postpaid");
    //                    java.util.logging.Logger.getLogger(FTRCreatorMitra.class.getName()).log(Level.SEVERE, null, ex + "postpaid pln error");
                    }
                    //break;
                }
                st.close();
                con.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("misi pengambilan data postpaid gagal");
        }

    }
     
    private static void plnPrepaidFtr() {
        map = new HashMap<String, String>();
        //String hasil = "";
        long row = 0;
        //long row = 0;
        double grandAmount = 0;
        double grandAdminCharge = 0;
        double grandVat = 0;
        double grandLightingTax = 0;
        double grandInstallment = 0;
        double grandPowerPurchase = 0;
//        SLSLogger slsLog = new SLSLogger();
        String switchingCid = setting.getCid();
        String lastId = "0";
        String param = "";
        String kodeLayanan = "50502";
        String partnerTid = partnerId;
        String sql = "";
        
        int counterPartnerPrepaid = 0;
        try {
            Connection con = setting.getConnection();
            Statement st = con.createStatement();

            String cidSql = " and partnertid = '" + partnerTid + "'";
//            String sql = "select * from pln_prepaids where date(create_date) >= '" + dateStart + "' and create_date <= '" + dateEnd + "'";
//            String sql = "select * from pln_prepaids where "
//                    + "date(create_date) >= '" + startParam + "' "
//                    + "and date(create_date) < '" + endParam + "' "
//                    + cidSql;
            
            if(status==true){
                sql = "select * from pln_prepaids where "
                    + "date(create_date) >= '" + getdatestart + "' "
                    + "and date(create_date) <='" + getdateend + "' and user_ = '"+getUserid+"' group by id, user_ order by create_date desc";
            }else{
                sql = "select * from pln_prepaids where "
                    + "date(create_date) >= '" + startParam + "' "
                    + "and date(create_date) <= '" + endParam + "' and user_ = '"+getUserid+"' group by id, user_ order by create_date desc";
            }
            
            
                    //+ cidSql;
            System.out.println(sql);
            //            String sql = "SELECT VERSION()";
            ResultSet rs = st.executeQuery(sql + limit);
            
            String xml = "";
            String strukFormat = "";
            PrepaidProcessing prepaidProcessing = new PrepaidProcessing();
            HuaweiStruk hs = new HuaweiStruk();
            int counter = 0;
            
            if(!rs.isBeforeFirst()){
                    System.out.println("No Data Found"); //data not exist
            }else{
                while (rs.next()) {
                    //hasil = rs.getString("iso");
                    System.out.println(rs.getString("iso"));
                    lastId = rs.getString("id");

                    String[] hasil = new String[17];
                    GenericPackager packager;

                    try {
                        packager = new GenericPackager("isoSLSascii.xml");
            //                    Map<String, String> purchaseResp = new HashMap<String, String>();
            //                    purchaseResp = parseISOMessage.getBitmap(rs.getString("iso"));
            //                    createRecap(purchaseResp, partnerTid, true, true);
                        xml = prepaidProcessing.processPurchase2(rs.getString("iso"));
                        System.out.println(xml);
                        strukFormat = hs.getPaymentPrePaidResponse(xml, "0");
                        //System.out.println(strukFormat);
                        createImage(strukFormat, rs.getString("idpel"), rs.getString("nama").replaceAll("[^\\w\\s]", ""),
                                "prepaid", rs.getString("inbox_id"),rs.getString("create_date"));
                        counter++;
                        System.out.println("counter : " + counter);
                    } catch (ISOException ex) {
                        System.out.println("gagal dalam parsing iso");
                        
            //                    java.util.logging.Logger.getLogger(FTRCreatorMitra.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                st.close();
                con.close();
            }

        } catch (Exception e) {
            System.out.println(e.toString());
            System.out.println("misi pengambilan data prepaid gagal");
        }

    }

    private static void TelkomFtr() {
        map = new HashMap<String, String>();

        String switchingCid = setting.getCid();        
        String lastId = "0";
        String param = "";
        String partnerTid = partnerId;
        String kodeLayanan = "50501";
        String sql = "";

        try {
            Connection con = setting.getConnection();
            Statement st = con.createStatement();
            if(status==true){
                sql = "select telkoms.*, transactions.create_date as trans_create_date from telkoms "
                        + "left join transactions on (telkoms.inbox_id = transactions.inbox_id)"
                        + "where (date(telkoms.create_date) >= '"+getdatestart+"' and date(telkoms.create_date) <= '"+getdateend+"') "
                        + "and transactions.user_id = "+getUserid+" order by telkoms.create_date desc";
            }else{
                sql = "select telkoms.*, transactions.create_date as trans_create_date from telkoms "
                        + "left join transactions on (telkoms.inbox_id = transactions.inbox_id)"
                        + "where (date(telkoms.create_date) >= '"+startParam+"' and date(telkoms.create_date) <= '"+endParam+"') "
                        + "and transactions.user_id = "+getUserid+" order by telkoms.create_date desc";
            }
            ResultSet rs = st.executeQuery(sql + limit);
//            ResultSet rs = st.executeQuery(sql + limit);
            if(!rs.isBeforeFirst()){
                System.out.println("No Data Found"); //data not exist
            }else{
                String xml = "";
                String TransDate = "";
                String strukFormat = "";
                HuaweiStruk hs = new HuaweiStruk();
                int counter = 0;
                while (rs.next()) {
                    //hasil = rs.getString("iso");
                    //System.out.println(rs.getString("iso"));
                    //lastId = rs.getString("id");

                    String[] hasil = new String[17];
                    GenericPackager packager;
                    try {
                        packager = new GenericPackager("isoSLSascii.xml");
                        xml = rs.getString("xml");
                        TransDate = rs.getString("trans_create_date");

                        System.out.println(xml);
                        strukFormat = hs.getTelkomResponse(xml, "0", TransDate);
                        createImage2(strukFormat, rs.getString("idpel"), rs.getString("nama").replaceAll("[^\\w\\s]", ""), "telkom", rs
              .getString("inbox_id"), rs.getString("create_date"));

                        counter++;
                        System.out.println("counter : " + counter);

                    } catch (ISOException ex) {
                        System.out.println("gagal dalam pembuatan ftr Telkom");
    //                    java.util.logging.Logger.getLogger(FTRCreatorMitra.class.getName()).log(Level.SEVERE, null, ex + "postpaid pln error");
                    }
                    //break;
                }
                st.close();
                con.close();

            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("misi pengambilan data telkom gagal");
        }

    }
    
    private static void PdamFtr() {
        map = new HashMap<String, String>();

        String switchingCid = setting.getCid();        
        String lastId = "0";
        String param = "";
        String partnerTid = partnerId;
        String kodeLayanan = "50501";
        String sql = "";

        try {
            Connection con = setting.getConnection();
            Statement st = con.createStatement();
            //String yesterdayStart = dateFormat.format(cal.getTime()) + " 00:00:00";
            //String yesterdayEnd = dateFormat.format(cal.getTime()) + " 23:59:59";
//            String sql = "select * from pln_postpaids where create_date >= '" + dateStart + "' and create_date <= '" + dateEnd + "' ";
//            String sql = "select * from pln_postpaids where date(create_date) >= '" + startParam + "' ";
            String cidSql = " and partnertid = '" + partnerTid + "'";
            /*String sql = "select * from pln_postpaids where "
                    + "date(create_date) >= '" + startParam + "' "
                    + "and date(create_date) < '" + endParam + "' " //                    + "and cast(jumlahrek as integer) > 1 limit 1"
                    + cidSql;*/
            /*String sql = "select telkoms.*, transactions.create_date as trans_create_date from telkoms "
                    + "left join transactions on (telkoms.inbox_id = transactions.inbox_id)"
                    + "where (date(telkoms.create_date) >= '2016-08-16' and date(telkoms.create_date) <= '2016-08-16') order by telkoms.create_date desc limit 10";*/
            
            if(status==true){
                sql = "select pdams.*, transactions.create_date as trans_create_date from pdams "
                        + "left join transactions on (pdams.inbox_id = transactions.inbox_id)"
                        + "where (date(pdams.create_date) >= '"+getdatestart+"' and date(pdams.create_date) <= '"+getdateend+"') "
                        + "and transactions.user_id = "+getUserid+" order by pdams.create_date desc";
            }else{
                sql = "select pdams.*, transactions.create_date as trans_create_date from pdams "
                        + "left join transactions on (pdams.inbox_id = transactions.inbox_id)"
                        + "where (date(pdams.create_date) >= '"+startParam+"' and date(pdams.create_date) <= '"+endParam+"') "
                        + "and transactions.user_id = "+getUserid+" order by pdams.create_date desc";
            }
                     
                        //+ cidSql;
    //            String sql = "SELECT VERSION()";
            System.out.println(sql);
            ResultSet rs = st.executeQuery(sql + limit);
            if(!rs.isBeforeFirst()){
                System.out.println("No Data Found"); //data not exist
            }else{
                String xml = "";
                String TransDate = "";
                String strukFormat = "";
                HuaweiStruk hs = new HuaweiStruk();
                int counter = 0;
                while (rs.next()) {
                    //hasil = rs.getString("iso");
                    //System.out.println(rs.getString("iso"));
                    //lastId = rs.getString("id");

                    String[] hasil = new String[17];
                    GenericPackager packager;
                    try {
                        packager = new GenericPackager("isoSLSascii.xml");
                        xml = rs.getString("xml");
                        TransDate = rs.getString("trans_create_date");

                        System.out.println(xml);
                        strukFormat = hs.getPdamResponse(xml, "0", TransDate);
                        createImage(strukFormat, rs.getString("idpel").trim(), rs.getString("name").replaceAll("[^\\w\\s]", ""),
                                "pdam", rs.getString("inbox_id"),rs.getString("create_date"));

                        counter++;
                        System.out.println("counter : " + counter);

                    } catch (ISOException ex) {
                        System.out.println("gagal dalam pembuatan ftr pdam");
    //                    java.util.logging.Logger.getLogger(FTRCreatorMitra.class.getName()).log(Level.SEVERE, null, ex + "postpaid pln error");
                    }
                    //break;
                }
                st.close();
                con.close();

            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("misi pengambilan data pdams gagal");
        }

    }
    
    private static void BpjsFtr() {
        map = new HashMap<String, String>();

        String switchingCid = setting.getCid();        
        String lastId = "0";
        String param = "";
        String partnerTid = partnerId;
        String kodeLayanan = "50501";
        String sql = "";

        try {
            Connection con = setting.getConnection();
            Statement st = con.createStatement();
           
            String cidSql = " and partnertid = '" + partnerTid + "'";
           
            if(status==true){
                sql = "select outboxes.*, transactions.create_date as trans_create_date,inboxes.idpel from outboxes "
                        + "left join transactions on (outboxes.inbox_id = transactions.inbox_id) "
                        + "left join inboxes on (inboxes.id = transactions.inbox_id) "
                        + "where (date(outboxes.create_date) >= '"+getdatestart+"' and date(outboxes.create_date) <= '"+getdateend+"') "
                        + "and transactions.product_id = 388 and transactions.user_id = "+getUserid+" order by outboxes.create_date desc";
            }else{
                sql = "select outboxes.*, transactions.create_date as trans_create_date,inboxes.idpel from outboxes "
                        + "left join transactions on (outboxes.inbox_id = transactions.inbox_id) "
                        + "left join inboxes on (inboxes.id = transactions.inbox_id) "
                        + "where (date(outboxes.create_date) >= '"+startParam+"' and date(outboxes.create_date) <= '"+endParam+"') "
                        + "and transactions.product_id = 388 and transactions.user_id = "+getUserid+" order by outboxes.create_date desc";
            }
                     
                        //+ cidSql;
    //            String sql = "SELECT VERSION()";
            System.out.println(sql);
            //System.exit(0);
            ResultSet rs = st.executeQuery(sql + limit);
            if(!rs.isBeforeFirst()){
                System.out.println("No Data Found"); //data not exist
            }else{
                String xml = "";
                String TransDate = "";
                String strukFormat = "";
                HuaweiStruk hs = new HuaweiStruk();
                int counter = 0;
                while (rs.next()) {
                    //hasil = rs.getString("iso");
                    //System.out.println(rs.getString("iso"));
                    //lastId = rs.getString("id");

                    String[] hasil = new String[17];
                    GenericPackager packager;
                    try {
                        packager = new GenericPackager("isoSLSascii.xml");
                        xml = rs.getString("message");
                        TransDate = rs.getString("trans_create_date");

                        System.out.println(xml);
                        
                        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                        InputSource src = new InputSource();
                        src.setCharacterStream(new StringReader(xml));

                        Document doc = builder.parse(src);
                        String name = doc.getElementsByTagName("nama").item(0).getTextContent();
                        
                        strukFormat = hs.getBpjsResponse(xml, "0", TransDate);
                        createImage2(strukFormat, rs.getString("idpel"), name.replaceAll("[^\\w\\s]", ""),
                                "bpjs", rs.getString("inbox_id"),rs.getString("create_date"));

                        counter++;
                        System.out.println("counter : " + counter);

                    } catch (ISOException ex) {
                        System.out.println("gagal dalam pembuatan ftr pdam");
    //                    java.util.logging.Logger.getLogger(FTRCreatorMitra.class.getName()).log(Level.SEVERE, null, ex + "postpaid pln error");
                    }
                    //break;
                }
                st.close();
                con.close();

            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("misi pengambilan data pdams gagal");
        }

    }
    
    private static void PulsaFtr() {
        map = new HashMap<String, String>();

        String switchingCid = setting.getCid();        
        String lastId = "0";
        String param = "";
        String partnerTid = partnerId;
        String kodeLayanan = "50501";
        String sql = "";

        try {
            Connection con = setting.getConnection();
            Statement st = con.createStatement();
           
            String cidSql = " and partnertid = '" + partnerTid + "'";
           
            if(status==true){
                sql = "select outboxes.*, transactions.create_date as trans_create_date,inboxes.msisdn from outboxes "
                        + "left join transactions on (outboxes.inbox_id = transactions.inbox_id) "
                        + "left join inboxes on (inboxes.id = transactions.inbox_id) "
                        + "left join products on (products.id = inboxes.prod_id) "
                        + "left join product_categories on (product_categories.id = products.product_category_id) "
                        + "left join categories on (categories.id = product_categories.category_id) "
                        + "where (date(outboxes.create_date) >= '"+getdatestart+"' and date(outboxes.create_date) <= '"+getdateend+"') "
                        + "and categories.id = 3 and transactions.user_id = "+getUserid+" order by outboxes.create_date desc";
            }else{
                sql = "select outboxes.*, transactions.create_date as trans_create_date,inboxes.msisdn from outboxes "
                        + "left join transactions on (outboxes.inbox_id = transactions.inbox_id) "
                        + "left join inboxes on (inboxes.id = transactions.inbox_id) "
                        + "left join products on (products.id = inboxes.prod_id) "
                        + "left join product_categories on (product_categories.id = products.product_category_id) "
                        + "left join categories on (categories.id = product_categories.category_id) "
                        + "where (date(outboxes.create_date) >= '"+startParam+"' and date(outboxes.create_date) <= '"+endParam+"') "
                        + "and categories.id = 3 and transactions.user_id = "+getUserid+" order by outboxes.create_date desc";
            }
                     
                        //+ cidSql;
    //            String sql = "SELECT VERSION()";
            System.out.println(sql);
            //System.exit(0);
            ResultSet rs = st.executeQuery(sql + limit);
            if(!rs.isBeforeFirst()){
                System.out.println("No Data Found"); //data not exist
            }else{
                String xml = "";
                String TransDate = "";
                String strukFormat = "";
                HuaweiStruk hs = new HuaweiStruk();
                int counter = 0;
                while (rs.next()) {
                    //hasil = rs.getString("iso");
                    //System.out.println(rs.getString("iso"));
                    //lastId = rs.getString("id");

                    String[] hasil = new String[17];
                    GenericPackager packager;
                    try {
                        packager = new GenericPackager("isoSLSascii.xml");
                        xml = rs.getString("message");
                        TransDate = rs.getString("trans_create_date");

                        System.out.println(xml);
                        
                        
                        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                        InputSource src = new InputSource();
                        src.setCharacterStream(new StringReader(xml));

                        Document doc = builder.parse(src);
                        String rcnya = doc.getElementsByTagName("rc").item(0).getTextContent();
                        //System.out.println("rcnya "+rcnya);
                        
                        if(rcnya.equalsIgnoreCase("0000")){
                            //String xmlnya = "<?xml version='1.0' encoding='UTF-8' standalone='no'?><response><trx_id/><stan>100920194</stan><produk> TELKOMSEL HALO</produk><msisdn> 0811309508( BAPAK HARTONO )</msisdn><product_type>PULSA PASCABAYAR</product_type><code>0000</code><rc>0000</rc><sn/><price>475501</price><amount>478001</amount><bill_count>1</bill_count><bill_repeat_count>1</bill_repeat_count><description> TELKOMSEL HALO</description><info/><refnumber>54000002813</refnumber></response>";
//System.out.println(strukFormat);
                            strukFormat = hs.getPulsaResponse(xml, "0", TransDate);
                            createImage(strukFormat, rs.getString("msisdn"), "pulsa","pulsa", rs.getString("inbox_id"),rs.getString("create_date"));
                        }else{
                            System.out.println("tidak mencetak struk karena rc 0001");
                        }

                        counter++;
                        System.out.println("counter : " + counter);

                    } catch (ISOException ex) {
                        System.out.println("gagal dalam pembuatan ftr pulsa");
    //                    java.util.logging.Logger.getLogger(FTRCreatorMitra.class.getName()).log(Level.SEVERE, null, ex + "postpaid pln error");
                    }
                    //break;
                }
                st.close();
                con.close();

            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("misi pengambilan data pdams gagal");
        }

    }
    
    private static void GameFtr() {
        map = new HashMap<String, String>();

        String switchingCid = setting.getCid();        
        String lastId = "0";
        String param = "";
        String partnerTid = partnerId;
        String kodeLayanan = "50501";
        String sql = "";

        try {
            Connection con = setting.getConnection();
            Statement st = con.createStatement();
           
            String cidSql = " and partnertid = '" + partnerTid + "'";
           
            if(status==true){
                sql = "select outboxes.*, transactions.create_date as trans_create_date,inboxes.msisdn from outboxes "
                        + "left join transactions on (outboxes.inbox_id = transactions.inbox_id) "
                        + "left join inboxes on (inboxes.id = transactions.inbox_id) "
                        + "left join products on (products.id = inboxes.prod_id) "
                        + "left join product_categories on (product_categories.id = products.product_category_id) "
                        + "left join categories on (categories.id = product_categories.category_id) "
                        + "where (date(outboxes.create_date) >= '"+getdatestart+"' and date(outboxes.create_date) <= '"+getdateend+"') "
                        + "and categories.id = 2 and transactions.user_id = "+getUserid+" and inboxes.trx_type = '2200' order by outboxes.create_date desc";
            }else{
                sql = "select outboxes.*, transactions.create_date as trans_create_date,inboxes.msisdn from outboxes "
                        + "left join transactions on (outboxes.inbox_id = transactions.inbox_id) "
                        + "left join inboxes on (inboxes.id = transactions.inbox_id) "
                        + "left join products on (products.id = inboxes.prod_id) "
                        + "left join product_categories on (product_categories.id = products.product_category_id) "
                        + "left join categories on (categories.id = product_categories.category_id) "
                        + "where (date(outboxes.create_date) >= '"+startParam+"' and date(outboxes.create_date) <= '"+endParam+"') "
                        + "and categories.id = 2 and transactions.user_id = "+getUserid+" and inboxes.trx_type = '2200' order by outboxes.create_date desc";
            }
                     
                        //+ cidSql;
    //            String sql = "SELECT VERSION()";
            System.out.println(sql);
            //System.exit(0);
            ResultSet rs = st.executeQuery(sql + limit);
            if(!rs.isBeforeFirst()){
                System.out.println("No Data Found"); //data not exist
            }else{
                String xml = "";
                String TransDate = "";
                String strukFormat = "";
                HuaweiStruk hs = new HuaweiStruk();
                int counter = 0;
                while (rs.next()) {
                    //hasil = rs.getString("iso");
                    //System.out.println(rs.getString("iso"));
                    //lastId = rs.getString("id");

                    String[] hasil = new String[17];
                    GenericPackager packager;
                    try {
                        packager = new GenericPackager("isoSLSascii.xml");
                        xml = rs.getString("message");
                        TransDate = rs.getString("trans_create_date");

                        System.out.println(xml);
                      
                        //if(rcnya.equalsIgnoreCase("0000")){
                            strukFormat = hs.getGameResponse(xml, "0", TransDate);
                            createImage(strukFormat, rs.getString("msisdn"), "game","game", rs.getString("inbox_id"),rs.getString("create_date"));
                        //}else{
                            //System.out.println("tidak mencetak struk karena rc 0001");
                        //}

                        counter++;
                        System.out.println("counter : " + counter);

                    } catch (ISOException ex) {
                        System.out.println("gagal dalam pembuatan ftr pulsa");
    //                    java.util.logging.Logger.getLogger(FTRCreatorMitra.class.getName()).log(Level.SEVERE, null, ex + "postpaid pln error");
                    }
                    //break;
                }
                st.close();
                con.close();

            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("misi pengambilan data pdams gagal");
        }

    }
    
    private static void MultiFtr() {
        map = new HashMap<String, String>();
        MFUtils mfutil = new MFUtils();

        String switchingCid = setting.getCid();        
        String lastId = "0";
        String param = "";
        String partnerTid = partnerId;
        String kodeLayanan = "50501";
        String sql = "";

        try {
            Connection con = setting.getConnection();
            Statement st = con.createStatement();
           
            String cidSql = " and partnertid = '" + partnerTid + "'";
           
            if(status==true){
                sql = "select outboxes.*, transactions.create_date as trans_create_date,inboxes.idpel from outboxes "
                        + "left join transactions on (outboxes.inbox_id = transactions.inbox_id) "
                        + "left join inboxes on (inboxes.id = transactions.inbox_id) "
                        + "left join products on (products.id = inboxes.prod_id) "
                        + "left join product_categories on (product_categories.id = products.product_category_id) "
                        + "left join categories on (categories.id = product_categories.category_id) "
                        + "where (date(outboxes.create_date) >= '"+getdatestart+"' and date(outboxes.create_date) <= '"+getdateend+"') "
                        + "and categories.id = 5 and transactions.user_id = "+getUserid+" and inboxes.trx_type = '2200' order by outboxes.create_date desc";
            }else{
                sql = "select outboxes.*, transactions.create_date as trans_create_date,inboxes.idpel from outboxes "
                        + "left join transactions on (outboxes.inbox_id = transactions.inbox_id) "
                        + "left join inboxes on (inboxes.id = transactions.inbox_id) "
                        + "left join products on (products.id = inboxes.prod_id) "
                        + "left join product_categories on (product_categories.id = products.product_category_id) "
                        + "left join categories on (categories.id = product_categories.category_id) "
                        + "where (date(outboxes.create_date) >= '"+startParam+"' and date(outboxes.create_date) <= '"+endParam+"') "
                        + "and categories.id = 5 and transactions.user_id = "+getUserid+" and inboxes.trx_type = '2200' order by outboxes.create_date desc";
            }
                     
                        //+ cidSql;
    //            String sql = "SELECT VERSION()";
            System.out.println(sql);
            //System.exit(0);
            ResultSet rs = st.executeQuery(sql + limit);
            if(!rs.isBeforeFirst()){
                System.out.println("No Data Found"); //data not exist
            }else{
                String xml = "";
                String TransDate = "";
                String strukFormat = "";
                HuaweiStruk hs = new HuaweiStruk();
                int counter = 0;
                while (rs.next()) {
                    //hasil = rs.getString("iso");
                    //System.out.println(rs.getString("iso"));
                    //lastId = rs.getString("id");

                    String[] hasil = new String[17];
                    GenericPackager packager;
                    try {
                        packager = new GenericPackager("isoSLSascii.xml");
                        xml = rs.getString("message");
                        TransDate = rs.getString("trans_create_date");

                        System.out.println(xml);
                      
                        //if(rcnya.equalsIgnoreCase("0000")){
                        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                        InputSource src = new InputSource();
                        src.setCharacterStream(new StringReader(xml));

                        Document doc = builder.parse(src);
                        String name = doc.getElementsByTagName("name").item(0).getTextContent();
//                            strukFormat = hs.getMultiResponse(xml, "0", TransDate);
                        strukFormat = mfutil.getMultiResponse(xml, "0", TransDate);
                        createImage(strukFormat, rs.getString("idpel"), name,"Multifinance", rs.getString("inbox_id"),rs.getString("create_date"));
                        //}else{
                            //System.out.println("tidak mencetak struk karena rc 0001");
                        //}

                        counter++;
                        System.out.println("counter : " + counter);

                    } catch (ISOException ex) {
                        System.out.println("gagal dalam pembuatan ftr pulsa");
    //                    java.util.logging.Logger.getLogger(FTRCreatorMitra.class.getName()).log(Level.SEVERE, null, ex + "postpaid pln error");
                    }
                    //break;
                }
                st.close();
                con.close();

            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("misi pengambilan data pdams gagal");
        }

    }
    
    private static void TvkFtr() {
        TVUtils Tv = new TVUtils();
        map = new HashMap<String, String>();

        String switchingCid = setting.getCid();        
        String lastId = "0";
        String param = "";
        String partnerTid = partnerId;
        String kodeLayanan = "50501";
        String sql = "";

        try {
            Connection con = setting.getConnection();
            Statement st = con.createStatement();
           
            String cidSql = " and partnertid = '" + partnerTid + "'";
           
            if(status==true){
                sql = "select products.name as product_name, outboxes.*, transactions.create_date as trans_create_date,inboxes.idpel from outboxes "
                        + "left join transactions on (outboxes.inbox_id = transactions.inbox_id) "
                        + "left join inboxes on (inboxes.id = transactions.inbox_id) "
                        + "left join products on (products.id = inboxes.prod_id) "
                        + "left join product_categories on (product_categories.id = products.product_category_id) "
                        + "left join categories on (categories.id = product_categories.category_id) "
                        + "where (date(outboxes.create_date) >= '"+getdatestart+"' and date(outboxes.create_date) <= '"+getdateend+"') "
                        + "and categories.id = 7 and transactions.user_id = "+getUserid+" and inboxes.trx_type = '2200' order by outboxes.create_date desc";
            }else{
                sql = "select products.name as product_name, outboxes.*, transactions.create_date as trans_create_date,inboxes.idpel from outboxes "
                        + "left join transactions on (outboxes.inbox_id = transactions.inbox_id) "
                        + "left join inboxes on (inboxes.id = transactions.inbox_id) "
                        + "left join products on (products.id = inboxes.prod_id) "
                        + "left join product_categories on (product_categories.id = products.product_category_id) "
                        + "left join categories on (categories.id = product_categories.category_id) "
                        + "where (date(outboxes.create_date) >= '"+startParam+"' and date(outboxes.create_date) <= '"+endParam+"') "
                        + "and categories.id = 7 and transactions.user_id = "+getUserid+" and inboxes.trx_type = '2200' order by outboxes.create_date desc";
            }
                     
                        //+ cidSql;
    //            String sql = "SELECT VERSION()";
            System.out.println(sql);
            //System.exit(0);
            ResultSet rs = st.executeQuery(sql + limit);
            if(!rs.isBeforeFirst()){
                System.out.println("No Data Found"); //data not exist
            }else{
                String xml = "";
                String TransDate = "";
                String strukFormat = "";
                HuaweiStruk hs = new HuaweiStruk();
                int counter = 0;
                while (rs.next()) {
                    //hasil = rs.getString("iso");
                    //System.out.println(rs.getString("iso"));
                    //lastId = rs.getString("id");

                    String[] hasil = new String[17];
                    GenericPackager packager;
                    try {
                        System.out.println("\n"+rs.getString("product_name")+"\n");
                        packager = new GenericPackager("isoSLSascii.xml");
                        xml = rs.getString("message");
                        TransDate = rs.getString("trans_create_date");

                        System.out.println(xml);
                        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                        InputSource src = new InputSource();
                        src.setCharacterStream(new StringReader(xml));

                        Document doc = builder.parse(src);
                        String name = doc.getElementsByTagName("nama_pelanggan").item(0).getTextContent();
                        System.out.println(name);
                        //if(rcnya.equalsIgnoreCase("0000")){
                            strukFormat = Tv.getTvResponse(rs.getString("product_name"),xml, "0", TransDate);
                            createImage(strukFormat, rs.getString("idpel"), name.replaceAll("[^\\w\\s]", ""),"TVKabel", rs.getString("inbox_id"),rs.getString("create_date"));
                        //}else{
                            //System.out.println("tidak mencetak struk karena rc 0001");
                        //}

                        counter++;
                        System.out.println("counter : " + counter);

                    } catch (ISOException ex) {
                        System.out.println("gagal dalam pembuatan ftr pulsa");
    //                    java.util.logging.Logger.getLogger(FTRCreatorMitra.class.getName()).log(Level.SEVERE, null, ex + "postpaid pln error");
                    }
                    //break;
                }
                st.close();
                con.close();

            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("misi pengambilan data pdams gagal");
        }

    }

    
    private static String nonTagListPaymentResponse() {
        String nonTagListResponse = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<response>"
                + "<transaction_name>PEMASANGAN LISTRIK BARU</transaction_name>"
                + "<registration_no>5333113000027</registration_no>"
                + "<registration_date>20120303000000</registration_date>"
                + "<subscriber_name>Turyono</subscriber_name>"
                + "<subscriber_id>SID123456879</subscriber_id>"
                + "<pln_bill>500000</pln_bill>"
                + "<switching_refno>123654654654</switching_refno>"
                + "<admin_charge>1600</admin_charge>"
                + "<amount>501600</amount>"
                + "<rc>0000</rc>"
                ////                + "<amount>55945</amount>"
                ////                + "<stan>000000000002</stan>"
                ////                + "<datetime>20120628151332</datetime>"
                ////                + "<merchant_code>6010</merchant_code>"
                ////                + "<bank_code>4510017</bank_code>"
                ////                + "<rc>0000</rc>"
                ////                + "<terminal_id>0000000000000002</terminal_id>"
                ////                + "<registration_number>123456</registration_number>"
                ////                + "<transaction_code>987654</transaction_code>"
                ////                + "<transaction_name>TRANSACTION NAME</transaction_name>"
                ////                + "<registration_date>20120302000000</registration_date>"
                ////                + "<expiration_date>20120302</expiration_date>"
                ////                + "<subscriber_id>SID1234567891011</subscriber_id>"
                ////                + "<subscriber_name>SUBSCRIBER NAME</subscriber_name>"
                ////                + "<pln_refno>PLNREF12456</pln_refno>"
                ////                + "<receipt_refno>RECEIPT12456</receipt_refno>"
                ////                + "<service_unit>SU12456</service_unit>"
                ////                + "<service_unit_address>SUADDRESS</service_unit_address>"
                ////                + "<service_unit_phone>021-0983984</service_unit_phone>"
                ////                + "<total_transaction_amount>1234</total_transaction_amount>"
                ////                + "<rptag>1234</rptag>"
                ////                + "<admin_charge>12345</admin_charge>"
                ////                + "<bill_component_amount>00000000000000000</bill_component_amount>"
                ////                //+ "<info_text>Rincian tagihan dapat diakses di www.pln.co.id</info_text>"
                + "<info_text>Informasi hubungi call center 123</info_text>"
                + "</response>";

        nonTagListResponse = "<?xml version='1.0' encoding='UTF-8'?><response><transaction_name>PEMASANGAN LISTRIK BARU  </transaction_name><registration_no>5333113000027</registration_no><registration_date>20120824</registration_date><subscriber_name>ABDUL KADIR NANANANAN    </subscriber_name><subscriber_id>000000000000</subscriber_id><pln_bill_minor>2</pln_bill_minor><pln_bill>00000000001234500</pln_bill><switching_refno>02337241E965A857029320014O713272</switching_refno><service_unit>53121</service_unit><service_unit_phone>111111111111111</service_unit_phone><admin_charge_minor>2</admin_charge_minor><admin_charge>0000160000</admin_charge><amount_minor>2</amount_minor><amount>00000000001236100</amount><rc>0000</rc><stan>000001172430</stan><info_text>Hemat Listrik dan Bebas Narkoba</info_text></response>";
        //nonTagListResponse = "<?xml version='1.0' encoding='UTF-8'?><response><code>0088</code><desc>abcd</desc></response>";
        //nonTagListResponse = "<?xml version='1.0' encoding='UTF-8'?><response><msg_type>REVERSAL</msg_type><transaction_name>PEMASANGAN LISTRIK BARU  </transaction_name><registration_no>0000000000000</registration_no><registration_date>20121019</registration_date><subscriber_name>ABDUL KADIR NANANANAN    </subscriber_name><subscriber_id>000000000000</subscriber_id><pln_bill_minor>2</pln_bill_minor><pln_bill>00000000001234500</pln_bill><switching_refno>460627O97713511701993A1904907851</switching_refno><service_unit>53121</service_unit><service_unit_phone>111111111111111</service_unit_phone><admin_charge_minor>2</admin_charge_minor><admin_charge>0000001600</admin_charge><amount_minor>2</amount_minor><amount>00000000001234500</amount><rc>0012</rc><stan>000001172865</stan><info_text>null</info_text></response>";

        //reversal
        //nonTagListResponse = "<response><msg_type>REVERSAL</msg_type><transaction_name>PEMASANGAN LISTRIK BARU  </transaction_name><registration_no>5369511000756</registration_no><registration_date>20121025</registration_date><subscriber_name>ABDUL KADIR NAN.         </subscriber_name><subscriber_id>000000000000</subscriber_id><pln_bill_minor>2</pln_bill_minor><pln_bill>00000000001234500</pln_bill><switching_refno>7U6E14119O332084690E2411750735U5</switching_refno><service_unit>53121</service_unit><service_unit_phone>111111111111111</service_unit_phone><admin_charge_minor>2</admin_charge_minor><admin_charge>0000160000</admin_charge><amount_minor>2</amount_minor><amount>00000000001234500</amount><rc>0000</rc><stan>000000001856</stan><info_text>null</info_text></response>";
        return nonTagListResponse;
    }

    private String parseBulan(String kode) {
        String bulan = "";
        if (kode.equalsIgnoreCase("01")) {
            bulan = "JAN";
        } else if (kode.equalsIgnoreCase("02")) {
            bulan = "FEB";
        } else if (kode.equalsIgnoreCase("03")) {
            bulan = "MAR";
        } else if (kode.equalsIgnoreCase("04")) {
            bulan = "APR";
        } else if (kode.equalsIgnoreCase("05")) {
            bulan = "MEI";
        } else if (kode.equalsIgnoreCase("06")) {
            bulan = "JUN";
        } else if (kode.equalsIgnoreCase("07")) {
            bulan = "JUL";
        } else if (kode.equalsIgnoreCase("08")) {
            bulan = "AUG";
        } else if (kode.equalsIgnoreCase("09")) {
            bulan = "SEP";
        } else if (kode.equalsIgnoreCase("10")) {
            bulan = "OKT";
        } else if (kode.equalsIgnoreCase("11")) {
            bulan = "NOV";
        } else if (kode.equalsIgnoreCase("12")) {
            bulan = "DES";
        }
        return bulan;

    }

    public String getRcDescription(String layanan, String rcNumber) {
        Properties pro = null;
        FileInputStream in = null;
        String rcDescription = "";
        try {
            File f = new File("rc.properties");
            if (f.exists()) {
                pro = new Properties();
                in = new FileInputStream(f);
                pro.load(in);
                rcDescription = pro.getProperty("pln." + layanan + "." + rcNumber);
            } else {
//                //System.out.println("File rc.properties not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
//                    //logger.log(Priority.FATAL, ex);
                    //java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
            }
        }
        return rcDescription;
    }

    private String setPrePaidPaymentPrinting3(Map<String, String> map, String reprintInfo) {
        String bulanTahunPrint = "";
        String del = "";
        String swithReffNo = "";
        String info = "";
        int maxLength = 25;
        int i = 0;

        //System.out.println("proses 0");
        swithReffNo = map.get("switcher_refno");
        info = map.get("info_text");

        logger.info("masuk printing format");
        //System.out.println("proses 1");
        int maxSpace = 40;
        String space = "";

        String title = "STRUK PEMBELIAN LISTRIK PRABAYAR";
        String newTitle = "";
        int space1 = maxSpace - (title.length() / 2);
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newTitle = space + title;

        //System.out.println("proses 2");
        //String title = "STRUK PEMBELIAN LISTRIK PRABAYAR";
        String newInfo = "";
        space1 = maxSpace - (info.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newInfo = space + info;

        //System.out.println("proses 3");
        String infoRincian1 = "Rincian Tagihan Dapat Diakses di www.pln.co.id";
        String newRincian1 = "";
        space1 = maxSpace - (infoRincian1.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newRincian1 = space + infoRincian1;

        //System.out.println("proses 4");
        String infoRincian2 = "INFORMASI HUB : 123 atau PLN Terdekat " + map.get("service_unit_phone").trim();
        String newRincian2 = "";
        space1 = maxSpace - (infoRincian2.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newRincian2 = space + infoRincian2;

        //System.out.println("proses 4");
        Calendar currentDate = Calendar.getInstance();
        SimpleDateFormat formatter
                = new SimpleDateFormat("dd-MM-yyyy");
//        String tanggalNow = formatter.format(currentDate.getTime());
        String tanggalNow = map.get("datetime").substring(0, 10);

        String jarak1 = "%-15s";
        String jarak11 = "%-20s";
        String jarak2 = "%-45s";

        String printingFormat
                = String.format(jarak1, tanggalNow)
                + String.format(jarak11, "")
                + newTitle
                + "\n"
                + "\n"
                + String.format(jarak1, "NO. METER")
                + String.format(jarak11, ": " + map.get("material_number").trim())
                + String.format(jarak2, "NO. METER  : " + map.get("material_number").trim())
                + "ADMIN CA        : " + map.get("admin_charge_format")
                + "\n"
                + String.format(jarak1, "IDPEL.")
                + String.format(jarak11, ": " + map.get("subscriber_id"))
                + String.format(jarak2, "IDPEL.     :" + map.get("subscriber_id"))
                + "METERAI         : " + map.get("meterai_format")
                + "\n"
                + String.format(jarak1, "NAMA")
                + String.format(jarak11, ": " + map.get("subscriber_name"))
                + String.format(jarak2, "NAMA       :" + map.get("subscriber_name"))
                + "PPN             : " + map.get("ppn_format")
                + "\n"
                + String.format(jarak1, "TARIF/DAYA")
                + String.format(jarak11, ": " + map.get("subscriber_segmentation").trim() + " / " + map.get("power_format"))
                + String.format(jarak2, "TARIF/DAYA :" + map.get("subscriber_segmentation").trim() + " / " + map.get("power_format"))
                + "PPJ             : " + map.get("ppj_format")
                + "\n"
                + String.format(jarak1, "ADMIN CA")
                + String.format(jarak11, ": " + map.get("admin_charge_format"))
                + String.format(jarak2, "NO REF     :" + swithReffNo)
                + "ANGSURAN        : " + map.get("angsuran_format")
                + "\n"
                + String.format(jarak1, "METERAI")
                + String.format(jarak11, ": " + map.get("meterai_format"))
                + String.format(jarak2, "RP. BAYAR  : " + map.get("amount_format"))
                + "RP STROOM/TOKEN : " + map.get("power_purchase_format")
                //+ "\n"
                //+ "Admin CA : " + map.get("admin_charge_format")
                //+ "\n"
                //+ "Meterai : " + map.get("meterai_format")
                //+ "\n"
                //+ "PPn : " + map.get("ppn_format")
                //+ "\n"
                //+ "PPj : " + map.get("ppj_format")
                //+ "\n"
                //+ "Angsuran : " + map.get("angsuran_format")
                //+ "\n"
                //+ "Rp. Tag : " + map.get("amount_format")
                + "\n"
                + String.format(jarak1, "PPN")
                + String.format(jarak11, ": " + map.get("ppn_format"))
                + String.format(jarak2, " ")
                + "JML KWH         : " + map.get("jml_kwh_format")
                + "\n"
                + String.format(jarak1, "PPJ")
                + String.format(jarak11, ": " + map.get("ppj_format"))
                + "STROOM/TOKEN    : " + map.get("token_format")
                + "\n"
                + String.format(jarak1, "ANGSURAN")
                + String.format(jarak11, ": " + map.get("angsuran_format"))
                + "\n"
                + String.format(jarak1, "RP STROOM/TOKEN")
                + String.format(jarak11, ": " + map.get("power_purchase_format"))
                + newInfo
                + "\n"
                + String.format(jarak1, "JML KWH")
                + String.format(jarak11, ": " + map.get("jml_kwh_format"))
                + newRincian1
                + "\n"
                + String.format(jarak1, "NO REF")
                + String.format(jarak11, ": ")
                + newRincian2
                + "\n "
                + swithReffNo
                + "\n"
                + String.format(jarak1, "STROOM/TOKEN")
                + String.format(jarak11, ": ")
                + "\n "
                + map.get("token_format");
        
        //System.out.println("proses 5");
        String loketname = "";
        String newReprintInfo = "";
        if (reprintInfo.equalsIgnoreCase("0")) {
            reprintInfo = "*CA:" + loketname;
        } else {
            reprintInfo += loketname;
        }

        //System.out.println("proses 6");
        space1 = maxSpace - (reprintInfo.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        //reprintInfo += loketname;
        newReprintInfo = space + reprintInfo;

//        if (reprintInfo.equalsIgnoreCase("0")) {
//            reprintInfo = "*CA:" + loketname;
//            newReprintInfo = space + "*CA:" + loketname;
//        }
        printingFormat += "\n"
                + String.format(jarak1, reprintInfo)
                + String.format(jarak11, " ")
                + newReprintInfo;

        //+ "\n"
        //+ String.format(jarak1, "Rp Stroom/Token : " + map.get("power_purchase_format"));
        //+ "  "+swithReffNo;
        logger.info("info : " + info);
        logger.info("new info : " + newInfo);

        logger.info("printing format done ==> hasil : ");
        logger.info(printingFormat);

        ///////////////////// test print
        printingFormat = "";

        int[] spacer1 = {16, 26, 11, 34, 16, 32};

        String BSM = "BANK SHINHAN";
        String newBSM = "";
        if (!BSM.equalsIgnoreCase("0")) {
            space1 = maxSpace - (BSM.length() / 2);
            space = "";
            for (int j = 0; j < space1; j++) {
                space += " ";
            }
            newBSM = space + BSM;
        }

        String[] printingan = new String[16];
        printingan[0] = loketname + "__  __" + newBSM;
        printingan[1] = "STRUK PEMBELIAN TOKEN PLN__  __" + newTitle;
        printingan[2] = "  __  __  ";
        printingan[3] = "NO METER__:" + map.get("material_number") + "__NO METER__:" + map.get("material_number") + "__METERAN__:" + map.get("meterai_format");
        printingan[4] = "IDPEL__:" + map.get("subscriber_id") + "__IDPEL__:" + map.get("subscriber_id") + "__PPN__:" + map.get("ppn_format");
        printingan[5] = "NAMA__:" + map.get("subscriber_name").trim() + "__NAMA__:" + map.get("subscriber_name").trim() + "__PPJ__:" + map.get("ppj_format");
        printingan[6] = "TARIF/DAYA__:" + map.get("subscriber_segmentation").trim() + "/" + map.get("power_format") + "__TARIF/DAYA__:" + map.get("subscriber_segmentation").trim() + "/" + map.get("power_format") + "__ANGSURAN__:" + map.get("angsuran_format");

        printingan[7] = "TGL BELI__:" + tanggalNow.trim() + "__NO REF *)__:" + swithReffNo + "__RP STROOM/TOKEN__:" + map.get("power_purchase_format");
        printingan[8] = "RP BAYAR__:" + map.get("amount_format").trim() + "__RP BAYAR__:" + map.get("amount_format").trim() + "__JML KWH__:" + map.get("jml_kwh_format");
        //printingan[9] = "__ __ __ __ __ ";

        printingan[9] = "STROOM/TOKEN__  __  __  __  __  ";
//        printingan[11] = map.get("token_format").trim() + "__  __STROOM/TOKEN__:  " + map.get("token_format") + "__  __  ";
        printingan[10] = map.get("token_format").trim() + "__  __STROOM/TOKEN__:  " + map.get("token_format") + "__  __  ";
        printingan[11] = "SWREF__  __  __  __  __  ";

        printingan[12] = swithReffNo + "__  __" + newInfo;

        printingan[13] = reprintInfo + "__  __" + newRincian1;
        printingan[14] = "  __  __" + newRincian2;
        printingan[15] = "  __  __" + newReprintInfo;

//        printingan[12] = "  __  __" + newRincian2;
//        printingan[13] = "  __  __*" + newReprintInfo;
        logger.info("neh hasilnya detailan : ");
        //System.out.println("proses 7");
        String detailan = "";
        //String[] spliter = new String[20];
//        StyledDocument doc = viewApp.geta().getStyledDocument();
//        addStylesToDocument(doc);
        int tokenCounter = 0;
        int breaker = 0;
        for (int j = 0; j < printingan.length; j++) {
            breaker = 0;
            int over = 0;
            String[] spliter = printingan[j].split("__");
            int splitCounter = printingan[j].split("__").length;
            //
            //logger.info("j : " + j + " printingan : " + printingan[j] + " ==> length : " + printingan.length);

            for (int k = 0; k < 6; k++) {
                int sisa = spacer1[k] - spliter[k].length();
                //System.out.print("k : " + k + " ==> lemgth : " + 6 + " sisa = " + sisa + " over : " + over);
                String whiteSpace = "";

                if (over > 0) {
                    sisa -= over;
                    over = 0;
                }

                if (sisa < 0) {
                    //sisa = 0;
                    over = (sisa * (-1));// - 2;
                    //over -= 1;
                    //if((over+1) == spacer1[k+1]){
                    //    over -= 1; 
                    //}
                }
                //logger.info(" sisa2 = " + sisa + " over2 : " + over);

                if (splitCounter == 3 && k == 2) {
                    breaker = 1;
                }
                //else {

                for (int l = 0; l < sisa; l++) {
                    logger.info("l : " + l + " ==> lemgth : " + sisa);
                    whiteSpace += " ";
                }
                //}

                detailan += spliter[k] + whiteSpace;
                //logger.info("detailan " + j + " : " + spliter[k]);

//                if (j != 11) {
//                    try {
////                for (int i = 0; i < initString.length; i++) {
//                        String abc = spliter[k] + whiteSpace;
////                            doc.insertString(doc.getLength(), abc,
//                        doc.insertString(doc.getLength(), abc,
//                                doc.getStyle("regular"));
////                doc.
////                }
//                    } catch (Exception ble) {
//                        System.err.println("Couldn't insert initial text into text pane.");
//                    }
//                } else {
//                    System.err.println("entering index 11 " + spliter[k] + " :: " + map.get("token_format") + " :: " + tokenCounter);
//
//                    if (spliter[k].contains(map.get("token_format"))
//                            && tokenCounter > 0) {
//                        try {
////                for (int i = 0; i < initString.length; i++) {
//                            doc.insertString(doc.getLength(), spliter[k],
//                                    doc.getStyle("large"));
////                doc.
////                }
//                        } catch (Exception ble) {
//                            System.err.println("Couldn't insert initial text into text pane.");
//                        }
//                    } else {
//                        try {
////                for (int i = 0; i < initString.length; i++) {
//                            String abc = spliter[k] + whiteSpace;
//                            doc.insertString(doc.getLength(), abc,
//                                    doc.getStyle("regular"));
////                doc.
////                }
//                        } catch (Exception ble) {
//                            System.err.println("Couldn't insert initial text into text pane.");
//                        }
//                    }
//                    tokenCounter++;
//                }
                if (breaker == 1) {
                    k = 6;
                }
            }
            detailan += "\n";
//            try {
////                for (int i = 0; i < initString.length; i++) {
//                doc.insertString(doc.getLength(), "\n",
//                        doc.getStyle("regular"));
////                doc.
////                }
//            } catch (Exception ble) {
//                System.err.println("Couldn't insert initial text into text pane.");
//            }

            //spliter = new String[20];
            logger.info("detailan " + j + " : " + detailan);
        }
        //System.out.println("proses 9");
        ///////////////////// test print
        printingFormat = detailan;
        
        //System.out.println("hasil detailan");
        //System.out.println(printingFormat);

        logger.info("neh hasilnya detailan : ");
        logger.info(detailan);
//ljklj
        /// printingFormat = '<table style="font-family: monospace; font-size: 9px;"><tr><td width="25%" valign="top" style="padding:5px; spacing:5px;">LOKET Loketers<BR/>STRUK PEMBELIAN TOKEN PLN<br/><br/><table ><tr><td>NO METER</td><td>:</td><td>0000000</td></tr><tr><td>IDPEL</td><td>:</td><td>44411035199</td></tr><tr><td>NAMA</td><td>:</td><td>ABI KASEP </td></tr><tr><td>TRF/DY</td><td>:</td><td> R1 / 000000900 VA</td></tr><tr><td>TGL BELI</td><td>:</td><td>15/10/2012</td></tr><tr><td>RP TAG PLN</td><td>:</td><td>Rp 18,400</td></tr><tr><td>ADMIN</td><td>:</td><td>Rp 1,600</td></tr><tr><td>TOTAL</td><td>:</td><td>Rp 20,000</td></tr><tr><td colspan="3" align="center"><br/>PREPAID PLN</td></tr><tr><td>SW REF</td><td> : </td><td></td></tr><tr><td colspan="3">83E9186I859997782707750142E57999 </td></tr><tr><td colspan="3"></td></tr></table></td><td width="1%">&nbsp;</td><td><table border="0" cellspacing="1" width="937"><tr><td colspan="4" align="center"><b>STRUK PEMBELIAN LISTRIK PRABAYAR</b><br /><br /></td></tr><tr><td width="15%">NO METER </td><td>: 0000000</td><td width="20%">ADMIN CA *) </td><td>: Rp 1,600.00</td></tr><tr><td>IDPEL </td><td>: 44411035199</td><td>METERAI </td><td>: Rp 0.00</td></tr><tr><td>NAMA </td><td>: ABI KASEP </td><td>PPN </td><td>: Rp 0.00</td></tr><tr><td>TARIF / DAYA </td><td>: R1 / 000000900</td><td>PPJ</td><td>: Rp 1,500.00</td></tr><tr><td>NO REF *) </td><td>: 83E9186I859997782707750142E57999</td><td>ANGSURAN </td><td>: Rp 5,000.00</td></tr><tr><td>RP BAYAR </td><td>: Rp 20,000.00</td><td >RP STROOM / TOKEN </td><td>: Rp 11,900.00</td></tr><tr><td colspan="2"></td><td>JML KWH </td><td>: 19.67</td></tr><!--<tr><td colspan="4" align="left"><br /><b>STROOM / TOKEN : 56866166363255363512</b></td></tr>--><tr><td colspan="4" align="left"><br /><b>STROOM / TOKEN :56866166363255363512</b></td></tr><tr><td colspan="4" align="center"><br/>Hemat Listrik & Ayo Bebas Narkoba<br/>Informasi Hubungi Call Center : 123<br/>Atau Hub. PLN Terdekat : 022-1234567</td></tr><tr><td colspan="4" align="left"><br />*CU</td></tr></table></td></tr></table>';

        //jEditorPane1.setText(cobain);
        return printingFormat;
    }

    private String setPostPaidPaymentPrinting2(Map<String, String> map, String reprintInfo) {
        //String printingFormat = "";

        int bill_lentgh = Integer.parseInt(map.get("bill_length"));
        String bulanTahunPrint = "";
        String del = "";
        String swithReffNo = "";
        String info = "";
        int maxLength = 25;
        int i = 0;
        double total_pln_bill = 0;
        for (i = 0; i < bill_lentgh; i++) {
            //Stringnodeee.setTextContent(tanggal + "-" + bulan + "-" + tahun);
            String bulanTahun = map.get("bill_period_" + i);
            String bulan = map.get("bill_period_" + i).substring(0, 2);
            String tahun = map.get("bill_period_" + i).substring(5, 7);

            if (!bulanTahunPrint.equalsIgnoreCase("")) {
                del = ",";
            }
            bulanTahunPrint += del + bulan + "" + tahun;

            logger.info("duite : " + map.get("total_electricity_bill_" + i));
            total_pln_bill += Double.parseDouble(map.get("total_electricity_bill_" + i));
        }

        /*
         if (map.get("switcher_refno").length() > maxLength) {
         swithReffNo = map.get("switcher_refno").substring(0, maxLength)
         + "\n"
         + map.get("switcher_refno").substring(maxLength);
         }

         if (map.get("info_text").length() > (maxLength + 5)) {
         info = map.get("info_text").substring(0, (maxLength + 5))
         + "\n   "
         + map.get("info_text").substring((maxLength + 5));
         }
         *
         */
        swithReffNo = map.get("switcher_refno");
        info = map.get("info_text");
        String startStandMeter = map.get("current_meter_reading1_" + 0);
        String finishStandMeter = map.get("current_meter_reading1_" + (i - 1));
        String standMeter = startStandMeter + "-" + finishStandMeter;

        int maxSpace = 40;
        String space = "";

        String title = "STRUK PEMBAYARAN TAGIHAN LISTRIK";
        String newTitle = "";
        int space1 = maxSpace - (title.length() / 2);
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newTitle = space + title;

        Calendar currentDate = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String tanggalNow = formatter.format(currentDate.getTime());
////////////////////
        String jarak1 = "%-45s";
        String printingFormat
                = ////////////////////                //String.format("%55s", "STRUK PEMBAYARAN TAGIHAN LISTRIK") + "\n"
                ////////////////////                String.format(jarak1, tanggalNow)
                ////////////////////                + space
                ////////////////////                + title
                ////////////////////                + "\n"
                ////////////////////                + "\n"
                ////////////////////                + String.format(jarak1, "ID Pel.     : " + map.get("subscriber_id"))
                ////////////////////                + String.format("%-45s", "ID Pel.     : " + map.get("subscriber_id"))
                ////////////////////                + "Bln/Th      : " + map.get("blth_summary")
                ////////////////////                + "\n"
                ////////////////////                + String.format(jarak1, "Nama        : " + map.get("subscriber_name"))
                ////////////////////                + String.format("%-45s", "Nama        : " + map.get("subscriber_name"))
                ////////////////////                + "Stand Meter : " + map.get("stand_meter_summary")
                ////////////////////                + "\n"
                ////////////////////                + String.format(jarak1, "Tarif/Daya  : " + map.get("subscriber_segmentation").trim() + " / " + map.get("power_format"))
                ////////////////////                + "Tarif/Daya  : " + map.get("subscriber_segmentation").trim() + " / " + map.get("power_format")
                ////////////////////                //+ "\n"
                ////////////////////                //+ "Bln/Th      : " + bulanTahunPrint
                ////////////////////                //+ "\n"
                ////////////////////                //+ "Stand Meter : " + standMeter
                ////////////////////                + "\n"
                ////////////////////                + String.format(jarak1, "Bln/Th      : " + map.get("blth_summary"))
                ////////////////////                + "Rp. Tag PLN : Rp. " + formatter2.format(total_pln_bill)
                ////////////////////                + "\n"
                ////////////////////                + String.format(jarak1, "Tgl. Bayar  : " + tanggalNow)
                ////////////////////                + "No Ref      : " + swithReffNo
                ////////////////////                + "\n"
                ////////////////////                + String.format(jarak1, "Stand Meter : " + map.get("stand_meter_summary"))
                ////////////////////                + "\n";
                title = "PLN menyatakan struk ini sbg bukti Pembayaran yang sah";
        space1 = maxSpace - (title.length() / 2);
        space = "";
        String buktiSahPLN = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        buktiSahPLN = space + title;
////////////////////
////////////////////        printingFormat +=
////////////////////                String.format(jarak1, "Rp. Tag PLN : Rp. " + formatter2.format(total_pln_bill))
////////////////////                + space
////////////////////                + title
////////////////////                + "\n"
////////////////////                + String.format(jarak1, "Admin CA    : " + map.get("admin_charge_format"))
////////////////////                + String.format("%-45s", "Admin CA    : " + map.get("admin_charge_format"))
////////////////////                + "\n"
////////////////////                + String.format(jarak1, "Total Bayar : " + map.get("amount_format"))
////////////////////                + "Total Bayar : " + map.get("amount_format")
////////////////////                + "\n";

        if (Integer.parseInt(map.get("outstanding_bill")) > 4) {
            if (Integer.parseInt(map.get("outstanding_bill")) > 0) {
                title = "ANDA MASIH MEMILIKI TUNGGAKAN " + (Integer.parseInt(map.get("outstanding_bill")) - 4) + " BULAN";
                space1 = maxSpace - (title.length() / 2);
                space = "";
                for (int j = 0; j < space1; j++) {
                    space += " ";
                }
                title = space + title;
            }
        } else {
            title = "TERIMA KASIH";
            space1 = maxSpace - (title.length() / 2);
            space = "";
            for (int j = 0; j < space1; j++) {
                space += " ";
            }
            title = space + title;
        }

        String infoRincian1 = "Rincian Tagihan Dapat Diakses di www.pln.co.id";
        String newRincian1 = "";
        space1 = maxSpace - (infoRincian1.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newRincian1 = space + infoRincian1;

        String infoRincian2 = "INFORMASI HUB : 123 atau PLN Terdekat " + map.get("service_unit_phone").trim();
        String newRincian2 = "";
        space1 = maxSpace - (infoRincian2.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newRincian2 = space + infoRincian2;

//////////////////////
//////////////////////        printingFormat +=
//////////////////////                String.format(jarak1, "      LISTRIK")
//////////////////////                //+ space
//////////////////////                + title
//////////////////////                + "\n"
//////////////////////                + String.format(jarak1, "No Ref      :")
//////////////////////                + newRincian1
//////////////////////                + "\n"
//////////////////////                + String.format(jarak1, "    " + swithReffNo)
//////////////////////                //+ String.format(jarak1, " ")
//////////////////////                + newRincian2;
//////////////////////        //+ String.format(jarak1, " ");
//////////////////////
//////////////////////        //+ info;
//////////////////////        logger.info("==info : " + info + "==");
////
////        info += " Atau Hub. PLN terdekat : " + map.get("service_unit_phone");
////        String[] tempInfo = info.split(" ");
////        String newInfo = "";
////        title = "";
////        //space = "";
////        int c = 0;
////        for (int j = 0; j < tempInfo.length; j++) {
////            c++;
////            title += tempInfo[j] + " ";
////
////            if (c == 6) {
////                title = title.trim();
////                space1 = maxSpace - (title.length() / 2);
////                space = "";
////                for (int k = 0; k < space1; k++) {
////                    space += " ";
////                }
////                newInfo += space + title + "\n";
////                title = "";
////                c = 0;
////            }
////        }
//////////////        info += " Atau Hub. PLN terdekat : " + map.get("service_unit_phone");
//////////////        String[] tempInfo = info.split(" Atau ");
//////////////        String newInfo = "";
//////////////        title = "";
//////////////        //space = "";
//////////////        int c = 0;
//////////////        for (int j = 0; j < tempInfo.length; j++) {
//////////////            c++;
//////////////            //title += tempInfo[j] + " ";
//////////////
//////////////            if (c == 2) {
//////////////                String title2 = "Atau";
//////////////                space1 = maxSpace - (title.length() / 2);
//////////////                space = "";
//////////////                for (int k = 0; k < space1; k++) {
//////////////                    space += " ";
//////////////                }
//////////////                newInfo += space + title2 + "\n";
//////////////                //title = "";
//////////////            }
//////////////            title = tempInfo[j].trim();
//////////////            space1 = maxSpace - (title.length() / 2);
//////////////            space = "";
//////////////            for (int k = 0; k < space1; k++) {
//////////////                space += " ";
//////////////            }
//////////////            newInfo += space + title + "\n";
//////////////            title = "";
//////////////            //c=0;
//////////////        }
        //String title = "STRUK PEMBAYARAN TAGIHAN LISTRIK";
        String newInfo = "";
        space1 = maxSpace - (info.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newInfo = space + info;

//        String loketname = viewApp.user;
        String loketname = "";
        String newReprintInfo = "";
        String tanggalBayar = "";
        if (reprintInfo.equalsIgnoreCase("0")) {
            reprintInfo = "*CA:" + loketname;
//            tanggalBayar = tanggalNow;
            tanggalBayar = map.get("datetime").substring(0, 10);
        } else {
            reprintInfo += loketname;
            tanggalBayar = "00-00-00";
        }

        space1 = maxSpace - (reprintInfo.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        //reprintInfo += loketname;
        newReprintInfo = space + reprintInfo;

        printingFormat += "\n"
                + String.format(jarak1, reprintInfo)
                + newReprintInfo;

        //tempat meletakkan info PLN postpaid
        //printingFormat += newInfo;
        logger.info(".>>>> new info postpaid : " + newInfo);

        //test printing
        //int[] spacer1 = {16, 25, 16, 33, 16, 32};
        int[] spacer1 = {16, 26, 11, 34, 12, 32};

        String BSM = "BANK SHINHAN";
        String newBSM = "";
        if (!BSM.equalsIgnoreCase("0")) {
            space1 = maxSpace - (BSM.length() / 2);
            space = "";
            for (int j = 0; j < space1; j++) {
                space += " ";
            }
            newBSM = space + BSM;
        }

        String[] printingan = new String[17];
        printingan[0] = loketname + "__  __" + newBSM;
        printingan[1] = "STRUK PEMBAYARAN PLN POSTPAID__  __" + newTitle;
        printingan[2] = "  __  __  __  __  __  ";
        printingan[3] = "IDPEL__:" + map.get("subscriber_id") + "__ IDPEL__  :" + map.get("subscriber_id") + "__  BL/TH__  :" + map.get("blth_summary");
        printingan[4] = "NAMA__:" + map.get("subscriber_name").trim() + "__ NAMA__  :" + map.get("subscriber_name").trim() + "__  STAND METER__ :" + map.get("stand_meter_summary").replace(" ", "");
        printingan[5] = "TARIF/DAYA__:" + map.get("subscriber_segmentation").trim() + "/" + map.get("power_format") + "__ TARIF/DAYA__  :" + map.get("subscriber_segmentation").trim() + "/" + map.get("power_format") + "__  __  ";
        printingan[6] = "TGL BAYAR__:" + tanggalBayar + "__ RP TAG PLN__  :" + "Rp. " + map.get("amount_format").trim() + "__  __  ";

        printingan[7] = "ST METER__:" + map.get("stand_meter_summary") + "__ NO REF*)__  :" + swithReffNo + "__  __  ";
        printingan[8] = "RP TAG PLN__:" + map.get("amount_format").trim() + "__  __  __  __ ";
        printingan[9] = "ADMIN__:" + map.get("admin_charge_format").trim() + "__" + buktiSahPLN;

        printingan[10] = "TOTAL__:" + map.get("amount_format") + "__ ADMIN CA*)__  :" + map.get("admin_charge_format") + "__  __  ";
        printingan[11] = "      POSTPAID PLN" + "__ __TOTAL BAYAR__ :" + map.get("amount_format") + "__  __  ";
        printingan[12] = "SWREF__:  __  __  __  __  ";

        printingan[13] = swithReffNo + "__  __" + newInfo;
//--
        printingan[14] = reprintInfo + "__  __" + newRincian1.replace("null", "");
        printingan[15] = "  __  __" + newRincian2.replace("null", "");
        printingan[16] = "  __  __" + newReprintInfo.replace("null", "");
        //printingan[17] = "  __  __" + newReprintInfo;

//        printingan[12] = "  __  __" + newRincian2;
//        printingan[13] = "  __  __*" + newReprintInfo;
        logger.info("neh hasilnya detailan : ");
        String detailan = "";
        //String[] spliter = new String[20];

        int breaker = 0;
        for (int j = 0; j < printingan.length; j++) {
            //if(j!=2){

            breaker = 0;
            int over = 0;
            String[] spliter = printingan[j].split("__");
            int splitCounter = printingan[j].split("__").length;
            //
            //logger.info("j : " + j + " printingan : " + printingan[j] + " ==> length : " + printingan.length);
            for (int k = 0; k < 6; k++) {
                int sisa = 0;
                try {
                    sisa = spacer1[k] - spliter[k].length();
                } catch (Exception e) {
                    logger.info("error pada bagian sisa " + "j : " + j + ", k : " + k + ", printingan : " + printingan[j] + " ==> length : " + printingan.length);
                    e.printStackTrace();
                    //logger.info("neh error : k " + k);
                }
                //
                //System.out.print("k : " + k + " ==> lemgth : " + 6 + " sisa = " + sisa + " over : " + over + " spacer : " + spacer1[k] + " splitter : " + splitCounter + " splitterVal : " + spliter[k]);
                String whiteSpace = "";

                if (over > 0) {
                    sisa -= over;
                    over = 0;
                }

                if (sisa < 0) {
                    //sisa = 0;
                    over = (sisa * (-1)) - 1;
                    //over -= 1;
                    //if((over+1) == spacer1[k+1]){
                    //    over -= 1; 
                    //}
                }
                //
                //logger.info(" sisa2 = " + sisa + " over2 : " + over);

                if (splitCounter == 3 && k == 2) {
                    breaker = 1;
                }
                //else {

                for (int l = 0; l < sisa; l++) {
                    //logger.info("l : " + l + " ==> lemgth : " + sisa);
                    whiteSpace += " ";
                }
                //}

                detailan += spliter[k] + whiteSpace;
                //
                //logger.info("detailan " + j + " : " + spliter[k]);
                if (breaker == 1) {
                    k = 6;
                }
            }
            detailan += "\n";
            //spliter = new String[20];
            logger.info("detailan " + j + " : " + detailan);

            //}
        }

        ///////////////////// test print
        printingFormat = detailan;
        
        logger.info("neh hasilnya detailan : ");
        logger.info(detailan);
        //test printing done

        return printingFormat;
    }

    private String setNonTagListPaymentPrinting2(Map<String, String> map, String reprintInfo, String create_date) {
        //String printingFormat = "";

        //int bill_lentgh = Integer.parseInt(map.get("bill_length"));
        //System.out.println("1");
        String bulanTahunPrint = "";
        String del = "";
        String swithReffNo = "";
        String info = "";
        int maxLength = 25;
        int i = 0;

        //System.out.println("2");
        info = map.get("info_text");
        //info += " Atau Hub. PLN terdekat : " + map.get("service_unit_phone");
        //String[] tempInfo = info.split(" ");
  
        int maxSpace = 40;
        String space = "";
        int space1 = 0;

        String title = "";

        title = info;
        String newInfo = "";
        space1 = maxSpace - (title.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newInfo = space + title;
        
        //System.out.println("3");
        ////System.out.println("new info : "+newInfo);
        space = "";
        title = "STRUK NON TAGIHAN LISTRIK";
        String newTitle = "";
        space1 = maxSpace - (title.length() / 2);
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newTitle = space + title;

        space = "";
        String bukti = "PLN menyatakan struk ini sebagai Bukti pembayaran yang sah";
        String newBukti = "";
        space1 = maxSpace - (bukti.length() / 2);
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newBukti = space + bukti;
        
        //String re = "PLN menyatakan struk ini sebagai Bukti pembayaran yang sah";
////        String newReprintInfo = "";
////        if (!reprintInfo.equalsIgnoreCase("0")) {
////            space = "";
////            space1 = maxSpace - (reprintInfo.length() / 2);
////            for (int j = 0; j < space1; j++) {
////                space += " ";
////            }
////            newReprintInfo = space + reprintInfo;
////        }
//////        }
        ////System.out.println("jalan 2");
        //String startStandMeter = map.get("current_meter_reading1_" + 0);
        //String finishStandMeter = map.get("current_meter_reading1_" + (i - 1));
        //String standMeter = startStandMeter + "-" + finishStandMeter;
        //SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        //Date date = new Date();
        //System.out.println("4");
        Calendar currentDate = Calendar.getInstance();
        SimpleDateFormat formatter
                = new SimpleDateFormat("dd-MM-yyyy");
        String tanggalNow = formatter.format(currentDate.getTime());
        ////System.out.println("Now the date is :=>  " + tanggalNow);

        //String tanggalNow = sdf.format(date.getDate());
        String jarak1 = "%-45s";
        String printingFormat = "";
////////////////////                printingFormat = String.format(jarak1, tanggalNow)
////////////////////                //+ String.format(jarak1, "STRUK NON TAGIHAN LISTRIK")
////////////////////                + newTitle
////////////////////                + "\n"
////////////////////                + "\n"
////////////////////                //+ "No. Meter : " + map.get("subscriber_id")
////////////////////                //+ "\n"
////////////////////                + String.format(jarak1, "TRANSAKSI.     : " + map.get("transaction_name"))
////////////////////                + "TRANSAKSI.     : " + map.get("transaction_name")
////////////////////                + "\n"
////////////////////                + String.format(jarak1, "NO REGISTRASI  : " + map.get("registration_no"))
////////////////////                + "NO REGISTRASI  : " + map.get("registration_no")
////////////////////                + "\n"
////////////////////                + String.format(jarak1, "TGL REGISTRASI : " + map.get("registration_date_format"))
////////////////////                + "TGL REGISTRASI : " + map.get("registration_date_format")
////////////////////                + "\n"
////////////////////                + String.format(jarak1, "NAMA           : " + map.get("subscriber_name"))
////////////////////                + "NAMA           : " + map.get("subscriber_name")
////////////////////                + "\n"
////////////////////                + String.format(jarak1, "ID PEL.        : " + map.get("subscriber_id"))
////////////////////                + "ID PEL.        : " + map.get("subscriber_id")
////////////////////                + "\n"
////////////////////                + String.format(jarak1, "BIAYA PLN      : " + map.get("pln_bill_format"))
////////////////////                + "BIAYA PLN      : " + map.get("pln_bill_format")
////////////////////                + "\n"
////////////////////                + String.format(jarak1, "NO REF         : ")
////////////////////                + "NO REF         : " + map.get("switching_refno")
////////////////////                + "\n"
////////////////////                + String.format(jarak1, map.get("switching_refno"))
////////////////////                + "\n"
////////////////////                + String.format(jarak1, "ADMIN CA       : " + map.get("admin_charge_format"))
////////////////////                + newBukti
////////////////////                + "\n"
////////////////////                + String.format(jarak1, "TOTAL BAYAR    : " + map.get("amount_format"))
////////////////////                + "ADMIN CA       : " + map.get("admin_charge_format")
////////////////////                + "\n"
////////////////////                + String.format(jarak1, " ")
////////////////////                + "TOTAL BAYAR    : " + map.get("amount_format")
////////////////////                + "\n"
////////////////////                + String.format(jarak1, " ")
////////////////////                + "\n"
////////////////////                + String.format(jarak1, " ")
////////////////////                + newInfo
////////////////////                + "\n"
////////////////////                + String.format(jarak1, reprintInfo);
//////////////////////                + newReprintInfo;
////////////////////        ////System.out.println("jalan 3");
////////////////////        //new String("test").;
////////////////////

        //test printing
        //System.out.println("5");
        int[] spacer1 = {16, 26, 11, 34, 16, 32};
        //int[] spacer1 = {16, 25, 16, 33, 16, 32};

        String infoRincian1 = "Rincian Tagihan Dapat Diakses di www.pln.co.id";
        String newRincian1 = "";
        space1 = maxSpace - (infoRincian1.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newRincian1 = space + infoRincian1;

        //System.out.println("5-1");
//        String infoRincian2 = "INFORMASI HUB : 123 atau PLN Terdekat " + map.get("service_unit_phone").trim();
        String infoRincian2 = "Informasi Hubungi Call Center 123 Atau Hub PLN Terdekat";
        String newRincian2 = "";
        space1 = maxSpace - (infoRincian2.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newRincian2 = space + infoRincian2;

        //System.out.println("5-2");
//        String loketname = viewApp.user;
        String loketname = "";
        //System.out.println("5-2-1");
        String newReprintInfo = "";
        String tanggalBayar = "";
        //System.out.println("5-2-2");
        if (reprintInfo.equalsIgnoreCase("0")) {
            //System.out.println("5-2-3");
            reprintInfo = "*CA:" + loketname;
            tanggalBayar = create_date.substring(8, 10) + "-"
                    + create_date.substring(5, 7) + "-"
                    + create_date.substring(0, 4);
            //System.out.println("5-2-4");
        } else {
            //System.out.println("5-2-5");
            reprintInfo += loketname;
            tanggalBayar = "00-00-00";
            //System.out.println("5-2-6");
        }

        //System.out.println("5-3");
        space1 = maxSpace - (reprintInfo.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        //reprintInfo += loketname;
        newReprintInfo = space + reprintInfo;

        //System.out.println("5-4");
        String buktinye = "PLN menyatakan struk ini sbg bukti Pembayaran yang sah";
        space1 = maxSpace - (buktinye.length() / 2);
        space = "";
        String buktiSahPLN = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        buktiSahPLN = space + buktinye;

        //System.out.println("5-5");
        String BSM = "BANK SHINHAN";
        String newBSM = "";
        if (!BSM.equalsIgnoreCase("0")) {
            space1 = maxSpace - (BSM.length() / 2);
            space = "";
            for (int j = 0; j < space1; j++) {
                space += " ";
            }
            newBSM = space + BSM;
        }

        //System.out.println("6");
        String[] printingan = new String[18];
        printingan[0] = loketname + "__  __" + newBSM;
        printingan[1] = "STRUK PEMBAYARAN PLN NONTAGLIS__  __" + newTitle;
        printingan[2] = "  __  __  __  __  __  ";
        printingan[3] = "TRANSAKSI__:" + map.get("transaction_name") + "__ TRANSAKSI__ :" + map.get("transaction_name") + "__  __  ";
        printingan[4] = "NO REG__:" + map.get("registration_no").trim() + "__ NO REG__ :" + map.get("registration_no").trim() + "__  __  ";
        printingan[5] = "TGL REG__:" + map.get("registration_date_format") + "__ TGL REG__ :" + map.get("registration_date_format").trim() + "__  __  ";
        printingan[6] = "TGL BAYAR__:" + tanggalBayar + "__ NAMA__ :" + map.get("subscriber_name").trim() + "__  __  ";
        printingan[7] = "NAMA__:" + map.get("subscriber_name").trim() + "__ IDPEL__ :" + map.get("subscriber_id") + "__  __  ";
        printingan[8] = "IDPEL__:" + map.get("subscriber_id") + "__ BIAYA PLN__ :" + map.get("pln_bill_format").trim() + "__  __  ";
        printingan[9] = "BIAYA PLN__:" + map.get("pln_bill_format").trim() + "__ NO REF__ :" + map.get("switching_refno") + "__  __  ";
        printingan[10] = "ADMIN__:" + map.get("admin_charge_format").trim() + "__" + buktiSahPLN;
        printingan[11] = "      NONTAGLIS PLN__  __ADMIN CA*)__:" + map.get("admin_charge_format") + "__  __  ";
        printingan[12] = "SWREF" + "__:  __ TOTAL BAYAR__:" + map.get("amount_format") + "__  __  ";
        printingan[13] = map.get("switching_refno") + "__  __  ";
        printingan[14] = reprintInfo + "__  __" + newInfo.replace("null", "");
        printingan[15] = "  __  __" + newRincian1.replace("null", "");
        printingan[16] = "  __  __" + newRincian2.replace("null", "");
        printingan[17] = "  __  __" + newReprintInfo.replace("null", "");

//        printingan[12] = "  __  __" + newRincian2;
//        printingan[13] = "  __  __*" + newReprintInfo;
        //System.out.println("7");
        //System.out.println("neh hasilnya detailan : ");
        String detailan = "";
        //String[] spliter = new String[20];
        
        int breaker = 0;
        for (int j = 0; j < printingan.length; j++) {
            //if(j!=2){
            breaker = 0;
            int over = 0;
            String[] spliter = printingan[j].split("__");
            int splitCounter = printingan[j].split("__").length;
            //
            //System.out.println("j : " + j + " printingan : " + printingan[j] + " ==> length : " + printingan.length);
            for (int k = 0; k < 6; k++) {
                int sisa = 0;
                try {
                    sisa = spacer1[k] - spliter[k].length();
                } catch (Exception e) {
                    //System.out.println("error pada bagian sisa " + "j : " + j + ", k : " + k + ", printingan : " + printingan[j] + " ==> length : " + printingan.length);
                    e.printStackTrace();
                }
                //
                //System.out.print("k : " + k + " ==> lemgth : " + 6 + " sisa = " + sisa + " over : " + over + " spacer : " + spacer1[k] + " splitter : " + splitCounter + " splitterVal : " + spliter[k]);
                String whiteSpace = "";

                if (over > 0) {
                    sisa -= over;
                    over = 0;
                }

                if (sisa < 0) {
                    //sisa = 0;
                    over = (sisa * (-1)) - 1;
                    //over -= 1;
                    //if((over+1) == spacer1[k+1]){
                    //    over -= 1; 
                    //}
                }
                //
                ////System.out.println(" sisa2 = " + sisa + " over2 : " + over);

                if (splitCounter == 3 && k == 2) {
                    breaker = 1;
                }
                //else {

                for (int l = 0; l < sisa; l++) {
                    ////System.out.println("l : " + l + " ==> lemgth : " + sisa);
                    whiteSpace += " ";
                }
                //}

                detailan += spliter[k] + whiteSpace;
                //System.out.println("detailan " + j + " : " + spliter[k]);
                if (breaker == 1) {
                    k = 6;
                }
            }
            detailan += "\n";
            //spliter = new String[20];
            //System.out.println("detailan " + j + " : " + detailan);

            //}
        }
        //System.out.println("8");

        ///////////////////// test print
        printingFormat = detailan;

        //System.out.println("neh hasilnya detailan : ");
        //System.out.println(detailan);
        //test printing done
        //System.out.println("9");
        return printingFormat;
    }
   
    private String setTelkomPaymentPrinting(Map<String, String> map, String reprintInfo, String create_date) {
        //String printingFormat = "";

        //int bill_lentgh = Integer.parseInt(map.get("bill_length"));
        //System.out.println("1");
        logger.info("masuk printing format");
        
        int i = 0;

        //System.out.println("2");  
        int maxSpace = 40;
        String space = "";
        int space1 = 0;

        String title = "TELKOM menyatakan struk ini sebagai bukti pembayaran yang sah.";

        //title = info;
        String newInfo = "";
        space1 = maxSpace - (title.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newInfo = space + title;
        
        //System.out.println("3");
        ////System.out.println("new info : "+newInfo);
        space = "";
        title = "STRUK PEMBAYARAN TAGIHAN TELKOM";
        String newTitle = "";
        space1 = maxSpace - (title.length() / 2);
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newTitle = space + title;
        
        
        //System.out.println("4");
        Calendar currentDate = Calendar.getInstance();
//        SimpleDateFormat formatter
//                = new SimpleDateFormat("dd-MM-yyyy");
//        String tanggalNow = formatter.format(currentDate.getTime());
        ////System.out.println("Now the date is :=>  " + tanggalNow);

        //String tanggalNow = sdf.format(date.getDate());
        String jarak1 = "%-45s";
        String printingFormat = "";

        //test printing
        //System.out.println("5");
        int[] spacer1 = {16, 35, 22, 34, 16, 32};
        //int[] spacer1 = {16, 25, 16, 33, 16, 32};


        //System.out.println("5-1");
//        String loketname = viewApp.user;
        String loketname = "";
        //System.out.println("5-2-1");
        String newReprintInfo = "";
        String tanggalBayar = "";
        //System.out.println("5-2-2");
        if (reprintInfo.equalsIgnoreCase("0")) {
            //System.out.println("5-2-3");
            reprintInfo = "*CA:" + loketname;
            tanggalBayar = create_date.substring(8, 10) + "-"
                    + create_date.substring(5, 7) + "-"
                    + create_date.substring(0, 4);
            //System.out.println("5-2-4");
        } else {
            //System.out.println("5-2-5");
            reprintInfo += loketname;
            tanggalBayar = "00-00-00";
            //System.out.println("5-2-6");
        }

        //System.out.println("5-3");
        space1 = maxSpace - (reprintInfo.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        //reprintInfo += loketname;
        newReprintInfo = space + reprintInfo;

        //System.out.println("5-4");
        String BSM = "BANK SHINHAN";
        String newBSM = "";
        if (!BSM.equalsIgnoreCase("0")) {
            space1 = maxSpace - (BSM.length() / 2);
            space = "";
            for (int j = 0; j < space1; j++) {
                space += " ";
            }
            newBSM = space + BSM;
        }
        
        
        String formattedTanggal = "";
        
            String tanggal = create_date;
            try {
                String sdf11 = "yyyy-MM-dd HH:mm:ss.s";
                String sdf22 = "dd/MM/yyyy HH:mm:ss";

                SimpleDateFormat sdff = new SimpleDateFormat(sdf11);

                Date d = sdff.parse(tanggal);
                sdff.applyPattern(sdf22);
                formattedTanggal = sdff.format(d);
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            //System.out.println(formattedTanggal);
        
        //System.out.println("6");
        String[] printingan = new String[13];
        printingan[0] = loketname + "__  __" + newBSM;
        printingan[1] = "STRUK TAGIHAN TELKOM__  __" + newTitle;
        printingan[2] = "  __  __  __  __  __  ";
        printingan[3] = "NAMA PRODUK__:" + map.get("produk") + "__NAMA PRODUK__ :" + map.get("produk") + "__  __  ";
        printingan[4] = "TGL LUNAS__:" + formattedTanggal+ "__NO REK__ :" + map.get("idpelanggan") + "__  __  ";
        printingan[5] = "NO REF__:" + map.get("noreference")+ "__NAMA__ :" + map.get("namapelanggan") + "__  __  ";
        printingan[6] = "NO REK__:" + map.get("idpelanggan") + "__TANGGAL LUNAS__ :" + formattedTanggal+ "__  __  ";
        printingan[7] = "NAMA__:" + map.get("namapelanggan") + "__NO REF__ :" + map.get("noreference")+ "__  __  ";
        printingan[8] = "RP TAG__:" + "Rp. " + formatter.format(Double.parseDouble(map.get("jumlahtagihan"))) + "__RP TAG__ :" + "Rp. " + formatter.format(Double.parseDouble(map.get("jumlahtagihan")))+ "__  __  ";
        printingan[9] = "RP ADMIN__:" + "Rp. " + formatter.format(Double.parseDouble(map.get("jumlahadm"))) + "__RP ADMIN__ :" + "Rp. " + formatter.format(Double.parseDouble(map.get("jumlahadm")))+ "__  __  ";
        printingan[10] = "TOTAL TAGIHAN__:" + "Rp. " + formatter.format(Double.parseDouble(map.get("jumlahbayar"))) + "__TOTAL TAGIHAN__ :" + "Rp. " + formatter.format(Double.parseDouble(map.get("jumlahbayar"))) + "__  __  ";
        printingan[11] = reprintInfo + "__  __" + newInfo.replace("null", "");
        printingan[12] = "  __  __" + newReprintInfo.replace("null", "");

        //System.out.println("7");
        String detailan = "";
        //String[] spliter = new String[20];

        int breaker = 0;
        for (int j = 0; j < printingan.length; j++) {
            //if(j!=2){
            breaker = 0;
            int over = 0;
            String[] spliter = printingan[j].split("__");
            int splitCounter = printingan[j].split("__").length;
            //
            ////System.out.println("j : " + j + " printingan : " + printingan[j] + " ==> length : " + printingan.length);
            for (int k = 0; k < 6; k++) {
                int sisa = 0;
                try {
                    sisa = spacer1[k] - spliter[k].length();
                } catch (Exception e) {
                    //System.out.println("error pada bagian sisa " + "j : " + j + ", k : " + k + ", printingan : " + printingan[j] + " ==> length : " + printingan.length);
                    e.printStackTrace();
                }
                //
                //System.out.print("k : " + k + " ==> lemgth : " + 6 + " sisa = " + sisa + " over : " + over + " spacer : " + spacer1[k] + " splitter : " + splitCounter + " splitterVal : " + spliter[k]);
                String whiteSpace = "";

                if (over > 0) {
                    sisa -= over;
                    over = 0;
                }

                if (sisa < 0) {
                    //sisa = 0;
                    over = (sisa * (-1)) - 1;
                    //over -= 1;
                    //if((over+1) == spacer1[k+1]){
                    //    over -= 1; 
                    //}
                }
                //
                ////System.out.println(" sisa2 = " + sisa + " over2 : " + over);

                if (splitCounter == 3 && k == 2) {
                    breaker = 1;
                }
                //else {

                for (int l = 0; l < sisa; l++) {
                    ////System.out.println("l : " + l + " ==> lemgth : " + sisa);
                    whiteSpace += " ";
                }
                //}

                detailan += spliter[k] + whiteSpace;
                //System.out.println("detailan " + j + " : " + spliter[k]);
                if (breaker == 1) {
                    k = 6;
                }
            }
            detailan += "\n";
            //spliter = new String[20];
            //System.out.println("detailan " + j + " : " + detailan);

            //}
        }
        
        //System.out.println("8");
        

        ///////////////////// test print
        printingFormat = detailan;

        //System.out.println("neh hasilnya detailan : ");
        
        //System.out.println(detailan);
        
        //System.exit(1);
        //test printing done
        //System.out.println("9");
        return printingFormat;
    }
    
    private String setPdamPaymentPrinting(Map<String, String> map, String reprintInfo, String create_date) {
        //String printingFormat = "";

        //int bill_lentgh = Integer.parseInt(map.get("bill_length"));
        //System.out.println("1");
        logger.info("masuk printing format");
        
        int i = 0;

        //System.out.println("2");  
        int maxSpace = 40;
        String space = "";
        int space1 = 0;

        String title = "PDAM menyatakan struk ini sebagai bukti pembayaran yang sah.";

        //title = info;
        String newInfo = "";
        space1 = maxSpace - (title.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newInfo = space + title;
        
        //System.out.println("3");
        ////System.out.println("new info : "+newInfo);
        space = "";
        title = "BUKTI PEMBAYARAN PDAM";
        String newTitle = "";
        space1 = maxSpace - (title.length() / 2);
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newTitle = space + title;
        
        space1 = maxSpace - (title.length() / 2);
        title = "PDAM menyatakan struk ini sebagai bukti pembayaran yang sah.";
        space = "";
        String buktiSah = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        buktiSah = space + title;
        
        //System.out.println("4");
        Calendar currentDate = Calendar.getInstance();
        String jarak1 = "%-45s";
        String printingFormat = "";

        //test printing
        //System.out.println("5");
        int[] spacer1 = {16, 26, 11, 34, 16, 32};
        //int[] spacer1 = {16, 25, 16, 33, 16, 32};


        //System.out.println("5-1");
//        String loketname = viewApp.user;
        String loketname = "";
        //System.out.println("5-2-1");
        String newReprintInfo = "";
        String tanggalBayar = "";
        //System.out.println("5-2-2");
        if (reprintInfo.equalsIgnoreCase("0")) {
            //System.out.println("5-2-3");
            reprintInfo = "*CA:" + loketname;
            tanggalBayar = create_date.substring(8, 10) + "-"
                    + create_date.substring(5, 7) + "-"
                    + create_date.substring(0, 4);
            //System.out.println("5-2-4");
        } else {
            //System.out.println("5-2-5");
            reprintInfo += loketname;
            tanggalBayar = "00-00-00";
            //System.out.println("5-2-6");
        }

        //System.out.println("5-3");
        space1 = maxSpace - (reprintInfo.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
             space += " ";
        }
        //reprintInfo += loketname;
        newReprintInfo = space + reprintInfo;

        //System.out.println("5-4");
        String BSM = "  BANK SHINHAN";
        String newBSM = "";
        if (!BSM.equalsIgnoreCase("0")) {
            space1 = maxSpace - (BSM.length() / 2);
            space = "";
            for (int j = 0; j < space1; j++) {
                space += " ";
            }
            newBSM = space + BSM;
        }
        
        
        String formattedTanggal = "";
        
            String tanggal = create_date;
            try {
                String sdf11 = "yyyy-MM-dd HH:mm:ss.s";
                String sdf22 = "dd/MM/yyyy HH:mm:ss";

                SimpleDateFormat sdff = new SimpleDateFormat(sdf11);

                Date d = sdff.parse(tanggal);
                sdff.applyPattern(sdf22);
                formattedTanggal = sdff.format(d);
            } catch (Exception e) {
                e.printStackTrace();
            }   
            
         /*201303201303*/   
            
        String blthasli = map.get("blth"); 
        String blth1 = blthasli.substring(0, 6);
        String th1 = blth1.substring(0, 4);
        String bl1 = getMonthName(blth1.substring(4, 6));
        String blth2 = blthasli.substring(6, 12);
        String th2 = blth2.substring(0, 4);
        String bl2 = getMonthName(blth2.substring(4, 6));
        String blthConvert = bl1+" "+th1+" - "+bl2+" "+th2;
        
        /*20130408165152*/
        
        String waktulunasasli = map.get("waktu_lunas");
        
        String lunasyear = waktulunasasli.substring(0, 4);
        
        String lunasmonth = waktulunasasli.substring(4, 6);
        
        String lunasDate = waktulunasasli.substring(6, 8);
        
        String lunasHour = waktulunasasli.substring(8, 10);
        
        String lunasMin = waktulunasasli.substring(10, 12);
        
        String lunasSec = waktulunasasli.substring(12, 14);
        
        String waktuLunas = lunasyear+"-"+lunasmonth+"-"+lunasDate+" "+lunasHour+":"+lunasMin+":"+lunasSec;     
        
        String swref = map.get("switching_ref");
        
        String potong1 = swref.substring(0, 16);
        String potong2 = swref.substring(16, 32);
            //System.out.println(formattedTanggal);
        
        //System.out.println("6");
        String[] printingan = new String[14];
        printingan[0] = loketname + "__  __" + newBSM;
        printingan[1] = "BUKTI PEMBAYARAN PDAM__  __" + newTitle;
        printingan[2] = "  __  __  __  __  __  ";
        printingan[3] = "NO REK__:" + map.get("idpel") + "__ NO REK__  :" + map.get("idpel") + "__  RP TAG__ :" + map.get("rp_tag");
        printingan[4] = "BLN/THN__:" + blthConvert + "__ NAMA__  :" + map.get("name").trim() + "__  BLN/THN__ :" + blthConvert;
        printingan[5] = "NAMA__:" + map.get("name").trim() + "__ TGL LUNAS__  :" + waktuLunas + "__  __  ";
        printingan[6] = "TGL LUNAS__:" + waktuLunas + "__ NO REF__  :" + map.get("switching_ref") + "__  __  ";
        printingan[7] = "NO REF__:" + potong1 + "__ __ __  __  ";
        printingan[8] = "__:" + potong2 +""+buktiSah+"__ __ __ __ ";
        //printingan[8] = "__:" + potong2 + "__sdfs :"+buktiSah+"__ __ __ ";
        printingan[9] = "RP TAG__:" + "Rp. " + formatter.format(Integer.parseInt(map.get("rp_tag"))) + "__  __  __  __ ";
        printingan[10] = "ADMIN__:" + "Rp. " + formatter.format(Integer.parseInt(map.get("biaya_admin"))) + "__ RP ADMIN__  :" + "Rp. " + formatter.format(Integer.parseInt(map.get("biaya_admin")))+"__  __  ";
        printingan[11] = "TOTAL__:" + "Rp. " + formatter.format(Integer.parseInt(map.get("amount"))) + "__ TOTAL BAYAR__ :" + "Rp. " + formatter.format(Integer.parseInt(map.get("amount"))) + "__  __  ";
        printingan[12] = "  __  __  __  __  __  ";
        printingan[13] = "  __  __" + newReprintInfo.replace("null", "");
        //System.out.println("7");
        String detailan = "";
        //String[] spliter = new String[20];
        
        int breaker = 0;
        for (int j = 0; j < printingan.length; j++) {
            //if(j!=2){
            breaker = 0;
            int over = 0;
            String[] spliter = printingan[j].split("__");
            int splitCounter = printingan[j].split("__").length;
            //
            //System.out.println("j : " + j + " printingan : " + printingan[j] + " ==> length : " + printingan.length);
            for (int k = 0; k < 6; k++) {
                int sisa = 0;
                try {
                    sisa = spacer1[k] - spliter[k].length();
                } catch (Exception e) {
                    //System.out.println("error pada bagian sisa " + "j : " + j + ", k : " + k + ", printingan : " + printingan[j] + " ==> length : " + printingan.length);
                    e.printStackTrace();
                }
                //
                //System.out.print("k : " + k + " ==> lemgth : " + 6 + " sisa = " + sisa + " over : " + over + " spacer : " + spacer1[k] + " splitter : " + splitCounter + " splitterVal : " + spliter[k]);
                String whiteSpace = "";

                if (over > 0) {
                    sisa -= over;
                    over = 0;
                }

                if (sisa < 0) {
                    //sisa = 0;
                    over = (sisa * (-1)) - 1;
                    //over -= 1;
                    //if((over+1) == spacer1[k+1]){
                    //    over -= 1; 
                    //}
                }
                //
                ////System.out.println(" sisa2 = " + sisa + " over2 : " + over);

                if (splitCounter == 3 && k == 2) {
                    breaker = 1;
                }
                //else {

                for (int l = 0; l < sisa; l++) {
                    ////System.out.println("l : " + l + " ==> lemgth : " + sisa);
                    whiteSpace += " ";
                }
                //}

                detailan += spliter[k] + whiteSpace;
                //System.out.println("detailan " + j + " : " + spliter[k]);
                if (breaker == 1) {
                    k = 6;
                }
            }
            detailan += "\n";
            //spliter = new String[20];
            //System.out.println("detailan " + j + " : " + detailan);

            //}
        }
        
        //System.out.println("8");
        

        ///////////////////// test print
        printingFormat = detailan;

        //System.out.println("neh hasilnya detailan : ");
        
        //System.out.println(detailan);
        
       
        //test printing done
        //System.out.println("9");
        return printingFormat;
    }
    
    private String setBpjsPaymentPrinting(Map<String, String> map, String reprintInfo, String create_date) {
        //String printingFormat = "";

        //int bill_lentgh = Integer.parseInt(map.get("bill_length"));
       // System.out.println("1");
        logger.info("masuk printing format");
        
        int i = 0;

        //System.out.println("2");  
        int maxSpace = 40;
        String space = "";
        int space1 = 0;

        String title = "";
        
        title = map.get("info_text");

        //title = info;
        String newInfo = "";
        space1 = maxSpace - (title.length() / 2);
        //System.out.println(space1);
        space = "";
        for (int j = 0; j < 10; j++) {
            space += " ";
        }
        newInfo = space + title;
        
        //System.out.println("3");
        ////System.out.println("new info : "+newInfo);
        space = "";
        title = "STRUK BPJS KESEHATAN";
        String newTitle = "";
        space1 = maxSpace - (title.length() / 2);
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newTitle = space + title;
        
        space1 = maxSpace - (title.length() / 2);
       
        title = "BPJS KESEHATAN MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH";
        space = "";
        String buktiSah = "";
        for (int j = 0; j < 20; j++) {
            space += " ";
        }
        buktiSah = space + title;
        
        //System.out.println("4");
        Calendar currentDate = Calendar.getInstance();
        String jarak1 = "%-45s";
        String printingFormat = "";

        //test printing
        //System.out.println("5");
        int[] spacer1 = {16, 35, 11, 40, 25, 50};
        //int[] spacer1 = {16, 25, 16, 33, 16, 32};


        //System.out.println("5-1");
//        String loketname = viewApp.user;
        String loketname = "";
        //System.out.println("5-2-1");
        String newReprintInfo = "";
        String tanggalBayar = "";
        //System.out.println("5-2-2");
        if (reprintInfo.equalsIgnoreCase("0")) {
            //System.out.println("5-2-3");
            reprintInfo = "*CA:" + loketname;
            tanggalBayar = create_date.substring(8, 10) + "-"
                    + create_date.substring(5, 7) + "-"
                    + create_date.substring(0, 4);
            //System.out.println("5-2-4");
        } else {
            //System.out.println("5-2-5");
            reprintInfo += loketname;
            tanggalBayar = "00-00-00";
            //System.out.println("5-2-6");
        }

        //System.out.println("5-3");
        space1 = maxSpace - (reprintInfo.length() / 2);
        //System.out.println(space1);
        space = "";
        for (int j = 0; j < 80; j++) {
             space += " ";
        }
        //reprintInfo += loketname;
        newReprintInfo = space + reprintInfo;

        //System.out.println("5-4");
        String BSM = "  BANK SHINHAN";
        String newBSM = "";
        if (!BSM.equalsIgnoreCase("0")) {
            space1 = maxSpace - (BSM.length() / 2);
            space = "";
            for (int j = 0; j < space1; j++) {
                space += " ";
            }
            newBSM = space + BSM;
        }
        
        
        String formattedTanggal = "";
        
            String tanggal = create_date;
            try {
                String sdf11 = "yyyy-MM-dd HH:mm:ss.s";
                String sdf22 = "dd/MM/yyyy HH:mm:ss";

                SimpleDateFormat sdff = new SimpleDateFormat(sdf11);

                Date d = sdff.parse(tanggal);
                sdff.applyPattern(sdf22);
                formattedTanggal = sdff.format(d);
            } catch (Exception e) {
                e.printStackTrace();
            }   
// System.exit(1);
        /*20130408165152*/
        
        String waktulunasasli = map.get("tgl_lunas");
        //System.out.println("waktulunasasli "+waktulunasasli);
        String lunasyear = waktulunasasli.substring(0, 4);
       // System.out.println("lunas YEAR "+lunasyear);
        String lunasmonth = waktulunasasli.substring(4, 6);
        //System.out.println("lunasmonth "+lunasmonth);
        String lunasDate = waktulunasasli.substring(6, 8);
        //System.out.println("lunasDate "+lunasDate);
        String lunasHour = waktulunasasli.substring(8, 10);
        //System.out.println("lunasHour "+lunasHour);
        String lunasMin = waktulunasasli.substring(10, 12);
        //System.out.println("lunasMin "+lunasMin);
        String lunasSec = waktulunasasli.substring(12, 14);
        //System.out.println("lunasSec "+lunasSec);
        String waktuLunas = lunasyear+"-"+lunasmonth+"-"+lunasDate+" "+lunasHour+":"+lunasMin+":"+lunasSec;     
       
       //System.out.println("waktuLunas "+waktuLunas);
       // System.exit(1);
        //System.out.println("6");
        String[] printingan = new String[17];
        printingan[0] = loketname + "__  __" + newBSM;
        printingan[1] = "STRUK BPJS KESEHATAN__  __" + newTitle;
        printingan[2] = "  __  __  __  __  __  ";
        printingan[3] = "TANGGAL__:" + waktuLunas + "__ NO REFERANSI__          :" + map.get("reffno") + "__ __ __";
        printingan[4] = "NO VA__:" + map.get("no_va").trim()+"__ TANGGAL__            :" + waktuLunas + "__ __ __ ";
        printingan[5] = "NAMA__:" + map.get("nama").trim() + "__ NO VA__            :" + map.get("no_va").trim() + "__  __  ";
        printingan[6] = "SISA__:" + "Rp. " + formatter.format(Integer.parseInt(map.get("sisa"))) + "__ NAMA PESERTA__          :" + map.get("nama") + "__  __ ";
        printingan[7] = "PERIODE__:" + map.get("jumlah_bulan") + "__ SISA SEBELUMNYA__       :" +"Rp. " + formatter.format(Integer.parseInt(map.get("sisa")))+"__  __ ";
        printingan[8] = "TOTAL__:" + "Rp. " + formatter.format(Integer.parseInt(map.get("amount"))) + "__ PERIODE__            :" +map.get("jumlah_bulan")+"__  __ ";
        printingan[9] = "NOMOR REFERENSI__:__ JUMLAH TAGIHAN__        :"+"Rp. " + formatter.format(Integer.parseInt(map.get("biaya_premi")))+"__ __ __";
        printingan[10] =map.get("reffno")+"__"+buktiSah+"__ __ __ __ ";
        printingan[11] = reprintInfo+"__ __ __ __ __ ";
        printingan[12] = "__ __ ADMIN BANK__            :" + "Rp. " + formatter.format(Integer.parseInt(map.get("biaya_admin")))+"__ __ ";
        printingan[13] = "__ __ TOTAL BAYAR__           :"+"Rp. " + formatter.format(Integer.parseInt(map.get("amount"))) + "__ __ ";
        printingan[14] = "__ __  __  __  __  ";
        printingan[15] = "__ __" + newInfo.replace("null", "");
        printingan[16] = "__ __ __              "+reprintInfo+"__  __ ";
        //System.out.println("7");
        String detailan = "";
        //System.exit(0);
        //String[] spliter = new String[20];
        
        int breaker = 0;
        for (int j = 0; j < printingan.length; j++) {
            //if(j!=2){
            breaker = 0;
            int over = 0;
            String[] spliter = printingan[j].split("__");
            int splitCounter = printingan[j].split("__").length;
            //
            //System.out.println("j : " + j + " printingan : " + printingan[j] + " ==> length : " + printingan.length);
            for (int k = 0; k < 6; k++) {
                int sisa = 0;
                try {
                    sisa = spacer1[k] - spliter[k].length();
                } catch (Exception e) {
                    //System.out.println("error pada bagian sisa " + "j : " + j + ", k : " + k + ", printingan : " + printingan[j] + " ==> length : " + printingan.length);
                    e.printStackTrace();
                }
                //
                //System.out.print("k : " + k + " ==> lemgth : " + 6 + " sisa = " + sisa + " over : " + over + " spacer : " + spacer1[k] + " splitter : " + splitCounter + " splitterVal : " + spliter[k]);
                String whiteSpace = "";

                if (over > 0) {
                    sisa -= over;
                    over = 0;
                }

                if (sisa < 0) {
                    //sisa = 0;
                    over = (sisa * (-1)) - 1;
                    //over -= 1;
                    //if((over+1) == spacer1[k+1]){
                    //    over -= 1; 
                    //}
                }
                //
                ////System.out.println(" sisa2 = " + sisa + " over2 : " + over);

                if (splitCounter == 3 && k == 2) {
                    breaker = 1;
                }
                //else {

                for (int l = 0; l < sisa; l++) {
                    ////System.out.println("l : " + l + " ==> lemgth : " + sisa);
                    whiteSpace += " ";
                }
                //}

                detailan += spliter[k] + whiteSpace;
                //System.out.println("detailan " + j + " : " + spliter[k]);
                if (breaker == 1) {
                    k = 6;
                }
            }
            detailan += "\n";
            //spliter = new String[20];
            //System.out.println("detailan " + j + " : " + detailan);

            //}
        }
        
        //System.out.println("8");
        

        ///////////////////// test print
        printingFormat = detailan;

        //System.out.println("neh hasilnya detailan : ");
        
        //System.out.println(detailan);
        
       
        //test printing done
        //System.out.println("9");
        return printingFormat;
    }
    
    private String setPulsaPaymentPrinting(Map<String, String> map, String reprintInfo, String create_date) {
        //String printingFormat = "";

        //int bill_lentgh = Integer.parseInt(map.get("bill_length"));
       // System.out.println("1");
        logger.info("masuk printing format");
        
        int i = 0;

        //System.out.println("2");  
        int maxSpace = 40;
        String space = "";
        int space1 = 0;

        String title = "";
        
        //System.out.println("3");
        space = "";
        title = "STRUK PEMBELIAN PULSA PRABAYAR";
        String newTitle = "";
        space1 = maxSpace - (title.length() / 2);
        for (int j = 0; j < 20; j++) {
            space += " ";
        }
        newTitle = space + title;
           
        //System.out.println("4");
        String printingFormat = "";

        //test printing
        //System.out.println("5");
        int[] spacer1 = {8, 35, 11, 34, 16, 32};
        //int[] spacer1 = {16, 25, 16, 33, 16, 32};


        //System.out.println("5-1");
//        String loketname = viewApp.user;
        String loketname = "";
        //System.out.println("5-2-1");
        
        //System.out.println("5-2-2");

        //System.out.println("5-4");
        String BSM = "  BANK SHINHAN";
        String newBSM = "";
        if (!BSM.equalsIgnoreCase("0")) {
            space1 = maxSpace - (BSM.length() / 2);
            space = "";
            for (int j = 0; j < 28; j++) {
                space += " ";
            }
            newBSM = space + BSM;
        }
        
        
        String formattedTanggal = "";
        
            String tanggal = create_date;
   
            try {
                String sdf11 = "yyyy-MM-dd HH:mm:ss";
                String sdf22 = "dd/MM/yyyy HH:mm:ss";

                SimpleDateFormat sdff = new SimpleDateFormat(sdf11);

                Date d = sdff.parse(tanggal);
                sdff.applyPattern(sdf22);
                formattedTanggal = sdff.format(d);
            } catch (Exception e) {
                e.printStackTrace();
            }   

        //System.out.println("6");
        String[] printingan = new String[8];
        printingan[0] = loketname + "__  __" + newBSM;
        printingan[1] = "STRUK PULSA PRABAYAR__  __" + newTitle;
        printingan[2] = "  __  __  __  __  __  ";
        printingan[3] = "TANGGAL__:" + formattedTanggal + "__ TANGGAL__:" + formattedTanggal + "__ __ __";
        printingan[4] = "TRX ID__:" + map.get("stan")+"__ TRX ID__:" + map.get("stan") + "__ __ __ ";
        String producttype = map.get("product_type");
        //System.out.println(producttype);
        String value = "PULSA PASCABAYAR";
        //System.out.println(value);
        if(producttype.equalsIgnoreCase(value)){                      
            printingan[5] = "PRODUK__:"+map.get("produk").trim()+"__ PRODUK__:"+map.get("produk").trim() + "__  __  ";
            printingan[6] = "SN__:"+map.get("sn").trim()+"__ SN__:"+map.get("sn").trim()+ "__  __ ";
            printingan[7] = "MSISDN__:"+map.get("msisdn").trim()+"__ MSISDN__:"+map.get("msisdn").trim()+"__  __ ";
                
        }else{
            
            printingan[5] = "PRODUK__:"+map.get("desc").trim()+"__ PRODUK__:"+map.get("desc").trim()+ "__  __  ";
            printingan[6] = "SN__:"+map.get("sn").trim() + "__ SN__:"+map.get("sn").trim()+ "__  __ ";
            printingan[7] = "MSISDN__:"+map.get("msisdn").trim()+"__ MSISDN__:"+map.get("msisdn").trim()+"__  __ ";
        }
        //System.out.println("7");
        String detailan = "";
        //System.exit(0);
        //String[] spliter = new String[20];
        
        int breaker = 0;
        for (int j = 0; j < printingan.length; j++) {
            //if(j!=2){
            breaker = 0;
            int over = 0;
            String[] spliter = printingan[j].split("__");
            int splitCounter = printingan[j].split("__").length;
            //
            //System.out.println("j : " + j + " printingan : " + printingan[j] + " ==> length : " + printingan.length);
            for (int k = 0; k < 6; k++) {
                int sisa = 0;
                try {
                    sisa = spacer1[k] - spliter[k].length();
                } catch (Exception e) {
                    //System.out.println("error pada bagian sisa " + "j : " + j + ", k : " + k + ", printingan : " + printingan[j] + " ==> length : " + printingan.length);
                    e.printStackTrace();
                }
                //
                //System.out.print("k : " + k + " ==> lemgth : " + 6 + " sisa = " + sisa + " over : " + over + " spacer : " + spacer1[k] + " splitter : " + splitCounter + " splitterVal : " + spliter[k]);
                String whiteSpace = "";

                if (over > 0) {
                    sisa -= over;
                    over = 0;
                }

                if (sisa < 0) {
                    //sisa = 0;
                    over = (sisa * (-1)) - 1;
                    //over -= 1;
                    //if((over+1) == spacer1[k+1]){
                    //    over -= 1; 
                    //}
                }
                //
                ////System.out.println(" sisa2 = " + sisa + " over2 : " + over);

                if (splitCounter == 3 && k == 2) {
                    breaker = 1;
                }
                //else {

                for (int l = 0; l < sisa; l++) {
                    ////System.out.println("l : " + l + " ==> lemgth : " + sisa);
                    whiteSpace += " ";
                }
                //}

                detailan += spliter[k] + whiteSpace;
                //System.out.println("detailan " + j + " : " + spliter[k]);
                if (breaker == 1) {
                    k = 6;
                }
            }
            detailan += "\n";
            //spliter = new String[20];
            //System.out.println("detailan " + j + " : " + detailan);

            //}
        }
        
        //System.out.println("8");
        

        ///////////////////// test print
        printingFormat = detailan;

        //System.out.println("neh hasilnya detailan : ");
        
        //System.out.println(detailan);
        
       
        //test printing done
        //System.out.println("9");
        return printingFormat;
    }
    
    private String setPulsaPascabayarPaymentPrinting(Map<String, String> map, String reprintInfo, String create_date) {
        //String printingFormat = "";

        //int bill_lentgh = Integer.parseInt(map.get("bill_length"));
       // System.out.println("1");
        logger.info("masuk printing format");
        
        int i = 0;

        //System.out.println("2");  
        int maxSpace = 40;
        String space = "";
        int space1 = 0;

        String title = "";
        
        //System.out.println("3");
        space = "";
        title = "STRUK PEMBAYARAN TAGIHAN PULSA PASCA BAYAR";
        String newTitle = "";
        space1 = maxSpace - (title.length() / 2);
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newTitle = space + title;
           
        //System.out.println("4");
        String printingFormat = "";

        //test printing
        //System.out.println("5");
        int[] spacer1 = {8, 35, 11, 34, 16, 32};
        //int[] spacer1 = {16, 25, 16, 33, 16, 32};


        //System.out.println("5-1");
//        String loketname = viewApp.user;
        String loketname = "";
        //System.out.println("5-2-1");
        
        //System.out.println("5-2-2");

        //System.out.println("5-4");
        String BSM = "  BANK SHINHAN";
        String newBSM = "";
        if (!BSM.equalsIgnoreCase("0")) {
            space1 = maxSpace - (BSM.length() / 2);
            space = "";
            for (int j = 0; j < 35; j++) {
                space += " ";
            }
            newBSM = space + BSM;
        }
        
        title = "Terima kasih atas kepercayaan Anda menjadi mitra kami. ";

        //title = info;
        String newInfo = "";
        space1 = maxSpace - (title.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newInfo = space + title;
        
        
        loketname = "";
        //System.out.println("5-2-1");
        String newReprintInfo = "";
        String tanggalBayar = "";
        //System.out.println("5-2-2");
        if (reprintInfo.equalsIgnoreCase("0")) {
            //System.out.println("5-2-3");
            reprintInfo = "*CA:" + loketname;
            tanggalBayar = create_date.substring(8, 10) + "-"
                    + create_date.substring(5, 7) + "-"
                    + create_date.substring(0, 4);
            //System.out.println("5-2-4");
        } else {
            //System.out.println("5-2-5");
            reprintInfo += loketname;
            tanggalBayar = "00-00-00";
            //System.out.println("5-2-6");
        }

        //System.out.println("5-3");
        space1 = maxSpace - (reprintInfo.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
             space += " ";
        }
        //reprintInfo += loketname;
        newReprintInfo = space + reprintInfo;
        
        String formattedTanggal = "";
        
            String tanggal = create_date;
   
            try {
                String sdf11 = "yyyy-MM-dd HH:mm:ss";
                String sdf22 = "dd/MM/yyyy HH:mm:ss";

                SimpleDateFormat sdff = new SimpleDateFormat(sdf11);

                Date d = sdff.parse(tanggal);
                sdff.applyPattern(sdf22);
                formattedTanggal = sdff.format(d);
            } catch (Exception e) {
                e.printStackTrace();
            }   
        
        String dataori = map.get("msisdn").trim();
        System.out.println(dataori);
        String[] parts = dataori.split("\\(");
        String part1 = parts[0];
        String part2 = parts[1].trim();
        String[] splitpart2 = part2.split("\\)");
 
        String[] removespace =  splitpart2[0].split(" ");
        String nama1;
        String nama2;   
    
        //System.out.println("6");
        String[] printingan = new String[12];
        printingan[0] = loketname + "__  __" + newBSM;
        printingan[1] = "STRUK TAGIHAN PULSA PASCA BAYAR__  __" + newTitle;
        printingan[2] = "  __  __  __  __  __  ";
        printingan[3] = "PRODUK__  :"+map.get("produk").trim()+"__ PRODUK__    :"+map.get("produk").trim() + "__  __  ";
        printingan[4] = "TGL BAYAR__ :" + formattedTanggal + "__NO PELANGGAN__  :" + map.get("msisdn").trim() + "__ __ ";
        printingan[5] = "NO PEL__  :" + part1 + "__ TANGGAL BAYAR__ :" + formattedTanggal + "__ __ ";
  
//        if(removespace.length==1){
//            nama1 = removespace[0];
//            printingan[6] = "__  " + nama1 + "__ RP TAG__    :Rp. "+formatter.format(Integer.parseInt(map.get("price")))+" __ __ ";
//            printingan[7] = "RP TAG__:"+formatter.format(Integer.parseInt(map.get("price")))+"__ RP ADMIN__    :Rp. 0 __ __ ";
//        }else if(removespace.length==2){
//            nama1 = removespace[0]+" "+removespace[1];
//            printingan[6] = "__  " + nama1 + "__ RP TAG__    :Rp. "+formatter.format(Integer.parseInt(map.get("price")))+" __ __ ";
//            printingan[7] = "RP TAG__:"+formatter.format(Integer.parseInt(map.get("price")))+"__ RP ADMIN__    :Rp. 0 __ __ ";
//        }else if(removespace.length==3){
            if(removespace.length==1){
                nama1 = removespace[0];
            }else if(removespace.length==2){
                nama1 = removespace[0]+" "+removespace[1];
            }else if(removespace.length==3){
                nama1 = removespace[0]+" "+removespace[1]+" "+removespace[2];
            }else{
                nama1 = removespace[0]+" "+removespace[1]+" "+removespace[2]+" "+removespace[3];
            
            }//nama1 = "BAPAK YOSEFIN  AJENG ANGRAINI";
            printingan[6] = "__   " + nama1 + "__ RP TAG__    :Rp. "+formatter.format(Integer.parseInt(map.get("price")))+" __ __ ";
            printingan[7] = "RP TAG__  :Rp. "+formatter.format(Integer.parseInt(map.get("price")))+"__ RP ADMIN__    :Rp. 0 __ __ ";
            printingan[8] = "TOTAL TAG__ :Rp. "+formatter.format(Integer.parseInt(map.get("price")))+"__TOTAL TAGIHAN__ :Rp. "+formatter.format(Integer.parseInt(map.get("price")))+"__ __ ";
            printingan[9] = "  __  __  __  __  __  ";
            printingan[10] = "__  __" + newInfo.replace("null", "");
            printingan[11] = "__  __" + newReprintInfo.replace("null", "");
//        }else if(removespace.length==4){
//            nama1 = removespace[0]+" "+removespace[1];
//            nama2 = removespace[2]+" "+removespace[3];
//            printingan[6] = "__  " + nama1 + "__ RP TAG__    :Rp. "+formatter.format(Integer.parseInt(map.get("price")))+" __ __ ";
//            printingan[7] = "__    "+ nama2+"__ RP ADMIN__    :Rp. 0 __ __ ";
//        }
        //printingan[4] = "TRX ID__:" + map.get("stan")+"__ TRX ID__:" + map.get("stan") + "__ __ __ ";                            
        //printingan[6] = "SN__:"+map.get("sn").trim()+"__ SN__:"+map.get("sn").trim()+ "__  __ ";
        //printingan[7] = "MSISDN__:"+map.get("msisdn").trim()+"__ MSISDN__:"+map.get("msisdn").trim()+"__  __ ";
                
        
        //System.out.println("7");
        String detailan = "";
        //System.exit(0);
        //String[] spliter = new String[20];
        
        int breaker = 0;
        for (int j = 0; j < printingan.length; j++) {
            //if(j!=2){
            breaker = 0;
            int over = 0;
            String[] spliter = printingan[j].split("__");
            int splitCounter = printingan[j].split("__").length;
            //
            //System.out.println("j : " + j + " printingan : " + printingan[j] + " ==> length : " + printingan.length);
            for (int k = 0; k < 6; k++) {
                int sisa = 0;
                try {
                    sisa = spacer1[k] - spliter[k].length();
                } catch (Exception e) {
                    //System.out.println("error pada bagian sisa " + "j : " + j + ", k : " + k + ", printingan : " + printingan[j] + " ==> length : " + printingan.length);
                    e.printStackTrace();
                }
                //
                //System.out.print("k : " + k + " ==> lemgth : " + 6 + " sisa = " + sisa + " over : " + over + " spacer : " + spacer1[k] + " splitter : " + splitCounter + " splitterVal : " + spliter[k]);
                String whiteSpace = "";

                if (over > 0) {
                    sisa -= over;
                    over = 0;
                }

                if (sisa < 0) {
                    //sisa = 0;
                    over = (sisa * (-1)) - 1;
                    //over -= 1;
                    //if((over+1) == spacer1[k+1]){
                    //    over -= 1; 
                    //}
                }
                //
                ////System.out.println(" sisa2 = " + sisa + " over2 : " + over);

                if (splitCounter == 3 && k == 2) {
                    breaker = 1;
                }
                //else {

                for (int l = 0; l < sisa; l++) {
                    ////System.out.println("l : " + l + " ==> lemgth : " + sisa);
                    whiteSpace += " ";
                }
                //}

                detailan += spliter[k] + whiteSpace;
                //System.out.println("detailan " + j + " : " + spliter[k]);
                if (breaker == 1) {
                    k = 6;
                }
            }
            detailan += "\n";
            //spliter = new String[20];
            //System.out.println("detailan " + j + " : " + detailan);

            //}
        }
        
        //System.out.println("8");
        

        ///////////////////// test print
        printingFormat = detailan;

        //System.out.println("neh hasilnya detailan : ");
        
        //System.out.println(detailan);
        
       
        //test printing done
        //System.out.println("9");
        return printingFormat;
    }
    
    private String setGamePaymentPrinting(Map<String, String> map, String reprintInfo, String create_date) {
        //String printingFormat = "";

        //int bill_lentgh = Integer.parseInt(map.get("bill_length"));
       // System.out.println("1");
        logger.info("masuk printing format");
        
        int i = 0;

        //System.out.println("2");  
        int maxSpace = 40;
        String space = "";
        int space1 = 0;

        String title = "";
        
        //System.out.println("3");
        space = "";
        title = "STRUK PEMBELIAN VOUCHER GAME";
        String newTitle = "";
        space1 = maxSpace - (title.length() / 2);
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newTitle = space + title;
           
        //System.out.println("4");
        String printingFormat = "";

        //test printing
        //System.out.println("5");
        int[] spacer1 = {16, 26, 11, 34, 16, 32};
        //int[] spacer1 = {16, 25, 16, 33, 16, 32};


        //System.out.println("5-1");
//        String loketname = viewApp.user;
        String loketname = "";
        //System.out.println("5-2-1");
        
        //System.out.println("5-2-2");

        //System.out.println("5-4");
        String BSM = "  BANK SHINHAN";
        String newBSM = "";
        if (!BSM.equalsIgnoreCase("0")) {
            space1 = maxSpace - (BSM.length() / 2);
            space = "";
            for (int j = 0; j < space1; j++) {
                space += " ";
            }
            newBSM = space + BSM;
        }
        
        
        String formattedTanggal = "";
        
            String tanggal = create_date;
            try {
                String sdf11 = "yyyy-MM-dd HH:mm:ss.s";
                String sdf22 = "dd/MM/yyyy HH:mm:ss";

                SimpleDateFormat sdff = new SimpleDateFormat(sdf11);

                Date d = sdff.parse(tanggal);
                sdff.applyPattern(sdf22);
                formattedTanggal = sdff.format(d);
            } catch (Exception e) {
                e.printStackTrace();
            }   

        //System.out.println("6");
        String[] printingan = new String[11];
        printingan[0] = loketname + "__  __" + newBSM;
        printingan[1] = "STRUK PEMBELIAN VOUCHER GAME__  __" + newTitle;
        printingan[2] = "  __  __  __  __  __  ";
        printingan[3] = "TANGGAL__:" + formattedTanggal + "__ TANGGAL__   :" + formattedTanggal + "__ __ __";
        printingan[4] = "TRX ID__:" + map.get("trx_id")+"__ TRX ID__   :" + map.get("trx_id") + "__ __ __ ";
        printingan[5] = "VOUCHER__:" + map.get("desc") + "__ VOUCHER__   :" + map.get("desc") + "__  __  ";
        printingan[6] = "HP__:" + map.get("hp") + "__ PIN__   :   " + map.get("pin") + "__  __ ";
        printingan[7] = "__ __ PASSWORD__   :"+map.get("voucher_password")+"__  __ ";
        printingan[8] = "__ __ SN__   :"+map.get("serial_number")+"__  __ ";
        printingan[9] = "__ __ VOUCHER CODE__ :"+map.get("voucher_code")+"__  __ ";
        printingan[10] = "__ __ HP__   :"+map.get("hp")+"__  __ ";
        
        //System.out.println("7");
        String detailan = "";
        //System.exit(0);
        //String[] spliter = new String[20];
        
        int breaker = 0;
        for (int j = 0; j < printingan.length; j++) {
            //if(j!=2){
            breaker = 0;
            int over = 0;
            String[] spliter = printingan[j].split("__");
            int splitCounter = printingan[j].split("__").length;
            //
            //System.out.println("j : " + j + " printingan : " + printingan[j] + " ==> length : " + printingan.length);
            for (int k = 0; k < 6; k++) {
                int sisa = 0;
                try {
                    sisa = spacer1[k] - spliter[k].length();
                } catch (Exception e) {
                    //System.out.println("error pada bagian sisa " + "j : " + j + ", k : " + k + ", printingan : " + printingan[j] + " ==> length : " + printingan.length);
                    e.printStackTrace();
                }
                //
                //System.out.print("k : " + k + " ==> lemgth : " + 6 + " sisa = " + sisa + " over : " + over + " spacer : " + spacer1[k] + " splitter : " + splitCounter + " splitterVal : " + spliter[k]);
                String whiteSpace = "";

                if (over > 0) {
                    sisa -= over;
                    over = 0;
                }

                if (sisa < 0) {
                    //sisa = 0;
                    over = (sisa * (-1)) - 1;
                    //over -= 1;
                    //if((over+1) == spacer1[k+1]){
                    //    over -= 1; 
                    //}
                }
                //
                ////System.out.println(" sisa2 = " + sisa + " over2 : " + over);

                if (splitCounter == 3 && k == 2) {
                    breaker = 1;
                }
                //else {

                for (int l = 0; l < sisa; l++) {
                    ////System.out.println("l : " + l + " ==> lemgth : " + sisa);
                    whiteSpace += " ";
                }
                //}

                detailan += spliter[k] + whiteSpace;
                //System.out.println("detailan " + j + " : " + spliter[k]);
                if (breaker == 1) {
                    k = 6;
                }
            }
            detailan += "\n";
            //spliter = new String[20];
            //System.out.println("detailan " + j + " : " + detailan);

            //}
        }
        
        //System.out.println("8");
        

        ///////////////////// test print
        printingFormat = detailan;

        //System.out.println("neh hasilnya detailan : ");
        
        //System.out.println(detailan);
        
       
        //test printing done
        //System.out.println("9");
        return printingFormat;
    }
    
    public String getPaymentPrePaidResponse(String response, String reprintInfo) {
        response = response.replace("&", "dan");
        String printingFormatPrepaid = "";

        //System.out.println("prepaid 1");
        Document doc;
        String temp = "";
        Map<String, String> titleMap = new HashMap<String, String>();
        titleMap.put("amount", "Jumlah");
        titleMap.put("stan", "stan"); //hapus
        titleMap.put("datetime", "Tanggal");
        titleMap.put("date_settlement", "Tanggal sett.");
        titleMap.put("merchant_code", "Kode Merchant");
        titleMap.put("bank_code", "Kode Bank");
        titleMap.put("rc", "Pesan Balasan");
        //titleMap.put("terminal_id", "ID Terminal"); //hapus
        titleMap.put("material_number", "No Meter");
        titleMap.put("power_purchase", "Rp. Stroom/Token");

        titleMap.put("subscriber_id", "ID Pelanggan");
        titleMap.put("pln_refno", "No. Ref PLN");
        titleMap.put("switcher_refno", "switcher refno");
        titleMap.put("vending_reffno", "vending reffno");
        titleMap.put("subscriber_name", "Nama Pelanggan");
        titleMap.put("subscriber_segmentation", "Golongan Pelanggan");
        titleMap.put("power", "Daya");
        titleMap.put("admin_charge", "Biaya Administrasi");
        titleMap.put("meterai", "Meterai");
        titleMap.put("ppn", "ppn");
        titleMap.put("ppj", "ppj");
        titleMap.put("angsuran", "angsuran");
        titleMap.put("jml_kwh", "Jml KWH");
        titleMap.put("token", "Token");

        titleMap.put("distribution_code", "Kode Distribusi");
        titleMap.put("service_unit", "Service Unit");
        titleMap.put("service_unit_phone", "Service Unit Phone");
        titleMap.put("max_kwh", "KWH Maks.");
        titleMap.put("total_repeat", "Total Perulangan");
        titleMap.put("power_purchase_unsold", "PPU");

        titleMap.put("bit48", "BIT48");
        titleMap.put("bit62", "BIT62");

        titleMap.put("info_text", "Info");
        titleMap.put("desc", "keterangan");

        /*
         titleMap.put("bill_status", "Tagihan yang harus dibayarkan");
         titleMap.put("outstanding_bill", "Tagihan yang harus dibayarkan");

         titleMap.put("bill_period", "Periode Tunggakan");
         titleMap.put("due_date", "Jatuh Tempo");
         titleMap.put("meter_read_date", "Tanggal Baca Meteran");
         titleMap.put("total_electricity_bill", "Total Tunggakan");
         titleMap.put("incentive", "Insentif");
         titleMap.put("value_added_tax", "PPN");
         titleMap.put("penalty_fee", "Penalti");
         *
         */
        titleMap.put("code", "Pesan Balasan");
        //titleMap.put("desc", "Keterangan");

        map.put("code", "0000");
        //System.out.println("prepaid 2");

        try {
            byte[] theByteArray = response.getBytes(); //InputStream x =
            //String oi = new String(responseBody);
            //logger.info("Response : " + oi);
            ByteArrayInputStream instream = new ByteArrayInputStream(theByteArray);
            //String parseResult = formatResponse(instream);
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            doc = docBuilder.parse(instream);
            doc.getDocumentElement().normalize();

            String detailResult = "";
            String detailResult2 = "";
            String detailError = "";
            int flagErr = 0;
            //Map<String, String> map = new HashMap<String, String>();
            boolean child = false;
            //NumberFormat formatter = new DecimalFormat("##,000.00");
            //NumberFormat formatter2 = new DecimalFormat("##,000.00");

            PrepaidResponseCode prerc = new PrepaidResponseCode();
            MerchantCode mc = new MerchantCode();
            Node nodeee = doc.getElementsByTagName("response").item(0).getFirstChild();
            //logger.info("content : "+nodeee.getTextContent());
            //System.out.println("prepaid 3");
            
            while (nodeee != null) {
                temp = "";
                if (titleMap.get(nodeee.getNodeName()) != null) {
                    logger.info(nodeee.getNodeName() + " => " + nodeee.getTextContent());
                    if (nodeee.getNodeName().equalsIgnoreCase("datetime")) {
                        logger.info("masuk date time");
                        if (nodeee.getTextContent().length() < 14) {
                            DateFormat dateFormat = new SimpleDateFormat("yyyy");
                            Calendar currentDate = Calendar.getInstance();
                            nodeee.setTextContent(nodeee.getTextContent() + "" + dateFormat.format(currentDate.getTime()));
                        }
                        logger.info(nodeee.getTextContent());
                        String tahun = nodeee.getTextContent().substring(0, 4);
                        String bulan = nodeee.getTextContent().substring(4, 6);
                        String tanggal = nodeee.getTextContent().substring(6, 8);
                        String jam = nodeee.getTextContent().substring(8, 10);
                        String menit = nodeee.getTextContent().substring(10, 12);
                        String detik = nodeee.getTextContent().substring(12, 14);
                        //DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        nodeee.setTextContent(tanggal + "-" + bulan + "-" + tahun + " " + jam + ":" + menit + ":" + detik);
                        logger.info(nodeee.getTextContent());
                        
                        //System.out.println("tanggal "+nodeee.getTextContent());
                        
                    } else if (nodeee.getNodeName() == "amount"
                            || nodeee.getNodeName() == "admin_charge") {
                        //nodeee.setTextContent("Rp. " + formatter.format(Integer.parseInt(temp)));
                        try {
                            if (nodeee.getNodeName().equalsIgnoreCase("amount")) {
                                String amount = nodeee.getTextContent().substring(4);
                                nodeee.setTextContent(amount);
                            } else if (nodeee.getNodeName().equalsIgnoreCase("admin_charge")) {
                                int admChargeLength = nodeee.getTextContent().length();
                                String admCharge = nodeee.getTextContent().substring(0, (admChargeLength - 2));
                                nodeee.setTextContent(nodeee.getTextContent().trim());
                            }
                            temp = nodeee.getTextContent();
                            nodeee.setTextContent("Rp. " + formatter2.format(Double.parseDouble(temp)));
                           // System.out.println("amount or admin_charge "+nodeee.getTextContent());
                        } catch (Exception e) {
                            e.printStackTrace();
                            temp = "0";
                            nodeee.setTextContent("Rp. " + formatter2.format(0));
                        }
                        map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                        //nodeee.setTextContent(temp);
                    } else if (nodeee.getNodeName() == "meterai"
                            || nodeee.getNodeName() == "power_purchase"
                            || nodeee.getNodeName() == "ppn"
                            || nodeee.getNodeName() == "ppj"
                            || nodeee.getNodeName() == "angsuran") {

                        //nodeee.setTextContent("Rp. " + formatter.format(Integer.parseInt(temp)));
                        try {
                            int admChargeLength = nodeee.getTextContent().length();
                            String admCharge = nodeee.getTextContent().substring(0, (admChargeLength - 2)) + "." + nodeee.getTextContent().substring((admChargeLength - 2));
                            nodeee.setTextContent(admCharge);

                            temp = nodeee.getTextContent();
                            nodeee.setTextContent("Rp. " + formatter.format(Double.parseDouble(temp)));
                            
                            //System.out.println("meterai or power_purchase or ppn or ppj or angsuran "+nodeee.getTextContent());
                        } catch (Exception e) {
                            e.printStackTrace();
                            temp = "0";
                            nodeee.setTextContent("Rp. " + formatter.format(0));
                        }
                        map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                        //nodeee.setTextContent(temp);
                    } else if (nodeee.getNodeName() == "jml_kwh") {
                        
                        try {
                            int admChargeLength = nodeee.getTextContent().length();
                            String admCharge = nodeee.getTextContent().substring(0, (admChargeLength - 2)) + "." + nodeee.getTextContent().substring((admChargeLength - 2));
                            nodeee.setTextContent(admCharge);

                            temp = nodeee.getTextContent();
                            nodeee.setTextContent(formatter.format(Double.parseDouble(temp)));
                            
                            
                        } catch (Exception e) {
                            e.printStackTrace();
                            temp = "0";
                            nodeee.setTextContent(formatter.format(0));
                        }
                        map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                        //nodeee.setTextContent(temp);
                    } else if (nodeee.getNodeName() == "power") {
                        if (nodeee.getTextContent() == null) {
                            nodeee.setTextContent("0");
                        }
                        temp = nodeee.getTextContent();
                        //nodeee.setTextContent("" + Integer.parseInt(nodeee.getTextContent()) + " KWH");
                        nodeee.setTextContent(Integer.parseInt(nodeee.getTextContent().trim()) + " VA");
                        map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                        
                        //System.out.println("power "+nodeee.getTextContent());
                        
                        
                    } 
                    else if (nodeee.getNodeName() == "rc" || nodeee.getNodeName() == "code") {
                        //temp = "0088";

                        temp = nodeee.getTextContent().trim();
                        logger.info("rc || code ==> " + temp);

                        nodeee.setTextContent(getRcDescription("pre", temp));
                        map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                        if (!temp.equalsIgnoreCase("0000")) {
                            //logger.info("BUKAN 0000");
                            flagErr = 1;
                        } else {
                            //logger.info("ADALAH 0000");
                            flagErr = 0;
                        }

                        if (!nodeee.getNodeName().equalsIgnoreCase("rc")) {
                            map.put("rc", temp);
                            map.put("rc_format", nodeee.getTextContent());
                        }
                        
                        //System.out.println("rc or code "+nodeee.getTextContent());
                        logger.info(nodeee.getNodeName() + " ==> " + map.get("rc_format"));
                        //nodeee.setTextContent(prc.getInquiryMsgResponseName(nodeee.getTextContent()));
                    } else if (nodeee.getNodeName() == "merchant_code") {
                        temp = nodeee.getTextContent();
                        //nodeee.setTextContent(formatter.format(Integer.parseInt(temp)) + " KWH");
                        nodeee.setTextContent(mc.getMerchantNameByCode(nodeee.getTextContent()));
                        map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                        //System.out.println("merchant_code "+nodeee.getTextContent());
                        //nodeee.setTextContent(mc.getMerchantNameByCode(nodeee.getTextContent()));
                    } else if (nodeee.getNodeName() == "token") {
                        temp = nodeee.getTextContent();

                        String tokenFormat = "";
                        int per = 4;
                        int awal = 0;
                        int akhir = 0;
                        for (int i = 0; i < 5; i++) {
                            //byte b = theByteArray[i];
                            akhir += per;
                            tokenFormat += temp.substring(awal, akhir) + " ";
                            awal = akhir;
                            //logger.info("part " + i + " : " + tokenFormat);
                        }
                        //logger.info("implode " + tokenFormat);

                        //nodeee.setTextContent(formatter.format(Integer.parseInt(temp)) + " KWH");
                        nodeee.setTextContent(tokenFormat.trim());
                        map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                        //System.out.println("token "+nodeee.getTextContent());
                        //nodeee.setTextContent(mc.getMerchantNameByCode(nodeee.getTextContent()));
                    } else if (flagErr == 1  && nodeee.getNodeName().equalsIgnoreCase("desc")) {
                       
                    }


                    if (flagErr == 1
                            && (nodeee.getNodeName().equalsIgnoreCase("rc")
                            || nodeee.getNodeName().equalsIgnoreCase("code")
                            || nodeee.getNodeName().equalsIgnoreCase("desc"))) {
                        //detailError = String.format("%-50s", titleMap.get(nodeee.getNodeName())) + " : " + nodeee.getTextContent() + "\n";
                        //logger.info("RC TIDAK TERDAFTAR 2 : " + map.get("rc"));
                        detailError = map.get("rc_format");//nodeee.getTextContent();
                         //System.out.println("detailError "+detailError);
                    } else if (nodeee.getNodeName().equalsIgnoreCase("material_number")
                            || nodeee.getNodeName().equalsIgnoreCase("admin_charge")
                            || nodeee.getNodeName().equalsIgnoreCase("subscriber_id")
                            || nodeee.getNodeName().equalsIgnoreCase("meterai")
                            || nodeee.getNodeName().equalsIgnoreCase("subcriber_name")
                            || nodeee.getNodeName().equalsIgnoreCase("ppn")
                            || nodeee.getNodeName().equalsIgnoreCase("ppj")
                            || nodeee.getNodeName().equalsIgnoreCase("switcher_refno")
                            || nodeee.getNodeName().equalsIgnoreCase("angsuran")
                            || nodeee.getNodeName().equalsIgnoreCase("amount")
                            || nodeee.getNodeName().equalsIgnoreCase("power_purchase")
                            || nodeee.getNodeName().equalsIgnoreCase("jumlah_kwh")
                            || nodeee.getNodeName().equalsIgnoreCase("token")) {

                        //if(!nodeee.getNodeName().equalsIgnoreCase("power_purchase_unsold") && nodeee.getTextContent().equals("")){
                        detailResult += String.format("%-50s", titleMap.get(nodeee.getNodeName())) + " : " + nodeee.getTextContent() + "\n";
                        //}
                        
                        //System.out.println("detailResult "+detailResult);
                    }

              
                    if (temp != "") {
                        nodeee.setTextContent(temp);
                        //System.out.println("tmp "+nodeee.getTextContent());
                    }
                    map.put(nodeee.getNodeName(), nodeee.getTextContent());
                    logger.info(nodeee.getNodeName() + " -->> " + nodeee.getTextContent());
                    //map.put(nodeee.getNodeName(), nodeee.getTextContent());
                }
                nodeee = nodeee.getNextSibling();
            }
            //System.out.println("flag "+flagErr);
            //System.out.println("prepaid 4");
      
            logger.info("flag : " + flagErr);
            if (flagErr == 0) {
                //System.out.println("masuk if");
                //String printingFormat = "\n\n" + setPrePaidPaymentPrinting2(map, reprintInfo);
                String printingFormat = "\n\n" + setPrePaidPaymentPrinting3(map, reprintInfo);
                printingFormatPrepaid = printingFormat;
                
                System.out.println(printingFormat);
                logger.info(printingFormatPrepaid);
                //System.out.println(printingFormat);
                detailResult = printingFormatPrepaid;
                return detailResult;
                //return printingFormat;
            } else {
                //System.out.println("masuk else ");
                //note: gunakan ruang untuk set pesan advice

//                String datetimeX = "";
//                datetimeX = map.get("datetime").toUpperCase().replaceAll("[^\\w\\s]", "");
//                datetimeX = datetimeX.substring(0, 4) + datetimeX.substring(9);
//
//                String pesanAdvice = map.get("bit48")
//                        + "." + map.get("bit62")
//                        + "." + map.get("stan")
//                        + "." + datetimeX;
//
//                txtInquiryResponse.setText(pesanAdvice);
//
//                System.out.println(pesanAdvice);
                return detailError;
            }

            //String printingFormat = setPrePaidPaymentPrinting(map);
            //return printingFormat;
            //setResponse(map);
            //return map;
        } catch (Exception ex) {
            ex.printStackTrace();
            //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public String getPaymentPostPaidResponse(String response, String reprintInfo) {
        String printingFormatPostpaid = "";
        //logger.info("Response : " + oi);
        logger.info("=====================postpaid payment=======================");
        Document doc;
        Map<String, String> titleMap = new HashMap<String, String>();
        titleMap.put("amount", "Tagihan");
        //titleMap.put("stan", "stan"); //hapus
        titleMap.put("datetime", "Tanggal");
        titleMap.put("merchant_code", "Kode Merchant");
        titleMap.put("bank_code", "Kode Bank");
        titleMap.put("rc", "Pesan Balasan");
        //titleMap.put("terminal_id", "ID Terminal"); //hapus
        titleMap.put("subscriber_id", "ID Pelanggan");
        titleMap.put("bill_status", "Tagihan harus dibayarkan");
        titleMap.put("outstanding_bill", "Sisa Tunggakan");
        titleMap.put("switcher_refno", "switcher refno");
        titleMap.put("subscriber_name", "Nama Pelanggan");
        titleMap.put("service_unit", "Service Unit");
        titleMap.put("service_unit_phone", "Service Unit Phone");//service_unit_phone
        titleMap.put("subscriber_segmentation", "Golongan Pelanggan");
        titleMap.put("power", "Daya");
        titleMap.put("admin_charge", "Biaya Administrasi");

        titleMap.put("bill_period", "Periode Tunggakan");
        titleMap.put("due_date", "Jatuh Tempo");
        titleMap.put("meter_read_date", "Tanggal Baca Meteran");
        titleMap.put("total_electricity_bill", "Total Tunggakan");
        titleMap.put("incentive", "Insentif");
        titleMap.put("value_added_tax", "PPN");
        titleMap.put("penalty_fee", "Penalti");
        titleMap.put("previous_meter_reading1", "Meteran Sebelumnya 1");
        titleMap.put("current_meter_reading1", "Meteran Saat Ini 1");
        titleMap.put("previous_meter_reading2", "Meteran Sebelumnya 2");
        titleMap.put("current_meter_reading2", "Meteran Saat Ini 2");
        titleMap.put("previous_meter_reading3", "Meteran Sebelumnya 3");
        titleMap.put("current_meter_reading3", "Meteran Saat Ini 3");
        titleMap.put("blth_summary", "Bulan Tahun");
        titleMap.put("stand_meter_summary", "Stan Meter");

        titleMap.put("code", "Pesan Balasan");
        //titleMap.put("code", "Pesan Balasan");
        titleMap.put("desc", "Keterangan");

        titleMap.put("info_text", "info");

        map.put("code", "0000");

        try {
            byte[] theByteArray = response.getBytes(); //InputStream x =
            //String oi = new String(responseBody);
            //logger.info("Response : " + oi);
            ByteArrayInputStream instream = new ByteArrayInputStream(theByteArray);
            //String parseResult = formatResponse(instream);
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            doc = docBuilder.parse(instream);
            doc.getDocumentElement().normalize();
            /*
             NodeList nodeLst = doc.getElementsByTagName("response");
             Node fstNode = nodeLst.item(0);
             Element fstElmnt = (Element) fstNode;
             */
            String detailResult = "";
            int flagErr = 0;
            String detailError = "";
            //Map<String, String> map = new HashMap<String, String>();
            boolean child = false;
            //NumberFormat formatter = new DecimalFormat("##,000.00");

            PostpaidResponseCode prc = new PostpaidResponseCode();
            MerchantCode mc = new MerchantCode();
            Node nodeee = doc.getElementsByTagName("response").item(0).getFirstChild();
            while (nodeee != null) {
                String temp = "";
                if (nodeee.getNodeName() != "bills") {
                    //logger.info(nodeee.getNodeName() + " => " + nodeee.getTextContent());
                    if (titleMap.get(nodeee.getNodeName()) != null) {
                        if (nodeee.getNodeName() == "datetime") {
                            if (nodeee.getTextContent().length() < 14) {
                                DateFormat dateFormat = new SimpleDateFormat("yyyy");
                                Calendar currentDate = Calendar.getInstance();
                                nodeee.setTextContent(nodeee.getTextContent() + "" + dateFormat.format(currentDate.getTime()));
                            }

                            String tahun = nodeee.getTextContent().substring(0, 4);
                            String bulan = nodeee.getTextContent().substring(4, 6);
                            String tanggal = nodeee.getTextContent().substring(6, 8);
                            String jam = nodeee.getTextContent().substring(8, 10);
                            String menit = nodeee.getTextContent().substring(10, 12);
                            String detik = nodeee.getTextContent().substring(12, 14);
                            //DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            nodeee.setTextContent(tanggal + "-" + bulan + "-" + tahun + " " + jam + ":" + menit + ":" + detik);
                        } else if (nodeee.getNodeName() == "amount"
                                || nodeee.getNodeName() == "admin_charge") {
                            temp = nodeee.getTextContent();
                            nodeee.setTextContent("Rp. " + formatter2.format(Double.parseDouble(temp)));
                            map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                        } else if (nodeee.getNodeName() == "power") {
                            //nodeee.setTextContent(nodeee.getTextContent() + " KWH");
                            temp = nodeee.getTextContent();
                            nodeee.setTextContent("" + Double.parseDouble(temp) + " VA");
                            map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                        } else if (nodeee.getNodeName() == "rc" || nodeee.getNodeName() == "code") {
                            //temp = nodeee.getTextContent().trim();
                            temp = nodeee.getTextContent();
                            //temp = "0088";
                            //nodeee.setTextContent(formatter.format(Integer.parseInt(temp)) + " KWH");
                            //nodeee.setTextContent(prc.getPurchaseMsgResponseName(temp));
//                            nodeee.setTextContent(prc.responseParser(temp));
                            nodeee.setTextContent(getRcDescription("post", temp));
                            logger.info(nodeee.getNodeName() + " - " + temp + " - " + nodeee.getTextContent());
                            map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                            if (!temp.equalsIgnoreCase("0000")) {
                                flagErr = 1;
                            }

                            if (nodeee.getNodeName() == "code" && nodeee.getTextContent() != null) {
                                map.put("rc", temp);
                                map.put("rc_format", nodeee.getTextContent());
                            }

                            //nodeee.setTextContent(prc.getInquiryMsgResponseName(nodeee.getTextContent()));
                        } else if (nodeee.getNodeName() == "merchant_code") {
                            nodeee.setTextContent(mc.getMerchantNameByCode(nodeee.getTextContent()));
                        } else if (flagErr == 1 && nodeee.getNodeName().equalsIgnoreCase("desc")
                                && map.get("rc_format").equalsIgnoreCase("")) {
                            //logger.info("RC TIDAK TERDAFTAR : " + map.get("rc"));
//                            map.put("rc_format", nodeee.getTextContent());
                        }
                        logger.info(nodeee.getNodeName() + " => " + nodeee.getTextContent());
////                        if (flagErr == 1 && (nodeee.getNodeName() == "rc" || nodeee.getNodeName() == "code")) {
////                            //detailError = String.format("%-50s", titleMap.get(nodeee.getNodeName())) + " : " + nodeee.getTextContent() + "\n";
////                            detailError = nodeee.getTextContent();
////                        }
////
////                        detailResult += String.format("%-50s", titleMap.get(nodeee.getNodeName())) + " : " + nodeee.getTextContent() + "\n";
////                        if (temp != "") {
////                            nodeee.setTextContent(temp);
////                        }
                        if (flagErr == 1 && (nodeee.getNodeName() == "rc" || nodeee.getNodeName() == "code" || nodeee.getNodeName() == "desc")) {
                            //logger.info("==>masup2");
                            //detailError = String.format("%-50s", titleMap.get(nodeee.getNodeName())) + " : " + nodeee.getTextContent() + "\n";
                            detailError = map.get("rc_format");//nodeee.getTextContent();
                        } else if (nodeee.getNodeName().equalsIgnoreCase("subscriber_id")
                                || nodeee.getNodeName().equalsIgnoreCase("blth_summary")
                                || nodeee.getNodeName().equalsIgnoreCase("subscriber_name")
                                || nodeee.getNodeName().equalsIgnoreCase("stand_meter_summary")
                                || nodeee.getNodeName().equalsIgnoreCase("subscriber_segmentation")
                                || nodeee.getNodeName().equalsIgnoreCase("power")
                                || nodeee.getNodeName().equalsIgnoreCase("total_pln_bill")
                                || nodeee.getNodeName().equalsIgnoreCase("admin_charge")
                                || nodeee.getNodeName().equalsIgnoreCase("amount")
                                || nodeee.getNodeName().equalsIgnoreCase("switcher_refno") //                                || nodeee.getNodeName().equalsIgnoreCase("outstanding_bill")
                                ) {

                            if (nodeee.getNodeName().equalsIgnoreCase("blth_summary")) {
                                nodeee.setTextContent(nodeee.getTextContent().replace(", ", ","));
                            }

                            detailResult += String.format("%-50s", titleMap.get(nodeee.getNodeName())) + " : " + nodeee.getTextContent() + "\n";
                        }

                        if (temp != "") {
                            nodeee.setTextContent(temp);
                        }

                        map.put(nodeee.getNodeName(), nodeee.getTextContent());
                    }
                }
//                logger.info("blar");
                nodeee = nodeee.getNextSibling();
//                else {
//                    nodeee = null;
//                }
            }

            NodeList bills = doc.getElementsByTagName("bill");
            String temp = "";
            double total_pln_bill = 0;
            for (int s1 = 0; s1 < bills.getLength(); s1++) {
                nodeee = bills.item(s1).getFirstChild();
                //detailResult += "\nTunggakan ke : " + (s1 + 1) + "\n";
                while (nodeee != null) {
                    temp = "";
                    if (titleMap.get(nodeee.getNodeName()) != null) {
                        if (nodeee.getNodeName() == "due_date") {
                            String tahun = nodeee.getTextContent().substring(0, 4);
                            String bulan = nodeee.getTextContent().substring(4, 6);
                            String tanggal = nodeee.getTextContent().substring(6, 8);
                            //DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            nodeee.setTextContent(tanggal + "-" + bulan + "-" + tahun);
                        } else if (nodeee.getNodeName() == "bill_period") {
                            String tahun = nodeee.getTextContent().substring(0, 4);
                            String bulan = nodeee.getTextContent().substring(4, 6);
                            //DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            nodeee.setTextContent(bulan + "-" + tahun);
                        } else if (nodeee.getNodeName() == "total_electricity_bill"
                                || nodeee.getNodeName() == "value_added_tax"
                                || nodeee.getNodeName() == "penalty_fee") {
                            temp = nodeee.getTextContent();
                            //nodeee.setTextContent(formatter.format(Integer.parseInt(temp)) + " KWH");
                            //total_pln_bill += Integer.parseInt(temp);
                            total_pln_bill += Double.parseDouble(temp);
                            nodeee.setTextContent("Rp. " + formatter.format(Double.parseDouble(nodeee.getTextContent())));
                            map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                        }

                        //detailResult += "     " + String.format("%-50s", titleMap.get(nodeee.getNodeName())) + " : " + nodeee.getTextContent() + "\n";
                        if (temp != "") {
                            nodeee.setTextContent(temp);
                        }
                        map.put(nodeee.getNodeName() + "_" + s1, nodeee.getTextContent());
                    }
                    nodeee = nodeee.getNextSibling();
                }
            }
            map.put("bill_length", "" + bills.getLength());

            //if (map.get("rc").equalsIgnoreCase("0000")) {
            if (flagErr == 0) {
//                logger.info("masuk atas");
                String printingFormat = "\n\n" + setPostPaidPaymentPrinting2(map, reprintInfo);
//                String printingFormat = "\n\n" + setPostPaidPaymentPrinting3(map, reprintInfo);
                printingFormatPostpaid = printingFormat.replace("null", "");
                logger.info(detailResult);
                logger.info(printingFormat);
                System.out.println(printingFormat);
                //return printingFormat;
                if (response.toLowerCase().contains("reversal")) {
                    logger.info("TRANSAKSI MASUK REVERSAL");
                    detailResult = "PEMBAYARAN DIGAGALKAN\n"
                            + "REVERSAL BERHASIL\n"
                            + "SILAHKAN ULANGI KEMBALI\n"
                            + detailResult;
                } else {
                    logger.info("TRANSAKSI TIDAK MASUK REVERSAL");
                }
                detailResult = printingFormatPostpaid;
                return detailResult;
            } else {
                logger.info("ERRORNYA : " + detailError);
                if (detailError.equalsIgnoreCase("")) {
                    detailError = map.get("desc");
                }
                return detailError;
            }

            //logger.info(detailResult);
            //setResponse(map);
            //return map;
        } catch (SAXException ex) {
            //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        } //data end
        catch (ParserConfigurationException ex) {
            //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public String getPaymentNonTagListResponse(String response, String reprintInfo, String createDate) {
        //System.out.println("Response : " + response);
//        //System.out.println("PARSING NONTAGLIS PAYMENT RESPONSE");
        String printingFormatNontaglist = "";
        Map<String, String> map = new HashMap<String, String>();

        response = response.replace("&", "dan");
        Document doc;
        Map<String, String> titleMap = new HashMap<String, String>();
        titleMap.put("transaction_name", "TRANSAKSI");
        titleMap.put("registration_no", "NO. REGISTRASI");
        titleMap.put("registration_date", "TGL REGISTRASI");
        titleMap.put("subscriber_name", "NAMA");
        titleMap.put("subscriber_id", "IDPEL");
        titleMap.put("pln_bill_minor", "BIAYA PLN MINOR");
        titleMap.put("pln_bill", "BIAYA PLN");
        titleMap.put("switching_refno", "NO REF");
        titleMap.put("admin_charge_minor", "ADMIN CA");
        titleMap.put("admin_charge", "ADMIN CA");
        titleMap.put("amount_minor", "TOTAL BAYAR");
        titleMap.put("amount", "TOTAL BAYAR");
        titleMap.put("service_unit_phone", "TLP. SERVICE UNIT");
        titleMap.put("msg_type", "");
        titleMap.put("rc", "KETERANGAN");
        titleMap.put("desc", "KETERANGAN");
        //titleMap.put("rc", "rc");

////////
////////
////////        titleMap.put("amount", "Tagihan");
////////        titleMap.put("stan", "stan"); //hapus
////////        titleMap.put("datetime", "Tanggal");
////////        titleMap.put("merchant_code", "Kode Merchant");
////////        titleMap.put("bank_code", "Kode Bank");
////////        titleMap.put("rc", "Pesan Balasan");
////////        titleMap.put("terminal_id", "ID Terminal"); //hapus
////////
////////        titleMap.put("transaction_name", "Nama Trans."); //hapus
////////        titleMap.put("registration_date", "Tanggal Registrasi"); //hapus
////////        titleMap.put("expiration_date", "Tanggal Expired"); //hapus
////////
////////        titleMap.put("subscriber_id", "ID Pelanggan");
////////        titleMap.put("subscriber_name", "Nama Pelanggan");
////////
////////        titleMap.put("pln_refno", "PLN Ref No");
////////        titleMap.put("receipt_refno", "Receipt Ref No");
////////
////////        titleMap.put("service_unit", "Service Unit");
////////        titleMap.put("service_unit_address", "Alamat Service Unit");
////////        titleMap.put("service_unit_phone", "Tlp. Service Unit");
////////
////////        titleMap.put("total_transaction_amount", "Total Transaction Amount");
////////        titleMap.put("rp_tag", "Rp Tag");
////////        titleMap.put("admin_charge", "Biaya Admin");
////////        titleMap.put("bill_component_amount", "Tagihan Komponen");
        titleMap.put("info_text", "Info");

        titleMap.put("code", "Kode");
        //titleMap.put("desc", "Keterangan");

        map.put("code", "0000");
//        map.put("info_text", "Informasi Hubungi Call Center 123\natau\nHub. PLN Terdekat");
        map.put("info_text", "Informasi Hubungi Call Center 123 Atau Hub PLN Terdekat");

        try {
            byte[] theByteArray = response.getBytes(); //InputStream x =
            //String oi = new String(responseBody);
            ////System.out.println("Response : " + oi);
            ByteArrayInputStream instream = new ByteArrayInputStream(theByteArray);
            //String parseResult = formatResponse(instream);
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            doc = docBuilder.parse(instream);
            doc.getDocumentElement().normalize();
            /*
             NodeList nodeLst = doc.getElementsByTagName("response");
             Node fstNode = nodeLst.item(0);
             Element fstElmnt = (Element) fstNode;
             */
            String detailResult = "";
            String detailError = "";
            //Map<String, String> map = new HashMap<String, String>();
            boolean child = false;
            //NumberFormat formatter = new DecimalFormat("##,000.00");

            //PostpaidResponseCode prc = new PostpaidResponseCode();
            NonTagListResponseCode ntlrc = new NonTagListResponseCode();
            int flagErr = 0;
            MerchantCode mc = new MerchantCode();
            Node nodeee = doc.getElementsByTagName("response").item(0).getFirstChild();
            while (nodeee != null) {
                String temp = "";
                //if (nodeee.getNodeName() != "bills") {
                ////System.out.println(nodeee.getNodeName() + " => " + nodeee.getTextContent());
                System.out.print(nodeee.getNodeName() + " ==> ");
                if (titleMap.get(nodeee.getNodeName()) != null) {
//                    //System.out.println(titleMap.get(nodeee.getNodeName()));
                    if (nodeee.getNodeName() == "datetime" || nodeee.getNodeName() == "registration_date") {

                        String tahun = nodeee.getTextContent().substring(2, 4);
                        String bulan = parseBulan(nodeee.getTextContent().substring(4, 6));
                        String tanggal = nodeee.getTextContent().substring(6);

                        //String jam = nodeee.getTextContent().substring(8, 10);
                        //String menit = nodeee.getTextContent().substring(10, 12);
                        //String detik = nodeee.getTextContent().substring(12, 14);
                        //DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        //nodeee.setTextContent(tanggal + "-" + bulan + "-" + tahun + " " + jam + ":" + menit + ":" + detik);
                        temp = nodeee.getTextContent();
                        //nodeee.setTextContent(tanggal + "-" + bulan + "-" + tahun + " " + jam + ":" + menit + ":" + detik);
                        nodeee.setTextContent(tanggal + bulan + tahun);
                        map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                    } else if (nodeee.getNodeName() == "amount"
                            || nodeee.getNodeName() == "pln_bill"
                            || nodeee.getNodeName() == "admin_charge"
                            || nodeee.getNodeName() == "rptag"
                            || nodeee.getNodeName() == "total_transaction_amount"
                            || nodeee.getNodeName() == "bill_component_amount") {

                        try {
                            /*
                             if (nodeee.getNodeName() == "admin_charge") {
                             int length = nodeee.getTextContent().length();
                             String adminCharge = nodeee.getTextContent().substring(0, (length - 2));
                             nodeee.setTextContent(adminCharge);
                             }
                             *
                             */
                            //nonTagListResponse = "<?xml version='1.0' encoding='UTF-8'?><response><transaction_name>PEMASANGAN LISTRIK BARU  </transaction_name><registration_no>5333113000027</registration_no><registration_date>20120824</registration_date><subscriber_name>ABDUL KADIR NANANANAN    </subscriber_name><subscriber_id>000000000000</subscriber_id><pln_bill_minor>2</pln_bill_minor><pln_bill>00000000001234500</pln_bill><switching_refno>02337241E965A857029320014O713272</switching_refno><service_unit>53121</service_unit><service_unit_phone>111111111111111</service_unit_phone><admin_charge_minor>2</admin_charge_minor><admin_charge>0000160000</admin_charge><amount_minor>2</amount_minor><amount>00000000001236100</amount><rc>0000</rc><stan>000001172430</stan><info_text>Hemat Listrik dan Bebas Narkoba</info_text></response>";

                            String charge = "";
                            if (!nodeee.getNodeName().equalsIgnoreCase("amount") || !nodeee.getNodeName().equalsIgnoreCase("pln_bill")) {
                                String amount = nodeee.getTextContent();
                                int length = amount.length();
                                String minor = map.get(nodeee.getNodeName() + "_minor");

                                //double tempMoney = Double.parseDouble(nodeee.getTextContent());
                                String moneyStr = "" + amount;

//                                //System.out.println(" >> moneystr :: " + amount);
                                moneyStr = moneyStr.replace(".0", "");

//                                //System.out.println(" >> moneystr2 :: " + amount);
                                int lengthNoMinor = length - Integer.parseInt(minor);
                                charge = Long.parseLong(moneyStr.substring(0, lengthNoMinor)) + "."
                                        + moneyStr.substring(lengthNoMinor);

                                /*
                                 //System.out.println("masuk atas");
                                 double tempMoney = Double.parseDouble(nodeee.getTextContent());
                                 String moneyStr = "" + tempMoney;
                                 moneyStr = moneyStr.replace(".0", "");
                                 int length = moneyStr.length();

                                 String minor = map.get(nodeee.getNodeName() + "_minor");
                                 int lengthNoMinor = length - Integer.parseInt(minor);

                                 charge = moneyStr.substring(0, lengthNoMinor) + "." + moneyStr.substring(lengthNoMinor);
                                 ////System.out.println("pln bill : "+moneyStr+" tempMoney : "+tempMoney+ " length : "+length+ " lengthNoMinor : "+ lengthNoMinor+" charge : "+charge );

                                 */
                            } else {
//                                //System.out.println("masuk bawah");
                                double tempMoney = Double.parseDouble(nodeee.getTextContent());
                                String moneyStr = "" + tempMoney;
                                charge = moneyStr;
                            }

//                            //System.out.println(nodeee.getNodeName() + " => " + charge);
                            //temp = nodeee.getTextContent();
                            temp = charge;
                            nodeee.setTextContent("Rp. " + formatter.format(Double.parseDouble(temp)));
                        } catch (Exception e) {
                            nodeee.setTextContent("Rp. " + formatter.format(0));
                        }
                        map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                        //nodeee.setTextContent(temp);
                    } else if (nodeee.getNodeName() == "power") {
                        //nodeee.setTextContent(nodeee.getTextContent() + " KWH");
                        temp = nodeee.getTextContent();
                        nodeee.setTextContent(formatter.format(Integer.parseInt(temp)) + " VA");
                        map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                    } else if (nodeee.getNodeName() == "rc" || nodeee.getNodeName() == "code") {
////                        //nodeee.setTextContent(prc.getInquiryMsgResponseName(nodeee.getTextContent()));
////                        temp = nodeee.getTextContent();
////                        //temp = "0088";
////                        //nodeee.setTextContent(ntlrc.getPurchaseMsgResponseName(temp));
////                        nodeee.setTextContent(ntlrc.responseParser(temp));
////                        map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                        //map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());
                        temp = nodeee.getTextContent();
                        //temp = "0088";
                        //nodeee.setTextContent(ntlrc.getInquiryMsgResponseName(temp));
//                        nodeee.setTextContent(ntlrc.responseParser(temp));
                        nodeee.setTextContent(getRcDescription("non", temp));
                        map.put(nodeee.getNodeName() + "_format", nodeee.getTextContent());

                        if (!temp.equalsIgnoreCase("0000")) {
                            flagErr = 1;
                        }

                        if (nodeee.getNodeName() == "code" && nodeee.getTextContent() != null) {
                            System.out.println("masuk code");
                            map.put("rc_format", nodeee.getTextContent());
                            map.put("rc", temp);
                        }

                    } else if (nodeee.getNodeName() == "merchant_code") {
                        nodeee.setTextContent(mc.getMerchantNameByCode(nodeee.getTextContent()));
                    } else if (flagErr == 1 && nodeee.getNodeName().equalsIgnoreCase("desc") && map.get("rc_format").equalsIgnoreCase("")) {
                        ////System.out.println("RC TIDAK TERDAFTAR : " + map.get("rc"));
//                        map.put("rc_format", nodeee.getTextContent());
                    }

////                    if (nodeee.getNodeName() == "rc" && temp != "0000") {
////                        detailResult += String.format("%-50s", titleMap.get(nodeee.getNodeName())) + " : " + nodeee.getTextContent() + "\n";
////                    }
                    //if (nodeee.getNodeName() == "rc" && temp != "0000") {
                    //if (nodeee.getNodeName() == "rc" && temp != "0000") {
//                        detailResult += String.format("%-50s", titleMap.get(nodeee.getNodeName())) + " : " + nodeee.getTextContent() + "\n";
                    if (flagErr == 1 && (nodeee.getNodeName() != "rc" || nodeee.getNodeName() != "code" || nodeee.getNodeName() != "desc")) {
//                        //System.out.println(">>>>jalankan");
                        //detailError = nodeee.getTextContent() + "\n";
                        detailError = map.get("rc_format") + "\n";
                        //detailError = nodeee.getTextContent();
                    } else if (nodeee.getNodeName().equalsIgnoreCase("transaction_name")
                            || nodeee.getNodeName().equalsIgnoreCase("registration_no")
                            || nodeee.getNodeName().equalsIgnoreCase("registration_date")
                            || nodeee.getNodeName().equalsIgnoreCase("subscriber_name")
                            || nodeee.getNodeName().equalsIgnoreCase("subscriber_id")
                            || nodeee.getNodeName().equalsIgnoreCase("pln_bill")
                            || nodeee.getNodeName().equalsIgnoreCase("amount")
                            || nodeee.getNodeName().equalsIgnoreCase("total_transaction_amount")
                            || nodeee.getNodeName().equalsIgnoreCase("admin_charge")
                            || nodeee.getNodeName().equalsIgnoreCase("switching_refno")) {

                        //System.out.println(">>" + nodeee.getNodeName());
                        detailResult += String.format("%-50s", titleMap.get(nodeee.getNodeName())) + " : " + nodeee.getTextContent() + "\n";
                    }
                    //}
                    if (temp != "") {
                        nodeee.setTextContent(temp);
                    }
                    map.put(nodeee.getNodeName(), nodeee.getTextContent());
                }
                nodeee = nodeee.getNextSibling();
                //}
                //else {
                //    nodeee = null;
                //}
            }

//            //System.out.println("==neh");
//            //System.out.println("rc : " + map.get("rc") + "  ==> " + map.get("rc_format"));
//            //System.out.println("=========");
            //if (map.get("rc").equalsIgnoreCase("0000") || map.get("rc").equalsIgnoreCase("0012")) {
            if (map.get("rc").equalsIgnoreCase("0000")) {
//                //System.out.println("====payment printing format====");
                //String printingFormat = setNonTagListPaymentPrinting(map);               
                String printingFormat = "\n\n" + setNonTagListPaymentPrinting2(map, reprintInfo, createDate);
                System.out.println(printingFormat);
                printingFormatNontaglist = printingFormat;
                detailResult = printingFormatNontaglist;
//                //System.out.println(printingFormat);
                ////System.out.println(printingFormat);
                //return printingFormat;
//                //System.out.println(response);
                try {
                    if (response.toLowerCase().contains("reversal")) {
//                        //System.out.println("TRANSAKSI MASUK REVERSAL");
                        detailResult = "PEMBAYARAN DIGAGALKAN\n"
                                + "REVERSAL BERHASIL\n"
                                + "SILAHKAN ULANGI KEMBALI\n"
                                + detailResult;
                    } else {
//                        //System.out.println("TRANSAKSI TIDAK MASUK REVERSAL");
                    }
                } catch (Exception e) {
                }
//                //System.out.println(detailResult);
                return detailResult;
                ////System.out.println(detailResult);
                //return detailResult;
                //setResponse(map);
                //return map;
            } else {
//                //System.out.println("====payment error format====");
                //return detailResult;
                return detailError;
            }
        } catch (Exception ex) {
            //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        /*catch (IOException ex) {
         //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
         } //data end
         catch (ParserConfigurationException ex) {
         //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
         }
         *
         */
        return "";
    }
    
    public String getTelkomResponse(String response, String reprintInfo, String createDate) {
        String printingFormatTelkom = "";
        Map<String, String> map = new HashMap<String, String>();

        //System.out.println("original file "+response);
        response = response.replace("&", "dan");
       //System.out.println("after replace "+response.getBytes());
        Document doc;
        
        Map<String, String> titleMap = new HashMap<String, String>();
        titleMap.put("produk", "PRODUK");
        titleMap.put("idpelanggan", "NO REK");
        titleMap.put("namapelanggan", "NAMA PELANGGAN");
        titleMap.put("bulan_thn", "BLN THN");
        titleMap.put("noreference", "NO REFF");
        titleMap.put("status", "STATUS");
        titleMap.put("rc_description", "RC DESCRIPTION");
        titleMap.put("ACK", "ACK");
        titleMap.put("jumlah_lembar", "JML LEMBAR");
        titleMap.put("jumlahtagihan", "RP TAG");
        titleMap.put("jumlahadm", "ADMIN CA");
        titleMap.put("jumlahbayar", "TOTAL TAGIHAN");
        titleMap.put("pesan", "PESAN");
        titleMap.put("rc", "RC");
        
        map.put("code", "0000");

        try {
            byte[] theByteArray = response.getBytes(); //InputStream x =
            ByteArrayInputStream instream = new ByteArrayInputStream(theByteArray);

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            doc = docBuilder.parse(instream);
            doc.getDocumentElement().normalize();
        
            String detailResult = "";
            String detailError = "";
                 
            
            int flagErr = 0;
            MerchantCode mc = new MerchantCode();
            Node nodeee = doc.getElementsByTagName("response").item(0).getFirstChild();
            while (nodeee != null) {
                String temp = "";
                
                System.out.print(nodeee.getNodeName() + " ==> ");
                if (titleMap.get(nodeee.getNodeName()) != null) {
                    
                    map.put(nodeee.getNodeName(), nodeee.getTextContent());
                    
                }
                
                nodeee = nodeee.getNextSibling();
            }    
                
            if (map.get("rc").equalsIgnoreCase("0000")) {
                
                //System.out.println("masuk");
                String printingFormat = "\n\n" + setTelkomPaymentPrinting(map, "0",createDate);
                
                
                System.out.println(printingFormat);
                printingFormatTelkom = printingFormat;
                detailResult = printingFormatTelkom;

                
                return detailResult;
               
            } else {
             
                return detailError;
            }
            
        } catch (Exception ex) {
            //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        return "";
    }
    
    public String getPdamResponse(String response, String reprintInfo, String createDate) {
        String printingFormatPDAM = "";
        Map<String, String> map = new HashMap<String, String>();

        //System.out.println("original file "+response);
        response = response.replace("&", "dan");
       //System.out.println("after replace "+response.getBytes());
        Document doc;
        
        Map<String, String> titleMap = new HashMap<String, String>();
        titleMap.put("idpel", "NO REK");
        titleMap.put("name", "NAMA");
        titleMap.put("blth", "BLN/THN");
        titleMap.put("waktu_lunas", "TGL LUNAS");
        titleMap.put("switching_ref", "NO REF");
        titleMap.put("rp_tag", "RP TAG");
        titleMap.put("biaya_admin", "ADMIN CA");
        titleMap.put("amount", "TOTAL");
        titleMap.put("reffno", "reffno");
        titleMap.put("tgl_lunas", "tgl lunas");
        titleMap.put("rc", "RC");
        
        map.put("code", "0000");

        try {
            byte[] theByteArray = response.getBytes(); //InputStream x =
            ByteArrayInputStream instream = new ByteArrayInputStream(theByteArray);

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            doc = docBuilder.parse(instream);
            doc.getDocumentElement().normalize();
        
            String detailResult = "";
            String detailError = "";
                 
            
            int flagErr = 0;
            MerchantCode mc = new MerchantCode();
            Node nodeee = doc.getElementsByTagName("response").item(0).getFirstChild();
            while (nodeee != null) {
                String temp = "";
                
                System.out.print(nodeee.getNodeName() + " ==> ");
                if (titleMap.get(nodeee.getNodeName()) != null) {
                    
                    map.put(nodeee.getNodeName(), nodeee.getTextContent());
                    
                }
                
                nodeee = nodeee.getNextSibling();
            }    
                
            if (map.get("rc").equalsIgnoreCase("00")) {
                
                //System.out.println("masuk");
                String printingFormat = "\n\n" + setPdamPaymentPrinting(map, "0",createDate);
                
                
                System.out.println(printingFormat);
                printingFormatPDAM = printingFormat;
                detailResult = printingFormatPDAM;

                
                return detailResult;
               
            } else {
             
                return detailError;
            }
            
        } catch (Exception ex) {
            //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        return "";
    }
    
    public String getBpjsResponse(String response, String reprintInfo, String createDate) {
        String printingFormatBPJS = "";
        Map<String, String> map = new HashMap<String, String>();

        //System.out.println("original file "+response);
        response = response.replace("&", "dan");
       //System.out.println("after replace "+response.getBytes());
        Document doc;
        
        Map<String, String> titleMap = new HashMap<String, String>();
        titleMap.put("date_time", "TANGGAL;");
        titleMap.put("no_va", "NO VA");
        titleMap.put("nama", "NAMA");
        titleMap.put("sisa", "TGL LUNAS");
        titleMap.put("jumlah_bulan", "PERIODE");
        titleMap.put("amount", "TOTAL");
        titleMap.put("biaya_admin", "ADMIN BANK");
        titleMap.put("biaya_premi", "JUMLAH TAGIHAN");
        titleMap.put("info_text", "info text");
        titleMap.put("rc", "RC");
        titleMap.put("reffno", "reffno");
        titleMap.put("tgl_lunas", "tgl_lunas");
        
//        map.put("code", "0000");

        try {
            byte[] theByteArray = response.getBytes(); //InputStream x =
            ByteArrayInputStream instream = new ByteArrayInputStream(theByteArray);

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            doc = docBuilder.parse(instream);
            doc.getDocumentElement().normalize();
        
            String detailResult = "";
            String detailError = "";
                 
            
            int flagErr = 0;
            MerchantCode mc = new MerchantCode();
            Node nodeee = doc.getElementsByTagName("response").item(0).getFirstChild();
            while (nodeee != null) {
                String temp = "";
                
                System.out.print(nodeee.getNodeName() + " ==> ");
                if (titleMap.get(nodeee.getNodeName()) != null) {
                    
                    
                    map.put(nodeee.getNodeName(), nodeee.getTextContent());
                    
                }
                
                nodeee = nodeee.getNextSibling();
            }
            
            //System.out.println("sizenya "+map.size());
                
            if (map.get("rc").equalsIgnoreCase("0000")) {
                
                //System.out.println("masuk");
                String printingFormat = "\n\n" + setBpjsPaymentPrinting(map, "0",createDate);
                
                
                System.out.println(printingFormat);
                printingFormatBPJS = printingFormat;
                detailResult = printingFormatBPJS;

                
                return detailResult;
               
            } else {
             
                return detailError;
            }
            
        } catch (Exception ex) {
            //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        return "";
    }
    
    public String getPulsaResponse(String response, String reprintInfo, String createDate) {
        String printingFormatPulsa = "";
        Map<String, String> map = new HashMap<String, String>();

        //System.out.println("original file "+response);
        response = response.replace("&", "dan");
       //System.out.println("after replace "+response.getBytes());
        Document doc;
        
        Map<String, String> titleMap = new HashMap<String, String>();
        titleMap.put("msisdn", "MSISDN");
        titleMap.put("stan", "TRX ID");
        titleMap.put("sn", "SN");        
        titleMap.put("code", "RC");
        titleMap.put("desc", "PRODUK");
        titleMap.put("product_type", "product_type");
        titleMap.put("produk", "produk");
        titleMap.put("amount", "amount");
        titleMap.put("price", "price");
        
        
        /*<?xml version="1.0" encoding="UTF-8" standalone="no"?>
        <response>
        <trx_id/>
        <stan>104803607</stan>
        <produk> TELKOMSEL HALO</produk>
        <msisdn> 08112705784( PT)</msisdn>
        <product_type>PULSA PASCABAYAR</product_type>
        <code>0000</code>
        <rc>0000</rc>
        <sn> 70539043005</sn>
        <price>329137</price>
        <amount>331637</amount>
        <saldo>0</saldo>
        <bill_count>1</bill_count>
        <bill_repeat_count>1</bill_repeat_count>
        <description> TELKOMSEL HALO</description>
        <info/>
        </response>  */

        try {
            byte[] theByteArray = response.getBytes(); //InputStream x =
            ByteArrayInputStream instream = new ByteArrayInputStream(theByteArray);

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            doc = docBuilder.parse(instream);
            doc.getDocumentElement().normalize();
        
            String detailResult = "";
            String detailError = "";
                 
            
            int flagErr = 0;
            MerchantCode mc = new MerchantCode();
            Node nodeee = doc.getElementsByTagName("response").item(0).getFirstChild();
            while (nodeee != null) {
                String temp = "";
                
                System.out.print(nodeee.getNodeName() + " ==> ");
                if (titleMap.get(nodeee.getNodeName()) != null) {
                    
                    
                    map.put(nodeee.getNodeName(), nodeee.getTextContent());
                    
                }
                
                nodeee = nodeee.getNextSibling();
            }
            
            
                
            if (map.get("code").equalsIgnoreCase("0000")) {
                String value = "PULSA PASCABAYAR";
                String value2 = "Pulsa Pascabayar";
                //System.out.println("masuk");
                if(map.get("product_type").equalsIgnoreCase(value) || map.get("product_type").equalsIgnoreCase(value2)){ 
                    String printingFormat = "\n\n" + setPulsaPascabayarPaymentPrinting(map, "0",createDate);
                    System.out.println(printingFormat);
                    printingFormatPulsa = printingFormat;
                }else{
                    String printingFormat = "\n\n" + setPulsaPaymentPrinting(map, "0",createDate);
                    System.out.println(printingFormat);
                    printingFormatPulsa = printingFormat;
                }          
          
                detailResult = printingFormatPulsa;

                
                return detailResult;
               
            } else {
             
                return detailError;
            }
            
        } catch (Exception ex) {
            
            //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        return "";
    }
        
    public String getGameResponse(String response, String reprintInfo, String createDate) {
        String printingFormatGame = "";
        Map<String, String> map = new HashMap<String, String>();

        //System.out.println("original file "+response);
        response = response.replace("&", "dan");
       //System.out.println("after replace "+response.getBytes());
        Document doc;
               
        Map<String, String> titleMap = new HashMap<String, String>();
        titleMap.put("trx_id", "TRX ID");
        titleMap.put("voucher_code", "VOUCHER");
        titleMap.put("hp", "HP");        
        titleMap.put("voucher_password", "PASSWORD");        
        titleMap.put("serial_number", "SN");        
        titleMap.put("code", "code");        
        titleMap.put("pin", "PIN");        
        titleMap.put("desc", "VOUCHER");        


        try {
            byte[] theByteArray = response.getBytes(); //InputStream x =
            ByteArrayInputStream instream = new ByteArrayInputStream(theByteArray);

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            doc = docBuilder.parse(instream);
            doc.getDocumentElement().normalize();
        
            String detailResult = "";
            String detailError = "";
                 
            
            int flagErr = 0;
            MerchantCode mc = new MerchantCode();
            Node nodeee = doc.getElementsByTagName("response").item(0).getFirstChild();
            while (nodeee != null) {
                String temp = "";
                
                System.out.print(nodeee.getNodeName() + " ==> ");
                if (titleMap.get(nodeee.getNodeName()) != null) {
                    
                    
                    map.put(nodeee.getNodeName(), nodeee.getTextContent());
                    
                }
                
                nodeee = nodeee.getNextSibling();
            }
            
            
                
            if (map.get("code").equalsIgnoreCase("0000")) {
                
                //System.out.println("masuk");
                String printingFormat = "\n\n" + setGamePaymentPrinting(map, "0",createDate);
                
                
                System.out.println(printingFormat);
                printingFormatGame = printingFormat;
                detailResult = printingFormatGame;

                
                return detailResult;
               
            } else {
             
                return detailError;
            }
            
        } catch (Exception ex) {
            //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        return "";
    }
    
    private String getMonthName(String substring) {
        String bulan = "";

        switch (substring) {
            case "01":
                bulan="JAN";
                break;
            case "02":
                bulan="FEB";
                break;
            case "03":
                bulan="MAR";
                break;
            case "04":
                bulan="APR";
                break;
            case "05":
                bulan="MEI";
                break;
            case "06":
                bulan="JUN";
                break;
            case "07":
                bulan="JUL";
                break;
            case "08":
                bulan="AUG";
                break;
            case "09":
                bulan="SEP";
                break;
            case "10":
                bulan="OKT";
                break;
            case "11":
                bulan="NOV";
                break;
            case "12":
                bulan="DES";
                break;
        }

        return bulan; //To change body of generated methods, choose Tools | Templates.
    }

   

}
