/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package huaweistruk;

import java.io.ByteArrayInputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 *
 * @author egateza
 */
public class TVUtils {
    NumberFormat formatter = new DecimalFormat("##,##0.00");
    public String getTvResponse(String product_name, String response, String reprintInfo, String createDate) {
        System.out.println("getTvResponse =============> ");
        String printingFormatMulti = "";
        Map<String, String> map = new HashMap<String, String>();

        //System.out.println("original file "+response);
        response = response.replace("&", "dan");
       //System.out.println("after replace "+response.getBytes());
        Document doc;
               
        Map<String, String> titleMap = new HashMap<String, String>();
        titleMap.put("product_type", "PRODUK");
        titleMap.put("id_pelanggan", "IDPEL");
        titleMap.put("nama_pelanggan", "NAMA");        
//        titleMap.put("angsuran", "ANGSURAN");        
        titleMap.put("admin_charge", "ADMIN");        
        titleMap.put("amount", "AMOUNT");              
        titleMap.put("rc", "rc");        
//        titleMap.put("due_date", "due_date");        
        titleMap.put("noreff_sw", "NO REF");        
        titleMap.put("rptag", "TAGIHAN");        

//        System.out.println("============> PRODUK ====> "+titleMap.get("produk"));
        try {
            byte[] theByteArray = response.getBytes(); //InputStream x =
            ByteArrayInputStream instream = new ByteArrayInputStream(theByteArray);

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            doc = docBuilder.parse(instream);
            doc.getDocumentElement().normalize();
        
            String detailResult = "";
            String detailError = "";
                 
            
            int flagErr = 0;
            MerchantCode mc = new MerchantCode();
            Node nodeee = doc.getElementsByTagName("response").item(0).getFirstChild();
            while (nodeee != null) {
//                System.out.println(nodeee.getNodeValue());
                String temp = "";
                
                System.out.print(titleMap.get(nodeee.getNodeName()) + " ==> \n");
                if (titleMap.get(nodeee.getNodeName()) != null) {
//                    System.out.println(nodeee.getNodeName()+" ===> ");
//                    System.out.println(titleMap.get(nodeee.getNodeName())+"\n");
                    map.put(nodeee.getNodeName(), nodeee.getTextContent());
                }
                
                nodeee = nodeee.getNextSibling();
            }
            
            
                
            if (map.get("rc").equalsIgnoreCase("0000")) {
                
                System.out.println("RC 0000");
                String printingFormat = "\n\n" + setTvPaymentPrinting(product_name, map, "0",createDate);
                
                
                System.out.println(printingFormat);
                printingFormatMulti = printingFormat;
                detailResult = printingFormatMulti;

                
                return detailResult;
               
            } else {
             
                return detailError;
            }
            
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
            //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        return "";
    }
    
    private String setTvPaymentPrinting(String product_name, Map<String, String> map, String reprintInfo, String create_date) {
        //String printingFormat = "";
        System.out.println(create_date);
        //int bill_lentgh = Integer.parseInt(map.get("bill_length"));
        //System.out.println("1");
//        logger.info("masuk printing format");
        
        int i = 0;

        //System.out.println("2");  
        int maxSpace = 30;
        String space = "";
        int space1 = 0;

        String title = "Terima kasih atas kepercayaan Anda menjadi mitra kami.";

        //title = info;
        String newInfo = "";
        space1 = maxSpace - (title.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newInfo = space + title;
        
        //System.out.println("3");
        System.out.println("new info : "+newInfo);
        space = "";
        title = "STRUK PEMBAYARAN TAGIHAN TV KABEL PASCA BAYAR";
        String newTitle = "";
        space1 = maxSpace - (title.length() / 2);
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newTitle = space + title;
        
        
        //System.out.println("4");
        Calendar currentDate = Calendar.getInstance();
//        SimpleDateFormat formatter
//                = new SimpleDateFormat("dd-MM-yyyy");
//        String tanggalNow = formatter.format(currentDate.getTime());
        ////System.out.println("Now the date is :=>  " + tanggalNow);

        //String tanggalNow = sdf.format(date.getDate());
        String jarak1 = "%-45s";
        String printingFormat = "";

        //test printing
        //System.out.println("5");
        int[] spacer1 = {16, 26, 11, 34, 16, 32};
        //int[] spacer1 = {16, 25, 16, 33, 16, 32};


        //System.out.println("5-1");
//        String loketname = viewApp.user;
        String loketname = "";
        //System.out.println("5-2-1");
        String newReprintInfo = "";
        String tanggalBayar = "";
        //System.out.println("5-2-2");
        if (reprintInfo.equalsIgnoreCase("0")) {
            //System.out.println("5-2-3");
            reprintInfo = "*CA:" + loketname;
            tanggalBayar = create_date.substring(8, 10) + "-"
                    + create_date.substring(5, 7) + "-"
                    + create_date.substring(0, 4);
            //System.out.println("5-2-4");
        } else {
            //System.out.println("5-2-5");
            reprintInfo += loketname;
            tanggalBayar = "00-00-00";
            //System.out.println("5-2-6");
        }

        //System.out.println("5-3");
        space1 = maxSpace - (reprintInfo.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        //reprintInfo += loketname;
        newReprintInfo = space + reprintInfo;

        //System.out.println("5-4");
        String BSM = "BANK SHINHAN";
        String newBSM = "";
        if (!BSM.equalsIgnoreCase("0")) {
            space1 = maxSpace - (BSM.length() / 2);
            space = "";
            for (int j = 0; j < space1; j++) {
                space += " ";
            }
            newBSM = space + BSM;
        }
        
        
        String formattedTanggal = "";
        
            String tanggal = create_date;
            
            try {
                String sdf11 = "yyyy-MM-dd HH:mm:ss.s";
                String sdf22 = "dd/MM/yyyy HH:mm:ss";

                SimpleDateFormat sdff = new SimpleDateFormat(sdf11);

                Date d = sdff.parse(tanggal);
                sdff.applyPattern(sdf11);
                formattedTanggal = sdff.format(d);
            } catch (Exception e) {
                e.printStackTrace();
            }
        
//        String tgl = getMonthName(map.get("due_date").substring(4, 6));
//        String thn = map.get("due_date").substring(0, 4);
                
        //try{
//            String jatuhtempo = map.get("due_date").substring(6, 8)+" "+tgl+" "+thn;
            String jatuhtempo = "";
            //System.out.println(jatuhtempo);
        //}catch(Exception e){
            //e.printStackTrace();
        //}    
        
        
            
        
            //System.out.println(formattedTanggal);
        
        //System.out.println("6");
        String[] printingan = new String[13];
        printingan[0] = loketname + "__  __" + newBSM;
        printingan[1] = "STRUK TAGIHAN TV KABEL PASCA BAYAR__  __" + newTitle;
        printingan[2] = "  __  __  __  __  __  ";
        printingan[3] = "PRODUK__:" + map.get("product_type") + "__PRODUK__   :" + map.get("product_type") + "__  __  ";
        printingan[4] = "TGL BAYAR__:" + formattedTanggal+ "__IDPEL__   :" + map.get("id_pelanggan").trim() + "__  __  ";
        printingan[5] = "IDPEL__:" + map.get("id_pelanggan").trim()+ "__NAMA__   :" + map.get("nama_pelanggan").trim() + "__  __  ";
        printingan[6] = "NO REF__:" + map.get("noreff_sw").trim() + "__NO REF__   :" + map.get("noreff_sw").trim()+ "__  __  ";
        printingan[7] = "NAMA__:" + map.get("nama_pelanggan").trim() + "__TANGGAL BAYAR__ :" + formattedTanggal+ "__  __  ";
        printingan[8] = "RP TAG__:" + "Rp. " + formatter.format(Double.parseDouble(map.get("rptag"))) + "__RP TAG__   :" + "Rp. " + formatter.format(Double.parseDouble(map.get("rptag")))+ "__  __  ";
        printingan[9] = "RP ADMIN__:" + "Rp. " + formatter.format(Double.parseDouble(map.get("admin_charge"))) + "__RP ADMIN__   :" + "Rp. " + formatter.format(Double.parseDouble(map.get("admin_charge"))) + "__  __  ";
        printingan[10] = "TOTAL TAGIHAN__:" + "Rp. " + formatter.format(Double.parseDouble(map.get("amount"))) + "__TOTAL TAGIHAN__ :" + "Rp. " + formatter.format(Double.parseDouble(map.get("amount"))) + "__  __  ";
//        printingan[10] = "";
        printingan[11] = reprintInfo + "__  __" + newInfo.replace("null", "");
        printingan[12] = "__ __ __" + reprintInfo;
  

        //System.out.println("7");
        String detailan = "";
        //String[] spliter = new String[20];

        int breaker = 0;
        for (int j = 0; j < printingan.length; j++) {
            System.out.println(detailan);
            //if(j!=2){
            breaker = 0;
            int over = 0;
            String[] spliter = printingan[j].split("__");
            int splitCounter = printingan[j].split("__").length;
            //
            ////System.out.println("j : " + j + " printingan : " + printingan[j] + " ==> length : " + printingan.length);
            for (int k = 0; k < 4; k++) {
                int sisa = 0;
                try {
                    System.out.println("tulis pada bagian sisa " + "j : " + j + ", k : " + k + ", printingan : " + printingan[j] + " ==> length : " + printingan.length);
                    System.out.println("spacer1 : "+spacer1[k]);
//                    System.out.println("spliter : "+k);
                    sisa = spacer1[k] - spliter[k].length();
                } catch (Exception e) {
                    System.out.println("error pada bagian sisa " + "j : " + j + ", k : " + k + ", printingan : " + printingan[j] + " ==> length : " + printingan.length);
                    System.out.println("spacer1 : "+spacer1[k]);
                    System.out.println("spliter : "+k);

                    e.printStackTrace();
                }
                //
                //System.out.print("k : " + k + " ==> lemgth : " + 6 + " sisa = " + sisa + " over : " + over + " spacer : " + spacer1[k] + " splitter : " + splitCounter + " splitterVal : " + spliter[k]);
                String whiteSpace = "";

                if (over > 0) {
                    sisa -= over;
                    over = 0;
                }

                if (sisa < 0) {
                    //sisa = 0;
                    over = (sisa * (-1)) - 1;
                    //over -= 1;
                    //if((over+1) == spacer1[k+1]){
                    //    over -= 1; 
                    //}
                }
                //
                ////System.out.println(" sisa2 = " + sisa + " over2 : " + over);

                if (splitCounter == 3 && k == 2) {
                    breaker = 1;
                }
                //else {

                for (int l = 0; l < sisa; l++) {
                    ////System.out.println("l : " + l + " ==> lemgth : " + sisa);
                    whiteSpace += " ";
                }
                //}

                detailan += spliter[k] + whiteSpace;
                //System.out.println("detailan " + j + " : " + spliter[k]);
                if (breaker == 1) {
                    k = 6;
                }
            }
            detailan += "\n";
            //spliter = new String[20];
            //System.out.println("detailan " + j + " : " + detailan);

            //}
        }
        
        //System.out.println("8");
        

        ///////////////////// test print
        printingFormat = detailan;

        //System.out.println("neh hasilnya detailan : ");
        
        System.out.println("===> "+printingFormat);
        
        //System.exit(1);
        //test printing done
        //System.out.println("9");
        return printingFormat;
    }
}
