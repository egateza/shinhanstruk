/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package huaweistruk;

/**
 *
 * @author Administrator
 */
public class MerchantCode {
    public String getMerchantNameByCode(String code) {
        if (code.equalsIgnoreCase("6010")) {
            return "Teller";
        }else if(code.equalsIgnoreCase("6011")) {
            return "ATM";
        }else if(code.equalsIgnoreCase("6012")) {
            return "POS";
        }else if(code.equalsIgnoreCase("6013")) {
            return "AutoDebit/giralisasi";
        }else if(code.equalsIgnoreCase("6014")) {
            return "Internet";
        }else if(code.equalsIgnoreCase("6015")) {
            return "Kiosk";
        }else if(code.equalsIgnoreCase("6016")) {
            return "Phone Banking/Call Center";
        }else if(code.equalsIgnoreCase("6017")) {
            return "Mobile Banking";
        }else if(code.equalsIgnoreCase("6018")) {
            return "EDC";
        }

        return "";
    }
}
