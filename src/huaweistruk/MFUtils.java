/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package huaweistruk;

import static huaweistruk.HuaweiStruk.startParam;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 *
 * @author egateza
 */
public class MFUtils {
    NumberFormat formatter = new DecimalFormat("##,##0.00");

    public String getMultiResponse(String response, String reprintInfo, String createDate) {
        String printingFormatMulti = "";
        Map<String, String> map = new HashMap<String, String>();

        //System.out.println("original file "+response);
        response = response.replace("&", "dan");
       //System.out.println("after replace "+response.getBytes());
        Document doc;
               
        Map<String, String> titleMap = new HashMap<String, String>();
        titleMap.put("product_type", "PRODUK");
        titleMap.put("idpel", "IDPEL");
        titleMap.put("name", "NAMA");        
        titleMap.put("angsuran", "ANGSURAN");        
        titleMap.put("admin_charge", "ADMIN");        
        titleMap.put("amount", "AMOUNT");              
        titleMap.put("rc", "RC");        
        titleMap.put("due_date", "due_date");        
        titleMap.put("noreference", "noreference");        
        titleMap.put("period", "periode");        


        try {
            byte[] theByteArray = response.getBytes(); //InputStream x =
            ByteArrayInputStream instream = new ByteArrayInputStream(theByteArray);

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            doc = docBuilder.parse(instream);
            doc.getDocumentElement().normalize();
        
            String detailResult = "";
            String detailError = "";
                 
            
            int flagErr = 0;
            MerchantCode mc = new MerchantCode();
            Node nodeee = doc.getElementsByTagName("response").item(0).getFirstChild();
            while (nodeee != null) {
                String temp = "";
                
                System.out.print(nodeee.getNodeName() + " ==> ");
                if (titleMap.get(nodeee.getNodeName()) != null) {
                    
                    
                    map.put(nodeee.getNodeName(), nodeee.getTextContent());
                    System.out.println(map.get(nodeee.getNodeName()));
                }
                
                nodeee = nodeee.getNextSibling();
            }
            
            
                
            if (map.get("rc").equalsIgnoreCase("0000")) {
                
                System.out.println("=============== masuk =============");
                String printingFormat = "\n\n" + setMultiPaymentPrinting(map, "0",createDate);
                
                
                System.out.println(printingFormat);
                printingFormatMulti = printingFormat;
                detailResult = printingFormatMulti;

                
                return detailResult;
               
            } else {
                System.out.println("================ error ============= "+ map.get("rc"));
                return detailError;
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            //Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        return "";
    }
    
    
    private String setMultiPaymentPrinting(Map<String, String> map, String reprintInfo, String create_date) {
        System.out.println("=============== GET THE PRINTING FORMAT ===============");
        int i = 0;

        //System.out.println("2");  
        int maxSpace = 40;
        String space = "";
        int space1 = 0;

        String title = "Terima kasih atas kepercayaan Anda menjadi mitra kami.";

        //title = info;
        String newInfo = "";
        space1 = maxSpace - (title.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newInfo = space + title;
        
        //System.out.println("3");
        ////System.out.println("new info : "+newInfo);
        space = "";
        title = "STRUK PEMBAYARAN TAGIHAN MULTI FINANCE";
        String newTitle = "";
        space1 = maxSpace - (title.length() / 2);
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        newTitle = space + title;
        
        
        //System.out.println("4");
        Calendar currentDate = Calendar.getInstance();

        String jarak1 = "%-45s";
        String printingFormat = "";

        //test printing
        //System.out.println("5");
        int[] spacer1 = {16, 26, 11, 34, 16, 32};
        //int[] spacer1 = {16, 25, 16, 33, 16, 32};


        //System.out.println("5-1");
//        String loketname = viewApp.user;
        String loketname = "";
        //System.out.println("5-2-1");
        String newReprintInfo = "";
        String tanggalBayar = "";
        //System.out.println("5-2-2");
        if (reprintInfo.equalsIgnoreCase("0")) {
//            System.out.println("5-2-3");
            reprintInfo = "*CA:" + loketname;
            tanggalBayar = create_date.substring(8, 10) + "-"
                    + create_date.substring(5, 7) + "-"
                    + create_date.substring(0, 4);
            //System.out.println("5-2-4");
        } else {
            //System.out.println("5-2-5");
            reprintInfo += loketname;
            tanggalBayar = "00-00-00";
            //System.out.println("5-2-6");
        }

        //System.out.println("5-3");
        space1 = maxSpace - (reprintInfo.length() / 2);
        space = "";
        for (int j = 0; j < space1; j++) {
            space += " ";
        }
        //reprintInfo += loketname;
        newReprintInfo = space + reprintInfo;

        //System.out.println("5-4");
        String BSM = "BANK SHINHAN";
        String newBSM = "";
        if (!BSM.equalsIgnoreCase("0")) {
            space1 = maxSpace - (BSM.length() / 2);
            space = "";
            for (int j = 0; j < space1; j++) {
                space += " ";
            }
            newBSM = space + BSM;
        }
        
        
        String formattedTanggal = "";
        
            String tanggal = create_date;
            try {
                String sdf11 = "yyyy-MM-dd HH:mm:ss.s";
                String sdf22 = "dd/MM/yyyy HH:mm:ss";

                SimpleDateFormat sdff = new SimpleDateFormat(sdf11);

                Date d = sdff.parse(tanggal);
                sdff.applyPattern(sdf22);
                formattedTanggal = sdff.format(d);
            } catch (Exception e) {
                e.printStackTrace();
            }
        
        String tgl = getMonthName(map.get("due_date").substring(4, 6));
        String thn = map.get("due_date").substring(0, 4);
                
        String jatuhtempo = map.get("due_date").substring(6, 8)+" "+tgl+" "+thn;
        String[] printingan = new String[13];
        printingan[0] = loketname + "__  __" + newBSM;
        printingan[1] = "STRUK TAGIHAN MULTI FINANNCE__  __" + newTitle;
        printingan[2] = "  __  __  __  __  __  ";
        printingan[3] = "PRODUK__:" + map.get("product_type") + "__PRODUK__   :" + map.get("product_type") + "__  __  ";
        printingan[4] = "TGL BAYAR__:" + formattedTanggal+ "__NO PELANGGAN__  :" + map.get("idpel").trim() + "__  __  ";
        printingan[5] = "NO PEL__:" + map.get("idpel").trim()+ "__NAMA__   :" + map.get("name") + "__  __  ";
        printingan[6] = "NAMA__:" + map.get("name").trim() + "__TANGGAL BAYAR__ :" + formattedTanggal+ "__  __  ";
        printingan[7] = "ANGSURAN KE__:" + Integer.parseInt(map.get("period").trim()) + "__JATUH TEMPO__   :" + jatuhtempo+ "__  __  ";
        printingan[8] = "RP ADMIN__:" + "Rp. " + formatter.format(Double.parseDouble(map.get("admin_charge"))) + "__ANGSURAN KE__   :" + Integer.parseInt(map.get("period").trim())+ "__  __  ";
        printingan[9] = "TOTAL TAGIHAN__:" + "Rp. " + formatter.format(Double.parseDouble(map.get("amount"))) + "__RP ADMIN__   :" + "Rp. " + formatter.format(Double.parseDouble(map.get("admin_charge"))) + "__  __  ";
        printingan[10] = "__ __TOTAL TAGIHAN__ :" + "Rp. " + formatter.format(Double.parseDouble(map.get("amount"))) + "__  __  ";
        printingan[11] = reprintInfo + "__  __" + newInfo.replace("null", "");
        printingan[12] = "__ __ __" + "               "+reprintInfo.trim();
  

        //System.out.println("7");
        String detailan = "";
        //String[] spliter = new String[20];

        int breaker = 0;
        for (int j = 0; j < printingan.length; j++) {
            //if(j!=2){
            breaker = 0;
            int over = 0;
            String[] spliter = printingan[j].split("__");
            int splitCounter = printingan[j].split("__").length;
            //
            ////System.out.println("j : " + j + " printingan : " + printingan[j] + " ==> length : " + printingan.length);
            for (int k = 0; k < 4; k++) {
                int sisa = 0;
                try {
//                    System.out.println("tulis pada bagian sisa " + "j : " + j + ", k : " + k + ", printingan : " + printingan[j] + " ==> length : " + printingan.length);
                    sisa = spacer1[k] - spliter[k].length();
                } catch (Exception e) {
                    System.out.println("error pada bagian sisa " + "j : " + j + ", k : " + k + ", printingan : " + printingan[j] + " ==> length : " + printingan.length);
                    e.printStackTrace();
                }
                //
                //System.out.print("k : " + k + " ==> lemgth : " + 6 + " sisa = " + sisa + " over : " + over + " spacer : " + spacer1[k] + " splitter : " + splitCounter + " splitterVal : " + spliter[k]);
                String whiteSpace = "";

                if (over > 0) {
                    sisa -= over;
                    over = 0;
                }

                if (sisa < 0) {
                    //sisa = 0;
                    over = (sisa * (-1)) - 1;
                    //over -= 1;
                    //if((over+1) == spacer1[k+1]){
                    //    over -= 1; 
                    //}
                }
                //
                ////System.out.println(" sisa2 = " + sisa + " over2 : " + over);

                if (splitCounter == 3 && k == 2) {
                    breaker = 1;
                }
                //else {

                for (int l = 0; l < sisa; l++) {
                    ////System.out.println("l : " + l + " ==> lemgth : " + sisa);
                    whiteSpace += " ";
                }
                //}

                detailan += spliter[k] + whiteSpace;
                //System.out.println("detailan " + j + " : " + spliter[k]);
                if (breaker == 1) {
                    k = 6;
                }
            }
            detailan += "\n";
            //spliter = new String[20];
            //System.out.println("detailan " + j + " : " + detailan);

            //}
        }
        
        //System.out.println("8");
        

        ///////////////////// test print
        printingFormat = detailan;

        System.out.println("neh hasilnya detailan : ");
        
        System.out.println(detailan);
        
        //System.exit(1);
        //test printing done
        //System.out.println("9");
        return printingFormat;
    }
    
    
    private String getMonthName(String substring) {
        String bulan = "";

        switch (substring) {
            case "01":
                bulan="JAN";
                break;
            case "02":
                bulan="FEB";
                break;
            case "03":
                bulan="MAR";
                break;
            case "04":
                bulan="APR";
                break;
            case "05":
                bulan="MEI";
                break;
            case "06":
                bulan="JUN";
                break;
            case "07":
                bulan="JUL";
                break;
            case "08":
                bulan="AUG";
                break;
            case "09":
                bulan="SEP";
                break;
            case "10":
                bulan="OKT";
                break;
            case "11":
                bulan="NOV";
                break;
            case "12":
                bulan="DES";
                break;
        }

        return bulan; //To change body of generated methods, choose Tools | Templates.
    }
    
}
