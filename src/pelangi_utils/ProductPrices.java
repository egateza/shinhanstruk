
package pelangi_utils;

import java.sql.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import java.util.regex.Pattern;
import ppob_sls_iqbal.*;
/**
 *
 * @author ngonar
 */
public class ProductPrices {

    Connection conx = null;
    Connection conOtomax = null;
    Settings setting = new Settings();

    private static final Logger logger = Logger.getLogger(ProductPrices.class);

    public ProductPrices() {
        //this.conx = setting.getConnection();
        //this.conOtomax = setting.getConnectionOtomax();
    }

    public double getProductFeePLN(int price_template_id) {

        try {
            double price=0;

            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();

            String sql = "select top 1 price from product_prices with(nolock) "
                    + " left outer join products on products.id = product_prices.product_id "
                    + " where "
                    + " products.code='TOKENPLN' and price_template_id='"+price_template_id+"' ";
                    
            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next())
                price = rs.getDouble("price");
            else
                price = 0;

            st.close();
            rs.close();
            conx.close();

            return price;
        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return 0;

    }

    public double getProductFee(int price_template_id,String code) {

        try {
            double price=0;

            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();

            String sql = "select top 1 price from product_prices with(nolock) "
                    + " left outer join products on products.id = product_prices.product_id "
                    + " where "
                    + " products.code='"+code+"' and price_template_id='"+price_template_id+"' ";

            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next())
                price = rs.getDouble("price");
            else
                price = 0;

            st.close();
            rs.close();
            conx.close();

            return price;
        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return 0;

    }

    public double getProductPrice(int price_template_id, int product_id) {

        try {
            double price=0;

            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            
            String sql = "select top 1 price from product_prices with(nolock) where "
                    + " product_id='"+product_id+"' and price_template_id='"+price_template_id+"' "
                    + " order by create_date desc";
            
            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next())
                price = rs.getDouble("price");
            else
                price = 0;

            st.close();
            rs.close();
            conx.close();

            return price;
        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return 0;
        
    }

}
