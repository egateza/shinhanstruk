
package pelangi_utils;

import java.sql.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import java.util.regex.Pattern;
import java.util.*;
import javax.persistence.*;
import model.*;
import org.apache.log4j.Priority;
import ppob_sls_iqbal.*;

/**
 *
 * @author ngonar
 */
public class Saldo {

    Connection conx = null;
    Connection conOtomax = null;
    Settings setting = new Settings();

    private static final Logger logger = Logger.getLogger(Saldo.class);

    public Saldo() {
//        this.conx = setting.getConnection();
//        this.conOtomax = setting.getConnectionOtomax();
    }

    public void tambahBalance(double amount, String user_id, String note, String trx, double saldo_terakhir, EntityManager em) {
        //update mutasi
        try {

            Mutations mutasi = new Mutations();
            mutasi.setAmount((long)amount);
            mutasi.setNote(note);
            mutasi.setJenis((char)'K');
            //mutasi.setBalance(saldo_terakhir);
            mutasi.setInboxId(Integer.parseInt(trx));
            mutasi.setUserId(Integer.parseInt(user_id));
            mutasi.setCreateDate(new java.util.Date());

            em.lock(mutasi, LockModeType.OPTIMISTIC);

            //em.getTransaction().begin();
            em.persist(mutasi);
            //em.getTransaction().commit();

            logger.log(Level.INFO, "add saldo");
        }
        catch(Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }
    }

    public void tambahBalance(double amount, String user_id, String note, String trx, double saldo_terakhir) {
        //update mutasi
        try {

            EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
            EntityManager em = factory.createEntityManager();

            Mutations mutasi = new Mutations();
            mutasi.setAmount((long)amount);
            mutasi.setNote(note);
            mutasi.setJenis((char)'K');
            //mutasi.setBalance(saldo_terakhir);
            mutasi.setInboxId(Integer.parseInt(trx));
            mutasi.setUserId(Integer.parseInt(user_id));
            mutasi.setCreateDate(new java.util.Date());

            em.lock(mutasi, LockModeType.OPTIMISTIC);

            em.getTransaction().begin();
            em.persist(mutasi);
            em.getTransaction().commit();

            em.refresh(mutasi);

            em.close();
            factory.close();
            logger.log(Level.INFO, "add saldo");
        }
        catch(Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }
    }

    public void kurangBalance(double amount, String user_id, String note, String trx, double saldo_terakhir, EntityManager em) {
        //update mutasi
        try {


            Mutations mutasi = new Mutations();
            mutasi.setAmount((long) (amount*-1));
            mutasi.setNote(note);
            mutasi.setJenis((char)'D');
            //mutasi.setBalance(Integer.parseInt(saldo_terakhir));
            mutasi.setInboxId(Integer.parseInt(trx));
            mutasi.setUserId(Integer.parseInt(user_id));
            mutasi.setCreateDate(new java.util.Date());

            //em.lock(mutasi, LockModeType.OPTIMISTIC);

            //em.getTransaction().begin();
            em.persist(mutasi);
            //em.getTransaction().commit();

            
            logger.log(Level.INFO, "sub saldo");
        }
        catch(Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }
    }

    public void kurangBalance(double amount, String user_id, String note, String trx, double saldo_terakhir) {
        //update mutasi
        try {

            EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());
            EntityManager em = factory.createEntityManager();

            Mutations mutasi = new Mutations();
            mutasi.setAmount((long) (amount*-1));
            mutasi.setNote(note);
            mutasi.setJenis((char)'D');
            //mutasi.setBalance(Integer.parseInt(saldo_terakhir));
            mutasi.setInboxId(Integer.parseInt(trx));
            mutasi.setUserId(Integer.parseInt(user_id));
            mutasi.setCreateDate(new java.util.Date());

            em.lock(mutasi, LockModeType.OPTIMISTIC);

            em.getTransaction().begin();
            em.persist(mutasi);
            em.getTransaction().commit();

            em.close();
            factory.close();

            logger.log(Level.INFO, "sub saldo");
        }
        catch(Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }
    }

    public double getBalance(String user_id, EntityManager em) {

        try {
            
            String sql = "select SUM(m.amount) from Mutations m where m.userId =:uid";
            Query query = em.createQuery(sql);
            query.setParameter("uid",Integer.parseInt(user_id));
            Long balance = (Long) query.getSingleResult();

            double bal = 0;
            //System.out.println("Balance of "+user_id+" : "+balance);
            logger.log(Level.INFO, "Balance of "+user_id+" : "+balance);
            bal = balance;

            return bal;
        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
            e.printStackTrace();
        }

        return 0;
    }

    public double getBalance(String user_id) {

        try {
            EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", System.getProperties());

            EntityManager em = factory.createEntityManager();

            String sql = "select SUM(m.amount) from Mutations m where m.userId =:uid";
            Query query = em.createQuery(sql);
            query.setParameter("uid",Integer.parseInt(user_id));
            Long balance = (Long) query.getSingleResult();

            double bal = 0;
            //System.out.println("Balance of "+user_id+" : "+balance);
            logger.log(Level.INFO, "Balance of "+user_id+" : "+balance);
            bal = balance;

            em.close();
            factory.close();

            return bal;
        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
            e.printStackTrace();
        }

        return 0;
    }

}
