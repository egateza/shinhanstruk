
package pelangi_utils;

import java.sql.*;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import ppob_sls_iqbal.*;

public class Products {

    Connection conx = null;
    Connection conOtomax = null;
    Settings setting = new Settings();

    private static final Logger logger = Logger.getLogger(Products.class);

    public Products() {
        
    }

    public String getProductDescription(String vcode) {
        String hasil="";

        try {
            //get regex format for the request
            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            String sql = " select description from products where code = '"+vcode+"'";
            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next())
                hasil = rs.getString("description");

            rs.close();
            st.close();
            conx.close();

            return hasil;
        }
        catch(Exception e ){
            logger.log(Level.FATAL,e.getMessage());
        }

        return hasil;
    }

    public int getProductIdByCode(String code) {
        try {

            int id = 0;

            //get regex format for the request
            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            String sql = "select top 1 id from products where code = '"+code+"'";
            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next())
                id = rs.getInt("id");

            rs.close();
            st.close();
            conx.close();

            return id;

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }


        return 0;
    }

    public int getCategory(String produk) {

        try {

            int cat=0;

            //get regex format for the request
            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            String sql = " select category_id from products "+
                         " left outer join product_categories on products.product_category_id = product_categories.id " +
                         " where products.code = '"+produk+"' ";
            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next())
                cat = rs.getInt("category_id");

            rs.close();
            st.close();
            conx.close();

            return cat;
        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return 0;
    }

    public boolean cekProduk(String produk) {

        try {

            boolean ada = false;

            //get regex format for the request
            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            String sql = "select top 1 name from products where code = '"+produk+"' and active=1";
            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next())
                ada = true;

            rs.close();
            st.close();
            conx.close();

            return ada;
        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return false;
    }

    public double getHargaModalVoucher(int id) {
        double modal = 0;

        try {
            
            //get regex format for the request
            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            String sql = "select top 1 price from vouchers where id = '"+id+"'";
            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next())
                modal = rs.getDouble("price");

            rs.close();
            st.close();
            conx.close();

            return modal;
            
        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage());
        }

        return 0;
    }

    public boolean cekProdukGangguan(String produk) {
        boolean result = false;

        try {

            //get regex format for the request
            this.conx = setting.getConnection();
            
            Statement st   = conx.createStatement();
            String sql = "select gangguan from products"
                    + " where code = '"+produk+"' ";
            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next())
                result= rs.getBoolean("gangguan");

            st.close();
            rs.close();
            conx.close();

            return result;

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return false;
    }

    public int cekStokVoucher(String produk) {

        try {
            
            int stok=0;

            //get regex format for the request
            this.conx = setting.getConnection();
            conx.setTransactionIsolation(conx.TRANSACTION_SERIALIZABLE);
            Statement st   = conx.createStatement();
            String sql = "select count(*) as jml from vouchers "
                    + " left outer join products on products.id = vouchers.product_id "
                    + " where code = '"+produk+"' and status = 0 and products.kosong = 0 ";
            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next())
                stok= rs.getInt("jml");

            st.close();
            rs.close();
            conx.close();

            return stok;

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return 0;
    }

    public void setProductUnavailable(int id) {
        try {
            this.conx = setting.getConnection();
            conx.setTransactionIsolation(conx.TRANSACTION_SERIALIZABLE);
            
            Statement st   = conx.createStatement();
            String sql = "update vouchers set status=1, sold_date=getdate() where id="+id;
            st.executeUpdate(sql);
            //logger.log(Level.INFO, sql);

            st.close();
            conx.close();
        }
        catch(Exception e) {
            logger.log(Level.FATAL, e.getMessage());
        }
    }

    //Game
    public String[] getVoucherGame(String code) {
        String[] product = new String[10];

        try {

            //get regex format for the request
            this.conx = setting.getConnection();
            conx.setTransactionIsolation(conx.TRANSACTION_SERIALIZABLE);
            Statement st   = conx.createStatement();
            String sql = "select top 1 "+
                        " vouchers.id, management_code, "+
                        " voucher_code, serial_number, "+
                        " voucher_password, pin, product_id, "+
                        " price,vcode,nominal "+
                        " from vouchers with(nolock) "+
                        " left outer join products on products.id = vouchers.product_id "+
                        " where status = 0 and products.code = '"+code+"' order by vouchers.create_date";
            
            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next()) {
                product[0] = rs.getString("id");
                product[1] = rs.getString("management_code");   System.out.println("mcode : "+product[1]);
                product[2] = rs.getString("voucher_code");      System.out.println("vou code : "+product[2]);
                product[3] = rs.getString("serial_number");     System.out.println("sn : "+product[3]);
                product[4] = rs.getString("voucher_password");  System.out.println("pass : "+product[4]);
                product[5] = rs.getString("pin");               System.out.println("pin : "+product[5]);
                product[6] = rs.getString("product_id");        System.out.println("prod id : "+product[6]);
                product[7] = rs.getString("price");             System.out.println("price : "+product[7]);
                product[8] = rs.getString("vcode");             System.out.println("vcode : "+product[8]);
                product[9] = rs.getString("nominal");           System.out.println("nom : "+product[9]);
            }


            sql = "update vouchers set status=1, sold_date=getdate() where id="+product[0];
            st.executeUpdate(sql);

            rs.close();
            st.close();
            conx.close();

            return product;

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return product;
    }

    public String[] getBoughtVoucherGame(String id) {
        String[] product = new String[10];

        try {

            //get regex format for the request
            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            String sql = "select top 1 "+
                        " vouchers.id, management_code, "+
                        " voucher_code, serial_number, "+
                        " voucher_password, pin, product_id, "+
                        " price,vcode,nominal "+
                        " from vouchers with(nolock) "+
                        " left outer join products on products.id = vouchers.product_id "+
                        " where vouchers.id="+id;

            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next()) {
                product[0] = rs.getString("id");
                product[1] = rs.getString("management_code");   System.out.println("mcode : "+product[1]);
                product[2] = rs.getString("voucher_code");      System.out.println("vou code : "+product[2]);
                product[3] = rs.getString("serial_number");     System.out.println("sn : "+product[3]);
                product[4] = rs.getString("voucher_password");  System.out.println("pass : "+product[4]);
                product[5] = rs.getString("pin");               System.out.println("pin : "+product[5]);
                product[6] = rs.getString("product_id");        System.out.println("prod id : "+product[6]);
                product[7] = rs.getString("price");             System.out.println("price : "+product[7]);
                product[8] = rs.getString("vcode");             System.out.println("vcode : "+product[8]);
                product[9] = rs.getString("nominal");           System.out.println("nom : "+product[9]);
            }

            rs.close();
            st.close();
            conx.close();

            return product;

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return product;
    }

    //PLN
    public String[] getVoucherPLN(String inbox_id) {
        String[] product = new String[40];

        try {

            //get regex format for the request
            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            String sql = "SELECT [id] "+
                         "     ,[inbox_id] "+
                         "     ,[transid] "+
                         "     ,[partnertid] "+
                         "     ,[restype] "+
                         "     ,[rescode] "+
                         "     ,[idpel] "+
                         "     ,[nama] "+
                         "     ,[sntoken] "+
                         "     ,[plnref] "+
                         "     ,[refnbr] "+
                         "     ,[kwh] "+
                         "     ,[tagihan] "+
                         "     ,[admin] "+
                         "     ,[meterai] "+
                         "     ,[ppn] "+
                         "     ,[ppj] "+
                         "     ,[angsuran] "+
                         "     ,[rptoken] "+
                         "     ,[jmkwh] "+
                         "     ,[info] "+
                         "     ,[saldo] "+
                         "     ,[snidpel] "+
                         "     ,[snmeter] "+
                         "     ,[settlement] "+
                         "     ,[xml] "+
                         "     ,[create_date] "+
                         "     ,[create_by] "+
                         "     ,[destination] "+
                         "     ,[user_] "+
                         "     ,[pass_] "+
                         " FROM [pelangi].[dbo].[pln_prepaids] where inbox_id ='"+inbox_id+"'";

            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next()) {
                product[0] = rs.getString("id");
                product[1] = rs.getString("inbox_id");      System.out.println("inbox_id : "+product[1]);
                product[2] = rs.getString("transid");       System.out.println("transid : "+product[2]);
                product[3] = rs.getString("partnertid");    System.out.println("partnertid : "+product[3]);
                product[4] = rs.getString("restype");       System.out.println("restype : "+product[4]);
                product[5] = rs.getString("rescode");       System.out.println("rescode : "+product[5]);
                product[6] = rs.getString("idpel");         System.out.println("idpel : "+product[6]);
                product[7] = rs.getString("nama");          System.out.println("nama : "+product[7]);
                product[8] = rs.getString("sntoken");       System.out.println("sntoken : "+product[8]);
                product[9] = rs.getString("plnref");        System.out.println("plnref : "+product[9]);
                product[10] = rs.getString("refnbr");
                product[11] = rs.getString("kwh");          System.out.println("kwh : "+product[11]);
                product[12] = rs.getString("tagihan");      System.out.println("tagihan : "+product[12]);
                product[13] = rs.getString("admin");        System.out.println("admin : "+product[13]);
                product[14] = rs.getString("meterai");      System.out.println("meterai : "+product[14]);
                product[15] = rs.getString("ppn");          System.out.println("ppn : "+product[15]);
                product[16] = rs.getString("ppj");          System.out.println("ppj : "+product[16]);
                product[17] = rs.getString("angsuran");     System.out.println("angsuran : "+product[17]);
                product[18] = rs.getString("rptoken");      System.out.println("rptoken : "+product[18]);
                product[19] = rs.getString("jmkwh");        System.out.println("jmkwh : "+product[19]);
                product[20] = rs.getString("info");
                product[21] = rs.getString("saldo");        System.out.println("saldo : "+product[21]);
                product[22] = rs.getString("snidpel");      System.out.println("snidpel : "+product[22]);
                product[23] = rs.getString("snmeter");      System.out.println("snmeter : "+product[23]);
                product[24] = rs.getString("settlement");   System.out.println("settlement : "+product[24]);
                product[25] = rs.getString("xml");          System.out.println("xml : "+product[25]);
                product[26] = rs.getString("create_date");  System.out.println("create_date : "+product[26]);
                product[27] = rs.getString("create_by");    System.out.println("create_by : "+product[27]);
                
                
            }

            rs.close();
            st.close();
            conx.close();

            return product;

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return product;
    }

    public String[] getDetailPLNPostpaid(String inbox_id) {
        String[] product = new String[40];

        try {

            //get regex format for the request
            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            String sql = "SELECT [id] "+
                         "     ,[transid] "+
                         "     ,[partnertid] "+
                         "     ,[restype] "+
                         "     ,[rescode] "+
                         "     ,[idpel] "+
                         "     ,[nama] "+
                         "     ,[kwh] "+
                         "     ,[jumlahrek] "+
                         "     ,[outstanding] "+
                         "     ,[tagihan] "+
                         "     ,[denda] "+
                         "     ,[admin] "+
                         "     ,[insentive] "+
                         "     ,[bulan] "+
                         "     ,[mlalu] "+
                         "     ,[refnbr] "+
                         "     ,[info] "+
                         "     ,[saldo] "+
                         " FROM [pelangi].[dbo].[pln_postpaid] where inbox_id = '"+inbox_id+"'";

            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next()) {
                product[0] = rs.getString("id");
                product[1] = rs.getString("inbox_id");      System.out.println("inbox_id : "+product[1]);
                product[2] = rs.getString("transid");       System.out.println("transid : "+product[2]);
                product[3] = rs.getString("partnertid");    System.out.println("partnertid : "+product[3]);
                product[4] = rs.getString("restype");       System.out.println("restype : "+product[4]);
                product[5] = rs.getString("rescode");       System.out.println("rescode : "+product[5]);
                product[6] = rs.getString("idpel");         System.out.println("idpel : "+product[6]);
                product[7] = rs.getString("nama");          System.out.println("nama : "+product[7]);
                product[8] = rs.getString("kwh");           System.out.println("kwh : "+product[8]);
                product[9] = rs.getString("jumlahrek");        System.out.println("jumlahrek : "+product[9]);
                product[10] = rs.getString("outstanding");
                product[11] = rs.getString("tagihan");          System.out.println("tagihan : "+product[11]);
                product[12] = rs.getString("denda");      System.out.println("denda : "+product[12]);
                product[13] = rs.getString("admin");        System.out.println("admin : "+product[13]);
                product[14] = rs.getString("insentive");      System.out.println("insentive : "+product[14]);
                product[15] = rs.getString("bulan");          System.out.println("bulan : "+product[15]);
                product[16] = rs.getString("mlalu");          System.out.println("mlalu : "+product[16]);
                product[17] = rs.getString("refnbr");     System.out.println("refnbr : "+product[17]);
                product[18] = rs.getString("info");      System.out.println("info : "+product[18]);
                product[19] = rs.getString("saldo");        System.out.println("saldo : "+product[19]);
                product[20] = rs.getString("xml");          System.out.println("xml : "+product[20]);
                product[21] = rs.getString("create_date");  System.out.println("create_date : "+product[21]);
                product[22] = rs.getString("create_by");    System.out.println("create_by : "+product[22]);


            }

            rs.close();
            st.close();
            conx.close();

            return product;

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return product;
    }

    public String[] getDetailTelkom(String inbox_id) {
        String[] product = new String[40];

        try {

            //get regex format for the request
            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            String sql = "SELECT [id] "+
                         "     ,[transid]"+
                         "     ,[partnertid]"+
                         "     ,[restype]"+
                         "     ,[rescode]"+
                         "     ,[idpel]"+
                         "     ,[nama]"+
                         "     ,[datel]"+
                         "     ,[tagihan]"+
                         "     ,[bulan]"+
                         "     ,[jumlahrek]"+
                         "     ,[admin]"+
                         "     ,[npwp]"+
                         "     ,[telref]"+
                         "     ,[refnbr]"+
                         "     ,[saldo]"+
                         "     ,[xml]"+
                         "     ,[create_date]"+
                         "     ,[destination]"+
                         "     ,[user_]"+
                         "     ,[pass_]"+
                         " FROM [pelangi].[dbo].[telkom] where inbox_id = '"+inbox_id+"'";

            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next()) {
                product[0] = rs.getString("id");
                product[1] = rs.getString("inbox_id");      System.out.println("inbox_id : "+product[1]);
                product[2] = rs.getString("transid");       System.out.println("transid : "+product[2]);
                product[3] = rs.getString("partnertid");    System.out.println("partnertid : "+product[3]);
                product[4] = rs.getString("restype");       System.out.println("restype : "+product[4]);
                product[5] = rs.getString("rescode");       System.out.println("rescode : "+product[5]);
                product[6] = rs.getString("idpel");         System.out.println("idpel : "+product[6]);
                product[7] = rs.getString("nama");          System.out.println("nama : "+product[7]);
                product[8] = rs.getString("datel");         System.out.println("datel : "+product[8]);
                product[9] = rs.getString("bulan");         System.out.println("bulan : "+product[9]);
                product[10] = rs.getString("jumlahrek");
                product[11] = rs.getString("admin");        System.out.println("admin : "+product[11]);
                product[12] = rs.getString("npwp");         System.out.println("npwp : "+product[12]);
                product[13] = rs.getString("telref");       System.out.println("telref : "+product[13]);
                product[14] = rs.getString("refnbr");       System.out.println("refnbr : "+product[14]);
                product[15] = rs.getString("saldo");        System.out.println("saldo : "+product[15]);
                product[16] = rs.getString("xml");          System.out.println("xml : "+product[16]);
                product[17] = rs.getString("create_date");  System.out.println("create_date : "+product[17]);
                product[18] = rs.getString("destination");  System.out.println("destination : "+product[18]);
                product[19] = rs.getString("user_");        System.out.println("user_ : "+product[19]);
                product[20] = rs.getString("pass_");
                
            }

            rs.close();
            st.close();
            conx.close();

            return product;

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return product;
    }

    //Pulsa
    public String[] getVoucherPulsa(String code) {
        String[] product = new String[4];

        return product;
    }

    public String[] getProductDetail(int category, String code) {

         String[] product = new String[4];

         return product;
    }

}

