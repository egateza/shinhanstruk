
package pelangi_utils;

import java.sql.*;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import ppob_sls_iqbal.*;

/**
 *
 * @author ngonar
 */
public class Inboxes {

    Connection conx = null;
    Connection conOtomax = null;
    Settings setting = new Settings();

    private static final Logger logger = Logger.getLogger("Inboxes");

    public Inboxes() {
        //this.conx = setting.getConnection();
        //this.conOtomax = setting.getConnectionOtomax();
        setting.setConnections();
    }

    public String[] getDetail(int id) {
        String[] result = new String[10];

        try {

            this.conx = setting.getConnection();
            
            Statement st   = conx.createStatement();
            String sql = "select top 1 * from inboxes with(nolock) "
                    + " left outer join media_types "
                    + " on media_types.name = inboxes.sender_type where inboxes.id = '"+id+"'";
                    
            ResultSet rs = st.executeQuery(sql);
            logger.log(Level.INFO,sql);


            if (rs.next()) {
                result[0] = rs.getString("message");

                String pengirim[] = rs.getString("sender").split("@");                
                result[1] = pengirim[0];                   System.out.println("pengirim : "+result[1]);
                
                result[2] = rs.getString("media_type_id"); System.out.println("sender type : "+result[2]);
                result[3] = rs.getString("id");            System.out.println("id : "+result[3]);
                result[4] = rs.getString("receiver");      System.out.println("receiver : "+result[4]);
                result[5] = rs.getString("user_id");       System.out.println("user id : "+result[5]);
                result[6] = rs.getString("sender_type");   System.out.println("sender type : "+result[6]);
                
            }
                
            st.close();
            rs.close();
            conx.close();

            return result;

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return result;
    }

}
