
package PLN.SLS;


public class MerchantCategory {

    /*
     * 6010 : teller
     * 6011 : ATM
     * 6012 : POS
     * 6013 : AutoDebit / giralisasi
     * 6014 : Internet
     * 6015 : Kiosk
     * 6016 : Phone Banking
     * 6017 : Mobile banking
     * 6018 : EDC
     */

    public MerchantCategory() {
        
    }

    public int getMerchantCode(String name) {

        if (name.equalsIgnoreCase("TELLER"))
            return 6010;
        else if (name.equalsIgnoreCase("ATM"))
            return 6011;
        else if (name.equalsIgnoreCase("POS"))
            return 6012;
        else if (name.equalsIgnoreCase("AUTO DEBIT / GIRALISASI"))
            return 6013;
        else if (name.equalsIgnoreCase("INTERNET"))
            return 6014;
        else if (name.equalsIgnoreCase("KIOSK"))
            return 6015;
        else if (name.equalsIgnoreCase("PHONE BANKING"))
            return 6016;
        else if (name.equalsIgnoreCase("MOBILE BANKING"))
            return 6017;
        else if (name.equalsIgnoreCase("EDC"))
            return 6018;

        return 0;
    }

    public String getMerchantCategory(int code) {
        if (code==6010)
            return "TELLER";
        else if (code==6011)
            return "ATM";
        else if (code==6012)
            return "POS";
        else if (code==6013)
            return "AUTO DEBIT / GIRALISASI";
        else if (code==6014)
            return "INTERNET";
        else if (code==6015)
            return "KIOSK";
        else if (code==6016)
            return "PHONE BANKING";
        else if (code==6017)
            return "MOBILE BANKING";
        else if (code==6018)
            return "EDC";

        return "POS";
    }

    

}
